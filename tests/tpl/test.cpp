/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QtCore>
#include <QDebug>

#include <QRegExp>
#include <QString>
#include <QStringList>
#include <QTextCodec>

#include "k8tpl.h"

#include "test.h"


int main (int argc, char *argv[]) {
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("koi8-u"));
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("koi8-u"));

  K8Template tpl;
  tpl.loadFromFile("test0.tpl");

  tpl.setVar("nick", "six");
  tpl.setVar("date", "aug");
  tpl.setVar("comment", "cmt");
  tpl.setVar("message", "msg!");
  qDebug() << tpl.result();

  tpl.clearVars();
  tpl.setVar("nick", "six1");
  tpl.setVar("date", "aug1");
  //tpl.setVar("comment", "cmt1");
  tpl.setVar("message", "msg!1");
  qDebug() << tpl.result();

  return 0;
}
