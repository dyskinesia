/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QtCore>
#include <QDebug>

#include <QString>
#include <QStringList>
#include <QTextCodec>

#include "k8strutils.h"


int main (int argc, char *argv[]) {
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("koi8-u"));
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("koi8-u"));

  QString cl;
  for (int f = 0; f < argc; f++) {
    QString a(argv[f]);
    a.replace('"', "\"\"");
    if (a.indexOf(' ') >= 0 || a.indexOf('\t') >= 0 || a.indexOf('"') >= 0) a = "\""+a+"\"";
    cl += a+" ";
  }

  qDebug() << cl;

  QStringList sl(parseCmdLine(cl));
  for (int f = 0; f < sl.length(); f++) {
    qDebug() << f << sl[f];
  }

  return 0;
}
