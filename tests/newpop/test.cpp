/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QtCore>
#include <QDebug>

#include <QApplication>
#include <QFile>
#include <QString>
#include <QTextCodec>
#include <QTextStream>

#include "k8popups.h"


QString loadFile (const QString &fileName) {
  QFile file(fileName);
  QString res;
  if (file.open(QIODevice::ReadOnly)) {
    QTextStream stream;
    stream.setDevice(&file);
    stream.setCodec("UTF-8");
    res = stream.readAll();
    file.close();
  }
  return res;
}


int main (int argc, char *argv[]) {
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("koi8-u"));
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("koi8-u"));

  QApplication app(argc, argv);

  K8PopupManager *pm = new K8PopupManager(300, 150, 6, argc>1 ? K8PopupManager::RightTop : K8PopupManager::RightBottom);
  pm->setFloatHeight(true);
  pm->addType("test", loadFile("pop.qss"));

  for (int f = 1; f > 0; f--) {
    pm->showMessage("test", "id0", loadFile("pop0.html"));
    pm->showMessage("test", "id1", loadFile("pop1.html"));
  }

  return app.exec();
}
