TEMPLATE = app
TARGET = zkstest

QT -= xml
QT += gui
CONFIG += qt warn_on
#CONFIG += debug_and_release
CONFIG += debug
#CONFIG += release

#QMAKE_CFLAGS_RELEASE ~= s/\-O./-Os
#QMAKE_CXXFLAGS_RELEASE ~= s/\-O./-Os

##QMAKE_CFLAGS_RELEASE ~= s/\-O./-O2
##QMAKE_CXXFLAGS_RELEASE ~= s/\-O./-O2

#QMAKE_CFLAGS_RELEASE += -march=native
#QMAKE_CXXFLAGS_RELEASE += -march=native
#QMAKE_CFLAGS_RELEASE += -mtune=native
#QMAKE_CXXFLAGS_RELEASE += -mtune=native

#QMAKE_LFLAGS_RELEASE += -s


DESTDIR = .
OBJECTS_DIR = _build/obj
UI_DIR = _build/uic
MOC_DIR = _build/moc
RCC_DIR = _build/rcc


include(../../src/k8utils/k8utils.pri)
include(../../src/k8history/k8history.pri)

HEADERS += \
  $$PWD/zkhtest.h
SOURCES += \
  $$PWD/zkhtest.cpp
