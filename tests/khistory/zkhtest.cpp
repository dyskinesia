/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QtCore>
#include <QDebug>

#include <QTime>

#include "k8history.h"

#include "zkhtest.h"


static void dumpMsg (const HistoryMessage &msg) {
  QString f;
  switch (msg.type) {
    case HistoryMessage::Incoming: f = "*>"; break;
    case HistoryMessage::Outgoing: f = "*<"; break;
    case HistoryMessage::Server: f = "*="; break;
    default: f = "**"; break;
  }
  qDebug() << "----------------------------------";
  qDebug() << f << msg.date.toString() << msg.uni;
  qDebug() << " " << msg.action;
  qDebug() << " " << msg.text;
}


int main (int argc, char *argv[]) {
  Q_UNUSED(argc)
  Q_UNUSED(argv)

  HistoryFile hf("0hist.txt", HistoryFile::Read);
  if (!hf.open()) {
    qDebug() << "can't open history file!";
    return 1;
  }

  HistoryMessage msg;

  qDebug() << "messages:" << hf.count();
  qDebug() << "back42:" << hf.backseek(42);
  if (hf.read(msg)) dumpMsg(msg);

  qDebug() << "back4:" << hf.backseek(4);

  while (hf.read(msg)) {
    dumpMsg(msg);
  }

  qDebug() << hf.seek(0);

  HistoryFile hf1("1hist.txt", HistoryFile::Write);
  hf1.remove();
  if (!hf1.open()) {
    qDebug() << "can't create new history file!";
    return 1;
  }

  qDebug() << "copying history file...";
  while (hf.read(msg)) {
    //dumpMsg(msg);
    if (!hf1.append(msg)) {
      qDebug() << "shit!";
    }
  }
  hf1.close();

  HistoryFile hf2("1hist.txt", HistoryFile::Write);
  if (!hf2.open()) {
    qDebug() << "can't open new history file!";
    return 1;
  }
  qDebug() << "messages:" << hf2.count();
  msg.action = "pwned";
  msg.text = "test\nmulti\\lining";
  qDebug() << hf2.append(msg);
  hf2.close();

  if (!hf2.open(HistoryFile::Read)) {
    qDebug() << "can't reopen new history file!";
    return 1;
  }
  qDebug() << "messages:" << hf2.count();
  qDebug() << "back1:" << hf2.backseek(1);
  if (hf2.read(msg)) dumpMsg(msg);

  return 0;
}
