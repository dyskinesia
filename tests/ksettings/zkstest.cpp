/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QtGui>
#include <QDebug>

#include "k8sdb.h"

#include "zkstest.h"



int main (int argc, char *argv[]) {
  Q_UNUSED(argc)
  Q_UNUSED(argv)

  K8SDB db("./profile.json", K8SDB::OpenAlways);

  QStringList kl(db.keys());
  foreach (const QString &key, kl) {
    qDebug() << key << "type:" << db.type(key) << K8SDB::typeName(db.type(key)) << "value:" << db.asString(key);
    bool ok, bv;
    int iv;
    QString sv;
    QKeySequence kq;
    QByteArray ba;
    switch (db.type(key)) {
      case K8SDB::Boolean:
        bv = db.toBool(key, &ok);
        qDebug() << "  " << ok << "v:" << bv;
        break;
      case K8SDB::Integer:
        iv = db.toInt(key, &ok);
        qDebug() << "  " << ok << "v:" << iv;
        break;
      case K8SDB::String:
        sv = db.toString(key, &ok);
        qDebug() << "  " << ok << "v:" << sv;
        break;
      case K8SDB::KeySequence:
        kq = db.toKeySequence(key, &ok);
        qDebug() << "  " << ok << "v:" << kq;
        break;
      case K8SDB::ByteArray:
        ba = db.toByteArray(key, &ok);
        qDebug() << "  " << ok << "v:" << ba.toHex();
        break;
      default: ;
    }
  }

  db.set("str_keyseq_altx", "ctrl+z");
  db.set("str_key_test", "test");
  db.set("bool_key_true", true);
  db.set("bool_key_false", false);
  db.set("int_key_42", 42);
  db.set("str_key_test", "zzzzzrioweo");
  //db.set("str_keyseq_altx", "Alt+x");
  db.set("str_keyseq_altx", QKeySequence("Alt+x"));

  return 0;
}
