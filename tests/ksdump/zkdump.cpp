/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QtCore>
#include <QDebug>

#include <QByteArray>
#include <QString>
#include <QTextCodec>

#include "k8sdb.h"
#include "k8json.h"
#include "k8ascii85.h"


bool generatorCB (void *udata, QString &err, QByteArray &res, const QVariant &val, int indent) {
  Q_UNUSED(udata)
  Q_UNUSED(indent)
  switch (val.type()) {
    case QVariant::ByteArray: {
      QByteArray ba(val.toByteArray());
      QByteArray a85;
      K8ASCII85::encode(a85, ba);
      res += K8JSON::quote(QString::fromAscii(a85));
      } break;
    default:
      err = QString("invalid variant type: %1").arg(val.typeName());
      return false;
  }
  return true;
}


static const char *typeName (K8SDB::Type type) {
  switch (type) {
    case K8SDB::Invalid: return "invalid";
    case K8SDB::Boolean: return "boolean";
    case K8SDB::Integer: return "integer";
    case K8SDB::String: return "string";
    case K8SDB::KeySequence: return "keyseq";
    case K8SDB::ByteArray: return "bytearray";
    default: ;
  }
  return "unknown";
}


int main (int argc, char *argv[]) {
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("koi8-r"));
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("koi8-r"));

  if (argc < 2) {
    fprintf(stderr, "ERROR: no file!\n");
    return 1;
  }

  K8SDB db(argv[1], K8SDB::OpenExisting);
  if (!db.isOpen()) {
    fprintf(stderr, "ERROR: can't open file (%s)!\n", argv[1]);
    return 1;
  }

  QStringList kl(db.keys());
/*
  foreach (const QString &key, kl) {
    qDebug() << key << "type:" << db.type(key) << "value:" << db.asString(key);
    bool ok, bv;
    int iv;
    QString sv;
    QKeySequence kq;
    switch (db.type(key)) {
      case K8SDB::Boolean:
        bv = db.toBool(key, &ok);
        qDebug() << "  " << ok << "v:" << bv;
        break;
      case K8SDB::Integer:
        iv = db.toInt(key, &ok);
        qDebug() << "  " << ok << "v:" << iv;
        break;
      case K8SDB::String:
        sv = db.toString(key, &ok);
        qDebug() << "  " << ok << "v:" << sv;
        break;
      case K8SDB::KeySequence:
        kq = db.toKeySequence(key, &ok);
        qDebug() << "  " << ok << "v:" << kq;
        break;
      default: ;
    }
  }
*/
  QVariantMap dbase;
  foreach (const QString &key, kl) {
    qDebug() << key << "type:" << typeName(db.type(key)) << "value:" << db.asString(key);
    bool ok, bv;
    int iv;
    QString sv;
    QKeySequence kq;
    QByteArray ba;
    QVariant val;
    bool addIt = true;
    switch (db.type(key)) {
      case K8SDB::Boolean:
        bv = db.toBool(key, &ok);
        qDebug() << "  " << ok << "v:" << bv;
        val = bv;
        break;
      case K8SDB::Integer:
        iv = db.toInt(key, &ok);
        qDebug() << "  " << ok << "v:" << iv;
        val = iv;
        break;
      case K8SDB::String:
        sv = db.toString(key, &ok);
        qDebug() << "  " << ok << "v:" << sv;
        if (!sv.isEmpty()) {
          val = sv;
        } else addIt = false;
        break;
      case K8SDB::KeySequence:
        kq = db.toKeySequence(key, &ok);
        qDebug() << "  " << ok << "v:" << kq;
        val = kq;
        break;
      case K8SDB::ByteArray:
        ba = db.toByteArray(key, &ok);
        qDebug() << "  " << ok << "v:" << ba;
        if (!ba.isEmpty()) {
          val = ba;
        } else addIt = false;
        break;
      default: addIt = false; break;
    }
    if (addIt) {
      QVariantMap item;
      item["type"] = typeName(db.type(key));
      item["value"] = val;
      dbase[key] = item;
    }
  }

  if (argc > 2) {
    QByteArray json;
    QString err;
    if (!K8JSON::generateExCB(0, generatorCB, err, json, dbase)) {
      fprintf(stderr, "ERROR: can't generate JSON.\n");
      qDebug() << "ERROR!" << err;
      return 1;
    }

    QFile fo(argv[2]);
    if (!fo.open(QIODevice::WriteOnly)) {
      fprintf(stderr, "ERROR: can't create JSON file.\n");
      return 1;
    }
    fo.write("// Dyskinesia database. DO NOT EDIT!\n");
    fo.write(json);
    fo.close();
  }

  return 0;
}
