/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef TEST_H
#define TEST_H

//#include <QtCore>
#include <QDebug>

#include <QObject>
#include <QStringList>


class TestClass : public QObject {
  Q_OBJECT

public:
  TestClass (QObject *parent=0) : QObject(parent) {}
  ~TestClass () {}

  int method42 () { return 42; }

signals:
  void uselessSignal ();

public slots:
  QStringList slot00 (const QStringList &v) { return v; }
  void onopt_slot00 (const QStringList &v) { }

private slots:
  QStringList slot01 (const QStringList &v) { return v; }
  void onopt_slot01 (const QStringList &v) { }

public:
  void showMt ();
  QStringList getOptList (const QString &pfx);
};


#endif
