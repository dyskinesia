/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QtCore>
#include <QDebug>

/*
#include <QMetaObject>
#include <QMetaObject>
*/

#include <QByteArray>
#include <QList>

#include <test.h>


void TestClass::showMt () {
  const QMetaObject *metaObject = this->metaObject();
  for (int f = metaObject->methodOffset(); f < metaObject->methodCount(); f++) {
    switch (metaObject->method(f).methodType()) {
      case QMetaMethod::Method: qDebug() << "*method"; break;
      case QMetaMethod::Signal: qDebug() << "*signal"; break;
      case QMetaMethod::Slot: qDebug() << "*slot"; break;
      case QMetaMethod::Constructor: qDebug() << "*constructor"; break;
      default: qDebug() << "*???"; break;
    }
    qDebug() << f << QString::fromLatin1(metaObject->method(f).signature());
    const char *sign = metaObject->method(f).signature();
    char *ps = strchr(sign, '(');
    if (!ps) {
      qDebug() << " " << ps;
    } else {
      char *t = (char *)calloc(ps-sign+1, 1);
      memcpy(t, sign, ps-sign);
      qDebug() << " " << t;
      free(t);
    }
    //qDebug() << f << QString::fromLatin1(metaObject->method(f).signature());
    QList<QByteArray> pn(metaObject->method(f).parameterNames());
    qDebug() << pn;
    QList<QByteArray> pt(metaObject->method(f).parameterTypes());
    qDebug() << pt;
    qDebug() << "================================";
  }
}


QStringList TestClass::getOptList (const QString &pfx) {
  QStringList res;
  char optPfx[128];
  strcpy(optPfx, "onopt_");
  int pp = strlen(optPfx);
  for (int f = 0; f < pfx.length(); f++) {
    if (f > 63) return res;
    QChar ch(pfx[f]);
    if (ch.unicode() < 33 || ch.unicode() > 127) return res;
    char bch = ch.unicode();
    if ((bch >= '0' && bch <= '9') || bch == '_' || bch == '/' ||
        (bch >= 'A' && bch <= 'Z') ||
        (bch >= 'a' && bch <= 'z')) {
      if (bch == '/') bch = '_';
      optPfx[pp++] = bch;
    } else return res;
  }
  optPfx[pp] = '\0';
  const QMetaObject *metaObject = this->metaObject();
  for (int f = metaObject->methodOffset(); f < metaObject->methodCount(); f++) {
    if (metaObject->method(f).methodType() != QMetaMethod::Slot) continue;
    const char *sign = metaObject->method(f).signature();
    const char *brc = strchr(sign, '(');
    if (!brc) continue; // can't be
    if (strcmp(brc, "(QStringList)")) continue; // invalid parameters
    bool ours = true; const char *p; int f;
    for (p = sign, f = 0; p < brc && optPfx[f]; p++, f++) {
      if (optPfx[f] != *p) { ours = false; break; }
    }
    if (!ours || optPfx[f]) continue;
    QByteArray ba(sign, brc-sign);
    QString mn(ba);
    res << mn;
  }
  return res;
}


int main (int argc, char *argv[]) {
/*
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("koi8-u"));
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("koi8-u"));
*/

  TestClass c;
  c.showMt();
  qDebug() << "----------------";
  qDebug() << c.getOptList("");
  qDebug() << "----------------";
  qDebug() << c.getOptList("sl");
  qDebug() << "----------------";
  qDebug() << c.getOptList("st");

  return 0;
}
