###############################################################################
## WARNING!
## remove `CONFIG += use_leveldb` to use JSON history format!
###############################################################################

TEMPLATE = app
TARGET = dyskinesia

QT -= xml
QT += gui network

CONFIG += packdata
#CONFIG += json_history

#CONFIG += use_hamsterdb
#CONFIG += use_leveldb
CONFIG += use_bhf

!win32 {
  CONFIG += otr
  LIBS += -lX11
}


CONFIG += usessl

CONFIG += qt warn_on
CONFIG += debug_and_release
#CONFIG += debug
#CONFIG += release

#QMAKE_CFLAGS_RELEASE ~= s/\-O./-Os
#QMAKE_CXXFLAGS_RELEASE ~= s/\-O./-Os

##QMAKE_CFLAGS_RELEASE ~= s/\-O./-O2
##QMAKE_CXXFLAGS_RELEASE ~= s/\-O./-O2

QMAKE_CFLAGS_RELEASE += -march=native
QMAKE_CXXFLAGS_RELEASE += -march=native
QMAKE_CFLAGS_RELEASE += -mtune=native
QMAKE_CXXFLAGS_RELEASE += -mtune=native

QMAKE_CFLAGS_RELEASE += -fwrapv
QMAKE_CXXFLAGS_RELEASE += -fwrapv

QMAKE_LFLAGS_RELEASE += -s


DESTDIR = .
OBJECTS_DIR = .build/obj
UI_DIR = .build/uic
MOC_DIR = .build/moc
RCC_DIR = .build/rcc


!CONFIG(use_hamsterdb) {
  !CONFIG(use_leveldb) {
    !CONFIG(use_bhf) {
      CONFIG += json_history
    }
  }
}

CONFIG(otr) {
  #LIBS += -lotr
  DEFINES += USE_OTR
  CONFIG += link_pkgconfig
  PKGCONFIG += libotr
}

CONFIG(use_hamsterdb) {
  DEFINES += USE_HAMSTER_DB
  LIBS += $$PWD/hamsterdb/lib/libhamsterdb.a
  INCLUDEPATH += $$PWD/hamsterdb
}

CONFIG(use_leveldb) {
  DEFINES += USE_LEVEL_DB LEVELDB_PLATFORM_POSIX OS_LINUX
  LIBS += $$PWD/leveldb/lib/libleveldb.a
  INCLUDEPATH += $$PWD/leveldb
}


CONFIG(usessl) {
  DEFINES += USE_SSL
}


CONFIG(staticpicplugs) {
  QTPLUGIN += qgif4 qjpeg4 qmng4 qico4 qsvg4 qtiff4
  DEFINES += USE_STATIC_PICPLUGS
  DEFINES += QT_STATICPLUGIN
  LIBS += -Lc:/Qtsdk/qt/plugins/imageformats
}


include(src/psycproto/psycproto.pri)

#include(src/test/parser/test.pri)
#include(src/test/socket/test.pri)

include(src/main.pri)

CONFIG(packdata) {
  RESOURCES += $$PWD/data.qrc
}

RESOURCES += $$PWD/icons.qrc

win32 {
  RC_FILE = winrc.rc
}
