(global-key-bind "C-x л" "chat-close")
(global-key-bind "C-x C-k" "chat-close")

; initiate various chats
(global-key-bind "C-c C-t C-e" "chat-with _google/en2ru")
(global-key-bind "C-c C-t C-r" "chat-with _google/ru2en")
;(global-key-bind "f2" "chat-with welcome")


; enter sends?
(editor-key-bind "KPRET" "chat-send")
(editor-key-bind "RET" "chat-send")
(editor-key-bind "C-RET" "edit-insert-new-line")
(editor-key-bind "S-RET" "edit-insert-new-line")
(editor-key-bind "M-RET" "edit-insert-new-line")

(editor-key-bind "TAB" "edit-autocomplete")
(global-key-bind "f1 f1" "do-command showbinds")
(global-key-bind "C-x C-c" "action-quit")
(global-key-bind "C-x esc" "window-minimize")
(global-key-bind "esc" "window-hide")


; more globals
(global-key-bind "C-u" "go-to-unread")
(global-key-bind "C-w" "chat-close")
(global-key-bind "C-x k" "chat-close")
(global-key-bind "C-q" "chat-quote")
(global-key-bind "C-s" "set-status-text")

(global-key-bind "C-x C-a C-e" "editor-activate")
(global-key-bind "C-x C-a C-c" "chat-activate")
(global-key-bind "C-x C-a C-b" "clist-activate")
(global-key-bind "C-x C-a C-l" "clist-activate")

; editor
(editor-key-bind "C-r" "edit-recode-text")

; smiles
(editor-key-bind "C-x C-s C-c" "edit-insert-text \"______,,,^..^,,,______\"")
(editor-key-bind "C-x C-s C-d" "edit-insert-desu")
(editor-key-bind "C-x C-s C-o" "edit-insert-smile-oo")
(editor-key-bind "C-x C-s C-w" "edit-insert-smile-oo-weird")

; emacs
(editor-key-bind "M-a" "edit-select-all")
(editor-key-bind "C-a" "edit-move-home")
(editor-key-bind "C-e" "edit-move-end")
(editor-key-bind "C-b" "edit-move-left")
(editor-key-bind "C-f" "edit-move-right")
(editor-key-bind "M-b" "edit-move-word-left")
(editor-key-bind "M-f" "edit-move-word-right")
(editor-key-bind "C-p" "edit-move-up")
(editor-key-bind "C-n" "edit-move-down")
(editor-key-bind "M-S-<" "edit-move-top")
(editor-key-bind "M-S->" "edit-move-bottom")
(editor-key-bind "C-j" "edit-insert-new-line")

; menus
(global-key-bind "M-s" "activate-menu-status")
(global-key-bind "M-m" "activate-menu-main")

; status change
(global-key-bind "C-0" "action-status-offline")
(global-key-bind "C-1" "action-status-here")
(global-key-bind "C-2" "action-status-ffc")
(global-key-bind "C-3" "action-status-realtime")
(global-key-bind "C-4" "action-status-busy")
(global-key-bind "C-5" "action-status-nearby")
(global-key-bind "C-6" "action-status-dnd")
(global-key-bind "C-7" "action-status-away")
(global-key-bind "C-8" "action-status-vacation")


(global-key-bind "M-a" "action-edit-account")
(global-key-bind "M-j" "action-join-place")
(global-key-bind "M-S" "action-send-raw-packet")
(global-key-bind "M-C" "action-show-packet-console")


; ukrainian letters
(editor-key-bind "C-' ы" "edit-insert-text \"і\"")
(editor-key-bind "C-' Ы" "edit-insert-text \"І\"")
(editor-key-bind "C-' й" "edit-insert-text \"ї\"")
(editor-key-bind "C-' Й" "edit-insert-text \"Ї\"")
(editor-key-bind "C-' е" "edit-insert-text \"є\"")
(editor-key-bind "C-' Е" "edit-insert-text \"Є\"")
(editor-key-bind "C-' э" "edit-insert-text \"є\"")
(editor-key-bind "C-' Э" "edit-insert-text \"Є\"")
(editor-key-bind "C-\ ы" "edit-insert-text \"і\"")
(editor-key-bind "C-\ Ы" "edit-insert-text \"І\"")
(editor-key-bind "C-\ й" "edit-insert-text \"ї\"")
(editor-key-bind "C-\ Й" "edit-insert-text \"Ї\"")
(editor-key-bind "C-\ е" "edit-insert-text \"є\"")
(editor-key-bind "C-\ Е" "edit-insert-text \"Є\"")
(editor-key-bind "C-\ э" "edit-insert-text \"є\"")
(editor-key-bind "C-\ Э" "edit-insert-text \"Є\"")
