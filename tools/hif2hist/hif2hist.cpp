/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QtCore>
#include <QDebug>

#include <QTime>

#include "k8history.h"
#include "k8strutils.h"

#include "hif2hist.h"


#define KHISTORY_DATE_FORMAT  "yyyy/MM/dd HH:mm:ss"


/*
 * return 0 if there aren't any line chars
 */
static char *nextLine (uchar **ptr, int *bleft, int *llen) {
  int left = *bleft;
  if (left < 1) {
    if (llen) *llen = 0;
    return 0;
  }
  uchar *p = *ptr; uchar *start = p;
  while (*p && left) {
    if (*p == '\n') {
      *ptr = p+1; *bleft = left-1;
      if (llen) {
        *llen = p-start;
        if (*llen > 0 && p[-1] == '\r') (*llen)--;
      }
      return (char *)start;
    }
    left--; p++;
  }
  *ptr = p; *bleft = left;
  if (llen) *llen = p-start;
  return (char *)start;
}


static void dateFromString (QDateTime &dt, const QString &dateStr) {
  dt = QDateTime::fromString(dateStr, KHISTORY_DATE_FORMAT);
  if (dt.isNull() || !dt.isValid()) dt = QDateTime::currentDateTime();
  else {
    dt.setTimeSpec(Qt::UTC);
    dt = dt.toLocalTime();
  }
}


bool parse (HistoryMessage &msg, uchar **ptr, int *bleft) {
  int len = 0; char *s;
  // type unixtime uni
  msg.valid = false;
  if (!(s = nextLine(ptr, bleft, &len))) return false;
 //write(2, s, len); write(2, "\n", 1);
  if (len < 22 || s[0] != '*') return false;
  switch (s[1]) {
    case '>': msg.type = HistoryMessage::Incoming; break;
    case '<': msg.type = HistoryMessage::Outgoing; break;
    case '=': msg.type = HistoryMessage::Server; break;
    case '*': msg.type = HistoryMessage::Info; break;
    default: return false;
  }
  // skip flag
  s += 3; len -= 3;
  // date
  QString dts(QByteArray(s, 19));
  dateFromString(msg.date, dts);
  // uni
  s += 20; len -= 20;
 //write(2, s, len); write(2, "\n", 1);
  if (len < 1) msg.uni = QString(); else msg.uni = QByteArray(s, len);
  // action and body
  if (!(s = nextLine(ptr, bleft, &len))) return false;
 //write(2, s, len); write(2, "\n", 1);
  if (len > 0 && s[0] == ':') {
    // action
   //write(2, s, len); write(2, "\n", 1);
    if (len > 1) msg.action = K8Str::unescapeNL(QString::fromUtf8(QByteArray(s+1, len-1)));
    else msg.action = QString();
    if (!(s = nextLine(ptr, bleft, &len))) return false;
  } else {
    msg.action = QString();
  }
  if (len < 0 || s[0] != ' ') return false;
 //write(2, s, len); write(2, "\n", 1);
  if (len > 1) msg.text = K8Str::unescapeNL(QString::fromUtf8(QByteArray(s+1, len-1)));
  else msg.text = QString();
  msg.valid = true;
  return true;
}


int main (int argc, char *argv[]) {
  Q_UNUSED(argc)
  Q_UNUSED(argv)
  QString fiName;//("dump.hif");
  QString foName;//("dump");
  QString myUNI;

  if (argc < 3) {
    qDebug() << "usage: hif2hist basename myuni";
  }

  fiName = QString(argv[1])+".hif";
  foName = argv[1];
  myUNI = argv[2];

  QTime st;
  //st.start();
  //qDebug() << "time taken (ms):" << st.elapsed();

  QFile fi(fiName);
  if (!fi.open(QIODevice::ReadOnly)) {
    qDebug() << "can't open input file!";
    return 1;
  }
  int mSize = fi.size();
  uchar *mMap;
  if (!(mMap = fi.map(0, mSize))) {
    fi.close();
    qDebug() << "can't mmap input file!";
    return 1;
  }
  uchar *mPos = mMap;

  HistoryFile hf(foName, myUNI);
  hf.remove();
  if (argc > 3 && QString(argv[3]) == "DELETE") return 0;

  qDebug() << "opening file...";
  st.start();
  if (!hf.open(HistoryFile::Write)) {
    qDebug() << "can't open history file!";
    return 1;
  }

  qDebug() << "dumping file...";
  int mTotal = 0;
  HistoryMessage msg;
  st.start();
  while (parse(msg, &mPos, &mSize)) {
    if (msg.valid) {
      ++mTotal;
      //dumpMsg(fo, msg);
      hf.append(msg);
      if (mTotal%8192 == 1) {
        fprintf(stderr, "\r%d messages processed", mTotal); fflush(stderr);
      }
    }
  }
  fprintf(stderr, "\r%d messages processed\n", mTotal); fflush(stderr);
  qDebug() << "time taken (ms):" << st.elapsed() << " messages:" << mTotal;
  hf.close();
  fi.unmap(mMap);
  fi.close();

  return 0;
}
