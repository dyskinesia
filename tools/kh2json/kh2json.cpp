/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QDebug>

#include <stdio.h>
#include <stdlib.h>

#include <QtCore>

#include "k8history.h"
#include "k8json.h"


#define DATE_FORMAT  "yyyy/MM/dd HH:mm:ss"


void convert (const char *infl) {
  QString infname(infl);
  QString ofname(infname);
  if (ofname.endsWith(".txt")) ofname.chop(4);
  ofname += ".json";

  int msgXCnt = 0;

  {
  HistoryFile hf(infname, HistoryFile::Read);
  if (!hf.open()) {
    fprintf(stderr, "ERROR: can't open input file!\r\n");
    return;
  }

  QFile fo(ofname);
  fo.remove();
  if (!fo.open(QIODevice::WriteOnly)) {
    fprintf(stderr, "ERROR: can't create output file!\r\n");
    return;
  }
  QTextStream sout(&fo);
  sout.setCodec("UTF-8");
  sout << "//Dyskinesia JSON history file\n";
  sout << "//'timestamp' is UTC unixtime, but commented date is local\n";
  //sout << "//WARNING: comments aren't the part of the official JSON spec!\n";
  sout << "[\n";

  HistoryMessage msg;
  int msgNo = 0;
  while (hf.read(msg)) {
    if (msgNo) sout << ","; else sout << " ";
    sout << "{//#" << msgXCnt << "\n";
    switch (msg.type) {
      case HistoryMessage::Incoming:
      case HistoryMessage::Outgoing:
        sout << "  " << K8JSON::quote("uni") << ": " << K8JSON::quote(msg.uni) << ",\n";
        break;
      default: ;
    }
/*
    sout << "  \"type\": \"";
    switch (msg.type) {
      case HistoryMessage::Incoming: sout << "in"; break;
      case HistoryMessage::Outgoing: sout << "out"; break;
      case HistoryMessage::Server: sout << "srv"; break;
      default: sout << "nfo"; break;
    }
    sout << "\",\n";
*/
    QString dtS = msg.date.toString(DATE_FORMAT);
    QDateTime utc(msg.date.toUTC());
    //sout << "  \"timestamp\": " << utc.toTime_t() << ", //" << utc.toString(DATE_FORMAT) << "\n";
    sout << "  " << K8JSON::quote("timestamp") << ": " << utc.toTime_t() << ", //" << dtS << "\n";
    msg.action.replace("\r\n", "\n");
    if (!msg.action.isEmpty()) {
      sout << "  " << K8JSON::quote("action") << ": " << K8JSON::quote(msg.action) << ",\n";
    }
    msg.text.replace("\r\n", "\n");
    sout << "  " << K8JSON::quote("text") << ": " << K8JSON::quote(msg.text) << "\n";
    sout << " }\n";
    msgNo++; msgXCnt++;
  }
  sout << "]\n";
  hf.close();
  }
}


int main (int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "usage: %s filelist\r\n", argv[0]);
    return 1;
  }

  for (int f = 1; f < argc; f++) {
    printf("converting [%s]...\r\n", argv[f]);
    convert(argv[f]);
  }

  return 0;
}
