/* coded by Ketmar // Vampire Avalon (ketmar@ketmar.no-ip.org)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QtCore>
#include <QDebug>

#include <QTime>

#include "k8history.h"
#include "k8strutils.h"

#include "js2hif.h"


#define KHISTORY_DATE_FORMAT  "yyyy/MM/dd HH:mm:ss"


static QString dateToString (const QDateTime &dt) {
  if (dt.isNull()) return QDateTime::currentDateTime().toUTC().toString(KHISTORY_DATE_FORMAT);
  return dt.toUTC().toString(KHISTORY_DATE_FORMAT);
}


static void dumpMsg (QFile &fo, const HistoryMessage &msg) {
  QByteArray res;
  // type
  switch (msg.type) {
    case HistoryMessage::Incoming: res.append("*>"); break;
    case HistoryMessage::Outgoing: res.append("*<"); break;
    case HistoryMessage::Server: res.append("*="); break;
    case HistoryMessage::Info: res.append("**"); break;
  }
  res.append(' ');
  // date
  //res.append(QString::number(msg.date.toUTC().toTime_t()).toAscii());
  res.append(dateToString(msg.date).toAscii());
  res.append(' ');
  // uni
  res.append(msg.uni.toUtf8());
  res.append('\n');
  // action
  if (!msg.action.isEmpty()) {
    res.append(':');
    res.append(K8Str::escapeNL(msg.action).toUtf8());
    res.append('\n');
  }
  // text
  res.append(' ');
  //res.append(K8Str::escapeNL(msg.text).toUtf8());
  res.append(K8Str::escapeNL(msg.text).toUtf8());
  res.append('\n');
  // done
  fo.write(res);
}


int main (int argc, char *argv[]) {
  Q_UNUSED(argc)
  Q_UNUSED(argv)
  QString fiName;//("dump.hif");
  QString foName;//("dump");
  QString myUNI;

  if (argc < 3) {
    qDebug() << "usage: hif2hist basename myuni";
  }

  fiName = QString(argv[1]);
  foName = QString(argv[1])+".hif";
  myUNI = argv[2];

  QTime st;
  //st.start();
  //qDebug() << "time taken (ms):" << st.elapsed();

  HistoryFile hf(fiName, myUNI);
  qDebug() << "opening file...";
  st.start();
  if (!hf.open(HistoryFile::Read)) {
    qDebug() << "can't open history file!";
    return 1;
  }
  qDebug() << "time taken (ms):" << st.elapsed();
  qDebug() << "number of messages:" << hf.count();

  QFile fo(foName);
  fo.remove();
  if (!fo.open(QIODevice::WriteOnly)) {
    qWarning() << "can't open output file";
    return 1;
  }

  qDebug() << "dumping file...";
  int mNo = 0, mTotal = 0;
  HistoryMessage msg;
  st.start();
  while (hf.read(mNo++, msg)) {
    if (!msg.valid) {
      qDebug() << mNo-1 << "is invalid!";
    } else {
      ++mTotal;
      dumpMsg(fo, msg);
      if (mTotal%8192 == 1) {
        fprintf(stderr, "\r%d messages processed", mTotal); fflush(stderr);
      }
    }
  }
  fprintf(stderr, "\r%d messages processed\n", mTotal); fflush(stderr);
  qDebug() << "time taken (ms):" << st.elapsed();
  hf.close();
  fo.close();

  return 0;
}
