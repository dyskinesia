#!/usr/bin/lua

local infn = arg[1] or "psyc___psyced.org_@welcome.txt";
local tfn = infn..".0";

local fi = assert(io.open(infn, "r"));
local fo = assert(io.open(tfn, "w"));

local cnt = 0;
local isk8qutim = arg[2];
repeat
  local sflg = fi:read();
  if not sflg then break; end;
  if isk8qutim then
    if not fi:read() then break; end;
  end;
  local suni = fi:read();
  if not suni then break; end;
  local sdate = fi:read();
  if not sdate then break; end;
  local stext = fi:read();
  if not stext then break; end;
  if sflg == "in" then sflg = "*>";
  elseif sflg == "out" then sflg = "*<";
  else sflg = "**";
  end;
  fo:write(sflg, " ", sdate, " ", suni, "\n", stext, "\n");
  cnt = cnt+1;
until false;

fi:close();
fo:close();

os.remove(infn);
os.rename(tfn, infn);
if infn:match("%.txt$") then
  local ifn = infn:sub(1, -4).."idx";
  os.remove(ifn);
end;

print(string.format("%i messages converted", cnt));
--[[
in
psyc://ketmar.no-ip.org/~ketmar
2009/02/27 16:29:16
not dad, only family friend %-)
]]
