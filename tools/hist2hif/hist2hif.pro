TEMPLATE = app
TARGET = hist2hif

QT -= xml
QT += gui
CONFIG += qt warn_on
#CONFIG += debug_and_release
#CONFIG += debug
CONFIG += release

#CONFIG += use_leveldb
CONFIG += use_bhf

#QMAKE_CFLAGS_RELEASE ~= s/\-O./-Os
#QMAKE_CXXFLAGS_RELEASE ~= s/\-O./-Os

##QMAKE_CFLAGS_RELEASE ~= s/\-O./-O2
##QMAKE_CXXFLAGS_RELEASE ~= s/\-O./-O2

#QMAKE_CFLAGS_RELEASE += -march=native
#QMAKE_CXXFLAGS_RELEASE += -march=native
#QMAKE_CFLAGS_RELEASE += -mtune=native
#QMAKE_CXXFLAGS_RELEASE += -mtune=native

#QMAKE_LFLAGS_RELEASE += -s


DESTDIR = .
OBJECTS_DIR = _build/obj
UI_DIR = _build/uic
MOC_DIR = _build/moc
RCC_DIR = _build/rcc


include($$PWD/../../src/k8utils/k8utils.pri)

CONFIG(use_leveldb) {
  DEFINES += HISTORY_OK
  DEFINES += LDB_HISTORY
  DEFINES += USE_LEVEL_DB LEVELDB_PLATFORM_POSIX OS_LINUX
  LIBS += $$PWD/../../leveldb/lib/libleveldb.a
  INCLUDEPATH += $$PWD/../../leveldb
  include($$PWD/../../src/k8ldbhistory/k8ldbhistory.pri)
}

!CONFIG(use_leveldb) {
  CONFIG(use_bhf) {
    DEFINES += HISTORY_OK
    DEFINES += BHF_HISTORY
    include($$PWD/../../src/k8bhfhistory/k8bhfhistory.pri)
  }
  !CONFIG(use_bhf) {
    DEFINES += HISTORY_OK
    DEFINES += JSON_HISTORY
    include($$PWD/../../src/k8jshistory/k8jshistory.pri)
  }
}


DEFINES += K8JSON_INCLUDE_WRITER
DEFINES += K8JSON_INCLUDE_GENERATOR
DEFINES += K8JSON_INCLUDE_COMPLEX_GENERATOR
include($$PWD/../../src/k8json/k8json.pri)

include($$PWD/../../src/k8history/k8history.pri)

HEADERS += \
  $$PWD/hist2hif.h
SOURCES += \
  $$PWD/hist2hif.cpp
