/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "dlogf.h"


#ifdef __cplusplus
extern "C" {
#endif

#ifdef WINNT
# include <windows.h>
#else
# include <time.h>
# include <sys/types.h>
# include <time.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#ifndef NO_DEBUG_LOG
static char *dbgLogFileName = NULL;
static int dlogFNTouched = -1;
static int dbgLogWriteToScreen = -1; /* default: no; not touched */
static FILE *dlogFile = NULL;

static int initialized = 0;
static char modulePath[4096];

#ifdef WINNT
static DWORD mypid;
#else
pid_t mypid;
#endif


static void dlogfInit (void) {
#ifdef WINNT
  char *p;
  DWORD len = GetModuleFileName(NULL, modulePath, sizeof(modulePath)/sizeof(char)-1);
  for (p = modulePath; *p; p++) if (*p == '\\') *p == '/';
  while (len >= 0) {
    if (!len) { strcpy(modulePath, "./"); break; }
    len--;
    if (modulePath[len] == '/') {
      len++;
      modulePath[len] = '\0';
      break;
    }
  }
  mypid = GetCurrentProcessId();
#else
  if (getcwd(modulePath, sizeof(modulePath)/sizeof(char)-1)) {
    if (!modulePath[0]) strcpy(modulePath, "/tmp/");
    else {
      if (modulePath[strlen(modulePath)-1] != '/') strcat(modulePath, "/");
    }
  } else strcpy(modulePath, "/tmp/");
  mypid = getpid();
#endif
  if (dlogFNTouched < 0) dlogfSetFile("debug.log");
  if (dbgLogWriteToScreen < 0) dlogfSetStdErr(0);
  initialized = 1;
}


void dlogfDeinit (void) {
  if (dlogFile) fclose(dlogFile);
  dlogFile = NULL;
}


void dlogfSetStdErr (int doIt) {
#ifdef WINNT
  if (doIt) {
    SetConsoleMode(GetStdHandle(STD_INPUT_HANDLE), ENABLE_PROCESSED_INPUT);
    SetConsoleMode(GetStdHandle(STD_ERROR_HANDLE), ENABLE_PROCESSED_OUTPUT | ENABLE_WRAP_AT_EOL_OUTPUT);
  }
#endif
  dbgLogWriteToScreen = doIt ? 1 : 0;
}


void dlogfSetFile (const char *fname) {
  dlogFNTouched = 1;
  if (dbgLogFileName) free(dbgLogFileName);
  dbgLogFileName = NULL;
  if (!fname || !fname[0]) return;
  char buf[8192];
  buf[0] = '\0';
#ifdef WINNT
  if (fname[0] != '/' && fname[0] != '\\' && (strlen(fname) < 3 || fname[1] != ':')) strcpy(buf, modulePath);
#else
  if (fname[0] != '/') strcpy(buf, modulePath);
#endif
  strcat(buf, fname);
  dbgLogFileName = malloc((strlen(buf)+1)*sizeof(char));
  if (dbgLogFileName) strcpy(dbgLogFileName, buf);
}


#define DBG_TMP_BUF_SIZE 65536
static char *fmtBuf (const char *sbuf, const char *pfx) {
  static char buf[DBG_TMP_BUF_SIZE*4+1024];
  const char *p = sbuf;
  char *d = buf;
  size_t left = sizeof(buf)-4;
  int lastWasN = 0, plen = strlen(pfx);
  strcpy(d, pfx); left -= plen; d += plen;
  while (left > 0 && *p) {
    while (*p == '\r') p++;
    if (lastWasN) {
#if WINNT
      *d++ = '\r'; *d++ = '\n'; left -= 2;
#else
      *d++ = '\n'; left--;
#endif
      if (left < strlen(pfx)) break; // no room
      strcpy(d, pfx); left -= plen; d += plen;
      lastWasN = 0;
    }
    if (*p == '\n') {
      lastWasN = 1;
      p++;
      continue;
    }
    *d++ = *p++; left--;
  }
#if WINNT
  if (d == buf || d[-1] != '\n') strcpy(d, "\r\n"); else *d = '\0';
#else
  if (d == buf || d[-1] != '\n') strcpy(d, "\n"); else *d = '\0';
#endif
  return buf;
}


void dlogfVA (const char *fmt, va_list inap) {
  va_list ap2;
  static char buf[DBG_TMP_BUF_SIZE+1024];
  const char *xbuf = NULL;
#ifdef WINNT
  SYSTEMTIME t;
#else
  time_t t;
  struct tm bt;
#endif
  char timebuf[64];

  if (!fmt) return;

  if (!initialized) dlogfInit();

  memset(buf, 0, DBG_TMP_BUF_SIZE);
  va_copy(ap2, inap);
  vsnprintf(buf, DBG_TMP_BUF_SIZE, fmt, ap2);
  va_end(ap2);
  buf[DBG_TMP_BUF_SIZE] = '\0';

#ifdef WINNT
  GetLocalTime(&t);
  sprintf(timebuf, "[%05i] %04i/%02i/%02i %02i:%02i:%02i: ",
    (int)mypid,
    t.wYear, t.wMonth, t.wDay,
    t.wHour, t.wMinute, t.wSecond
  );
#else
  t = time(NULL);
  localtime_r(&t, &bt);
  sprintf(timebuf, "[%05i] %04i/%02i/%02i %02i:%02i:%02i: ",
    (int)mypid,
    bt.tm_year+1900, bt.tm_mon, bt.tm_mday,
    bt.tm_hour, bt.tm_min, bt.tm_sec
  );
#endif

  xbuf = fmtBuf(buf, timebuf);

  if (dbgLogFileName) {
    if (!dlogFile) dlogFile = fopen(dbgLogFileName, "ab");
    if (dlogFile) {
      fputs(xbuf, dlogFile);
      fflush(dlogFile);
    }
  }

  if (dbgLogWriteToScreen) {
#ifdef WINNT
    HANDLE oh = GetStdHandle(STD_ERROR_HANDLE);
    if (oh != INVALID_HANDLE_VALUE) WriteConsole(oh, xbuf, strlen(xbuf), &wr, NULL);
#else
    fputs(xbuf, stderr);
    fflush(stderr);
  }
#endif
}


void __attribute__((format(printf, 1, 2))) dlogf (const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  dlogfVA(fmt, ap);
  va_end(ap);
}


#endif /* NO_DEBUG_LOG */

#ifdef __cplusplus
}
#endif
