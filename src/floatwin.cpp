/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * we are the Borg.
 */
#include "floatwin.h"

#include <QApplication>
#include <QDataStream>
#include <QDateTime>
#include <QDesktopWidget>
#include <QFont>
#include <QList>
#include <QMouseEvent>
#include <QPixmap>


///////////////////////////////////////////////////////////////////////////////
static bool DoSticking (int &x0, int &y0, int w, int h, const QRect &itsRect, int power) {
  bool res = false;
  int d;
  int x1, y1;
  int ix0, iy0, ix1, iy1;

  x1 = x0+w-1; y1 = y0+h-1;
  QRect my(x0-power, y0-power, x1+power, y1+power);
  if (!my.intersects(itsRect)) return false; // no magnet
  itsRect.getCoords(&ix0, &iy0, &ix1, &iy1);
  // left side of it
  d = ix0-x1;
  if (d > 0 && d <= power) {
    x0 += d; x1 += d;
    // top
    d = iy0-y0; if (d >= -power && d <= power) { y0 += d; y1 += d; }
    res = true;
  }
  // right side of it
  d = x0-ix1;
  if (d > 0 && d <= power) {
    x0 -= d;
    // top
    d = iy0-y0; if (d >= -power && d <= power) { y0 += d; y1 += d; }
    res = true;
  }
  // top side of it
  d = iy0-y1;
  if (d > 0 && d <= power) {
    y0 += d; y1 += d;
    // left
    d = ix0-x0; if (d >= -power && d <= power) { x0 += d; x1 += d; }
    // right
    d = ix1-x1; if (d >= -power && d <= power) { x0 += d; x1 += d; }
    res = true;
  }
  // bottom side of it
  d = y0-iy1;
  if (d > 0 && d <= power) {
    y0 -= d;
    // left
    d = ix0-x0; if (d >= -power && d <= power) { x0 += d; x1 += d; }
    // right
    d = ix1-x1; if (d >= -power && d <= power) { x0 += d; x1 += d; }
    res = true;
  }
  return res;
}


static bool DoDeskSticking (int &x0, int &y0, int w, int h, int power) {
  QDesktopWidget *desktop = QApplication::desktop();
  QRect geometry = desktop->availableGeometry();
  bool res = false;
  int d;
  int x1, y1;
  int ix0, iy0, ix1, iy1;

  x1 = x0+w-1; y1 = y0+h-1;
  geometry.getCoords(&ix0, &iy0, &ix1, &iy1); //ix1 += ix0-1; iy1 += iy0-1;
  //fprintf(stderr, "desk: (%i, %i)-(%i, %i)\n", ix0, iy0, ix1, iy1);
  // left side of desktop
  d = x0-ix0;
  if (d > 0 && d <= power) {
    x0 -= d; x1 -= d;
    res = true;
  }
  // right side of desctop
  d = ix1-x1;
  if (d > 0 && d <= power) {
    x0 += d;
    res = true;
  }
  // top side of desctop
  d = y0-iy0;
  if (d > 0 && d <= power) {
    y0 -= d; y1 -= d;
    res = true;
  }
  // bottom side of desctop
  d = iy1-y1;
  if (d > 0 && d <= power) {
    y0 += d;
    res = true;
  }
  return res;
}


///////////////////////////////////////////////////////////////////////////////
FloatingWindow::FloatingWindow (FloatWinManager *wman, const QString &uni) :
  QWidget(0), mMan(wman),
  mUni(uni), mTitle(""), mStatus(PsycProto::Offline),
  mMaybeMoving(false), mBMsgPhase(false), mBTitlePhase(false), mIgnoreMouseClickRelease(false),
  mIcon(0), mMsgIcon(0), mMoveTimer(0),
  mMousePos(QPoint()), mInitMousePos(QPoint()),
  mBackBuf(0)
{
  setMouseTracking(true);
  setAttribute(Qt::WA_QuitOnClose, false);
  setAttribute(Qt::WA_DeleteOnClose, true);
  setAttribute(Qt::WA_KeyboardFocusChange, false);
  setAttribute(Qt::WA_NoSystemBackground, true);
  setAttribute(Qt::WA_AlwaysShowToolTips, true);
  setWindowFlags(windowFlags() | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::ToolTip);

  QFont f(font());
  f.setPixelSize(12);
  setFont(f);

  calcSize();
}


FloatingWindow::~FloatingWindow () {
  if (mMan) mMan->remove(this);
  discardBuf();
}


const QIcon *FloatingWindow::statusIcon () {
  if (!mIcon) mIcon = mMan->getStatusIcon(this, mStatus);
  return mIcon;
}


const QIcon *FloatingWindow::msgIcon () {
  if (!mMsgIcon) mMsgIcon = mMan->getMsgIcon(this);
  return mMsgIcon;
}


void FloatingWindow::startMoveTimer () {
  if (!mMoveTimer) {
    mMoveTimer = new QTimer(this);
    connect(mMoveTimer, SIGNAL(timeout()), this, SLOT(onStartMove()));
    mMoveTimer->start(QApplication::startDragTime());
  }
}


void FloatingWindow::stopMoveTimer () {
  if (mMoveTimer) {
    delete mMoveTimer;
    mMoveTimer = 0;
  }
}


void FloatingWindow::discardBuf () {
  if (mBackBuf) {
    delete mBackBuf;
    mBackBuf = 0;
  }
}


void FloatingWindow::setTitle (const QString &title) {
  if (title != mTitle) {
    mTitle = title;
    calcSize();
    update();
  }
}


void FloatingWindow::setTip (const QString &tip) {
  setToolTip(tip);
}


void FloatingWindow::setStatus (PsycProto::Status status) {
  if (mStatus != status) {
    mStatus = status;
    mIcon = 0;
    discardBuf();
    update();
  }
}


void FloatingWindow::calcSize () {
  discardBuf();
  QPixmap pict(2, 2);
  QPainter p(&pict);
  QFont f(font());
  f.setUnderline(true);
  f.setItalic(true);
  f.setStrikeOut(true);
  f.setOverline(true);
  f.setBold(true);
  p.setFont(f);
  QRect br;
  p.drawText(0, 0, 1, 1, Qt::AlignLeft | Qt::AlignVCenter, mTitle, &br);
  int hgt = br.bottom(); if (hgt < 20) hgt = 20;
  resize(2+16+4+br.right()+4+2, hgt);
}


void FloatingWindow::paintEvent (QPaintEvent *e) {
  Q_UNUSED(e)
  const QIcon *icon;
  int w = width(), h = height();

  if (!mBackBuf) {
    mBackBuf = new QPixmap(w, h);
    QPainter p(mBackBuf);
    //p.begin(pict);
    int fClr = 96, bClr = 32;
    p.fillRect(QRect(0, 0, w, h), QColor(fClr, fClr, fClr));
    p.fillRect(QRect(1, 1, w-2, h-2), QColor(bClr, bClr, bClr));

    icon = mBMsgPhase ? msgIcon() : statusIcon();
    if (icon && !icon->isNull()) icon->paint(&p, 2, (h-16)/2, 16, 16, Qt::AlignLeft);

    //int ow = w, oh = h;
    p.setPen(mBTitlePhase ? QColor(255, 255, 0) :
      mStatus==PsycProto::Offline ? QColor(255, 0, 0) : QColor(255, 127, 0)
    );

    QFont f(font());
/*
    f.setUnderline(fBuddy->notAutho);
    f.setItalic(fBuddy->IsInVisList());
    f.setStrikeOut(fBuddy->IsInInvisList());
    f.setOverline(fBuddy->IsInIgnoreList());
    f.setBold(false);//!fBuddy->isOffline);
*/
    f.setUnderline(false);
    f.setItalic(false);
    f.setStrikeOut(false);
    f.setOverline(false);
    f.setBold(false);
    p.setFont(f);
    int x = 4+16+2; w -= x+2;
    p.drawText(x, 2, w, h-4, Qt::AlignLeft | Qt::AlignVCenter, mTitle);

    p.end();
  }

  // cache image, so we can redraw it only if something was changed?
  QPainter p(this);
  p.drawPixmap(0, 0, *mBackBuf);
  p.end();
}


void FloatingWindow::mouseDoubleClickEvent (QMouseEvent *e) {
  QPoint cp(e->globalPos());
  emit mouseDblClicked(cp.x(), cp.y());
}


void FloatingWindow::mousePressEvent (QMouseEvent *e) {
  if (e->button() == Qt::LeftButton) {
    mInitMousePos = e->pos();
    mMaybeMoving = true;
    startMoveTimer();
  }
  if (e->button() == Qt::RightButton) {
    QPoint cp(e->globalPos());
    emit mouseRButtonClicked(cp.x(), cp.y());
  }
}


void FloatingWindow::mouseReleaseEvent (QMouseEvent *e) {
  if (e->button() == Qt::LeftButton) {
    mInitMousePos = QPoint(0, 0);
    mMaybeMoving = false;
    stopMoveTimer();
    if (mIgnoreMouseClickRelease) {
      releaseMouse();
      mIgnoreMouseClickRelease = false;
      emit positionChanged();
      // neighbours
      QList<FloatingWindow *> neighbors;
      addNeighbors(neighbors);
      // list can grow, so can't do foreach here
      for (int f = 0; f < neighbors.size(); f++) neighbors.at(f)->addNeighbors(neighbors);
      foreach (FloatingWindow *w, neighbors) w->emitChangePosition();
      // done
      mMousePos = QPoint();
      return;
    }
  }
}


void FloatingWindow::mouseMoveEvent (QMouseEvent *e) {
  if ((e->buttons() & Qt::LeftButton) && !mInitMousePos.isNull() &&
      (QPoint(e->pos()-mInitMousePos).manhattanLength() > QApplication::startDragDistance())) onStartMove();
  if (mMousePos.isNull()) return;

  QPoint newPos = e->globalPos()-mMousePos;
  if (!(e->modifiers() & Qt::ShiftModifier)) {
    // find neighbors
    QList<FloatingWindow *> neighbors;
    FloatingWindow *rightmost = 0;
    QRect rgeo;
    addNeighbors(neighbors);
    // list can grow, so can't do foreach here
    for (int f = 0; f < neighbors.size(); f++) {
      if (!rightmost) {
        rightmost = neighbors.at(f);
        rgeo = rightmost->geometry();
      } else {
        QRect geo = neighbors.at(f)->geometry();
        if (rgeo.right() < geo.right()) {
          rightmost = neighbors.at(f);
          rgeo = geo;
        }
      }
      neighbors.at(f)->addNeighbors(neighbors);
    }
    // do sticking
    int power = e->modifiers()&Qt::ControlModifier?8:4;
    bool moved = false;
    int x0 = newPos.x(), y0 = newPos.y(), wdt = width(), h = height();
    foreach (QWidget *w, QApplication::topLevelWidgets()) {
      if (w != this && !w->isHidden() && w->inherits("FloatingWindow")) {
        FloatingWindow *its = static_cast<FloatingWindow *>(w);
        if (neighbors.contains(its)) continue;
        if (DoSticking(x0, y0, wdt, h, its->geometry(), power)) { moved = true; break; }
        if (rightmost) {
         QPoint p = this->pos();
         int rx0 = newPos.x()-p.x()+rgeo.left(), ry0 = newPos.y()-p.y()+rgeo.top();
         int ox = rx0, oy = ry0;
         int rwdt = rightmost->width(), rhgt = rightmost->height();
         if (DoSticking(rx0, ry0, rwdt, rhgt, its->geometry(), power)) {
           moved = true;
           x0 += rx0-ox; y0 += ry0-oy;
           break;
         }
        }
      }
    }
    if (!moved) { moved = DoDeskSticking(x0, y0, wdt, h, power); }
    if (!moved && rightmost) {
      QPoint p = this->pos();
      int rx0 = newPos.x()-p.x()+rgeo.left(), ry0 = newPos.y()-p.y()+rgeo.top();
      int ox = rx0, oy = ry0;
      int rwdt = rightmost->width(), rhgt = rightmost->height();
      moved = DoDeskSticking(rx0, ry0, rwdt, rhgt, power);
      if (moved) { x0 += rx0-ox; y0 += ry0-oy; }
    }
    if (moved) { newPos.setX(x0); newPos.setY(y0); }
    // move neighbors
    QPoint myPos = pos();
    int dx = newPos.x()-myPos.x();
    int dy = newPos.y()-myPos.y();
    foreach (FloatingWindow *w, neighbors) {
      QPoint p = w->pos();
      w->move(p.x()+dx, p.y()+dy);
    }
  }

  move(newPos.x(), newPos.y());
  //repaint();
  update();
}


void FloatingWindow::emitChangePosition () {
  emit positionChanged();
}


void FloatingWindow::addNeighbors (QList<FloatingWindow *> &list) {
  QRect myGeo = this->geometry();
  foreach (QWidget *w, QApplication::topLevelWidgets()) {
    if (w == this || w->isHidden() || !w->inherits("FloatingWindow")) continue;
    QRect geo = w->geometry();
    if (myGeo.right() == geo.left() && myGeo.top() == geo.top()) list.append(static_cast<FloatingWindow *>(w));
  }
}


void FloatingWindow::onStartMove () {
  if (mInitMousePos.isNull()) return;
  mMaybeMoving = false;
  mMoveTimer->stop();
  mMousePos = mInitMousePos;
  mInitMousePos = QPoint(0, 0);
  mIgnoreMouseClickRelease = true;
  grabMouse();
}


void FloatingWindow::saveFState (QDataStream &st) const {
  QPoint g = this->pos();
  st << (quint8)0; // version
  st << (int)(sizeof(int)*2); // data size
  int x = g.x(), y = g.y();
  st << x;
  st << y;
}


void FloatingWindow::restoreFState (QDataStream &st) {
  quint8 ver; int sz, x, y;
  st >> ver;
  st >> sz;
  if (ver == 0 && sz >= (int)(sizeof(int)*2)) {
    st >> x;
    st >> y;
    sz -= (sizeof(int)*2);
    move(QPoint(x, y));
  }
  // skip the rest
  if (sz > 0) st.skipRawData(sz);
}


///////////////////////////////////////////////////////////////////////////////
FloatWinManager::FloatWinManager (int titleBlinkTO, int msgBlinkTO, QObject *parent) :
  QObject(parent),
  mBTitleTO(titleBlinkTO), mBMsgTO(msgBlinkTO),
  mBTitlePhase(false), mBTitleTimer(0),
  mBMsgPhase(false), mBMsgTimer(0)
{
  mBTitleTimer = new QTimer(this);
  connect(mBTitleTimer, SIGNAL(timeout()), this, SLOT(btitleTick()));
  //
  mBMsgTimer = new QTimer(this);
  connect(mBMsgTimer, SIGNAL(timeout()), this, SLOT(bmsgTick()));
  //
}


FloatWinManager::~FloatWinManager () {
  clear();
}


void FloatWinManager::clear () {
  mMine.clear(); mBMsgWin.clear(); mBTitleStop.clear();
  foreach (FloatingWindow *w, mList) {
    w->mMan = 0;
    delete w;
  }
}


void FloatWinManager::startBTitleTimer () {
  if (!mBTitleTimer->isActive()) {
    mBTitleTimer->start(mBTitleTO);
    mBTitlePhase = true;
  }
}


void FloatWinManager::stopBTitleTimer () {
  if (mBTitleTimer->isActive()) {
    mBTitlePhase = false;
    mBTitleTimer->stop();
  }
}


void FloatWinManager::startBMsgTimer () {
  if (!mBMsgTimer->isActive()) {
    mBMsgTimer->start(mBMsgTO);
    mBMsgPhase = true;
  }
}


void FloatWinManager::stopBMsgTimer () {
  if (mBMsgTimer->isActive()) {
    mBMsgPhase = false;
    mBMsgTimer->stop();
  }
}


void FloatWinManager::setMsgBlink (FloatingWindow *w, bool doBlink) {
  if (mBMsgTO <= 0 || !w || !mMine.contains(w)) return;
  if (doBlink) {
    if (mBMsgWin.contains(w)) return;
    mBMsgWin << w;
    startBMsgTimer();
    w->mBMsgPhase = mBMsgPhase;
  } else {
    if (!mBMsgWin.contains(w)) return;
    mBMsgWin.remove(w);
    if (mBMsgWin.isEmpty()) stopBMsgTimer();
    w->mBMsgPhase = false;
  }
  w->discardBuf();
  w->update(); // redraw us
}


void FloatWinManager::setTitleBlink (FloatingWindow *w, bool doBlink) {
  if (mBTitleTO <= 0 || !w || !mMine.contains(w)) return;
  if (doBlink) {
    if (mBTitleStop.contains(w)) mBTitleStop.remove(w);
    uint to = QDateTime::currentDateTime().toTime_t()+6;
    mBTitleStop.insert(w, to);
    startBTitleTimer();
    w->mBTitlePhase = mBTitlePhase;
  } else {
    if (!mBTitleStop.contains(w)) return;
    mBTitleStop.remove(w);
    if (mBTitleStop.isEmpty()) stopBTitleTimer();
    w->mBTitlePhase = false;
  }
  w->discardBuf();
  w->update(); // redraw us
}


void FloatWinManager::bmsgTick () {
  if (!mBMsgWin.size()) {
    // nobody wants to blink anymore
    stopBMsgTimer();
    return;
  }
  mBMsgPhase = !mBMsgPhase;
  // iterate over all windows
  foreach (FloatingWindow *w, mBMsgWin) {
    w->mBMsgPhase = mBMsgPhase;
    w->discardBuf();
    w->update();
  }
}


void FloatWinManager::btitleTick () {
  mBTitlePhase = !mBTitlePhase;
  uint now = QDateTime::currentDateTime().toTime_t();
  // iterators, 'cause we can erase items
  QHash<FloatingWindow *, uint>::iterator i = mBTitleStop.begin();
  while (i != mBTitleStop.end()) {
    uint to = i.value();
    FloatingWindow *w = i.key();
    if (now >= to) {
      w->mBTitlePhase = false;
      i = mBTitleStop.erase(i);
    } else {
      w->mBTitlePhase = mBTitlePhase;
      ++i;
    }
    w->discardBuf();
    w->update();
  }
  if (mBTitleStop.isEmpty()) {
    // nobody wants to blink anymore
    stopBTitleTimer();
    return;
  }
}


FloatingWindow *FloatWinManager::change (const QString &uni, const QString &title, const QString &tip, PsycProto::Status status) {
  FloatingWindow *res = find(uni);
  if (res) {
    res->setTitle(title);
    res->setTip(tip);
    res->setStatus(status);
  }
  return res;
}


FloatingWindow *FloatWinManager::produce (const QString &uni, const QString &title, const QString &tip,
  PsycProto::Status status)
{
  //if (uni.isEmpty()) return 0;
  FloatingWindow *res = find(uni);
  if (!res) {
    res = new FloatingWindow(this, uni.toLower());
    connect(res, SIGNAL(positionChanged()), this, SLOT(doSave()));
    connect(res, SIGNAL(mouseRButtonClicked(int, int)), this, SLOT(onMouseRButtonClicked(int, int)));
    connect(res, SIGNAL(mouseDblClicked(int, int)), this, SLOT(onMouseDblClicked(int, int)));
    mMine.insert(res);
    mList.insert(uni, res);
  }
  change(uni, title, tip, status);
  res->show();
  return res;
}


void FloatWinManager::onMouseRButtonClicked (int x, int y) {
  emit mouseRButtonClicked(x, y, static_cast<FloatingWindow *>(sender())->mUni);
}


void FloatWinManager::onMouseDblClicked (int x, int y) {
  emit mouseDblClicked(x, y, static_cast<FloatingWindow *>(sender())->mUni);
}


void FloatWinManager::doSave () {
  saveFState();
}


bool FloatWinManager::contains (const QString &uni) {
  return find(uni) ? true : false;
}


FloatingWindow *FloatWinManager::find (const QString &uni) {
  if (!mList.contains(uni.toLower())) return 0;
  return mList.value(uni.toLower());
}


void FloatWinManager::remove (const QString &uni) {
  FloatingWindow *w = find(uni);
  if (w && w->mMan == this) {
    remove(w);
    w->close(); // it will remove itself
  }
}


void FloatWinManager::remove (FloatingWindow *w) {
  if (!w || !mMine.contains(w)) return;
  mMine.remove(w);
  mList.remove(w->mUni);
  mBMsgWin.remove(w);
  mBTitleStop.remove(w);
}


void FloatWinManager::newMessage (const QString &uni) {
  //qDebug() << "new msg:" << uni;
  setMsgBlink(find(uni), true);
}


void FloatWinManager::noMessages (const QString &uni) {
  //qDebug() << "NO msg:" << uni;
  setMsgBlink(find(uni), false);
}


void FloatWinManager::newStatus (const QString &uni, PsycProto::Status status) {
  FloatingWindow *w = find(uni);
  if (!w) return;
  if (w->mStatus != status) {
    w->setStatus(status);
    setTitleBlink(w, true);
  }
}


void FloatWinManager::newTip (const QString &uni, const QString &tip) {
  FloatingWindow *w = find(uni);
  if (!w) return;
  w->setTip(tip);
}


void FloatWinManager::newTitle (const QString &uni, const QString &title) {
  FloatingWindow *w = find(uni);
  if (!w) return;
  w->setTitle(title);
}


void FloatWinManager::offlineAll () {
  foreach (FloatingWindow *w, mList) w->setStatus(PsycProto::Offline);
}
