/*
 * ghotkeymanager.cpp - Class managing global shortcuts
 * Copyright (C) 2006  Maciej Niedzielski
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "ghotkeyman.h"

#include <QCoreApplication>

#include "ghotkeytrigger.h"


GHotKeyMan *GHotKeyMan::mInstance = 0;


/**
 * \brief Constructs new GHotKeyMan.
 */
GHotKeyMan::GHotKeyMan () : QObject(QCoreApplication::instance()) {
}


GHotKeyMan::~GHotKeyMan () {
  clear();
}


/**
 * \brief Returns the instance of GHotKeyMan.
 */
GHotKeyMan *GHotKeyMan::instance () {
  if (!mInstance) mInstance = new GHotKeyMan();
  return mInstance;
}


/**
 * \brief Connects a key sequence with a slot.
 * \param key, global shortcut to be connected
 * \param receiver, object which should receive the notification
 * \param slot, the SLOT() of the \a receiver which should be triggerd if the \a key is activated
 */
void GHotKeyMan::connect (const QKeySequence &key, QObject *receiver, const char *slot) {
  KeyTrigger *t = instance()->mTriggers[key];
  if (!t) {
    t = new KeyTrigger(key);
    instance()->mTriggers.insert(key, t);
  }
  QObject::connect(t, SIGNAL(activated()), receiver, slot);
}


/**
 * \brief Disonnects a key sequence from a slot.
 * \param key, global shortcut to be disconnected
 * \param receiver, object which \a slot is about to be disconnected
 * \param slot, the SLOT() of the \a receiver which should no longer be triggerd if the \a key is activated
 */
void GHotKeyMan::disconnect (const QKeySequence &key, QObject *receiver, const char *slot) {
  KeyTrigger *t = instance()->mTriggers[key];
  if (!t) return;
  QObject::disconnect(t, SIGNAL(activated()), receiver, slot);
  if (!t->isUsed()) delete instance()->mTriggers.take(key);
}


void GHotKeyMan::clear () {
  foreach (KeyTrigger* t, instance()->mTriggers) delete t;
  instance()->mTriggers.clear();
}
