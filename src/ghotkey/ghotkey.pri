HEADERS += $$PWD/ghotkeyman.h $$PWD/ghotkeytrigger.h
SOURCES += $$PWD/ghotkeyman.cpp
INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

unix:!mac {
  SOURCES += $$PWD/ghotkeyman_x11.cpp
}
win32: {
  SOURCES += $$PWD/ghotkeyman_win.cpp
}
