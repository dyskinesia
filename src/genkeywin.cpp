/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QDialog>

#include "winutils.h"

#include "genkeywin.h"


#define GKW_TEXT \
  "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n" \
  "<html>\n" \
  "<head>\n" \
  "<meta name=\"qrichtext\" content=\"1\" />\n" \
  "<style type=\"text/css\">p, li { white-space: pre-wrap; }</style>\n" \
  "</head>\n" \
  "<body>\n" \
  "<p align=\"center\">Generating OTR key for %1.</p>\n" \
  "<p align=\"center\"></p>\n" \
  "<p align=\"center\">This can take some time, please wait.</p>\n" \
  "<p align=\"center\"></p>\n" \
  "<p align=\"center\">Move your mouse, drink your coffee or just fap a little.</p>\n" \
  "</body>\n" \
  "</html>\n"


GenKeyWin::GenKeyWin (const QString &userName, QWidget *parent) : QDialog(parent) {
  setupUi(this);
  setAttribute(Qt::WA_QuitOnClose, false);
  setAttribute(Qt::WA_DeleteOnClose, false);
  centerWidget(this);
  lbText->setText(QString(GKW_TEXT).arg(userName));
}


GenKeyWin::~GenKeyWin () {
}
