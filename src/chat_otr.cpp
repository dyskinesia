#ifdef USE_OTR
# define C_FUNCTION  extern "C"
# define PROTO_NAME  "psyc"
/*
# define dlogf(...)
*/
///////////////////////////////////////////////////////////////////////////////
# include "dlogf.h"

static char *strToC (const QString &s) {
  //QTextCodec *tc = QTextCodec::codecForCStrings();
  QTextCodec *tc = QTextCodec::codecForLocale();
  QByteArray ba;
  if (tc) ba = tc->fromUnicode(s); else ba = s.toLatin1();
  char *res = (char *)malloc(ba.size()+8);
  strcpy(res, ba.constData());
  res[ba.size()] = '\0';
  return res;
}


/* frees fname */
static FILE *qxfopen (char *fname, bool readOnly) {
  QFile mQF(fname);
  free(fname);
  if (!mQF.open(readOnly ? QIODevice::ReadOnly : QIODevice::ReadWrite)) return 0;
  int hh = dup(mQF.handle());
  mQF.close();
  FILE *res = fdopen(hh, readOnly ? "rb" : "r+b");
  if (!res) close(hh);
  return res;
}



char *ChatForm::getOTRKeyFile (void) {
  QDir d;
  QString otp(settingsDBPath(mAccName));
  otp.append("otr/");
  d.mkdir(otp);
  otp.append("keys.otr");
  return strToC(otp);
}


char *ChatForm::getOTRFingerFile (void) {
  QDir d;
  QString otp(settingsDBPath(mAccName));
  otp.append("otr/");
  d.mkdir(otp);
  otp.append("fingers.otr");
  return strToC(otp);
}


char *ChatForm::getOTRInsTagFile (void) {
  QDir d;
  QString otp(settingsDBPath(mAccName));
  otp.append("otr/");
  d.mkdir(otp);
  otp.append("instags.otr");
  return strToC(otp);
}


/* Return a pointer to a newly-allocated OTR query message, customized
 * with our name.  The caller should free() the result when he's done
 * with it. */
static char *otrDefaultQueryMsg (const char *ourname, OtrlPolicy policy) {
  char *msg = otrl_proto_default_query_msg(ourname, policy);
  char *e = strchr(msg, '\n');
  if (!e) return msg;
  char *res = (char *)malloc(8192); //???
  *e = '\0';
  sprintf(res,
    "%s\n%s has requested an Off-the-Record private conversation.\n"
     "However, you do not have a plugin to support that.\n"
     "See http://otr.cypherpunks.ca/ for more information.",
    msg, ourname
  );
  free(msg);
  dlogf("otrDefaultQueryMsg:\n%s\n====", res);
  return res;
}


///////////////////////////////////////////////////////////////////////////////
#define GET_FORM  ChatForm *mForm = static_cast<ChatForm *>(opdata)


// return the OTR policy for the given context
C_FUNCTION OtrlPolicy otrPolicyCB (void *opdata, ConnContext *context) {
  Q_UNUSED(opdata)
  Q_UNUSED(context)
  //!dlogf("otrPolicyCB()\n");
  return OTRL_POLICY_DEFAULT;
}


///////////////////////////////////////////////////////////////////////////////
class GenKeyThread : public QThread {
public:
  void run ();

  ChatForm *mForm;
  const char *accName;
  //const char *protoName;
  void *keyp;

  bool mError;
};
void GenKeyThread::run () {
  /*
  mError = true;
  FILE *fl = qxfopen(mForm->getOTRKeyFile(), false);
  if (fl) {
    mError = otrl_privkey_generate_FILEp(mForm->mOTRState, fl, accName, protoName) != 0;
    fclose(fl);
  }
  */
  if (otrl_privkey_generate_calculate(keyp)) {
    dlogf("ERROR: can't generate new private key for '%s'!\n", accName);
    mError = true;
  } else {
    mError = false;
  }
}


void ChatForm::genOTRKey (const char *accName, const char *protoName) {
  void *keyp;
  if (otrl_privkey_generate_start(mOTRState, accName, protoName, &keyp)) {
    dlogf("ERROR: can't generate new private key for '%s'!\n", accName);
    return;
  }
  GenKeyWin *gkw = new GenKeyWin(accName, this);
  gkw->setModal(true);
  gkw->show();
  //!dlogf("dialog shown");
  QEventLoop eloop;
  GenKeyThread gtrd;
  gtrd.mForm = this;
  gtrd.accName = accName;
  //gtrd.protoName = protoName;
  gtrd.keyp = keyp;
  connect(&gtrd, SIGNAL(finished()), &eloop, SLOT(quit()));
  gtrd.start();
  //!dlogf("thread started");
  //gtrd.wait();
  eloop.exec();
  //!dlogf("all done");
  //TODO: check for error!
  if (!gtrd.mError) {
    FILE *fl = qxfopen(getOTRKeyFile(), false);
    if (fl) {
      if (otrl_privkey_generate_finish_FILEp(mOTRState, keyp, fl)) {
        dlogf("ERROR: can't generate new private key for '%s'!\n", accName);
      }
      fclose(fl);
    }
  }
  delete gkw;
}


// create a private key for the given accountname/protocol if desired
C_FUNCTION void otrCreateKeyCB (void *opdata, const char *accName, const char *protoName) {
  Q_UNUSED(opdata)
  Q_UNUSED(accName)
  Q_UNUSED(protoName)
  GET_FORM;
  dlogf("otrCreateKeyCB(): acc:[%s]; proto:[%s]\n", accName, protoName);
  //!dlogf("  otrl_privkey_generate()...");
  mForm->genOTRKey(accName, protoName);
  //otrl_privkey_generate(mOTRState, mForm->getOTRKeyFile(), accName, protoName);
  //!dlogf("done\n");
}


///////////////////////////////////////////////////////////////////////////////
/* Report whether you think the given user is online.  Return 1 if
 * you think he is, 0 if you think he isn't, -1 if you're not sure.
 *
 * If you return 1, messages such as heartbeats or other
 * notifications may be sent to the user, which could result in "not
 * logged in" errors if you're wrong. */
C_FUNCTION int otrIsLoggedInCB (void *opdata, const char *accName, const char *protoName, const char *recipient) {
  Q_UNUSED(opdata)
  Q_UNUSED(accName)
  Q_UNUSED(protoName)
  Q_UNUSED(recipient)
  GET_FORM;
  //!dlogf("otrIsLoggedInCB(): acc:[%s]; proto:[%s]; rcp:[%s]\n", accName, protoName, recipient);
  if (!mForm || !recipient || !recipient[0]) return 0;
  PsycContact *cc = mForm->findContact(recipient);
  if (!cc || cc->isPlace()) return 0;
  switch (cc->status()) {
    case PsycProto::Internal:
    case PsycProto::Offline:
      return 0;
    default: ;
  }
  //!dlogf("  online");
  return 1;
}


///////////////////////////////////////////////////////////////////////////////
// send the given IM to the given recipient from the given accountname/protocol
C_FUNCTION void otrSendMessageCB (void *opdata, const char *accName, const char *protoName,
  const char *recipient, const char *message)
{
  Q_UNUSED(opdata)
  Q_UNUSED(accName)
  Q_UNUSED(protoName)
  Q_UNUSED(recipient)
  Q_UNUSED(message)
  GET_FORM;
  dlogf("otrSendMessageCB(): acc:[%s]; proto:[%s]; rcp:[%s]\nmsg:[%s]\n=========", accName, protoName, recipient, message);
  //!dlogf("otrSendMessageCB(): acc:[%s]; proto:[%s]; rcp:[%s]\n", accName, protoName, recipient);
  if (!mForm->proto()) return;
  mForm->proto()->sendMessage(QString(recipient), QString::fromUtf8(message), QString());
}


///////////////////////////////////////////////////////////////////////////////
// When the list of ConnContexts changes (including a change in state), this is called so the UI can be updated
C_FUNCTION void otrUpdateContextListCB (void *opdata) {
  Q_UNUSED(opdata)
  ChatForm *mForm = static_cast<ChatForm *>(opdata);
  //!dlogf("otrUpdateContextListCB()\n");
  ConnContext *context = mForm->mOTRState->context_root;
  while (context) {
    ConnContext *next = context->next;
    PsycContact *cc = mForm->findContact(context->username);
    if (cc) {
      cc->setOTRVerified(context->active_fingerprint && context->active_fingerprint->trust &&
        (!strcmp(context->active_fingerprint->trust, "verified") || !strcmp(context->active_fingerprint->trust, "smp"))
      );
      if (cc->isOTRActive() != (context->msgstate == OTRL_MSGSTATE_ENCRYPTED)) {
        cc->setOTRActive(context->msgstate == OTRL_MSGSTATE_ENCRYPTED);
        mForm->redrawContact(cc);
      }
    }
    context = next;
  }
}


///////////////////////////////////////////////////////////////////////////////
/* Return a newly-allocated string containing a human-friendly name
 * for the given protocol id */
/*
C_FUNCTION const char *otrProtoNameCB (void *opdata, const char *protoName) {
  Q_UNUSED(opdata)
  Q_UNUSED(protoName)
  //ChatForm *mForm = static_cast<ChatForm *>(opdata);
  //!dlogf("otrProtoNameCB(): proto:[%s]\n", protoName);
  return protoName;
}
*/


/* Deallocate a string allocated by protocol_name */
/*
C_FUNCTION void otrProtoNameFreeCB (void *opdata, const char *protoName) {
  Q_UNUSED(opdata)
  Q_UNUSED(protoName)
  //ChatForm *mForm = static_cast<ChatForm *>(opdata);
  //!dlogf("otrProtoNameFreeCB(): proto:[%s]\n", protoName);
}
*/


///////////////////////////////////////////////////////////////////////////////
/* A new fingerprint for the given user has been received. */
C_FUNCTION void otrNewFingerCB (void *opdata, OtrlUserState us, const char *accName, const char *protoName,
  const char *userName, unsigned char fingerprint[20])
{
  Q_UNUSED(opdata)
  Q_UNUSED(us)
  Q_UNUSED(accName)
  Q_UNUSED(protoName)
  Q_UNUSED(userName)
  Q_UNUSED(fingerprint)
  ChatForm *mForm = static_cast<ChatForm *>(opdata);
  //!dlogf("otrNewFingerCB(): acc:[%s]; proto:[%s]; user:[%s]\n", accName, protoName, userName);
  ConnContext *context = otrl_context_find(us, userName, accName, protoName, OTRL_INSTAG_BEST, TRUE, 0, NULL/*add_appdata*/, NULL/*opdata*/);
  char hash[OTRL_PRIVKEY_FPRINT_HUMAN_LEN];
  // don't add unknown fingerprint
  Fingerprint *fp = otrl_context_find_fingerprint(context, fingerprint, 0/*FALSE*/, NULL);
  if (fp) {
    otrl_privkey_hash_to_human(hash, fp->fingerprint);
    dlogf(" known %strusted fingerprint: %s\n", (otrl_context_is_fingerprint_trusted(fp) ? "" : "un"), hash);
    // trust it (user query?)
    otrl_context_set_trust(fp, "verified");
  } else {
    otrl_privkey_hash_to_human(hash, fingerprint);
    dlogf(" new fingerprint: %s\n", hash);
    QMessageBox::StandardButton qres =
      QMessageBox::question(mForm, "Dyskinesia: new OTR fingerprint",
        QString("New OTR fingerpring received from %1.\n%2\nDo you trust it?").arg(userName).arg(hash),
        QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    if (qres == QMessageBox::Yes) {
      otrl_context_set_trust(fp, "verified");
    } else {
      otrl_context_set_trust(fp, "unknown");
    }
  }
}


///////////////////////////////////////////////////////////////////////////////
/* The list of known fingerprints has changed.  Write them to disk. */
C_FUNCTION void otrWriteFingersCB (void *opdata) {
  GET_FORM;
  //!dlogf("otrWriteFingersCB()\n");
  FILE *fl = qxfopen(mForm->getOTRFingerFile(), false);
  if (fl) {
    otrl_privkey_write_fingerprints_FILEp(mForm->mOTRState, fl);
    fclose(fl);
  }
}


///////////////////////////////////////////////////////////////////////////////
/* A ConnContext has entered a secure state. */
C_FUNCTION void otrGoneSecureCB (void *opdata, ConnContext *context) {
  Q_UNUSED(opdata)
  Q_UNUSED(context)
  GET_FORM;
  dlogf("otrGoneSecureCB()\n");
  bool trusted = (context->active_fingerprint && context->active_fingerprint->trust &&
    (!strcmp(context->active_fingerprint->trust, "verified") || !strcmp(context->active_fingerprint->trust, "smp"))
  );
  PsycContact *cc = mForm->findContact(context->username);
  //!k8:if (cc) cc->setOTRActive(true);
  if (trusted) {
    dlogf("  Beginning OTR encrypted session with [%s]", context->username);
    if (cc) cc->setOTRVerified(true);
    // opdata is hContact
    //SetEncryptionStatus((HANDLE)opdata, true);
  } else {
    //!!CloseHandle((HANDLE)_beginthreadex(0, 0, verify_fp_thread, context, 0, 0));
    dlogf("  Beginning OTR encrypted session with [%s] (NOT VERIFIED)", context->username);
    if (cc) cc->setOTRVerified(false);
    // opdata is hContact
    //SetEncryptionStatus((HANDLE)opdata, true);
  }
  //!k8:if (cc) mForm->redrawContact(cc);
}


///////////////////////////////////////////////////////////////////////////////
// a ConnContext has left a secure state
C_FUNCTION void otrGoneInsecureCB (void *opdata, ConnContext *context) {
  Q_UNUSED(opdata)
  Q_UNUSED(context)
  ChatForm *mForm = static_cast<ChatForm *>(opdata);
  //dlogf("otrGoneInsecureCB()\n");
  dlogf("  OTR encrypted session with [%s] has ended", context->username);
/*
  PsycContact *cc = mForm->findContact(context->username);
  if (cc && cc->isOTRActive()) {
    cc->setOTRActive(false);
    mForm->redrawContact(cc);
  }
*/
  mForm->otrDisconnect(context->username);
  // opdata is hContact
  //SetEncryptionStatus((HANDLE)opdata, false);
}


///////////////////////////////////////////////////////////////////////////////
/* We have completed an authentication, using the D-H keys we
 * already knew.  isReply indicates whether we initiated the AKE. */
C_FUNCTION void otrStillSecureCB (void *opdata, ConnContext *context, int isReply) {
  Q_UNUSED(opdata)
  Q_UNUSED(context)
  Q_UNUSED(isReply)
  //ChatForm *mForm = static_cast<ChatForm *>(opdata);
/*
  dlogf("otrStillSecureCB()\n");
  if (isReply) {
    dlogf("  OTR encrypted session with [%s] is being continued", context->username);
  }
*/
/*
  PsycContact *cc = mForm->findContact(context->username);
  if (cc && !cc->isOTRActive()) {
    cc->setOTRActive(true);
    mForm->redrawContact(cc);
  }
*/
  // opdata is hContact
  //SetEncryptionStatus((HANDLE)opdata, true);
}


///////////////////////////////////////////////////////////////////////////////
C_FUNCTION int otrMaxMessageSizeCB (void *opdata, ConnContext *context) {
  //ChatForm *mForm = static_cast<ChatForm *>(opdata);
  Q_UNUSED(opdata)
  Q_UNUSED(context)
  //dlogf("max_message_size()\n");
  return 65530;
}


/*
///////////////////////////////////////////////////////////////////////////////
C_FUNCTION const char *otrAccountNameCB (void *opdata, const char *accName, const char *protoName) {
  ChatForm *mForm = static_cast<ChatForm *>(opdata);
  dlogf("account_name(): acc:[%s]; proto:[%s]\n", accName, protoName);
  return ACC_NAME;
}

///////////////////////////////////////////////////////////////////////////////
C_FUNCTION void otrAccountNameFreeCB(void *opdata, const char *accName) {
  ChatForm *mForm = static_cast<ChatForm *>(opdata);
  dlogf("account_name_free(): acc:[%s]\n", accName);
}
*/


///////////////////////////////////////////////////////////////////////////////
C_FUNCTION const char *otrErrorMessageCB (void *opdata, ConnContext *context, OtrlErrorCode err_code) {
  Q_UNUSED(opdata)
  Q_UNUSED(context)
  Q_UNUSED(err_code)
  switch (err_code) {
    case OTRL_ERRCODE_ENCRYPTION_ERROR: return "ENCRYPTION_ERROR";
    case OTRL_ERRCODE_MSG_NOT_IN_PRIVATE: return "MSG_NOT_IN_PRIVATE";
    case OTRL_ERRCODE_MSG_UNREADABLE: return "MSG_UNREADABLE";
    case OTRL_ERRCODE_MSG_MALFORMED: return "MSG_MALFORMED";
    default: return "UNKNOWN";
  }
}


C_FUNCTION void otrErrorMessageFreeCB (void *opdata, const char *err_msg) {
  Q_UNUSED(opdata)
  Q_UNUSED(err_msg)
}


///////////////////////////////////////////////////////////////////////////////
C_FUNCTION void otrHandleSMPEvent (void *opdata, OtrlSMPEvent smp_event,
  ConnContext *context, unsigned short progress_percent, char *question);


C_FUNCTION void otrTimerControlCB (void *opdata, unsigned int interval) {
  dlogf("otrTimerControlCB: interval=%u", interval);
  ChatForm *frm = (ChatForm *)opdata;
  frm->restartOTRTimer(interval);
}


///////////////////////////////////////////////////////////////////////////////
C_FUNCTION void otrHandleMsgEvent (void *opdata, OtrlMessageEvent msg_event, ConnContext *context,
  const char *message, gcry_error_t err)
{
  Q_UNUSED(opdata)
  Q_UNUSED(msg_event)
  Q_UNUSED(context)
  Q_UNUSED(message)
  Q_UNUSED(err)
  switch (msg_event) {
    case OTRL_MSGEVENT_NONE:
      dlogf("OTRL_MSGEVENT_NONE\nNo event");
      break;
    case OTRL_MSGEVENT_ENCRYPTION_REQUIRED:
      dlogf("OTRL_MSGEVENT_ENCRYPTION_REQUIRED\nOur policy requires encryption but we are trying to send an unencrypted message out.");
      break;
    case OTRL_MSGEVENT_ENCRYPTION_ERROR:
      dlogf("OTRL_MSGEVENT_ENCRYPTION_ERROR\nAn error occured while encrypting a message and the message was not sent.");
      break;
    case OTRL_MSGEVENT_CONNECTION_ENDED:
      dlogf("OTRL_MSGEVENT_CONNECTION_ENDED\nMessage has not been sent because our buddy has ended the private conversation. We should either close the connection, or refresh it.");
      break;
    case OTRL_MSGEVENT_SETUP_ERROR:
      dlogf("OTRL_MSGEVENT_SETUP_ERROR\nA private conversation could not be set up. A gcry_error_t will be passed.");
      break;
    case OTRL_MSGEVENT_MSG_REFLECTED:
      dlogf("OTRL_MSGEVENT_MSG_REFLECTED\nReceived our own OTR messages.");
      break;
    case OTRL_MSGEVENT_MSG_RESENT:
      dlogf("OTRL_MSGEVENT_MSG_RESENT\nThe previous message was resent.");
      break;
    case OTRL_MSGEVENT_RCVDMSG_NOT_IN_PRIVATE:
      dlogf("OTRL_MSGEVENT_RCVDMSG_NOT_IN_PRIVATE\nReceived an encrypted message but cannot read it because no private connection is established yet.");
      break;
    case OTRL_MSGEVENT_RCVDMSG_UNREADABLE:
      dlogf("OTRL_MSGEVENT_RCVDMSG_UNREADABLE\nCannot read the received message.");
      break;
    case OTRL_MSGEVENT_RCVDMSG_MALFORMED:
      dlogf("OTRL_MSGEVENT_RCVDMSG_MALFORMED\nThe message received contains malformed data.");
      break;
    case OTRL_MSGEVENT_LOG_HEARTBEAT_RCVD:
      dlogf("OTRL_MSGEVENT_LOG_HEARTBEAT_RCVD\nReceived a heartbeat.");
      break;
    case OTRL_MSGEVENT_LOG_HEARTBEAT_SENT:
      dlogf("OTRL_MSGEVENT_LOG_HEARTBEAT_SENT\nSent a heartbeat.");
      break;
    case OTRL_MSGEVENT_RCVDMSG_GENERAL_ERR:
      dlogf("OTRL_MSGEVENT_RCVDMSG_GENERAL_ERR\nReceived a general OTR error. The argument 'message' will also be passed and it will contain the OTR error message."
        "\nmessage=%s\n====", message);
      break;
    case OTRL_MSGEVENT_RCVDMSG_UNENCRYPTED:
      dlogf("OTRL_MSGEVENT_RCVDMSG_UNENCRYPTED\nReceived an unencrypted message. The argument 'message' will also be passed and it will contain the plaintext message."
        "\nmessage=%s\n====", message);
      break;
    case OTRL_MSGEVENT_RCVDMSG_UNRECOGNIZED:
      dlogf("OTRL_MSGEVENT_RCVDMSG_UNRECOGNIZED\nCannot recognize the type of OTR message received.");
      break;
    case OTRL_MSGEVENT_RCVDMSG_FOR_OTHER_INSTANCE:
      dlogf("OTRL_MSGEVENT_RCVDMSG_FOR_OTHER_INSTANCE\nReceived and discarded a message intended for another instance.");
      break;
  }
}


////////////////////////////////////////////////////////////////////////////////
C_FUNCTION void otrCreateInsTagCB (void *opdata, const char *accountname, const char *protocol) {
  dlogf("otrCreateInsTagCB: acc=[%s]; proto=[%s]\n", accountname, protocol);
  GET_FORM;
  FILE *fl = qxfopen(mForm->getOTRInsTagFile(), false);
  if (fl) {
    if (otrl_instag_generate_FILEp(mForm->mOTRState, fl, accountname, protocol)) {
      dlogf("ERROR: can't generate new private key for '%s'!\n", accountname);
    }
    fclose(fl);
  }
}


///////////////////////////////////////////////////////////////////////////////
static OtrlMessageAppOps ops = {
  otrPolicyCB, /* Return the OTR policy for the given context. */
  otrCreateKeyCB, /* Create a private key for the given accountname/protocol if desired. */
    /* Report whether you think the given user is online.  Return 1 if
     * you think he is, 0 if you think he isn't, -1 if you're not sure.
     *
     * If you return 1, messages such as heartbeats or other
     * notifications may be sent to the user, which could result in "not
     * logged in" errors if you're wrong. */
  otrIsLoggedInCB,
    /* Send the given IM to the given recipient from the given
     * accountname/protocol. */
  otrSendMessageCB,
    /* When the list of ConnContexts changes (including a change in
     * state), this is called so the UI can be updated. */
  otrUpdateContextListCB, // can be NULL
    /* A new fingerprint for the given user has been received. */
  otrNewFingerCB,
    /* The list of known fingerprints has changed.  Write them to disk. */
  otrWriteFingersCB,
    /* A ConnContext has entered a secure state. */
  otrGoneSecureCB,
    /* A ConnContext has left a secure state. */
  otrGoneInsecureCB,
    /* We have completed an authentication, using the D-H keys we
     * already knew.  is_reply indicates whether we initiated the AKE. */
  otrStillSecureCB,
    /* Find the maximum message size supported by this protocol. */
  otrMaxMessageSizeCB,
    /* Return a newly allocated string containing a human-friendly
     * representation for the given account */
  NULL,  /* account_name */
    /* Deallocate a string returned by account_name */
  NULL,  /* account_name_free */
    /* We received a request from the buddy to use the current "extra"
     * symmetric key.  The key will be passed in symkey, of length
     * OTRL_EXTRAKEY_BYTES.  The requested use, as well as use-specific
     * data will be passed so that the applications can communicate other
     * information (some id for the data transfer, for example). */
  NULL,
    /* Return a string according to the error event. This string will then
     * be concatenated to an OTR header to produce an OTR protocol error
     * message. The following are the possible error events:
     * - OTRL_ERRCODE_ENCRYPTION_ERROR
     *    occured while encrypting a message
     * - OTRL_ERRCODE_MSG_NOT_IN_PRIVATE
     *    sent encrypted message to somebody who is not in a mutual OTR session
     * - OTRL_ERRCODE_MSG_UNREADABLE
     *    sent an unreadable encrypted message
     * - OTRL_ERRCODE_MSG_MALFORMED
     *    message sent is malformed */
  otrErrorMessageCB,
    /* Deallocate a string returned by otr_error_message */
  otrErrorMessageFreeCB, // can be NULL
    /* Return a string that will be prefixed to any resent message. If this
     * function is not provided by the application then the default prefix,
     * "[resent]", will be used.
     * */
  //const char *(*resent_msg_prefix)(void *opdata, ConnContext *context);
  NULL,
    /* Deallocate a string returned by resent_msg_prefix */
  //void (*resent_msg_prefix_free)(void *opdata, const char *prefix);
  NULL,
    /* Update the authentication UI with respect to SMP events
     * These are the possible events:
     * - OTRL_SMPEVENT_ASK_FOR_SECRET
     *      prompt the user to enter a shared secret. The sender application
     *      should call otrl_message_initiate_smp, passing NULL as the question.
     *      When the receiver application resumes the SM protocol by calling
     *      otrl_message_respond_smp with the secret answer.
     * - OTRL_SMPEVENT_ASK_FOR_ANSWER
     *      (same as OTRL_SMPEVENT_ASK_FOR_SECRET but sender calls
     *      otrl_message_initiate_smp_q instead)
     * - OTRL_SMPEVENT_CHEATED
     *      abort the current auth and update the auth progress dialog
     *      with progress_percent. otrl_message_abort_smp should be called to
     *      stop the SM protocol.
     * - OTRL_SMPEVENT_INPROGRESS and
     *   OTRL_SMPEVENT_SUCCESS    and
     *   OTRL_SMPEVENT_FAILURE    and
     *   OTRL_SMPEVENT_ABORT
     *      update the auth progress dialog with progress_percent
     * - OTRL_SMPEVENT_ERROR
     *      (same as OTRL_SMPEVENT_CHEATED)
     * */
  otrHandleSMPEvent,
    /* Handle and send the appropriate message(s) to the sender/recipient
     * depending on the message events. All the events only require an opdata,
     * the event, and the context. The message and err will be NULL except for
     * some events (see below). The possible events are:
     * - OTRL_MSGEVENT_ENCRYPTION_REQUIRED
     *      Our policy requires encryption but we are trying to send
     *      an unencrypted message out.
     * - OTRL_MSGEVENT_ENCRYPTION_ERROR
     *      An error occured while encrypting a message and the message
     *      was not sent.
     * - OTRL_MSGEVENT_CONNECTION_ENDED
     *      Message has not been sent because our buddy has ended the
     *      private conversation. We should either close the connection,
     *      or refresh it.
     * - OTRL_MSGEVENT_SETUP_ERROR
     *      A private conversation could not be set up. A gcry_error_t
     *      will be passed.
     * - OTRL_MSGEVENT_MSG_REFLECTED
     *      Received our own OTR messages.
     * - OTRL_MSGEVENT_MSG_RESENT
     *      The previous message was resent.
     * - OTRL_MSGEVENT_RCVDMSG_NOT_IN_PRIVATE
     *      Received an encrypted message but cannot read
     *      it because no private connection is established yet.
     * - OTRL_MSGEVENT_RCVDMSG_UNREADABLE
     *      Cannot read the received message.
     * - OTRL_MSGEVENT_RCVDMSG_MALFORMED
     *      The message received contains malformed data.
     * - OTRL_MSGEVENT_LOG_HEARTBEAT_RCVD
     *      Received a heartbeat.
     * - OTRL_MSGEVENT_LOG_HEARTBEAT_SENT
     *      Sent a heartbeat.
     * - OTRL_MSGEVENT_RCVDMSG_GENERAL_ERR
     *      Received a general OTR error. The argument 'message' will
     *      also be passed and it will contain the OTR error message.
     * - OTRL_MSGEVENT_RCVDMSG_UNENCRYPTED
     *      Received an unencrypted message. The argument 'message' will
     *      also be passed and it will contain the plaintext message.
     * - OTRL_MSGEVENT_RCVDMSG_UNRECOGNIZED
     *      Cannot recognize the type of OTR message received.
     * - OTRL_MSGEVENT_RCVDMSG_FOR_OTHER_INSTANCE
     *      Received and discarded a message intended for another instance. */
  otrHandleMsgEvent,
     /* Create a instance tag for the given accountname/protocol if
      * desired. */
  otrCreateInsTagCB,
     /* Called immediately before a data message is encrypted, and after a data
      * message is decrypted. The OtrlConvertType parameter has the value
      * OTRL_CONVERT_SENDING or OTRL_CONVERT_RECEIVING to differentiate these
      * cases. */
  //void (*convert_msg)(void *opdata, ConnContext *context, OtrlConvertType convert_type, char ** dest, const char *src);
  NULL,
     /* Deallocate a string returned by convert_msg. */
  //void (*convert_free)(void *opdata, ConnContext *context, char *dest);
  NULL,
    /* When timer_control is called, turn off any existing periodic
     * timer.
     *
     * Additionally, if interval > 0, set a new periodic timer
     * to go off every interval seconds.  When that timer fires, you
     * must call otrl_message_poll(userstate, uiops, uiopdata); from the
     * main libotr thread.
     *
     * The timing does not have to be exact; this timer is used to
     * provide forward secrecy by cleaning up stale private state that
     * may otherwise stick around in memory.  Note that the
     * timer_control callback may be invoked from otrl_message_poll
     * itself, possibly to indicate that interval == 0 (that is, that
     * there's no more periodic work to be done at this time).
     *
     * If you set this callback to NULL, then you must ensure that your
     * application calls otrl_message_poll(userstate, uiops, uiopdata);
     * from the main libotr thread every definterval seconds (where
     * definterval can be obtained by calling
     * definterval = otrl_message_poll_get_default_interval(userstate);
     * right after creating the userstate).  The advantage of
     * implementing the timer_control callback is that the timer can be
     * turned on by libotr only when it's needed.
     *
     * It is not a problem (except for a minor performance hit) to call
     * otrl_message_poll more often than requested, whether
     * timer_control is implemented or not.
     *
     * If you fail to implement the timer_control callback, and also
     * fail to periodically call otrl_message_poll, then you open your
     * users to a possible forward secrecy violation: an attacker that
     * compromises the user's computer may be able to decrypt a handful
     * of long-past messages (the first messages of an OTR
     * conversation).
     */
  otrTimerControlCB,
};


///////////////////////////////////////////////////////////////////////////////
C_FUNCTION void otrHandleSMPEvent (void *opdata, OtrlSMPEvent smp_event,
  ConnContext *context, unsigned short progress_percent, char *question)
{
  Q_UNUSED(context)
  Q_UNUSED(progress_percent)
  ChatForm *frm = (ChatForm *)opdata;
  dlogf("otrHandleSMPEvent: event=%u; user=[%s]; account=[%s]; proto=[%s]", (unsigned)smp_event, context->username, context->accountname, context->protocol);
  if (smp_event == OTRL_SMPEVENT_ASK_FOR_SECRET) {
    // need UNI here
    dlogf("otrHandleSMPEvent: ask_for_secret; user=[%s]; account=[%s]; proto=[%s]", context->username, context->accountname, context->protocol);
    bool ok = false;
    QString reply = QInputDialog::getText(frm, "OTR SMP query",
      QString("OTR SMP query from %1:\nsecret question:").arg(context->username),
      QLineEdit::Normal, QString(), &ok);
    if (!ok || reply.isEmpty()) {
      otrl_message_abort_smp(frm->mOTRState, &ops, opdata, context);
    } else {
      QByteArray ba(reply.toUtf8());
      otrl_message_respond_smp(frm->mOTRState, &ops, opdata, context, (const unsigned char *)ba.constData(), ba.size());
    }
    return;
  }
  if (smp_event == OTRL_SMPEVENT_ASK_FOR_ANSWER) {
    bool ok = false;
    QString reply = QInputDialog::getText(frm, "OTR SMP query",
      QString("OTR SMP query from %1:\nsecret question is: %2").arg(context->username).arg(question),
      QLineEdit::Normal, QString(), &ok);
    if (!ok || reply.isEmpty()) {
      otrl_message_abort_smp(frm->mOTRState, &ops, opdata, context);
    } else {
      QByteArray ba(reply.toUtf8());
      otrl_message_respond_smp(frm->mOTRState, &ops, opdata, context, (const unsigned char *)ba.constData(), ba.size());
    }
    return;
  }
  if (smp_event == OTRL_SMPEVENT_CHEATED /*|| smp_event == OTRL_SMPEVENT_ERROR*/) {
    dlogf(" ** protocol violated, aborting");
    otrl_message_abort_smp(frm->mOTRState, &ops, opdata, context);
    otrl_sm_state_free(context->smstate);
    return;
  }
  if (smp_event == OTRL_SMPEVENT_SUCCESS) {
    if (context->smstate->received_question) {
      dlogf(" ** correct answer, you are trusted");
    } else {
      dlogf(" ** secrets proved equal, fingerprint trusted");
    }
    otrl_sm_state_free(context->smstate);
    return;
  }
  if (smp_event == OTRL_SMPEVENT_FAILURE) {
    if (context->smstate->received_question) {
      dlogf(" ** wrong answer, you are not trusted");
    } else {
      dlogf(" ** secrets did not match, fingerprint not trusted");
    }
    otrl_sm_state_free(context->smstate);
    return;
  }
  if (smp_event == OTRL_SMPEVENT_FAILURE) {
    dlogf(" ** received abort");
    otrl_sm_state_free(context->smstate);
    return;
  }
  if (smp_event == OTRL_SMPEVENT_ERROR) {
    dlogf(" ** protocol error, aborting");
    otrl_message_abort_smp(frm->mOTRState, &ops, opdata, context);
    otrl_sm_state_free(context->smstate);
    return;
  }
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::otrSendMessage (const QString &auni, const QString &txt, const QString &act) {
  QString uni(auni.toLower());
  PsycContact *cc = findContact(uni);
  if (!cc || cc->isPlace() || !cc->isOTRActive() || txt.startsWith("?OTR")) {
    // internal OTR message or room message
    mProto->sendMessage(uni, txt, act);
  } else {
    // not OTR message, encrypt it
    gcry_error_t err;
    char *newMsg = 0;
    //ConnContext *context;
    QByteArray me(mProto->uni().toLower().toUtf8());
    QByteArray to(uni.toUtf8());
    QByteArray msg(txt.toUtf8());
    err = otrl_message_sending(mOTRState, &ops, (void *)this, me, PROTO_NAME, to,
      OTRL_INSTAG_BEST, // instag
      msg,
      NULL, // no additional TLVs
      &newMsg,
      /*OTRL_FRAGMENT_SEND_SKIP*/OTRL_FRAGMENT_SEND_ALL, // fragmentation policy
      NULL, //&context,
      NULL, // add_appdata callback
      NULL // data
    );
    if (err) {
      dlogf("failure to encrypt message!\n");
      return;
    }
    if (!newMsg) {
      dlogf("WTF?! an empty OTR resulting message!\n");
      //mProto->sendMessage(uni, txt, act);
      return;
    }
    //QByteArray mx(newMsg);
    //mProto->sendMessage(uni, mx, act);
    /*
    ConnContext *context = otrl_context_find(mOTRState, to, me, PROTO_NAME, OTRL_INSTAG_BEST, FALSE, 0, 0, 0);
    if (context) {
      char *frag = NULL;
      err = otrl_message_fragment_and_send(&ops, (void *)this, context, newMsg, OTRL_FRAGMENT_SEND_ALL, &frag);
      if (frag) free(frag);
    } else {
      dlogf("WTF?! no context found for otrl_message_fragment_and_send()\n");
    }
    */
    otrl_message_free(newMsg);
  }
}


///////////////////////////////////////////////////////////////////////////////
QString ChatForm::otrGotMessage (const QString &auni, const QString &txt) {
  int isInternal;
  char *newMsg = NULL;
  OtrlTLV *tlvs = 0;

  QString uni(auni.toLower());
  QByteArray me(mProto->uni().toLower().toUtf8());
  QByteArray sender(uni.toUtf8());
  QByteArray msg(txt.toUtf8());

  ConnContext *context;
  isInternal = otrl_message_receiving(mOTRState, &ops, (void *)this, me, PROTO_NAME, sender, msg,
    &newMsg, &tlvs, &context, NULL, NULL);
  //ConnContext *context = otrl_context_find(mOTRState, sender, me, PROTO_NAME, OTRL_INSTAG_BEST, FALSE, 0, 0, 0);
  if (context) {
    dlogf("otrGotMessage: user=[%s]; account=[%s]; proto=[%s]", context->username, context->accountname, context->protocol);
  } else {
    dlogf("otrGotMessage: NO CONTEXT!");
  }

  /*
  bool disconnected = false;
  if (tlvs) {
    OtrlTLV *tlv = otrl_tlv_find(tlvs, OTRL_TLV_DISCONNECTED);
    if (tlv) disconnected = true;
    if (tlvs) otrl_tlv_free(tlvs);
  }
  */

  if (isInternal) {
    dlogf("internal message\n");
    if (newMsg) {
      dlogf("MSG:===\n%s\n===", newMsg);
      otrl_message_free(newMsg);
    } else {
      dlogf("ORIGMSG:===\n%s\n===", msg.constData());
    }
    /*
    if (disconnected) {
      dlogf("[%s] has terminated the OTR session\n", me.constData());
      otrDisconnect(uni);
    }
    */
    return QString();
  }

  // it may not be encrypted however (e.g. tagged plaintext with tags stripped)
  //!if (context && context->msgstate == OTRL_MSGSTATE_PLAINTEXT) dlogf("PLAIN TEXT!\n");

  QString res;
  if (!newMsg) {
    QByteArray ba(txt.toUtf8());
    dlogf("plain message: [%s]\n", ba.constData());
    res = txt;
  } else {
    res = QString::fromUtf8(newMsg);
    dlogf("decrypted message: [%s]\n", newMsg);
  }

  if (newMsg) otrl_message_free(newMsg);

  /*
  if (disconnected) {
    dlogf("[%s] has terminated the OTR session\n", me.constData());
    otrDisconnect(uni);
  }
  */

  return res;
}
///////////////////////////////////////////////////////////////////////////////
#endif


const QString ChatForm::otrGetFinger (const QString &aUNI) {
  Q_UNUSED(aUNI)
#ifdef USE_OTR
  PsycContact *cc = findContact(aUNI);
  if (!cc || cc->isPlace()) return QString();
  QByteArray me(mProto->uni().toLower().toUtf8());
  QByteArray usr(aUNI.toLower().toUtf8());
  ConnContext *context = otrl_context_find(mOTRState, usr, me, PROTO_NAME, OTRL_INSTAG_BEST, FALSE, 0, NULL, NULL);
  if (context && context->active_fingerprint) {
    char hash[OTRL_PRIVKEY_FPRINT_HUMAN_LEN];
    otrl_privkey_hash_to_human(hash, context->active_fingerprint->fingerprint);
    return QString(hash);
  } else {
    dlogf("no finger for [%s]", usr.constData());
  }
#endif
  return QString();
}


void ChatForm::otrSetTrust (const QString &aUNI, bool tflag) {
  Q_UNUSED(aUNI)
  Q_UNUSED(tflag)
#ifdef USE_OTR
  PsycContact *cc = findContact(aUNI);
  if (!cc || cc->isPlace()) return;
  QByteArray me(mProto->uni().toLower().toUtf8());
  QByteArray usr(aUNI.toLower().toUtf8());
  ConnContext *context = otrl_context_find(mOTRState, usr, me, PROTO_NAME, OTRL_INSTAG_BEST, FALSE, 0, NULL, NULL);
  if (context && context->active_fingerprint) {
    otrl_context_set_trust(context->active_fingerprint, tflag ? "verified" : "unknown");
  }
  if (cc->isOTRVerified() != tflag) {
    cc->setOTRVerified(tflag);
    redrawContact(cc);
  }
#endif
}


void ChatForm::otrForget (const QString &aUNI) {
  Q_UNUSED(aUNI)
#ifdef USE_OTR
  PsycContact *cc = findContact(aUNI);
  if (!cc || cc->isPlace()) return;
  QByteArray me(mProto->uni().toLower().toUtf8());
  QByteArray usr(aUNI.toLower().toUtf8());
  ConnContext *context = otrl_context_find(mOTRState, usr, me, PROTO_NAME, OTRL_INSTAG_BEST, FALSE, 0, NULL, NULL);
  if (context) {
    otrl_context_forget(context);
    cc->setOTRVerified(false);
    redrawContact(cc);
  }
#endif
}


void ChatForm::otrInitiateSMP (const QString &aUNI, const QString &secret, const QString &question) {
  Q_UNUSED(aUNI)
  Q_UNUSED(secret)
  Q_UNUSED(question)
#ifdef USE_OTR
  PsycContact *cc = findContact(aUNI);
  if (!cc || cc->isPlace() || !cc->isOTRActive()) return;
  //cc->setOTRVerified(false);
  //redrawContact(cc);
  QByteArray me(mProto->uni().toLower().toUtf8());
  QByteArray user(aUNI.toLower().toUtf8());
  ConnContext *context = otrl_context_find(mOTRState, user, me, PROTO_NAME, OTRL_INSTAG_BEST, FALSE, 0, 0, 0);
  if (context && !secret.isEmpty()) {
    otrSetTrust(aUNI, false);
    QByteArray ba(secret.toUtf8());
    if (!question.isEmpty()) {
      QByteArray ba1(question.toUtf8());
      otrl_message_initiate_smp_q(mOTRState, &ops, (void *)this, context, ba1.constData(), (const unsigned char *)ba.constData(), ba.size());
    } else {
      otrl_message_initiate_smp(mOTRState, &ops, (void *)this, context, (const unsigned char *)ba.constData(), ba.size());
    }
  }
#endif
}


void ChatForm::deactivateAllOTR () {
  foreach (PsycContact *cc, mContactList) {
    cc->setOTRVerified(false);
    if (cc->isOTRActive()) {
      cc->setOTRActive(false);
      redrawContact(cc);
    }
  }
}


void ChatForm::disconnectAllOTR () {
  deactivateAllOTR();
#ifdef USE_OTR
  if (mOTRState) {
    ConnContext *context = mOTRState->context_root;
    while (context) {
      ConnContext *next = context->next;
      if (context->msgstate == OTRL_MSGSTATE_ENCRYPTED && context->protocol_version > 1) {
        otrDisconnect(context->username);
        //otrl_message_disconnect(mOTRState, &ops, (void *)this, context->accountname, context->protocol, context->username);
      }
      context = next;
    }
  }
#endif
}


void ChatForm::otrDisconnect (const QString &uni) {
#ifdef USE_OTR
  QByteArray user(uni.toLower().toUtf8());
  QByteArray me(mProto->uni().toLower().toUtf8());
  dlogf("  sending OTR disconnect to [%s]", user.constData());
  otrl_message_disconnect(mOTRState, &ops, (void *)this, me, PROTO_NAME, user, OTRL_INSTAG_BEST);
#else
  Q_UNUSED(uni)
#endif
/*
  PsycContact *cc = findContact(uni);
  if (cc && cc->isOTRActive()) {
    cc->setOTRActive(false);
    redrawContact(cc);
  }
*/
}


void ChatForm::otrConnect (const QString &uni) {
#ifdef USE_OTR
  QByteArray me(mProto->uni().toLower().toUtf8());
  QByteArray to(uni.toLower().toUtf8());
  dlogf("------------------- STARTING OTR WITH %s -------------------", to.constData());
  //char *msg = otrDefaultQueryMsg(me.constData(), OTRL_POLICY_OPPORTUNISTIC);
  //otrSendMessageCB((void *)this, me.constData(), PROTO_NAME, to.constData(), (msg ? msg : "?OTRv2?"));
  //if (msg) free(msg);
    gcry_error_t err;
    char *newMsg = 0;
    //ConnContext *context;
    err = otrl_message_sending(mOTRState, &ops, (void *)this, me, PROTO_NAME, to,
      OTRL_INSTAG_BEST, // instag
      "?OTR?",
      NULL, // no additional TLVs
      &newMsg,
      /*OTRL_FRAGMENT_SEND_SKIP*/OTRL_FRAGMENT_SEND_ALL, // fragmentation policy
      NULL, //&context,
      NULL, // add_appdata callback
      NULL // data
    );
    if (err) {
      dlogf("failure to encrypt message!\n");
      return;
    }
    if (!newMsg) {
      dlogf("WTF?! an empty OTR resulting message!\n");
      //mProto->sendMessage(uni, txt, act);
      return;
    }
    //QByteArray mx(newMsg);
    //mProto->sendMessage(uni, mx, act);
    /*
    ConnContext *context = otrl_context_find(mOTRState, to, me, PROTO_NAME, OTRL_INSTAG_BEST, FALSE, 0, 0, 0);
    if (context) {
      char *frag = NULL;
      err = otrl_message_fragment_and_send(&ops, (void *)this, context, newMsg, OTRL_FRAGMENT_SEND_ALL, &frag);
      if (frag) free(frag);
    } else {
      dlogf("WTF?! no context found for otrl_message_fragment_and_send()\n");
    }
    */
    otrl_message_free(newMsg);
#else
  Q_UNUSED(uni)
#endif
}


#ifdef USE_OTR
void ChatForm::restartOTRTimer (unsigned int interval) {
  mOTRTimer->stop();
  if (interval > 0) mOTRTimer->start(interval*1000);
}


void ChatForm::onOTRTimer () {
  if (mOTRState) otrl_message_poll(mOTRState, &ops, (void *)this);
}
#endif
