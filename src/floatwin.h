/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * we are the Borg.
 */
#ifndef FLOATWIN_H
#define FLOATWIN_H

//#include <QtGui>

#include <QDataStream>
#include <QHash>
#include <QIcon>
#include <QPainter>
#include <QSet>
#include <QString>
#include <QTimer>
#include <QWidget>

#include "psycproto.h"


class FloatWinManager;


class FloatingWindow : public QWidget {
  Q_OBJECT
  friend class FloatWinManager;

public:
  ~FloatingWindow ();

  void saveFState (QDataStream &st) const;
  void restoreFState (QDataStream &st);

signals:
  void mouseRButtonClicked (int x, int y);
  void mouseDblClicked (int x, int y);

  void positionChanged (); // due to end of movement

private slots:
  void onStartMove ();

private:
  FloatingWindow (FloatWinManager *wman, const QString &uni);

  void calcSize ();

  void paintEvent (QPaintEvent *e);
  void mousePressEvent (QMouseEvent *e);
  void mouseReleaseEvent(QMouseEvent *e);
  void mouseMoveEvent (QMouseEvent *e);
  void mouseDoubleClickEvent (QMouseEvent *e);

  void addNeighbors (QList<FloatingWindow *> &fList);

  const QIcon *statusIcon ();
  const QIcon *msgIcon ();

  void startMoveTimer ();
  void stopMoveTimer ();

  void emitChangePosition ();

  void setTitle (const QString &title);
  void setTip (const QString &tip);
  void setStatus (PsycProto::Status status);

  void discardBuf ();

protected:
  FloatWinManager *mMan;
  QString mUni; // user data, uni
  QString mTitle;
  PsycProto::Status mStatus;

  bool mMaybeMoving;
  bool mBMsgPhase; // false: normal
  bool mBTitlePhase; // false: normal
  bool mIgnoreMouseClickRelease;
  const QIcon *mIcon;
  const QIcon *mMsgIcon;
  QTimer *mMoveTimer;
  QPoint mMousePos;
  QPoint mInitMousePos;
  QPixmap *mBackBuf;
};


///////////////////////////////////////////////////////////////////////////////
class FloatWinManager : public QObject {
  Q_OBJECT
  friend class FloatingWindow;

public:
  FloatWinManager (int titleBlinkTO=500, int msgBlinkTO=300, QObject *parent=0);
  ~FloatWinManager ();

  void clear ();

  FloatingWindow *produce (const QString &uni, const QString &title, const QString &tip, PsycProto::Status status);
  FloatingWindow *change (const QString &uni, const QString &title, const QString &tip, PsycProto::Status status);
  FloatingWindow *find (const QString &uni);
  bool contains (const QString &uni);

  void remove (const QString &uni);
  void remove (FloatingWindow *w);

  void newMessage (const QString &uni);
  void noMessages (const QString &uni);
  void newStatus (const QString &uni, PsycProto::Status status);
  void newTip (const QString &uni, const QString &tip);
  void newTitle (const QString &uni, const QString &title);

  virtual const QIcon *getStatusIcon (FloatingWindow *sender, PsycProto::Status status) = 0;
  virtual const QIcon *getMsgIcon (FloatingWindow *sender) = 0;

  virtual void saveFState () const = 0;
  virtual void loadFState () = 0;

signals:
  void mouseRButtonClicked (int x, int y, const QString &mUni);
  void mouseDblClicked (int x, int y, const QString &mUni);

public slots:
  void offlineAll ();

private slots:
  void btitleTick ();
  void bmsgTick ();

  void doSave ();

  void onMouseRButtonClicked (int x, int y);
  void onMouseDblClicked (int x, int y);

private:
  void setTitleBlink (FloatingWindow *w, bool doBlink);
  void setMsgBlink (FloatingWindow *w, bool doBlink);

  void startBTitleTimer ();
  void stopBTitleTimer ();
  void startBMsgTimer ();
  void stopBMsgTimer ();

protected:
  QHash<QString, FloatingWindow *> mList;

  int mBTitleTO; // 'blink title' timeout (0: off)
  int mBMsgTO; // 'message title' timeout (0: off)
  bool mBTitlePhase; // 'blink title' -- false: 'normal'
  QTimer *mBTitleTimer;
  bool mBMsgPhase; // 'blink message' -- false: 'normal'
  QTimer *mBMsgTimer;

  QSet<FloatingWindow *> mMine;
  QSet<FloatingWindow *> mBMsgWin; // 'blink message' windows
  QHash<FloatingWindow *, uint> mBTitleStop; // 'blink title' when should stop (time_t)
};


#endif
