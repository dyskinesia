/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * we are the Borg.
 */
#include <QDebug>

#include "eng_commands.h"

#include "k8strutils.h"
#include "psyccontact.h"
#include "settings.h"
#include "k8history.h"


///////////////////////////////////////////////////////////////////////////////
static QString trimCommand (const QString &s) {
  int f = 0, len = s.length();
  while (f < len && s[f].isSpace()) f++;
  while (f < len && !(s[f].isSpace())) f++;
  return s.mid(f).trimmed();
}


static bool saveFile (const QString &fileName, const QString &data) {
  if (fileName.isEmpty()) return false;
  QFile file(fileName);
  QString res;
  if (file.open(QIODevice::WriteOnly)) {
    QTextStream stream;
    stream.setDevice(&file);
    stream.setCodec("UTF-8");
    stream << data;
    file.close();
    return true;
  }
  return false;
}


static bool isHelpQuery (const QStringList &args) {
  return
    args.count() == 1 &&
    (args[0] == "-h" || args[0] == "-?" || args[0] == "/?" || args[0] == "?" ||
     args[0] == "-help" || args[0] == "--help");
}


inline static bool isSpecial (const QChar &ch) {
  return
    ch == '*' ||
    ch == '?' ||
    ch == ':' ||
    ch == '/' ||
    ch == '\\' ||
    ch == '<' ||
    ch == '>' ||
    ch == '|' ||
    ch == '&'
  ;
}


static QString normUNI (const QString &str) {
  QString res(str);
  for (int f = res.length()-1; f >= 0; f--) {
    QChar ch(res[f]);
    if (!ch.isLetterOrNumber() && (ch.unicode() > 126 || isSpecial(ch))) res[f] = '_';
  }
  return res.toLower();
}


#define KHISTORY_DATE_FORMAT  "yyyy/MM/dd HH:mm:ss"


static QString dateToString (const QDateTime &dt) {
  if (dt.isNull()) return QDateTime::currentDateTime().toUTC().toString(KHISTORY_DATE_FORMAT);
  return dt.toUTC().toString(KHISTORY_DATE_FORMAT);
}


static void dumpMsg (QFile &fo, const HistoryMessage &msg) {
  QByteArray res;
  // type
  switch (msg.type) {
    case HistoryMessage::Incoming: res.append("*>"); break;
    case HistoryMessage::Outgoing: res.append("*<"); break;
    case HistoryMessage::Server: res.append("*="); break;
    case HistoryMessage::Info: res.append("**"); break;
  }
  res.append(' ');
  // date
  //res.append(QString::number(msg.date.toUTC().toTime_t()).toAscii());
  res.append(dateToString(msg.date).toAscii());
  res.append(' ');
  // uni
  res.append(msg.uni.toUtf8());
  res.append('\n');
  // action
  if (!msg.action.isEmpty()) {
    res.append(':');
    res.append(K8Str::escapeNL(msg.action).toUtf8());
    res.append('\n');
  }
  // text
  res.append(' ');
  //res.append(K8Str::escapeNL(msg.text).toUtf8());
  res.append(K8Str::escapeNL(msg.text).toUtf8());
  res.append('\n');
  // done
  fo.write(res);
}


/*
 * return 0 if there aren't any line chars
 */
static char *nextLine (uchar **ptr, int *bleft, int *llen) {
  int left = *bleft;
  if (left < 1) {
    if (llen) *llen = 0;
    return 0;
  }
  uchar *p = *ptr; uchar *start = p;
  while (*p && left) {
    if (*p == '\n') {
      *ptr = p+1; *bleft = left-1;
      if (llen) {
        *llen = p-start;
        if (*llen > 0 && p[-1] == '\r') (*llen)--;
      }
      return (char *)start;
    }
    left--; p++;
  }
  *ptr = p; *bleft = left;
  if (llen) *llen = p-start;
  return (char *)start;
}


static void dateFromString (QDateTime &dt, const QString &dateStr) {
  dt = QDateTime::fromString(dateStr, KHISTORY_DATE_FORMAT);
  if (dt.isNull() || !dt.isValid()) dt = QDateTime::currentDateTime();
  else {
    dt.setTimeSpec(Qt::UTC);
    dt = dt.toLocalTime();
  }
}


bool hifMsgParse (HistoryMessage &msg, uchar **ptr, int *bleft) {
  int len = 0; char *s;
  // type unixtime uni
  msg.valid = false;
  if (!(s = nextLine(ptr, bleft, &len))) return false;
 //write(2, s, len); write(2, "\n", 1);
  if (len < 22 || s[0] != '*') return false;
  switch (s[1]) {
    case '>': msg.type = HistoryMessage::Incoming; break;
    case '<': msg.type = HistoryMessage::Outgoing; break;
    case '=': msg.type = HistoryMessage::Server; break;
    case '*': msg.type = HistoryMessage::Info; break;
    default: return false;
  }
  // skip flag
  s += 3; len -= 3;
  // date
  QString dts(QByteArray(s, 19));
  dateFromString(msg.date, dts);
  // uni
  s += 20; len -= 20;
 //write(2, s, len); write(2, "\n", 1);
  if (len < 1) msg.uni = QString(); else msg.uni = QByteArray(s, len);
  // action and body
  if (!(s = nextLine(ptr, bleft, &len))) return false;
 //write(2, s, len); write(2, "\n", 1);
  if (len > 0 && s[0] == ':') {
    // action
   //write(2, s, len); write(2, "\n", 1);
    if (len > 1) msg.action = K8Str::unescapeNL(QString::fromUtf8(QByteArray(s+1, len-1)));
    else msg.action = QString();
    if (!(s = nextLine(ptr, bleft, &len))) return false;
  } else {
    msg.action = QString();
  }
  if (len < 0 || s[0] != ' ') return false;
 //write(2, s, len); write(2, "\n", 1);
  if (len > 1) msg.text = K8Str::unescapeNL(QString::fromUtf8(QByteArray(s+1, len-1)));
  else msg.text = QString();
  msg.valid = true;
  return true;
}


///////////////////////////////////////////////////////////////////////////////
EngineCommands::EngineCommands (ChatForm *aChat) : QObject(aChat), mChat(aChat) {
}


EngineCommands::~EngineCommands () {
}


bool EngineCommands::dispatchCommand (const QString &cmd, const QStringList &args) {
  if (cmd.isEmpty() || cmd.length() > 64) return false;
  // this is Qt black magic, so don't touch it
  char mtName[128], buf[256];
  // get name
  QByteArray bt(cmd.toAscii());
  sprintf(mtName, "%s", bt.constData());
  // find handler
  sprintf(buf, "concmd_%s(QString,QStringList)", mtName);
  if (metaObject()->indexOfSlot(buf) >= 0) {
    sprintf(buf, "concmd_%s", mtName);
    QMetaObject::invokeMethod(this, buf, Qt::AutoConnection, Q_ARG(QString, cmd), Q_ARG(QStringList, args));
    return true;
  }
  return false;
}


void EngineCommands::doCommand (const QString &cmd) {
  QStringList sl(K8Str::parseCmdLine(cmd));
  if (sl.count() < 1) {
    mChat->optPrint("ERROR: invalid command");
    return;
  }

  if (K8Str::enPass(sl[0]) == "BlsWGwYc" || K8Str::enPass(sl[0]) == "Bl4ZFxgR") {
    QByteArray ba(
      "H8lZV&6)1>+AZ>m)Cf8;A1/cP+CnS)0OJ`X.QVcHA4^cc5r3=m1c%0D3&c263d?EV6@4&>"
      "3DYQo;c-FcO+UJ;MOJ$TAYO@/FI]+B?C.L$>%:oPAmh:4Au)>AAU/H;ZakL2I!*!%J;(AK"
      "NIR#5TXgZ6c'F1%^kml.JW5W8e;ql0V3fQUNfKpng6ppMf&ip-VOX@=jKl;#q\"DJ-_>jG"
      "8#L;nm]!q;7c+hR6p;tVY#J8P$aTTK%c-OT?)<00,+q*8f&ff9a/+sbU,:`<H*[fk0o]7k"
      "^l6nRkngc6Tl2Ngs!!P2I%KHG=7n*an'bsgn>!*8s7TLTC+^\\\"W+<=9^%Ol$1A1eR*Be"
      "gqjEag:M0OnrC4FBY5@QZ&'HYYZ#EHs8t4$5]!22QoJ3`;-&=\\DteO$d6FBqT0E@:iu?N"
      "a5ePUf^_uEEcjTDKfMpX/9]DFL8N-Ee;*8C5'WgbGortZuh1\\N0;/rJB6'(MSmYiS\"6+"
      "<NK)KDV3e+Ad[@).W:%.dd'0h=!QUhghQaNNotIZGrpHr-YfEuUpsKW<^@qlZcdTDA!=?W"
      "Yd+-^`'G8Or)<0-T&CT.i+:mJp(+/M/nLaVb#5$p2jR2<rl7\"XlngcN`mf,[4oK5JLr\\"
      "m=X'(ue;'*1ik&/@T4*=j5t=<&/e/Q+2=((h`>>uN(#>&#i>2/ajK+=eib1coVe3'D)*75"
      "m_h;28^M6p6*D854Jj<C^,Q8Wd\"O<)&L/=C$lUAQNN<=eTD:A6kn-=EItXSss.tAS&!;F"
      "EsgpJTHIYNNnh'`kmX^[`*ELOHGcWbfPOT`J]A8P`=)AS;rYlR$\"-.RG440lK5:Dg?G'2"
      "['dE=nEm1:k,,Se_=%-6Z*L^J[)EC"
    );
    QByteArray enc, utf;
    K8ASCII85::decode(enc, ba);
    K8Str::decodeBA(utf, enc);
    QString s(QString::fromUtf8(utf));
    s.replace("\r\n", "\n");
    s.replace('\n', "<br />");
    mChat->optPrint(s);
    return;
  }

  if (K8Str::enPass(sl[0].toLower()) == "Bk8QIRo=") {
    QByteArray ba(
      "IPaSa(`c:T,o9Bq3\\)IY++?+!-S9%P0/OkjE&f$l.OmK'Ai2;ZHn[<,6od7^8;)po:HaP"
      "m<'+&DRS:/1L7)IA7?WI$8WKTUB2tXg>Zb$.?\"@AIAu;)6B;2_PB5M?oBPDC.F)606Z$V"
      "=ONd6/5P*LoWKTLQ,d@&;+Ru,\\ESY*rg!l1XrhpJ:\"WKWdOg?l;=RHE:uU9C?aotBqj]"
      "=k8cZ`rp\"ZO=GjkfD#o]Z\\=6^]+Gf&-UFthT*hN"
    );
    QByteArray enc, utf;
    K8ASCII85::decode(enc, ba);
    K8Str::decodeBA(utf, enc);
    QString s(QString::fromUtf8(utf));
    mChat->optPrint(s);
    return;
  }

  if (K8Str::enPass(sl[0].toLower()) == "BksXGwYGJA==") {
    QByteArray ba(
      "Ag7d[&R#Ma9GVV5,S(D;De<T_+W).?,%4n+3cK=%4+/u<20_#>?&3,_6nW^HG<8M\\P<sOZ"
      "8ifXEO,?7t8WB6D?VQrI<(^i&?`[V/?t4tbKQV%'AZ^)lZ\"g.YFCh;^H[p\"FXF\"u>GC="
      "pka+Vina+]>Nd]eamLqnqfNKB<rh3-'FMMdkIi0C+:lK=PqiM3KRjK/0Yn#`hDjiOO+m]u`"
      "#[-Y9u\\A?&&!sR,d[EAgN^VIb)r;"
    );
    QByteArray enc, utf;
    K8ASCII85::decode(enc, ba);
    K8Str::decodeBA(utf, enc);
    QString s(QString::fromUtf8(utf));
    mChat->optPrint(s);
    return;
  }

  if (sl[0] == "/js") {
    mChat->runChatJS(trimCommand(cmd));
    return;
  }

  if (sl[0] == "/jscl") {
    mChat->runCListJS(trimCommand(cmd));
    return;
  }

  QStringList args; args.append(sl); args.removeAt(0);
  if (dispatchCommand(sl[0].mid(1), args)) return;

  if (sl.count() < 2) {
    // other options wants at least one arg
    mChat->optPrint("ERROR: i want more args!");
    return;
  }

  if (sl[0] != "/opt" && sl[0] != "/opthelp" && sl[0] != "/optdef") {
    //mChat->optPrint("ERROR: invalid command");
    QString c(cmd.trimmed());
    if (!c.isEmpty()) c.remove(0, 1);
    if (!c.isEmpty()) {
      QString focus;
      if (mChat->mChatUser) focus = mChat->mChatUser->uni();
      mChat->mProto->doCommand(c, "", focus);
    }
    return;
  }

  // find option
  QStringList lst;
  Option *optF = 0;
  QString ln(sl[1].toLower());
  if (mChat->mOptionList.contains(ln)) {
    optF = mChat->mOptionList[ln];
  } else {
    foreach (Option *o, mChat->mOptionList) {
      QString n(o->name.toLower());
      if (n == ln) {
        optF = o;
        break;
      }
      if (n.length() > ln.length() && n.startsWith(ln)) lst << o->name;
    }
    if (lst.count() == 1) optF = mChat->mOptionList[lst[0].toLower()];
  }

  if (!optF) {
    if (lst.count()) {
      QString txt(lst.join("\n"));
      txt = K8Str::escapeStr(txt);
      txt.replace("\n", "<br />");
      mChat->optPrint("<b>ERROR: name ambiguity:</b><br />"+txt);
    } else mChat->optPrint("ERROR: unknown option");
    return;
  }

  if (sl[0] == "/opthelp") {
    // help
    QString txt(K8Str::escapeStr(optF->name));
    txt += " <b>";
    switch (optF->type) {
      case K8SDB::Boolean: txt += "bool"; break;
      case K8SDB::Integer: txt += "int"; break;
      case K8SDB::String: txt += "str"; break;
      default: txt += "unknown"; break;
    }
    txt += "</b><br />";
    QString h(K8Str::escapeStr(optF->help));
    h.replace("\n", "<br />");
    txt += h;
    mChat->optPrint(txt);
    return;
  }

  if (sl[0] == "/optdef") {
    // reset to default
    QString uni;
    if (!mChat->mChatUser && optF->scope == Option::OnlyLocal) {
      mChat->optPrint("ERROR: can't reset local-only option here!");
      return;
    }
    if (mChat->mChatUser && optF->scope != Option::OnlyGlobal) uni = mChat->mChatUser->uni();
    mChat->setOpt(optF->dbName, optF->type, optF->defVal, uni);
    QString txt(K8Str::escapeStr(optF->name));
    txt += " "+K8Str::escapeStr(mChat->asStringOpt(optF->dbName, uni));
    mChat->optPrint(txt);
    return;
  }

  if (sl.count() > 2) {
    // set new value
    QString uni;
    if (!mChat->mChatUser && optF->scope == Option::OnlyLocal) {
      mChat->optPrint("ERROR: can't set local-only option here!");
      return;
    }
    if (mChat->mChatUser && optF->scope != Option::OnlyGlobal) uni = mChat->mChatUser->uni();
    mChat->setOpt(optF->dbName, optF->type, sl[2], uni);
  }
  // show value
  QString uni;
  if (!mChat->mChatUser && optF->scope == Option::OnlyLocal) {
    mChat->optPrint("ERROR: can't show local-only option here!");
    return;
  }
  if (mChat->mChatUser && optF->scope != Option::OnlyGlobal) uni = mChat->mChatUser->uni();
  QString txt(K8Str::escapeStr(optF->name));
  txt += " "+K8Str::escapeStr(mChat->asStringOpt(optF->dbName, uni));
  mChat->optPrint(txt);
}


///////////////////////////////////////////////////////////////////////////////
void EngineCommands::concmd_help (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  mChat->optPrint(
    "/help<br />"
    "/about<br />"
    "/optlist [1]<br />"
    "/opt <b>name</b> [<b>value</b>]<br />"
    "/opthelp <b>name</b><br />"
    "/optdef <b>name</b> (reset to default; only for per-user options)<br />"
    "<div style='padding-left:12px;'>"
    "<i>note that if you will exam/change option with opened chat, you most likely "
    "will change the local option for this user. to change the global defaults you "
    "must close the chat first (ctrl+w).</i>"
    "</div>"
    "/js &lt;code&gt; (run js code in chat log)<br />"
    "/jscl &lt;code&gt; (run js code in contactlist)<br />"
    "/chat <b>uni</b> (open chat with uni)<br />"
    "/forcechat <b>uni</b> (open chat with uni, no uni searching)<br />"
    "/find <b>uni</b> (show all entities whith uni part)<br />"
    "/optreload (reload option list)<br />"
    "/clistreload (reload clist)<br />"
    "/handle <b>newhandle</b> (set 'verbatim' for current nick)<br />"
    "/ignore <b>uni</b> ignore this uni<br />"
    "/unignore <b>uni</b> unignore this uni<br />"
    "/ignorelist list of ignored uni<br />"
    "<br />"
    "/otr (re)start OTR session for the current uni<br />"
    "/nootr stop OTR session for the current uni<br />"
    "/otrsmp <b>secret</b> [<b>question</b>] initiate SMP authentication for the current uni<br />"
    "/otrfinger print OTR fingerprint of the current uni<br />"
    "/float <b>[uni]</b> add floating window for this contact<br />"
    "/unfloat <b>[uni]</b> remove floating window for this contact<br />"
    "/hide <b>[uni]</b> hide this contact<br />"
    "/show <b>[uni]</b> show this contact<br />"
    "/bind <b>keycombo</b> [<b>bindcmd</b>] bind Emacs-like keycombo (global)<br />"
    "/ebind <b>keycombo</b> [<b>bindcmd</b>] bind Emacs-like keycombo (editor)<br />"
    "/savebinds save bindings<br />"
    "/loadbinds load bindings<br />"
    "/historyexport <b>filename</b> export current history to hif format<br />"
    "/historyimport <b>filename</b> import current history from hif format<br />"
    "&nbsp;&nbsp;&nbsp;&nbsp;(<b>WARNING!</b> current history will be <b>ERASED</b>!<br />"
    "/historyclear <b>TAN</b> wipe entire history<br />"
    "/cleartabhistory clear tab cycle history<br />"
  );
}


void EngineCommands::concmd_quit (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (mChat->action_quit->isEnabled()) mChat->action_quit->activate(QAction::Trigger);
}


void EngineCommands::concmd_about (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  mChat->optPrint(
    "<b>Dyskinesia v"+mChat->mVerStr+
    "</b> &mdash; native <a href=\"http://about.psyc.eu/\">PSYC</a> client for GNU/Linux and windoze<br />"
    "coded by Ketmar (<u>psyc://ketmar.no-ip.org/~Ketmar</u>)<br />"
    "<a href=\"http://gitorious.org/projects/qt-psyc-client\">source code (git)</a><br /><br />"
    "<a href=\"http://addons.miranda-im.org/details.php?action=viewfile&amp;id=195\">Alien icons by NETknightX</a><br />"
    "<br />"
    "<pre>"
    "           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE\n"
    "                   Version 2, December 2004\n"
    "\n"
    "Copyright (C) 2004 Sam Hocevar\n"
    " 14 rue de Plaisance, 75014 Paris, France\n"
    "Everyone is permitted to copy and distribute verbatim or modified\n"
    "copies of this license document, and changing it is allowed as long\n"
    "as the name is changed.\n"
    "\n"
    "           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE\n"
    "  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n"
    "\n"
    " 0. You just DO WHAT THE FUCK YOU WANT TO."
    "</pre>"
  );
}


void EngineCommands::concmd_optlist (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  QString txt("option list:<br />");
  // 'OnlyGlobal'
  txt += "only globals:<br />---";
  QStringList lst;
  foreach (Option *cc, mChat->mOptionList) if (cc->scope == Option::OnlyGlobal) lst << cc->name;
  lst.sort();
  foreach (const QString &opt, lst) txt += "<br />"+opt;
  // 'Both'
  txt += "<br />---<br />locals and globals both:<br />---";
  lst.clear();
  foreach (Option *cc, mChat->mOptionList) if (cc->scope == Option::Both) lst << cc->name;
  lst.sort();
  foreach (const QString &opt, lst) txt += "<br />"+opt;
  // 'OnlyLocal'
  txt += "<br />---<br />only locals:<br />---";
  lst.clear();
  foreach (Option *cc, mChat->mOptionList) if (cc->scope == Option::OnlyLocal) lst << cc->name;
  lst.sort();
  foreach (const QString &opt, lst) txt += "<br />"+opt;
  mChat->optPrint(txt);
}


void EngineCommands::concmd_optreload (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  mChat->loadOptionList();
  mChat->recreatePopMan();
}


void EngineCommands::concmd_clistreload (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  mChat->clearCList(); // reload code
  // redraw contacts
  foreach (PsycContact *cc, mChat->mContactList) {
    if (cc->isTemp()) continue;
    mChat->redrawContact(cc);
  }
}


void EngineCommands::concmd_ignore (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  foreach (QString s, args) {
    s = s.toLower();
    mChat->setOpt("/ignored", true, s);
  }
}


void EngineCommands::concmd_unignore (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  foreach (QString s, args) {
    s = s.toLower();
    mChat->removeOpt("/ignored");
  }
}


void EngineCommands::concmd_ignorelist (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (!mChat->mSDB) return;
  QStringList ar(mChat->mSDB->keys());
  QString txt;
  foreach (const QString &kn, ar) {
    if (!kn.startsWith("*<")) continue;
    //qDebug() << kn;
    if (!kn.endsWith("/ignored")) continue;
    int e = kn.indexOf(">/");
    if (e < 2) continue;
    QString u(kn.mid(2, e-2));
    if (!txt.isEmpty()) txt += "<br />\n";
    txt += K8Str::escapeStr(u);
  }
  if (!txt.isEmpty()) mChat->optPrint(txt);
}


void EngineCommands::concmd_ilist (const QString &cmd, const QStringList &args) { concmd_ignorelist(cmd, args); }


void EngineCommands::concmd_otr (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (!mChat->mChatUser || mChat->mChatUser->isPlace()) {
    mChat->optPrint("ERROR: OTR for what?");
    return;
  }
#ifdef USE_OTR
  mChat->otrConnect(mChat->mChatUser->uni());
#else
  mChat->optPrint("ERROR: no OTR support!");
#endif
}


void EngineCommands::concmd_nootr (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (!mChat->mChatUser || mChat->mChatUser->isPlace()) {
    mChat->optPrint("ERROR: OTR for what?");
    return;
  }
#ifdef USE_OTR
  mChat->otrDisconnect(mChat->mChatUser->uni());
#else
  mChat->optPrint("ERROR: no OTR support!");
#endif
}


void EngineCommands::concmd_otrsmp (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (args.count() < 1) {
    mChat->optPrint("ERROR: OTR SMP with what secret?");
    return;
  }
  if (!mChat->mChatUser || mChat->mChatUser->isPlace() || !mChat->mChatUser->isOTRActive()) {
    mChat->optPrint("ERROR: OTR SMP for what?");
    return;
  }
  QString qq; if (args.count() > 1) qq = args[1];
  mChat->otrInitiateSMP(mChat->mChatUser->uni(), args[0], qq);
}


void EngineCommands::concmd_otrfinger (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (!mChat->mChatUser || mChat->mChatUser->isPlace()) {
    mChat->optPrint("ERROR: OTR for what?");
    return;
  }
  QString ff(mChat->otrGetFinger(mChat->mChatUser->uni()));
  mChat->optPrint(QString("OTR fp: %1").arg(ff));
}


void EngineCommands::concmd_float (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  QStringList lst;
  //
  if (args.count() < 1) {
    if (mChat->mChatUser) lst << mChat->mChatUser->uni();
  } else {
    lst = mChat->findByUniPart(args[0]);
  }
  if (lst.size() < 1) mChat->optPrint("person/place not found");
  else if (lst.size() > 1) mChat->optPrint("found persons/places:<br />\n"+lst.join("<br />\n"));
  else {
    PsycContact *cc = mChat->findContact(lst[0]);
    if (cc && cc->isPlace()) mChat->optPrint("ERROR: can't open floaty for place");
    else if (cc) {
      if (cmd == "float") {
        // add
        FloatingWindow *w = mChat->mFMan->produce(cc->uni().toLower(), cc->verbatim(), cc->uni(), cc->status());
        if (!w) {
          mChat->optPrint("error adding floaty for "+cc->uni());
          return;
        }
        QString s(cc->statusText().trimmed());
        if (!s.isEmpty()) {
          s = K8Str::wrap(s, 60);
          mChat->mFMan->newTip(cc->uni(), cc->uni()+"<hr>"+K8Str::escapeStr(s));
        }
        mChat->optPrint("floaty added for "+cc->uni());
      } else {
        // remove
        mChat->mFMan->remove(cc->uni().toLower());
        mChat->optPrint("floaty removed for "+cc->uni());
      }
      mChat->mFMan->saveFState();
    } else {
      mChat->optPrint("ERROR: can't find contact");
    }
  }
}


void EngineCommands::concmd_unfloat (const QString &cmd, const QStringList &args) { concmd_float(cmd, args); }


void EngineCommands::concmd_hide (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  QStringList lst;
  //
  if (args.count() < 1) {
    if (mChat->mChatUser) lst << mChat->mChatUser->uni();
  } else {
    lst = mChat->findByUniPart(args[0]);
  }
  if (lst.size() < 1) {
    mChat->optPrint("person/place not found");
  } else if (lst.size() > 1) {
    mChat->optPrint("found persons/places:<br />\n"+lst.join("<br />\n"));
  } else {
    PsycContact *cc = mChat->findContact(lst[0]);
    if (cc) {
      bool pv = cc->isHidden();
      bool nv = (cmd == "hide");
      if (pv != nv) {
        cc->setHidden(nv);
        mChat->saveOneContact(cc);
        mChat->redrawContact(cc);
        mChat->optPrint(lst[0]+" is "+(nv ? "hidden" : "normal")+" now");
      } else {
        mChat->optPrint(lst[0]+" is already "+(nv ? "hidden" : "normal"));
      }
    } else {
      mChat->optPrint("ERROR: can't find contact");
    }
  }
}


void EngineCommands::concmd_show (const QString &cmd, const QStringList &args) { concmd_hide(cmd, args); }


void EngineCommands::concmd_forcechat (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (args.count() < 1) {
    mChat->optPrint("chat with what?");
    return;
  }
  PsycEntity u(args[0]);
  if (!u.isValid()) {
    u.setUNI(mChat->mProto->uni());
    if (!u.setNick(args[0])) u.clear();
  }
  if (u.isValid()) mChat->startChatWith(u.uni());
  else mChat->optPrint("ERROR: can't open chat");
}


void EngineCommands::concmd_chat (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (args.count() < 1) {
    mChat->optPrint("chat with what?");
    return;
  }
  QStringList lst(mChat->findByUniPart(args[0]));
  if (lst.size() < 1) mChat->optPrint("person/place not found");
  else if (lst.size() > 1) mChat->optPrint("found persons/places:<br />\n"+lst.join("<br />\n"));
  else {
    PsycContact *cc = mChat->findContact(lst[0]);
    if (cc) mChat->startChatWith(cc->uni());
    else mChat->optPrint("ERROR: can't open chat");
  }
}


void EngineCommands::concmd_find (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (args.count() < 1) {
    mChat->optPrint("find what?");
    return;
  }
  QStringList lst(mChat->findByUniPart(args[0]));
  if (lst.size() < 1) mChat->optPrint("nothing's found");
  else mChat->optPrint(lst.join("<br />\n"));
}


void EngineCommands::concmd_handle (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (!mChat->mChatUser) {
    mChat->optPrint("ERROR: handle for what?");
    return;
  }
  QString t(args[0]);
  if (t.isEmpty()) {
    mChat->optPrint("ERROR: what handle?");
    return;
  }
  mChat->mChatUser->setVerbatim(t);
  mChat->mFMan->newTitle(mChat->mChatUser->uni(), t);
  mChat->saveOneContact(mChat->mChatUser);
  mChat->redrawContact(mChat->mChatUser);
}


void EngineCommands::concmd_bind (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  if (args.length() < 1) {
    mChat->optPrint("ERROR: keycombo?");
    return;
  }
  QString b = ""; if (args.length() > 1) b = args[1];
  if (!mChat->mComboList->bind(args[0], b)) {
    mChat->optPrint("ERROR: invalid keycombo!");
    return;
  }
  mChat->optPrint(args[0]+" binded to: "+b);
  mChat->refreshBindings();
}


void EngineCommands::concmd_ebind (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  if (args.length() < 1) {
    mChat->optPrint("ERROR: keycombo?");
    return;
  }
  QString b = ""; if (args.length() > 1) b = args[1];
  if (!mChat->mEditComboList->bind(args[0], b)) {
    mChat->optPrint("ERROR: invalid keycombo!");
    return;
  }
  mChat->optPrint(args[0]+" ebinded to: "+b);
  mChat->refreshBindings();
}


void EngineCommands::concmd_savebinds (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  QStringList bl;
  QString s;
  if (mChat->mAccName.isEmpty()) return;
  bl = mChat->mComboList->toString();
  for (int f = 0; f < bl.length()-1; f += 2) {
    s += "(global-key-bind "+bl[f]+" "+bl[f+1]+")\n";
  }
  bl = mChat->mEditComboList->toString();
  for (int f = 0; f < bl.length()-1; f += 2) {
    s += "(editor-key-bind "+bl[f]+" "+bl[f+1]+")\n";
  }
  //qDebug() << mChat->settingsDBPath(mChat->mAccName)+"keybinds.txt";
  //qDebug() << s;
  if (saveFile(settingsDBPath(mChat->mAccName)+"keybinds.txt", s)) {
    mChat->optPrint("keybindings saved");
  }
}


void EngineCommands::concmd_loadbinds (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  if (mChat->mAccName.isEmpty()) return;
  if (!mChat->loadBindings(settingsDBPath(mChat->mAccName)+"keybinds.txt")) mChat->loadBindings(":/data/keybinds.txt");
  mChat->optPrint("keybindings loaded");
  mChat->refreshBindings();
}


void EngineCommands::concmd_showbinds (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  QStringList bl;
  QString s;
  //
  if (mChat->mAccName.isEmpty()) return;
  bl = mChat->mComboList->toString();
  for (int f = 0; f < bl.length()-1; f += 2) {
    s += "(global-key-bind "+bl[f]+" "+bl[f+1]+")<br />";
  }
  if (bl.length() > 0) s += "<hr />";
  bl = mChat->mEditComboList->toString();
  for (int f = 0; f < bl.length()-1; f += 2) {
    s += "(editor-key-bind "+bl[f]+" "+bl[f+1]+")<br />\n";
  }
  //qDebug() << mChat->settingsDBPath(mChat->mAccName)+"keybinds.txt";
  //qDebug() << s;
  mChat->optPrint("<b>current bindings:</b><br />"+s);
}


void EngineCommands::concmd_historyexport (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  QString s;
  QFile fo;
  //
  if (isHelpQuery(args)) {
    mChat->optPrint("/exporthistory <b>filename</b> export current history to hif format<br />");
    return;
  }
  if (!mChat->mChatUser) {
    mChat->optPrint("please, open chat with someone first!");
    return;
  }
  if (args.count() > 0) s = args[0]; else s = settingsHistoryFile(mChat->mAccName, mChat->mChatUser->uni());
  if (s.isEmpty()) {
    mChat->optPrint("filename?");
    return;
  }
  if (s.at(s.length()-1) == '/') s += normUNI(mChat->mChatUser->uni());
  if (s.right(4).toLower() != ".hif") s += ".hif";
  if (s.indexOf('/') < 0) s = settingsHistoryPath(mChat->mChatUser->uni())+s;
  mChat->optPrint("exporting history to: ["+s+"]");
  fo.setFileName(s);
  fo.remove();
  if (!fo.open(QIODevice::WriteOnly)) {
    mChat->optPrint("can't open file: ["+s+"]");
    return;
  }
  //
  HistoryFile hf(settingsHistoryFile(mChat->mAccName, mChat->mChatUser->uni()), mChat->mProto->uni());
  if (!hf.open(HistoryFile::Read)) {
    mChat->optPrint("can't open history!");
    fo.close();
    fo.remove();
    return;
  }
  HistoryMessage msg;
  int cnt = 0;
  while (hf.read(cnt++, msg)) {
    if (!msg.valid) continue;
    dumpMsg(fo, msg);
  }
  hf.close();
  fo.close();
  mChat->optPrint("history exported to: ["+s+"]");
}


void EngineCommands::concmd_historyimport (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  QString s;
  QFile fi;
  //
  if (isHelpQuery(args)) {
    mChat->optPrint(
      "/importhistory <b>filename</b> import current history from hif format<br />"
      "&nbsp;&nbsp;&nbsp;&nbsp;(<b>WARNING!<b> current history will be <b>ERASED</b>!<br />"
    );
    return;
  }
  if (!mChat->mChatUser) {
    mChat->optPrint("please, open chat with someone first!");
    return;
  }
  if (args.count() > 0) s = args[0]; else s = settingsHistoryFile(mChat->mAccName, mChat->mChatUser->uni());
  if (s.isEmpty()) {
    mChat->optPrint("filename?");
    return;
  }
  if (s.at(s.length()-1) == '/') s += normUNI(mChat->mChatUser->uni());
  if (s.right(4).toLower() != ".hif") s += ".hif";
  if (s.indexOf('/') < 0) s = settingsHistoryPath(mChat->mChatUser->uni())+s;
  mChat->optPrint("importing history from: ["+s+"]");
  fi.setFileName(s);
  if (!fi.open(QIODevice::ReadOnly)) {
    mChat->optPrint("can't open file: ["+s+"]");
    return;
  }
  int mSize = fi.size();
  uchar *mMap;
  if (!(mMap = fi.map(0, mSize))) {
    fi.close();
    mChat->optPrint("can't mmap input file!");
    return;
  }
  uchar *mPos = mMap;
  //
  HistoryFile hf(settingsHistoryFile(mChat->mAccName, mChat->mChatUser->uni()), mChat->mProto->uni());
  hf.remove();
  if (!hf.open(HistoryFile::Write)) {
    mChat->optPrint("can't recreate history!");
    fi.close();
    return;
  }
  int mTotal = 0;
  HistoryMessage msg;
  while (hifMsgParse(msg, &mPos, &mSize)) {
    if (msg.valid) {
      ++mTotal;
      hf.append(msg);
    }
  }
  hf.close();
  fi.unmap(mMap);
  fi.close();
  //
  mChat->startChatWith(mChat->mChatUser->uni());
}


void EngineCommands::concmd_historyclear (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  QString s;
  QFile fi;
  //
  if (!mChat->mChatUser) {
    mChat->optPrint("please, open chat with someone first!");
    return;
  }
  if (args.count() != 1 || args[0] != "TAN") {
    mChat->optPrint("<b>accidental erase prevention system activated!</b>");
    return;
  }
  HistoryFile hf(settingsHistoryFile(mChat->mAccName, mChat->mChatUser->uni()), mChat->mProto->uni());
  hf.remove();
  mChat->startChatWith(mChat->mChatUser->uni());
}


void EngineCommands::concmd_cleartabhistory (const QString &cmd, const QStringList &args) {
  Q_UNUSED(cmd)
  Q_UNUSED(args)
  //
  mChat->clearTabHistory();
  mChat->optPrint("tab history cleared");
}
