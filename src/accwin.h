/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef ACCWIN_H
#define ACCWIN_H

#include <QDialog>


#include "ui_accwin.h"
class AccWindow : public QDialog, public Ui_accWin {
  Q_OBJECT

public:
  AccWindow (QWidget *parent = 0);
  ~AccWindow ();

signals:
  void updateAccountInfo (AccWindow *au, const QString &text);
  void accountCreated (AccWindow *au, const QString &text);
  void accountDeleted (AccWindow *au, const QString &text);

private slots:
  void on_leNick_textChanged (void);
  void on_leHost_textChanged (void);
  void on_btDelAcc_clicked (bool);
  void on_btAddAcc_clicked (bool);
  void on_cbAccount_currentIndexChanged (const QString &text);

public:
  int maxIdx;
  QString curAccName;
};


#endif
