/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifdef K8HISTORY_H
#ifndef K8BHFHISTORY_H
#define K8BHFHISTORY_H

#include <QDateTime>
#include <QFile>
#include <QString>
#include <QVector>


///////////////////////////////////////////////////////////////////////////////
class HistoryFile : public HistoryFileBase {
public:
  HistoryFile (const QString &fname, const QString &myUni);
  virtual ~HistoryFile ();

  virtual bool open (OpenMode newmode);
  virtual bool isOpen () const;
  virtual void close ();
  virtual void remove ();

  virtual int count ();

  virtual bool append (const HistoryMessage &msg);

  virtual bool read (int idx, HistoryMessage &msg);

private:
  bool initialize (bool newFile);

private:
  HistoryFile::OpenMode mMode;
  QString mName;
  QString mMyUni;
  qint32 mCount;
  QFile mFText;
  QFile mFIdx;
  QVector<qint32> mOffsets;
};


#endif
#endif
