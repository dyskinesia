/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QObject>
#include <QDir>

#include "k8strutils.h"
#include "k8json.h"

#include "k8history.h"


///////////////////////////////////////////////////////////////////////////////
static const char sign[4] = {'B', 'H', 'F', '0'};


///////////////////////////////////////////////////////////////////////////////
class QARC4Codec {
public:
  struct A4Context {
    int m[256]; // permutation table (unsigned char actually)
    int x, y; // permutation indicies (unsigned char actually)
  };

public:
  QARC4Codec () {}
  QARC4Codec (const QString &key) { setKey(key); }
  QARC4Codec (const QByteArray &key) { setKey(key); }
  ~QARC4Codec () {}

  inline bool isReady () const { return mInited; }

  // will reset codec
  inline void setKey (const QString &key) { setKey(key.toUtf8()); }
  inline void setKey (const QByteArray &key) { setup(key); }

  inline void reset () { if (mInited) memcpy(&mCtx, &mStartCtx, sizeof(mCtx)); }

  // process in-place
  inline void processIP (QByteArray &data) { doBuffer(data.data(), data.constData(), data.size()); }
  // copy
  inline QByteArray process (const QByteArray &data) {
    QByteArray res(data.size(), 0);
    doBuffer(res.data(), data.constData(), data.size());
    return res;
  }

private:
  void setup (const QByteArray &key);
  void doBuffer (void *dest, const void *src, int len);

private:
  bool mInited;
  struct A4Context mCtx;
  struct A4Context mStartCtx;
};


///////////////////////////////////////////////////////////////////////////////
void QARC4Codec::setup (const QByteArray &key) {
  int c = 0, k = 0, x, y, *m, a, b;
  A4Context *ctx = &mCtx;
  const unsigned char *kkk = (const unsigned char *)key.constData();
  int keylen = key.size();
  //
  ctx->x = ctx->y = 0; m = ctx->m;
  for (int f = 0; f < 256; ++f) m[f] = f;
  // create permutation table
  for (int f = 0; f < 256; ++f) {
    unsigned char kc = keylen>0?kkk[k++]:0;
    if (k >= keylen) k = 0;
    a = m[f];
    c = (unsigned char)(c+a+kc);
    m[f] = m[c]; m[c] = a;
  }
  // discard first 256 bytes
  x = ctx->x; y = ctx->y; m = ctx->m;
  for (int f = 256; f > 0; --f) {
    x = (unsigned char)((x+1)&0xff); a = m[x];
    y = (unsigned char)((y+a)&0xff);
    m[x] = b = m[y];
    m[y] = a;
  }
  ctx->x = x; ctx->y = y;
  memcpy(&mStartCtx, &mCtx, sizeof(mStartCtx));
  mInited = true;
}


void QARC4Codec::doBuffer (void *dest, const void *src, int len) {
//void a4CryptCpy (A4Context *ctx, void *dest, const void *buf, int buflen)
  int x, y, *m, a, b;
  A4Context *ctx = &mCtx;
  unsigned char *ddd = (unsigned char *)dest;
  const unsigned char *bbb = (const unsigned char *)src;
  //
  if (!mInited) {
    if (len > 0 && dest != src) memmove(dest, src, len);
    return;
  }
  x = ctx->x; y = ctx->y; m = ctx->m;
  for (int f = len; f > 0; --f, ++ddd, ++bbb) {
    x = (unsigned char)((x+1)&0xff); a = m[x];
    y = (unsigned char)((y+a)&0xff);
    m[x] = b = m[y];
    m[y] = a;
    ddd[0] = bbb[0]^m[(unsigned char)((a+b)&0xff)];
  }
  ctx->x = x; ctx->y = y;
}


///////////////////////////////////////////////////////////////////////////////
HistoryFile::HistoryFile (const QString &fname, const QString &myUni) :
  mMode(Read), mName(fname), mMyUni(myUni), mCount(-1),
  mFText(fname+".bht"), mFIdx(fname+".bhi")
{
}


HistoryFile::~HistoryFile () {
  close();
}


bool HistoryFile::isOpen () const {
  return mFText.isOpen();
}


int HistoryFile::count () {
  return mCount;
}


void HistoryFile::remove () {
  close();
  mFText.remove();
  mFIdx.remove();
}


static void createFileDir (const QString &fname) {
  QString dir(fname);
  int idx = dir.lastIndexOf('/');
  if (idx > 0) {
    dir.truncate(idx);
    QDir d;
    d.mkdir(dir);
  }
}


bool HistoryFile::open (OpenMode newmode) {
  bool newFile = false;
  //
  close();
  if (newmode == Read) {
    if (!mFText.open(QIODevice::ReadOnly)) return false;
    if (!mFIdx.open(QIODevice::ReadOnly)) { mFText.close(); return false; }
  } else {
    newFile = !(mFText.exists() && mFIdx.exists());
    if (newFile) createFileDir(mName);
    if (!mFText.open(QIODevice::ReadWrite)) return false;
    if (!mFIdx.open(QIODevice::ReadWrite)) {
      mFText.close();
      if (newFile) remove();
      return false;
    }
  }
  mMode = newmode;
  if (!initialize(newFile)) {
    close();
    if (newFile) remove();
    return false;
  }
  return true;
}


bool HistoryFile::initialize (bool newFile) {
  char fsign[4];
  //
  mOffsets.clear();
  if (newFile) {
    // just create empty files
    mCount = 0;
    if (mFIdx.write(sign, 4) != 4) return false;
    if (mFIdx.write((const char *)&mCount, 4) != 4) return false;
    return true;
  }
  // read index
  if (mFIdx.read(fsign, 4) != 4) return false;
  if (memcmp(fsign, sign, 4) != 0) return false;
  if (mFIdx.read((char *)&mCount, 4) != 4) return false;
  if (mCount < 0 || mCount > 1024*1024*1024) return false;
  // read offsets
  if (mCount > 0) {
    const qint32 *i;
    QByteArray ofs(mFIdx.read(mCount*sizeof(qint32)));
    if (ofs.size() != mCount*4) return false;
    mOffsets.resize(mCount);
    i = (const qint32 *)ofs.constData();
    for (int f = 0; f < mCount; ++f, ++i) mOffsets[f] = *i;
  }
  //
  return true;
}


void HistoryFile::close () {
  if (mFIdx.isOpen()) {
    mFIdx.close();
    mFText.close();
    mOffsets.clear();
    mCount = 0;
    mMode = Read;
  }
}


static QByteArray &baPutQStr (QByteArray &ba, const QString &str) {
  qint32 len = str.size();
  if (len > 0) {
    QByteArray u = str.toUtf8();
    len = u.size();
    ba.append((const char *)&len, sizeof(len));
    ba.append(u);
  } else {
    ba.append((const char *)&len, sizeof(len));
  }
  return ba;
}


static int baGetQStr (const QByteArray &ba, int pos, QString &str) {
  qint32 len;
  //qDebug() << " strpos:" << pos;
  if (pos < 0 || pos+4 > ba.size()) return -1;
  memcpy(&len, ba.constData()+pos, 4);
  //qDebug() << "  strlen:" << len;
  if (len < 0 || len > 1024*1024) return -1;
  pos += 4;
  if (pos+len > ba.size()) return -1;
  str = QString::fromUtf8(ba.constData()+pos, len);
  //qDebug() << "  str:" << str;
  pos += len;
  return pos;
}


bool HistoryFile::append (const HistoryMessage &msg) {
  unsigned char type;
  qint64 ms;
  qint32 bsz, tpos;
  QByteArray ba;
  //
  if (!mFIdx.isOpen() || mMode != Write || !msg.valid) return false;
  //
  if (!mFText.seek(mFText.size())) return false;
  ms = mFText.pos();
  if (ms < 0 || ms > 0x7fffffffL) return false;
  tpos = ms;
  //
  switch (msg.type) {
    case HistoryMessage::Incoming: type = 0; break;
    case HistoryMessage::Outgoing: type = 1; break;
    case HistoryMessage::Server: type = 2; break;
    case HistoryMessage::Info:
    default: type = 3; break;
  };
  // type
  ba.append((const char *)&type, 1);
  // date
  ms = msg.date.toUTC().toMSecsSinceEpoch();
  ba.append((const char *)&ms, sizeof(ms));
  // uni
  baPutQStr(ba, msg.uni);
  // action
  baPutQStr(ba, msg.action);
  // text
  baPutQStr(ba, msg.text);
  // encrypt
  {
    QARC4Codec a4(mMyUni.toLower()+QString::number(tpos, 10));
    a4.processIP(ba);
  }
  bsz = ba.size();
  if (mFText.write((const char *)&bsz, 4) != 4) return false;
  if (mFText.write(ba) != bsz) return false;
  // write offset
  if (!mFIdx.seek(mCount*4+8)) return false;
  if (mFIdx.write((const char *)&tpos, 4) != 4) return false;
  //
  ++mCount;
  if (!mFIdx.seek(4)) { --mCount; return false; }
  if (mFIdx.write((const char *)&mCount, 4) != 4) { --mCount; return false; }
  //
  mOffsets.append(tpos);
  return true;
}


bool HistoryFile::read (int idx, HistoryMessage &msg) {
  qint64 ms;
  qint32 sz;
  int pos;
  QByteArray ba;
  //
  msg.clear();
  if (!mFIdx.isOpen() || idx < 0 || idx >= mCount) return false;
  if (!mFText.seek(mOffsets[idx])) return true;
  if (mFText.read((char *)&sz, 4) != 4) return true;
  if (sz < 21 || sz > 1024*1024*16) return true;
  //qDebug() << "idx:" << idx << "size:" << sz;
  ba = mFText.read(sz);
  if (ba.size() != sz) return true;
  // decrypt
  {
    QARC4Codec a4(mMyUni.toLower()+QString::number(mOffsets[idx], 10));
    a4.processIP(ba);
  }
  // type
  //qDebug() << " type:" << (unsigned int)(ba.constData()[0]);
  switch (ba.constData()[0]) {
    case 0: msg.type = HistoryMessage::Incoming; break;
    case 1: msg.type = HistoryMessage::Outgoing; break;
    case 2: msg.type = HistoryMessage::Server; break;
    case 3: msg.type = HistoryMessage::Info; break;
    default: return true;
  }
  // date
  memcpy(&ms, ba.constData()+1, sizeof(ms));
  //qDebug() << " date:" << ms;
  if (ms < 0) return true;
  msg.date.setTimeSpec(Qt::UTC);
  msg.date.setMSecsSinceEpoch(ms);
  msg.date = msg.date.toLocalTime();
  //qDebug() << " date:" << msg.date;
  //
  pos = 1+sizeof(ms);
  // uni
  if ((pos = baGetQStr(ba, pos, msg.uni)) < 0) return true;
  // action
  if ((pos = baGetQStr(ba, pos, msg.action)) < 0) return true;
  // text
  if ((pos = baGetQStr(ba, pos, msg.text)) < 0) return true;
  //
  //qDebug() << " pos:" << pos << "ba.size:" << ba.size();
  if (pos != ba.size()) return true;
  if (msg.action.isEmpty()) msg.action = QString();
  //
  msg.valid = true;
  return true;
}
