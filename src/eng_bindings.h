/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef ENG_BINDINGS_H
#define ENG_BINDINGS_H

#include <QContextMenuEvent>
#include <QDesktopServices>
#include <QHash>
#include <QLabel>
#include <QList>
#include <QMap>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSet>
#include <QShortcut>
#include <QString>
#include <QSystemTrayIcon>
#include <QWebFrame>
#include <QWidget>
#include <QWidgetAction>

#include "chatform.h"
#include "keycmb.h"


///////////////////////////////////////////////////////////////////////////////
class EngineBindings : public QObject {
  Q_OBJECT

public:
  EngineBindings (ChatForm *aChat);
  ~EngineBindings ();

  bool dispatchBinding (const QString &cmd, const QString &combo, const QString &ctext);
  bool defaultBinding (const QString &cmd, const QString &combo, const QString &ctext);

private slots:
  void binding_editor_activate (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_clist_activate (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_chat_activate (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_chat_send (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_clear (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_window_minimize (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_window_hide (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_edit_select_all (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_cut (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_copy (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_paste (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_undo (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_redo (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_edit_move_home (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_move_end (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_move_top (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_move_bottom (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_move_left (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_move_right (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_move_up (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_move_down (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_move_word_left (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_move_word_right (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_edit_recode_text (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_edit_insert_new_line (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_insert_smile_oo (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_insert_smile_oo_weird (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_edit_insert_desu (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_edit_autocomplete (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_chat_close (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_chat_quote (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_go_to_unread (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_set_status_text (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_activate_menu_status (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_activate_menu_main (const QString &cmd, const QString &combo, const QString &ctext);

  void binding_action_quit (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_edit_account (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_join_place (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_send_raw_packet (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_show_packet_console (const QString &cmd, const QString &combo, const QString &ctext);

  /* rewrite! make one function to set status by number/name! */
  void binding_action_status_offline (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_status_vacation (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_status_away (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_status_dnd (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_status_nearby (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_status_busy (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_status_here (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_status_ffc (const QString &cmd, const QString &combo, const QString &ctext);
  void binding_action_status_realtime (const QString &cmd, const QString &combo, const QString &ctext);

private:
  ChatForm *mChat;
};


#endif
