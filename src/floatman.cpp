/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * we are the Borg.
 */
#include <QDebug>

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "floatman.h"

#include "chatform.h"
#include "psyccontact.h"


extern bool debugOutput;


///////////////////////////////////////////////////////////////////////////////
FloatersManager::FloatersManager (ChatForm *mCForm, int titleBlinkTO, int msgBlinkTO) :
  FloatWinManager(titleBlinkTO, msgBlinkTO, mCForm), mForm(mCForm)
{
  mMsgIcon = 0;
  loadFState();
}


FloatersManager::~FloatersManager () {
  //saveFState();
  foreach (QIcon *i, mIconCache) delete i;
  mIconCache.clear();
  delete mMsgIcon;
  QHash <PsycProto::Status, QIcon *> mIconCache;
}


const QIcon *FloatersManager::getStatusIcon (FloatingWindow *sender, PsycProto::Status status) {
  Q_UNUSED(sender)
  if (!mIconCache[status]) {
    QIcon *i = new QIcon(mForm->statusIcon(status));
    mIconCache[status] = i;
  }
  return mIconCache[status];
}


const QIcon *FloatersManager::getMsgIcon (FloatingWindow *sender) {
  Q_UNUSED(sender)
  if (!mMsgIcon) mMsgIcon = new QIcon(MSG_TRAY_ICON);
  return mMsgIcon;
}


static void dsSaveString (QDataStream &st, const QString &s) {
  QByteArray ba(s.toUtf8());
  int len = ba.size();
  st << len;
  for (int f = 0; f < ba.size(); f++) st << (quint8)(ba.at(f));
}


static QString dsLoadString (QDataStream &st) {
  QByteArray ba;
  int len;
  st >> len;
  for (int f = 0; f < len; f++) {
    quint8 ch;
    st >> ch;
    ba.append((char)ch);
  }
  return QString::fromUtf8(ba);
}


static QString toAscii85BA (const QByteArray &ba) {
  QByteArray res;
  K8ASCII85::encode(res, ba);
  return QString::fromAscii(res);
}


static QByteArray fromAscii85BA (const QString &s) {
  QByteArray res;
  QByteArray ba(s.toAscii());
  K8ASCII85::decode(res, ba);
  return res;
}


void FloatersManager::saveFState () const {
  if (!mList.count()) {
    mForm->setOpt("/floaty/*state", "");
    return;
  }

  QByteArray ba;
 {
  QDataStream st(&ba, QIODevice::WriteOnly);
  st << (quint8)0; // version
  // save uni list
  {
    int sz = mList.size();
    st << sz;
    QHash<QString, FloatingWindow *>::const_iterator i;
    for (i = mList.begin(); i != mList.end(); ++i) {
      dsSaveString(st, i.key());
      (*i)->saveFState(st);
    }
  }
 }
  //for (int f = 0; f < ba.size(); f++) fprintf(stderr, "%02X ", (quint8)(ba.at(f))); fprintf(stderr, "\n");

  QString o(toAscii85BA(ba));
  //fprintf(stderr, "%s\n", o.toAscii().constData());

  mForm->setOpt("/floaty/*state", o);
}


void FloatersManager::loadFState () {
  clear();
  QString s(mForm->toStringOpt("/floaty/*state"));
  //fprintf(stderr, "%s\n", s.toAscii().constData());

  QByteArray ba(fromAscii85BA(s));

  //for (int f = 0; f < ba.size(); f++) fprintf(stderr, "%02X ", (quint8)(ba.at(f))); fprintf(stderr, "\n");

  QDataStream st(&ba, QIODevice::ReadOnly);
  quint8 ver;
  st >> ver;
  if (ver != 0) return; // invalid version
  // load uni list
  int sz;
  st >> sz;
  //qDebug() << " " << sz << "items";
  while (sz-- > 0) {
    QString uniS(dsLoadString(st));
    //qDebug() << " " << uniS;
    FloatingWindow *w = produce(uniS, "", "", PsycProto::Offline);
    if (!w) return; // error!
    w->restoreFState(st);
    PsycContact *cc = mForm->findContact(uniS);
    if (!cc || cc->isPlace()) {
      qDebug() << "  no such contact!";
      remove(w);
    } else {
      //qDebug() << " *" << cc->uni() << cc->verbatim();
      newTitle(cc->uni(), cc->verbatim());
      newTip(cc->uni(), cc->uni());
    }
  }
}
