/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <stdio.h>

//#include <QTextStream>
#include <QByteArray>
#include <QRegExp>

#include "k8strutils.h"

namespace K8Str {


/*
QString readFile (QFile &file) {
  QString res;
  QTextStream stream;

  stream.setDevice(&file);
  stream.setCodec("UTF-8");
  res = stream.readAll();
  file.close();
  return res;
}
*/


QString wordSplit (QString &str, int width) {
  if (width < 1 || width >= str.length()) {
    QString res(str);
    str.chop(str.length());
    return res;
  }
  // find space
  int pos = width;
  while (pos >= 0) {
    QChar ch = str.at(pos);
    if (ch.isSpace()) break;
    pos--;
  }
  // find non-space
  int epos = pos;
  while (pos >= 0) {
    QChar ch = str.at(epos);
    if (!ch.isSpace()) break;
    epos--;
  }
  if (epos < 0 || pos < 0) {
    // no space to split
    QString res = str.left(width);
    str.remove(0, width);
    return res;
  }
  // split
  QString res = str.left(epos+1);
  // remove spaces
  while (pos < str.length()) {
    QChar ch = str.at(pos);
    if (!ch.isSpace()) break;
    pos++;
  }
  str.remove(0, pos);
  // done
  return res;
}


QString wrap (const QString &str, int width, const QString &pfx) {
  QString s(str); s.replace("\r\n", "\n");
  QString res;
  while (!s.isEmpty()) {
    int e = s.indexOf('\n');
    if (e < 0) e = s.length();
    QString t(s.left(e).trimmed());
    s.remove(0, e+1);
    do {
      QString w(wordSplit(t, width));
      res += pfx;
      res += w;
      res += "\n";
    } while (!t.isEmpty());
  }
  return res;
}


static const ushort qOpen = 0x00ab;
static const ushort qClose = 0x00bb;
static const ushort lDash = 0x2014;

int fixTypograpy (QString &str) {
  int res = 0, len = str.length();
  bool wasReps = false;
  for (int pos = 0; pos < len; pos++) {
    ushort ch = str.at(pos).unicode();
    if (ch == '"' || ch == qOpen || ch == qClose) {
      QChar pc, nc;
      if (pos == 0) pc = ' '; else pc = str.at(pos-1);
      if (pos >= len-1) nc = ' '; else nc = str.at(pos+1);
      if (pc == '(' || pc == '[' || pc == '{') {
        // prev is opening
        wasReps = true;
        str[pos] = qOpen;
      } else if (pc == '.' || pc == '!' || pc == '?') {
        // prev is closing
        wasReps = true;
        str[pos] = qClose;
      } else if (nc == ')' || nc == ']' || nc == '}') {
        // next is closing
        wasReps = true;
        str[pos] = qClose;
      } else if (pc.isLetterOrNumber()) {
        // prev is alnum
        wasReps = true;
        str[pos] = qClose;
      } else if (nc.isLetterOrNumber()) {
        // next is alnum
        wasReps = true;
        str[pos] = qOpen;
      } else if (nc.isSpace() != pc.isSpace()) {
        // not surrounded by spaces
        wasReps = true;
        if (pc.isSpace()) {
          // prev is space
          str[pos] = qOpen;
        } else {
          // next is space
          str[pos] = qClose;
        }
      }
    } else if (ch == '-' && pos < len-3 && str.at(pos+1) == '-') {
      QChar pc, nc(str.at(pos+2));
      if (pos == 0) pc = ' '; else pc = str.at(pos-1);
      if (nc != '-' && pc != '-') {
        if ((pc.isSpace() || pc.isPunct()) && (nc.isSpace() || nc.isPunct())) {
          wasReps = true;
          str.remove(pos, 1);
          str[pos] = lDash;
          ++res;
          --len;
        }
      }
    }
  }
  return wasReps ? res : -1;
}


int unfixTypograpy (QString &str) {
  int res = 0, len = str.length();
  bool wasReps = false;
  for (int pos = 0; pos < len; pos++) {
    QChar ch = str[pos];
    if (ch == qOpen || ch == qClose) {
      str[pos] = '"';
      wasReps = true;
    } else if (ch == lDash) {
      str[pos] = '-';
      str.insert(pos, '-');
      res++;
      len++;
    }
  }
  return wasReps ? res : -1;
}


static QRegExp linkRegExp(
  "((((https?|ftp|gopher|telnet|file|wais)://)"
   "(([A-z0-9_]+):([A-z0-9_-]*)@)?)|"
   "((www|ftp|(\\d+))\\.))"
   "(([A-Za-z0-9_-]+\\.)*)"
   //"(([A-Za-z0-9_-]+)\\.)"
   "([A-Za-z0-9_-]+)"
   "(:(\\d+))?"
   "((/[a-z0-9-_.%~#&!,:=()\\x0080-\\xff00]*)*)?"
   "(\\?[^? \n\r\t]*)?",
  Qt::CaseInsensitive,
  QRegExp::RegExp2  // greedy by default (more perl-like)
);


static QRegExp linkDomainRegExp(
  "^(\\d+\\.)*\\d+$",
  Qt::CaseInsensitive,
  QRegExp::RegExp2  // greedy by default (more perl-like)
);


#if 0
static bool isOnlyAlpha (const QString &str) {
  int len = str.length();
  //
  fprintf(stderr, "c1: [%s]\n", str.toLatin1().constData());
  for (int f = 0; f < len; ++f) {
    QChar ch = str[f];
    //
    fprintf(stderr, " (%d)\n", ch.isLetter());
    /*if (!((ch >= 'A' && ch <= 'Z') || (ch >= 'a' || ch <= 'z'))) {
      fprintf(stderr, " BAD\n");
      return false;
    }*/
    if (!ch.isLetter()) {
      //fprintf(stderr, " BAD\n");
      return false;
    }
  }
  //
  //fprintf(stderr, " OK\n");
  return true;
}
#endif


QString fixURLs (const QString &str, const QString &color) {
  QString res, style;
  QString s(normChars(str));
  int pos;
  //
  res.reserve(str.length());
  if (!color.isEmpty()) style = QString(" style=\"color:%1\"").arg(color); else style = QString("");
  //
  while ((pos = linkRegExp.indexIn(s)) != -1) {
    QString link = linkRegExp.cap(0);
    int linkLen = link.length();
    //
    //fprintf(stderr, "link: [%s]\n", link.toLatin1().constData());
    if (linkDomainRegExp.indexIn(link) == -1) {
      // check for "(" in url and drop ")" if necessary
      for (int f = 0, lvl = 0; f < linkLen; ++f) {
        QChar ch = link[f];
        //
        if (ch == '(') {
          ++lvl;
        } else if (ch == ')') {
          if (lvl == 0) {
            // extra ')', trim it
            link.truncate(f);
            break;
          }
          --lvl;
        }
      }
      linkLen = link.length();
      //
      QString text(link);
      if (link.toLower().startsWith("www.")) link.prepend("http://");
      link.replace("&", "&amp;");
      link.replace("<", "&lt;");
      link.replace(">", "&gt;");
      link.replace("\"", "&quot;");
      link = QString(QLatin1String("<a href=\"%1\"%2>%3</a>")).arg(link).arg(style).arg(escapeStr(text));
    }
    //
    res.append(escapeStr(s.left(pos)));
    res.append(link);
    s.remove(0, pos+linkLen);
  }
  res.append(escapeStr(s));
  //
  return res;
}


static QChar chLt = QLatin1Char('<');
static QChar chGt = QLatin1Char('>');
static QChar chAmp = QLatin1Char('&');
static QChar chSCol = QLatin1Char(';');
static QChar chL = QLatin1Char('l');
static QChar chG = QLatin1Char('g');
static QChar chT = QLatin1Char('t');
static QChar chA = QLatin1Char('a');
static QChar chM = QLatin1Char('m');
static QChar chP = QLatin1Char('p');
static QString sLt = QLatin1String("&lt;");
static QString sGt = QLatin1String("&gt;");
static QString sAmp = QLatin1String("&amp;");


QString escapeStr (const QString &str, bool noGt) {
  QString res;
  int len = str.length();
  res.reserve(int(len*1.1));
  for (int f = 0; f < len; f++) {
    QChar ch = str[f];
    if (ch == chLt) res += sLt;
    else if (!noGt && ch == chGt) res += sGt;
    else if (ch == chAmp) res += sAmp;
    else res += ch;
  }
  return res;
}


QString unescapeStr (const QString &str) {
  QString res;
  res.reserve(int(str.length()));
  int len = str.length();
  for (int f = 0; f < len; f++) {
    if (len-f < 4 || str[f] != chAmp) res += str[f];
    else {
      QChar ch = str[f+1];
      if (ch == chA) {
        if (len-f < 5 || str[f+2] != chM || str[f+3] != chP || str[f+4] != chSCol) res += str[f];
        else { res += chAmp; f += 4; }
      } else if (str[f+3] != chSCol || str[f+2] != chT) {
        res += str[f];
      } else {
        if (ch == chL) { res += chLt; f += 3; }
        else if (ch == chGt) { res += chGt; f += 3; }
        else res += str[f];
      }
    }
  }
  return res;
}


static QChar chEsc = QLatin1Char('\\');
static QChar chEOL = QLatin1Char('\n');
static QChar chCR = QLatin1Char('\r');
static QChar chFF = QLatin1Char('\f');
static QChar chBB = QLatin1Char('\b');
static QChar chTab = QLatin1Char('\t');

static QString sEOL = QLatin1String("\\n");
static QString sCR = QLatin1String("\\r");
static QString sTab = QLatin1String("\\t");
static QString sFF = QLatin1String("\\f");
static QString sBB = QLatin1String("\\b");
static QString sQuot = QLatin1String("\\\"");
static QString sEsc = QLatin1String("\\\\");

QString escapeNL (const QString &str) {
  QString res;
  int len = str.length();
  res.reserve(int(len*1.1));
  for (int f = 0; f < len; f++) {
    QChar ch = str[f];
    ushort uc = ch.unicode();
    switch (uc) {
      case '\n': res += sEOL; break;
      case '\r': res += sCR; break;
      case '\t': res += sTab; break;
      case '\f': res += sFF; break;
      case '\b': res += sBB; break;
      case '\\': res += sEsc; break;
      case '"': res += sQuot; break;
      default: if (uc >= 32) res += ch; break;
    }
  }
  return res;
}


QString unescapeNL (const QString &str) {
  QString res;
  int len = str.length();
  bool noLast = false;
  res.reserve(len);
  for (int f = 0; f < len-1; f++) {
    QChar ch = str[f];
    if (ch == chEsc) {
      f++; ch = str[f];
      ushort uc = ch.unicode();
      switch (uc) {
        case 'n': res += chEOL; break;
        case 'r': res += chCR; break;
        case 't': res += chTab; break;
        case 'f': res += chFF; break;
        case 'b': res += chBB; break;
        default: res += ch; break;
      }
      if (f == len-1) noLast = true;
    } else res += ch;
  }
  if (len > 0 && !noLast) res += str[len-1];
  return res;
}


QString normChars (const QString &str) {
  QString res = str;
  for (int f = res.length()-1; f >= 0; f--) {
    QChar ch = res[f];
    int u = ch.unicode();
    switch (u) {
      case 0xa0: res[f] = 0x20; break; // unicode "nbsp"
      case 0x2028: // unicode "new line"
      case 0x2029: // unicode "new paragraph"
        res[f] = 0x0a; break;
      // qt and other specials
      case 0xfdd0: case 0xfdd1: case 0xfffd: case 0xfffc: case 0xfeff: case 0xfffe:
        res[f] = 0x20; break;
      // qt stranhes
      case 0xffab: res[f] = 0xab; break; //<<
      case 0xffbb: res[f] = 0xbb; break; //>>
      case 0xffb2: res[f] = 0x406; break; //I
      case 0xffb3: res[f] = 0x456; break; //i
      case 0xffaa: res[f] = 0x404; break; //E
      case 0xffba: res[f] = 0x454; break; //e
      case 0xffaf: res[f] = 0x407; break; //II
      case 0xffbf: res[f] = 0x457; break; //ii
      default: ;
    }
  }
  return res;
}


QString deHTMLize (const QString &str, deHTMLConvert cvt) {
  QString res;
  int len = str.length();
  res.reserve(len);
  for (int f = 0; f < len; f++) {
    QChar ch = str[f];
    if (f < len-3 && ch == '&' && str[f+1] == '#') {
      int pp = f;
      f += 2;
      int uni = 0;
      if (str[f] == 'x' || str[f] == 'X') {
        f++;
        while (f < len) {
          ch = str[f++];
          if (ch == ';') { pp = -1; break; }
          if (ch >= '0' && ch <= '9') uni = uni*16+ch.unicode()-48;
          else if (ch >= 'a' && ch <= 'f') uni = uni*16+ch.unicode()-97+10;
          else if (ch >= 'A' && ch <= 'F') uni = uni*16+ch.unicode()-65+10;
          else break;
          uni &= 0xffff;
        }
      } else {
        while (f < len) {
          ch = str[f++];
          if (ch == ';') { pp = -1; break; }
          if (ch >= '0' && ch <= '9') uni = uni*10+ch.unicode()-48;
          else break;
          uni &= 0xffff;
        }
      }
      if (pp < 0) {
        // all ok
        f--;
        QChar ch1(uni);
        // esape
        if (cvt != deHTMLnone) {
          switch (ch1.unicode()) {
            case '<': res += "&lt;"; break;
            case '&': res += "&amp;"; break;
            case '"': res += "&quot;"; break;
            case '>':
              if (cvt != deHTMLnoGt) res += "&gt;"; else res += ch1;
              break;
            default: res += ch1; break;
          }
        } res += ch1;
      } else {
        f = pp;
        res += '&';
      }
    } else res += ch;
  }
  return res;
}


QString stringToB64 (const QString &str) {
  QByteArray ba(str.toUtf8());
  QByteArray r(ba.toBase64());
  return QString(r);
}


QString stringFromB64 (const QString &str) {
  QByteArray ba(str.toUtf8());
  QByteArray r(QByteArray::fromBase64(ba));
  return QString::fromUtf8(r);
}


void encodeBA (QByteArray &res, const QByteArray &ba) {
  const char *data = ba.constData();
  int len = ba.size(); char pch = 42;
  for (int f = 0; f < len; f++, data++) {
    char ch = *data;
    res.append((ch^pch)+f+1);
    pch = ch;
  }
}


void decodeBA (QByteArray &res, const QByteArray &ba) {
  const char *data = ba.constData();
  int len = ba.size(); char pch = 42;
  for (int f = 0; f < len; f++, data++) {
    char ch = *data;
    ch = (ch-f-1)^pch;
    res.append(ch);
    pch = ch;
  }
}


QString enPass (const QString &str) {
  QString res;
  QByteArray ba(str.toUtf8());
  int pch = 42;
  for (int f = 0; f < ba.length(); f++) {
    int c = ba.constData()[f];
    QChar ch((c^pch)+f+1);
    pch = c;
    res += ch;
  }
  ba = res.toUtf8();
  QByteArray r(ba.toBase64());
  return QString(r);
}


QString dePass (const QString &str) {
  QByteArray ba(str.toLatin1());
  QByteArray r(QByteArray::fromBase64(ba));
  QString cs(QString::fromUtf8(r)); // decoded string

  int pch = 42;
  QByteArray bu;
  for (int f = 0; f < cs.length(); f++) {
    QChar ch(((cs[f].unicode()-f-1)^pch));
    pch = ch.unicode();
    bu.append(pch&0xff);
  }
  return QString::fromUtf8(bu);
}


QString jsStr (const QString &str) {
  QString res;
  int len = str.length();
  res.reserve(len);
  for (int f = 0; f < len; f++) {
    QChar ch = str[f];
    if (ch == '\t') res += "\\t";
    else if (ch == '\n') res += "\\n";
    else if (ch == '\\') res += "\\\\";
    else if (ch == '"') res += "\\\"";
    else if (ch.unicode() < 32 || ch.unicode() >= 0xff00) res += ' ';
    else res += ch;
  }
  res.prepend("\"");
  res += "\"";
  return res;
}


QStringList parseCmdLine (const QString &cl) {
  QStringList res;
  int len = cl.length();
  for (int f = 0; f < len; ) {
    while (f < len && cl[f].isSpace()) f++;
    if (f >= len) break;
    QChar qch = cl[f];
    bool quoted = false;
    if (qch == '"' || qch == '\'') {
      // quoted string
      quoted = true;
      f++;
    }
    if (f >= len) break;
    QString t; t.reserve(len-f+1);
    while (f < len) {
      QChar ch = cl[f++];
      if (!quoted && ch.isSpace()) break; // delimiter
      if (quoted && ch == qch) {
        // can be eol
        if (f >= len || cl[f] != qch) break;
        // double-quote, convert to single
        t.append(ch); f++;
      } else if (ch == '\\') {
        // escape
        if (f >= len) break;
        ch = cl[f++];
        switch (ch.unicode()) {
          case 't': t.append("\t"); break;
          case 'n': t.append("\n"); break;
          case 'r': t.append("\r"); break;
          case 'a': t.append("\a"); break;
          case 'b': t.append("\b"); break;
          case 'f': t.append("\f"); break;
          case 'v': t.append("\v"); break;
          case 's': t.append(" "); break;
          //TODO: add \x and \nnn?
          default: t.append(ch); break;
        }
      } else t.append(ch);
    }
    res << t;
  }
  return res;
}


}
