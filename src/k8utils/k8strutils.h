/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef K8_STRUTILS_H
#define K8_STRUTILS_H

#include <QDateTime>
#include <QString>
#include <QStringList>

namespace K8Str {

/*
 * split string at 'width' (or <width) by word border
 * return leftmost part of the string; shortens 'str'
 */
QString wordSplit (QString &str, int width);

/*
 * word-wrap string
 */
QString wrap (const QString &str, int width, const QString &pfx="");

/*
 * 'fix' plain text to so-called 'russian typographic' style
 * change quoting marks, long dash, etc
 * return # of deleted chars ('cause '--' transforms to one char here)
 * return -1 if no replacements was made
 */
int fixTypograpy (QString &str);

/*
 * does the reverse of the above
 * returns # of inserted chars or -1
 */
int unfixTypograpy (QString &str);

/*
 * detect URLs and convert to 'a href'
 * if color isn't empty, add 'style' with color
 * resulting string will contain properly escaped URL
 * (so it can be directly added to HTML code)
 */
QString fixURLs (const QString &str, const QString &color=QString());

/*
 * escape '<', '&' (and '>', if noGt is false) to HTML/XML entities
 */
QString escapeStr (const QString &str, bool noGt=false);
/*
 * opposite of the above
 */
QString unescapeStr (const QString &str);

/*
 * c-like escaping '\n', '\r', '\t', '\b', '\f', '"' and '\' chars
 */
QString escapeNL (const QString &str);

/*
 * opposite of the above
 */
QString unescapeNL (const QString &str);

/*
 * 'norm' some unicode chars (ukrainian, etc)
 * i found that some windoze programs sends bad unicodes for some chars
 * (ot it's a Qt fault, i don't really care here)
 */
QString normChars (const QString &str);

/*
 * convert '&#...' HTML entities back to chars
 * decoded '&', '<', '"' (and maybe '>') becomes another entites though %-)
 * note, that only decoded specials becomes entities
 * added due to idiotic Psi habit of sending such encoded messages
 */
enum deHTMLConvert {
  deHTMLnone,
  deHTMLall,
  deHTMLnoGt
};

QString deHTMLize (const QString &str, deHTMLConvert cvt=deHTMLnone);

/*
 * encode string to base64
 */
QString stringToB64 (const QString &str);

/*
 * decode base64 string
 */
QString stringFromB64 (const QString &str);

/*
 * encode password; returns only ascii chars
 */
QString enPass (const QString &str);

/*
 * reverse of the above
 */
QString dePass (const QString &str);

/*
 * encode byte array ('res' size will be the same as the 'ba'
 */
void encodeBA (QByteArray &res, const QByteArray &ba);

/*
 * dencode byte array ('res' size will be the same as the 'ba'
 */
void decodeBA (QByteArray &res, const QByteArray &ba);

/*
 * quote string so it can be passed to JavaScript
 * encloses string in double quotes too
 */
QString jsStr (const QString &str);

/*
 * simple, buf flexible command-line parser
 * useful to implement consoles
 */
QStringList parseCmdLine (const QString &cl);


}

#endif
