/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
// http://en.wikipedia.org/wiki/Ascii85
#ifndef K8ASCII85_H
#define K8ASCII85_H

#include <QByteArray>


namespace K8ASCII85 {

/*
 * encode byte array to ascii85
 */
void encode (QByteArray &res, const QByteArray &ba, int width=0, int start=0, int length=-1);

/*
 * decode ascii85-encoded byte array
 */
bool decode (QByteArray &res, const QByteArray &ba, int start=0, int length=-1);

}
#endif
