/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
// http://en.wikipedia.org/wiki/Ascii85
#include "k8ascii85.h"


namespace K8ASCII85 {


#define ENCODE_TUPLE { \
  int tmp = 5; \
  char buf[5], *s = buf; \
  do { \
    *s++ = tuple%85; \
    tuple /= 85; \
  } while (--tmp > 0); \
  tmp = count; \
  do { \
    if (width > 0 && pos >= width) { res.append('\n'); pos = 0; } \
    res.append(*--s + '!'); pos++; \
  } while (tmp-- > 0); }


/*
 * encode byte array to ascii85
 */
void encode (QByteArray &res, const QByteArray &ba, int width, int start, int length) {
  const char *data = ba.constData();
  int len = ba.size();
  if (start < 0) start = 0; else { len -= start; data += start; }
  if (length < 0 || len < length) length = len;
  quint32 tuple = 0;
  int count = 0, pos = 0;
  if (length > 0) {
    int xlen = 5*((length+3)/4);
    res.reserve(res.size()+xlen);
  }
  //res.append("<~"); pos += 2; charCnt += 2;
  for (int f = length; f > 0; f--, data++) {
    // put85
    quint8 c = *((const quint8 *)data);
    switch (count++) {
      case 0: tuple |= (c << 24); break;
      case 1: tuple |= (c << 16); break;
      case 2: tuple |= (c <<  8); break;
      case 3:
        tuple |= c;
        if (tuple == 0) {
          // special case
          if (width > 0 && pos >= width) { res.append('\n'); pos = 0; }
          res.append('z'); pos++;
        } else ENCODE_TUPLE
        tuple = 0;
        count = 0;
        break;
    }
  }
  if (count > 0) ENCODE_TUPLE
  //if (width > 0 && pos+2 >= width) { res.append('\n'); pos = 0; }
  //res.append("~>");
}


///////////////////////////////////////////////////////////////////////////////
static quint32 pow85[] = { 85*85*85*85, 85*85*85, 85*85, 85, 1 };


//void wput(unsigned long tuple, int bytes) {
#define DECODE_TUPLE(tuple,bytes) \
  for (int tmp = bytes; tmp > 0; tmp--, tuple = (tuple & 0x00ffffff)<<8) \
    res.append((char)((tuple >> 24)&0xff))


/*
 * decode ascii85-encoded byte array
 */
bool decode (QByteArray &res, const QByteArray &ba, int start, int length) {
  const char *data = ba.constData();
  int len = ba.size();
  if (start < 0) start = 0; else { len -= start; data += start; }
  if (length < 0 || len < length) length = len;
  quint32 tuple = 0;
  int count = 0, c = 0;
  if (length > 0) {
    int xlen = 4*((length+4)/5);
    res.reserve(res.size()+xlen);
  }
  for (int f = length; f > 0; f--, data++) {
    c = *((const unsigned char *)data);
    if (c <= ' ') continue; // skip blanks
    switch (c) {
      case 'z': // zero tuple
      if (count != 0) {
        //fprintf(stderr, "%s: z inside ascii85 5-tuple\n", file);
        return false;
      }
      res.append('\0');
      res.append('\0');
      res.append('\0');
      res.append('\0');
      break;
    case '~': // '~>': end of sequence
      if (f < 1 || data[1] != '>') return false; // error
      if (count > 0) {
        f = -1;
        break;
      }
    default:
      if (c < '!' || c > 'u') {
        //fprintf(stderr, "%s: bad character in ascii85 region: %#o\n", file, c);
        return false;
      }
      tuple += ((quint8)(c-'!'))*pow85[count++];
      if (count == 5) {
        DECODE_TUPLE(tuple, 4);
        count = 0;
        tuple = 0;
      }
      break;
    }
  }
  // write last (possibly incomplete) tuple
  if (count > 0) {
    count--;
    tuple += pow85[count];
    DECODE_TUPLE(tuple, count);
  }
  return true;
}


}
