/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef K8POPUPS_H
#define K8POPUPS_H

#include <QHash>
#include <QLabel>
#include <QString>
#include <QTimer>
#include <QVBoxLayout>
#include <QVector>
#include <QWidget>


class K8PopupManager;
class K8PopupWin;
class K8PopupType;


class K8PopupLayout : public QVBoxLayout {
  Q_OBJECT

public:
  K8PopupLayout (int aWidth, QWidget *parent=0) : QVBoxLayout(parent) { mWidth = aWidth; }
  ~K8PopupLayout () { }

  QSize sizeHint () const;

private:
  int mWidth;
};


class K8PopupType {
public:
  K8PopupType (const QString &aName, const QString &aStyle=QString()) : name(aName), style(aStyle) {}

public:
  QString name;
  QString style;
};


///////////////////////////////////////////////////////////////////////////////
#ifdef FADING
class K8PopupFader : public QWidget {
  Q_OBJECT

public:
  K8PopupFader (QWidget *parent, bool aIn);

  inline QColor fadeColor () const { return mStartColor; }
  inline void setFadeColor (const QColor &newColor) { mStartColor = newColor; }

  inline int fadeDuration() const { return mDuration; }
  inline void setFadeDuration (int milliseconds) { mDuration = milliseconds; }

  void start ();

protected:
  void paintEvent (QPaintEvent *event);

private:
  QWidget *mPParent;
  QTimer *mTimer;
  QColor mStartColor;
  int mCurrentAlpha;
  int mDuration;
  bool mIn;
};
#endif


///////////////////////////////////////////////////////////////////////////////
class K8PopupWin : public QWidget {
  Q_OBJECT
  friend class K8PopupManager;

public:
  ~K8PopupWin ();
  virtual void show ();

protected:
  bool eventFilter (QObject *obj, QEvent *event);
  void enterEvent (QEvent *event);
  void leaveEvent (QEvent *event);

private:
  K8PopupWin (K8PopupManager *aMan, int aWidth, int aHeight, K8PopupType *aType,
    const QString &aId, const QString &msg, QWidget *parent=0);

  void initUI (int width, int height);
  void setData (const QString &msg);

  void getOut ();

  void buildAndSetMask (QWidget *w);
  void rebuildMask ();

#ifdef FADING
private slots:
  void onFaderInDies (QObject *obj);
  void onFaderOutDies (QObject *obj);
#endif

private:
  K8PopupManager *mMan;
  K8PopupType *mType;

  bool mShouldClose;
  bool mMouseInside;

  uint mShowTime;
  QString mId;

  QVBoxLayout *mLayout;
  QLabel *mContentLabel;

#ifdef FADING
  K8PopupFader *mFaderIn;
  K8PopupFader *mFaderOut;
  bool mFadingOut;
#endif

  //bool mMaskSet;
};


///////////////////////////////////////////////////////////////////////////////
class K8PopupManager : public QObject {
  Q_OBJECT
  friend class K8PopupWin;

public:
  enum Position {
    LeftTop=0, TopLeft=0,
    RightTop=1, TopRight=1,
    LeftBottom=2, BottomLeft=2,
    RightBottom=3, BottomRight=3
  };

  // timeout: seconds
  K8PopupManager (int width, int height, uint timeout, Position pos);
  ~K8PopupManager ();

  /* WARNING: DO NOT CALL THIS WHEN POPUPS ACTIVE! */
  void addType (const QString &aName, const QString &aStyle=QString());
  void delType (const QString &aName);
  bool hasType (const QString &aName);

  void showMessage (const QString &type, const QString &id, const QString &msg);
  void updateMessage (const QString &type, const QString &id, const QString &msg);

  inline Position position () const { return mPos; }
  inline void setPosition (Position pos) { if (mPos != pos) { mPos = pos; remove(0); } }

  inline int width () const { return mWidth; }
  inline int height () const { return mHeight; }
  inline int timeout () const { return mTimeout; }

  inline bool floatHeight () const { return mFloatHeight; }
  inline void setFloatHeight (bool fh) { if (mFloatHeight != fh) { mFloatHeight = fh; rearrange(); } }

  void setTimeout (int timeout);

  void removeById (const QString &type, const QString &id);
  void removeAll ();

signals:
  void click (const QString &type, const QString &id, Qt::MouseButton button);

private slots:
  void onTimer ();

private:
  void emitClick (K8PopupWin *w, Qt::MouseButton button);

  void allocSlot (K8PopupWin *wnd);
  void rearrange ();
  void remove (K8PopupWin *wnd);

private:
  Position mPos;
  int mWidth;
  int mHeight;
  uint mTimeout;
  bool mFloatHeight;

  QVector<K8PopupWin *>mList;
  QTimer *mCheckTimer;
  QHash<QString, K8PopupType *> mTypes;
};


namespace K8Popups {

/*
 * escape '<' and '&' chars
 */
QString escapeStr (const QString &str);

}


#endif
