/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QApplication>
#include <QBitmap>
#include <QDateTime>
#include <QDesktopWidget>
#include <QEvent>
#include <QMouseEvent>
#include <QPainter>
#include <QPixmap>

#include "k8popups.h"


namespace K8Popups {

const static QString sLt("&lt;");
const static QString sAmp("&amp;");


QString escapeStr (const QString &str) {
  QString res;
  int len = str.length();
  res.reserve(len*11/10);
  for (int f = 0; f < len; f++) {
    QChar ch(str[f]);
    switch (ch.unicode()) {
      case '<': res += sLt; break;
      case '&': res += sAmp; break;
      default: res += ch; break;
    }
  }
  return res;
}

}


///////////////////////////////////////////////////////////////////////////////
QSize K8PopupLayout::sizeHint () const {
  QDesktopWidget *desktop = QApplication::desktop();
  QRect geometry = desktop->availableGeometry();
  int minH = 64;
  int maxH = (geometry.bottom()-geometry.top())*2/3;
  QSize res = QVBoxLayout::sizeHint();
  int h = res.height();
  //
  if (h < minH) h = minH;
  if (h > maxH) h = maxH;
  res.setHeight(h);
  res.setWidth(mWidth);
  return res;
}


///////////////////////////////////////////////////////////////////////////////
#ifdef FADING
K8PopupFader::K8PopupFader (QWidget *parent, bool aIn) : QWidget(parent), mPParent(parent), mIn(aIn) {
  mStartColor = parent ? parent->palette().window().color() : Qt::white;
  mCurrentAlpha = 0;
  mDuration = 150;

  mTimer = new QTimer(this);
  connect(mTimer, SIGNAL(timeout()), this, SLOT(update()));

  setAttribute(Qt::WA_DeleteOnClose);
  resize(parent->size());
}


void K8PopupFader::start () {
  mCurrentAlpha = mIn ? 0 : 255;
  mTimer->start(33);
  //setMask(mPParent->mask());
  show();
}


void K8PopupFader::paintEvent (QPaintEvent * /*event*/) {
  bool end;
  //
  QPainter painter(this);
  QColor semiTransparentColor = mStartColor;
  //
  semiTransparentColor.setAlpha(mCurrentAlpha);
  painter.fillRect(rect(), semiTransparentColor);
  if (mIn) {
    mCurrentAlpha += 255*mTimer->interval()/mDuration;
    end = (mCurrentAlpha >= 255);
  } else {
    mCurrentAlpha -= 255*mTimer->interval()/mDuration;
    end = (mCurrentAlpha <= 0);
  }
  if (end) {
    mTimer->stop();
    close();
  }
}
#endif


///////////////////////////////////////////////////////////////////////////////
K8PopupWin::K8PopupWin (K8PopupManager *aMan, int aWidth, int aHeight, K8PopupType *aType,
    const QString &aId, const QString &msg, QWidget *parent) :
  QWidget(parent), mMan(aMan),
  mType(aType), mShouldClose(false), mMouseInside(false), mShowTime(0),
  mId(aId)
#ifdef FADING
  , mFaderIn(0), mFaderOut(0), mFadingOut(false)
#endif
  //mMaskSet(false)
{
  initUI(aWidth, aHeight);
  setData(msg);
}


K8PopupWin::~K8PopupWin () {
  if (mMan) mMan->remove(this);
}


#ifdef FADING
void K8PopupWin::onFaderInDies (QObject * /*obj*/) {
  mFaderIn = 0;
}


void K8PopupWin::onFaderOutDies (QObject * /*obj*/) {
  mFaderOut = 0;
  if (mFadingOut) close();
}
#endif


void K8PopupWin::initUI (int width, int height) {
  setWindowFlags(windowFlags() | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::ToolTip);
  setAttribute(Qt::WA_QuitOnClose, false);
  setAttribute(Qt::WA_DeleteOnClose, true);
  setAttribute(Qt::WA_KeyboardFocusChange, false);
  setAttribute(Qt::WA_AlwaysShowToolTips, false);
  setContextMenuPolicy(Qt::NoContextMenu);
  //
  resize(width, height);
  //
  if (!mMan || !mMan->floatHeight()) {
    mLayout = new QVBoxLayout(this);
    setFixedSize(QSize(width, height));
  } else {
    mLayout = new K8PopupLayout(width, this);
    mLayout->setSizeConstraint(QLayout::SetFixedSize);
  }
  //
  //mLayout = new QVBoxLayout(this);
  mLayout->setSpacing(0);
  mLayout->setContentsMargins(0, 0, 0, 0);
  //verticalLayout->setSpacing(4);
  //verticalLayout->setMargin(2);
  //
  mContentLabel = new QLabel(this);
  mContentLabel->setMinimumSize(QSize(0, 24));
  mContentLabel->setMaximumSize(QSize(16777215, 16777215));
  QSizePolicy szp = mContentLabel->sizePolicy();
  szp.setVerticalPolicy(QSizePolicy::Expanding);
  mContentLabel->setSizePolicy(szp);
  mContentLabel->setContextMenuPolicy(Qt::NoContextMenu);
  mContentLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
  //mContentLabel->setAlignment(Qt::AlignCenter);
  mContentLabel->setWordWrap(true);
  mContentLabel->setTextInteractionFlags(Qt::NoTextInteraction);
  mContentLabel->setTextFormat(Qt::RichText);
  mLayout->addWidget(mContentLabel);
  //
  mContentLabel->show();
  //
  mContentLabel->installEventFilter(this);
  installEventFilter(this);
}


bool K8PopupWin::eventFilter (QObject * /*obj*/, QEvent *event) {
  if (event->type() == QEvent::MouseButtonRelease) {
    QMouseEvent *mevent = (QMouseEvent *)event;
    //
    if (mMan) mMan->emitClick(this, mevent->button()); else close();
    return true;
  }
  return false;
}


void K8PopupWin::enterEvent (QEvent *event) {
  mMouseInside = true;
  QWidget::enterEvent(event);
}


void K8PopupWin::leaveEvent (QEvent *event) {
  mMouseInside = false;
  QWidget::leaveEvent(event);
  if (mShouldClose) close();
}


void K8PopupWin::buildAndSetMask (QWidget *w) {
  //qDebug() << "size:" << size();
  QPixmap pixmap(size());
  pixmap.fill(Qt::transparent);
  render(&pixmap, QPoint(), QRegion(), QWidget::DrawChildren | QWidget::IgnoreMask);
  w->clearMask();
  w->setMask(pixmap.mask());
}


void K8PopupWin::rebuildMask () {
  this->clearMask();
  if (isHidden()) {
    QPoint op = pos();
    move(10000, 10000);
    QWidget::show();
    buildAndSetMask(this);
    QWidget::hide();
    move(op);
  } else {
    QWidget::hide();
    QWidget::show();
    buildAndSetMask(this);
    QWidget::hide();
    QWidget::show();
  }
  //mMaskSet = true;
}


void K8PopupWin::show () {
#ifdef FADING
  if (mFadingOut) return;
#endif
  // cut edges (if any)
  rebuildMask();
  //
#ifdef FADING
  if (!mFaderIn) {
    mFaderIn = new K8PopupFader(this, false);
    connect(mFaderIn, SIGNAL(destroyed(QObject *)), this, SLOT(onFaderInDies(QObject *)));
    mFaderIn->setFadeColor(Qt::white);
    mFaderIn->start();
  }
#endif
  QWidget::show();
#ifdef FADING
  if (mFaderIn) {
    mFaderIn->resize(size());
    buildAndSetMask(mFaderIn);
  }
#endif
}


void K8PopupWin::getOut () {
#ifdef FADING
  if (mFadingOut) return;
  if (mFaderIn) mFaderIn->close();
  if (!mFaderOut) {
    mFaderOut = new K8PopupFader(this, true);
    connect(mFaderOut, SIGNAL(destroyed(QObject *)), this, SLOT(onFaderOutDies(QObject *)));
    mFaderOut->setFadeColor(QColor(255, 255, 255));
    buildAndSetMask(mFaderOut);
    mFaderOut->start();
  }
  mFadingOut = true;
#else
  close();
#endif
}


void K8PopupWin::setData (const QString &msg) {
  if (mType) {
    if (!mType->style.isEmpty()) mContentLabel->setStyleSheet(mType->style);
  }
  mContentLabel->setText(msg);
  rebuildMask();
}


///////////////////////////////////////////////////////////////////////////////
K8PopupManager::K8PopupManager (int width, int height, uint timeout, Position pos) :
  mPos(pos), mWidth(width), mHeight(height), mTimeout(timeout), mFloatHeight(false)
{
  mCheckTimer = new QTimer(this);
  connect(mCheckTimer, SIGNAL(timeout()), this, SLOT(onTimer()));
  //
  if (mWidth < 64) mWidth = 64; else if (mWidth > 750) mWidth = 750;
  if (mHeight < 62) mHeight = 64; else if (mHeight > 550) mHeight = 550;
  if (mTimeout < 1) mTimeout = 1; else if (mTimeout > 30) mTimeout = 30;
}


K8PopupManager::~K8PopupManager () {
  foreach (K8PopupWin *w, mList) {
    if (!w) continue;
    w->mMan = 0;
    w->mType = 0;
    delete w;
  }
  foreach (K8PopupType *t, mTypes) delete t;
}


void K8PopupManager::setTimeout (int timeout) {
  mTimeout = timeout;
  if (mTimeout < 1) mTimeout = 1; else if (mTimeout > 30) mTimeout = 30;
}


void K8PopupManager::addType (const QString &aName, const QString &aStyle) {
  K8PopupType *t;
  //
  if (mTypes.contains(aName)) {
    t = mTypes[aName];
    t->style = aStyle;
  } else {
    t = new K8PopupType(aName, aStyle);
    mTypes[aName] = t;
  }
}


void K8PopupManager::delType (const QString &aName) {
  if (!mTypes.contains(aName)) return;
  K8PopupType *t = mTypes[aName];
  foreach (K8PopupWin *w, mList) if (w && w->mType == t) w->mType = 0;
  mTypes.remove(aName);
  delete t;
}


bool K8PopupManager::hasType (const QString &aName) {
  return mTypes.contains(aName);
}


void K8PopupManager::showMessage (const QString &type, const QString &id, const QString &msg) {
  if (!mTypes.contains(type)) return;
  K8PopupType *t = mTypes[type];
  K8PopupWin *w = new K8PopupWin(this, mWidth, mHeight, t, id, msg);
  allocSlot(w);
  w->show();
  w->mShowTime = QDateTime::currentDateTime().toTime_t();
  if (mFloatHeight) rearrange();
}


void K8PopupManager::updateMessage (const QString &type, const QString &id, const QString &msg) {
  bool wasChanged = false;
  K8PopupType *t = 0;
  if (!type.isEmpty()) {
    if (!mTypes.contains(type)) return;
    t = mTypes[type];
  }
  foreach (K8PopupWin *w, mList) {
    if (!w->mMan || (t && w->mType != t) || w->mId != id) continue;
    w->setData(msg);
    w->mShowTime = QDateTime::currentDateTime().toTime_t();
    wasChanged = true;
  }
  if (!t) return;
  if (wasChanged) rearrange(); else showMessage(type, id, msg);
}


void K8PopupManager::onTimer () {
  uint now = QDateTime::currentDateTime().toTime_t();
  for (int f = mList.size()-1; f >= 0; f--) {
    K8PopupWin *w = mList[f];
    if (!w) continue;
    if (w->mShowTime+mTimeout > now) continue;
    if (w->mMouseInside) {
      //w->mShouldClose = true;
      w->mShowTime = QDateTime::currentDateTime().toTime_t();
    } else w->getOut();
  }
}


void K8PopupManager::allocSlot (K8PopupWin *w) {
  QDesktopWidget *desktop = QApplication::desktop();
  QRect geometry = desktop->availableGeometry();
  int x = 0, y = 0;
  //
  switch (mPos) {
    case LeftTop:
      x = geometry.left();
      y = geometry.top();
      break;
    case RightTop:
      x = geometry.right()-mWidth;
      y = geometry.top();
      break;
    case LeftBottom:
      x = geometry.left();
      y = geometry.bottom();
      break;
    case RightBottom:
      x = geometry.right()-mWidth;
      y = geometry.bottom();
      break;
    default: ;
  }
  if (w) {
    int idx = mList.indexOf(w);
    if (idx < 0) mList.append(w);
  }
  //
  QPoint curpos(QCursor::pos());
  //
  foreach (K8PopupWin *ww, mList) {
    int hh = ww->height()-1;
    //
    if (ww->mMouseInside) ww->mShowTime = QDateTime::currentDateTime().toTime_t();
    switch (mPos) {
      case LeftTop: case RightTop:
        if (y+hh > geometry.bottom()) {
          y = geometry.top();
          if (mPos == LeftTop) x += mWidth-1; else x -= mWidth-1;
        }
        ww->move(x, y);
        y += hh;
        break;
      case LeftBottom: case RightBottom:
        y -= hh;
        if (y < geometry.top()) {
          y = geometry.bottom();
          if (mPos == LeftBottom) x += mWidth-1; else x -= mWidth-1;
        }
        ww->move(x, y);
        break;
      default: ;
    }
    //
    QPoint wp(ww->mapFromGlobal(curpos));
    //
    ww->mMouseInside = (wp.x() >= 0 && wp.y() >= 0 && wp.x() < ww->width() && wp.y() < ww->height());
    if (ww->mMouseInside) ww->mShowTime = QDateTime::currentDateTime().toTime_t();
  }
  if (mList.size() == 0) {
    if (mCheckTimer->isActive()) mCheckTimer->stop();
  } else {
    if (!mCheckTimer->isActive()) mCheckTimer->start(500);
  }
}


void K8PopupManager::remove (K8PopupWin *wnd) {
  if (wnd) {
    int idx = mList.indexOf(wnd);
    if (idx < 0) return;
    mList.remove(idx);
  }
  rearrange();
}


void K8PopupManager::rearrange () {
  allocSlot(0);
}


void K8PopupManager::emitClick (K8PopupWin *w, Qt::MouseButton button) {
  if (!w || !w->mMan) return;
  if (!w->mType) return;
#ifdef FADING
  if (w->mFadingOut) return;
#endif
  emit click(w->mType->name, w->mId, button);
  w->getOut();
}


void K8PopupManager::removeAll () {
  foreach (K8PopupWin *w, mList) {
    w->mMan = 0;
    w->close();
  }
  mList.clear();
  if (mCheckTimer->isActive()) mCheckTimer->stop();
}


void K8PopupManager::removeById (const QString &type, const QString &id) {
  K8PopupType *t = 0;
  if (!type.isEmpty()) {
    if (!mTypes.contains(type)) return;
    t = mTypes[type];
  }

  for (int f = 0; f < mList.size(); ) {
    K8PopupWin *w = mList[f];
    if (!w->mMan || (t && w->mType != t) || w->mId != id) {
      f++;
      continue;
    }
    mList[f]->mMan = 0;
    mList[f]->close();
    mList.remove(f);
  }
  rearrange();
}
