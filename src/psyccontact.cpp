/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include "psyccontact.h"


MyMessage::MyMessage (const PsycEntity &aUser, const QString &aText, const QString &aAction, const QDateTime &aTime) :
    text(aText), action(aAction), time(aTime), user(aUser)
{
}


///////////////////////////////////////////////////////////////////////////////
PsycContact::PsycContact (const QString &uni, bool aTemp, QObject *parent) :
  QObject(parent), mEntity(new PsycEntity(uni)),
  mLastSeen(0),
  mAuthorized(false), mGroupName(QString()),
  mHidden(false),
  mTemp(aTemp), mIsOTRActive(false), mIsOTRVerified(false)
{
  mIsPlace = mEntity->isPlace();
}


PsycContact::~PsycContact () {
  clearMessages();
  clearPersons();
  delete mEntity;
}


///////////////////////////////////////////////////////////////////////////////
void PsycContact::addMessage (const PsycEntity &aUser, const QString &aText, const QString &aAction, const QDateTime &aTime) {
  MyMessage *msg = new MyMessage(aUser, aText, aAction, aTime);
  mMsgList << msg;
}


void PsycContact::clearMessages () {
  foreach (MyMessage *msg, mMsgList) delete msg;
  mMsgList.clear();
}


QList<MyMessage *> PsycContact::messages () {
  QList<MyMessage *> res(mMsgList);
  return res;
}


///////////////////////////////////////////////////////////////////////////////
void PsycContact::addPerson (const QString &uni) {
  if (mPersonList.indexOf(uni.toLower()) < 0) mPersonList << uni;
}


void PsycContact::delPerson (const QString &uni) {
  int idx = mPersonList.indexOf(uni.toLower());
  if (idx >= 0) mPersonList.removeAt(idx);
}


bool PsycContact::hasPerson (const QString &uni) const {
  return mPersonList.contains(uni.toLower());
}


QStringList PsycContact::persons () const {
  return mPersonList;
}


QString PsycContact::person (int idx) const {
  if (idx < 0 || idx >= mPersonList.count()) return QString("");
  return mPersonList[idx];
}
