/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include "keycmb.h"


typedef struct {
  int key;
  const char *name;
} tKeyCN;


static const tKeyCN keyList[] = {
{Qt::Key_Escape, "ESC"},
{Qt::Key_Tab, "TAB"},
//{Qt::Key_Backtab, "???"},
{Qt::Key_Backspace, "BSP"},
{Qt::Key_Return, "RET"},
//{Qt::Key_Enter, "RET"},
{Qt::Key_Enter, "KPRET"},
{Qt::Key_Insert, "INS"},
{Qt::Key_Delete, "DEL"},
{Qt::Key_Home, "HOME"},
{Qt::Key_End, "END"},
{Qt::Key_Left, "LEFT"},
{Qt::Key_Up, "UP"},
{Qt::Key_Right, "RIGHT"},
{Qt::Key_Down, "DOWN"},
{Qt::Key_PageUp, "PRIOR"},
{Qt::Key_PageDown, "NEXT"},
{Qt::Key_F1, "F1"},
{Qt::Key_F2, "F2"},
{Qt::Key_F3, "F3"},
{Qt::Key_F4, "F4"},
{Qt::Key_F5, "F5"},
{Qt::Key_F6, "F6"},
{Qt::Key_F7, "F7"},
{Qt::Key_F8, "F8"},
{Qt::Key_F9, "F9"},
{Qt::Key_F10, "F10"},
{Qt::Key_F11, "F11"},
{Qt::Key_F12, "F12"},
{Qt::Key_F13, "F13"},
{Qt::Key_F14, "F14"},
{Qt::Key_F15, "F15"},
{Qt::Key_F16, "F16"},
{Qt::Key_F17, "F17"},
{Qt::Key_F18, "F18"},
{Qt::Key_F19, "F19"},
{Qt::Key_F20, "F20"},
{Qt::Key_F21, "F21"},
{Qt::Key_F22, "F22"},
{Qt::Key_F23, "F23"},
{Qt::Key_F24, "F24"},
{Qt::Key_F25, "F25"},
{Qt::Key_F26, "F26"},
{Qt::Key_F27, "F27"},
{Qt::Key_F28, "F28"},
{Qt::Key_F29, "F29"},
{Qt::Key_F30, "F30"},
{Qt::Key_F31, "F31"},
{Qt::Key_F32, "F32"},
{Qt::Key_F33, "F33"},
{Qt::Key_F34, "F34"},
{Qt::Key_F35, "F35"},
{0, 0}
};


typedef enum {
  emCtrl=0x01,
  emMeta=0x02,
  emShift=0x04,
  emHyper=0x08
} eParserMod;


QString KeyComboList::normalizeKbd (const QString &kbd, bool strictShift) {
  unsigned char mods = 0;
  QString s(kbd.trimmed()), res, t;
  if (s.isEmpty()) return QString();
  int pos = 0, mFlag;
  while (pos+1 < s.length() && s.at(pos+1) == '-') {
    switch (s.at(pos).unicode()) {
      case 'C': mFlag = emCtrl; break;
      case 'M': mFlag = emMeta; break;
      case 'S': mFlag = emShift; break;
      case 'H': mFlag = emHyper; break;
      default: mFlag = 0;
    }
    if (!mFlag || (mods & mFlag)) return QString();
    mods |= mFlag;
    pos += 2;
  }
  if (pos) s = s.mid(pos);
  if (s.length() == 1) {
    if (mods & emShift) {
      mods &= ~emShift;
      s = s.toUpper();
    } else if (strictShift) s = s.toLower();
    if (mods == emCtrl) s = s.toLower();
  }
  if (mods & emCtrl) res += "C-";
  if (mods & emMeta) res += "M-";
  if (mods & emHyper) res += "H-";
  if (mods & emShift) res += "S-";
  //qDebug() << "key:" << t << "rmod:" << res;
  if (s.length() > 1) {
    for (int f = 0; keyList[f].key; f++) {
      QString kn(keyList[f].name);
      if (kn.compare(s, Qt::CaseInsensitive) == 0) {
        // key found!
        res += kn;
        //qDebug() << "res:" << res;
        return res;
      }
    }
  } else {
    res += s;
    //qDebug() << "res:" << res;
    return res;
  }
  //qDebug() << "wtf?";
  return QString();
}


QString KeyComboList::event2Kbd (QKeyEvent *keyev) {
  QString res;
  // check modifiers
  Qt::KeyboardModifiers md = keyev->modifiers();
  if (md & Qt::ControlModifier) res += "C-";
  if (md & Qt::AltModifier) res += "M-";
  if (md & Qt::MetaModifier) res += "H-";
  if (md & Qt::ShiftModifier) res += "S-";
  int k = keyev->key();
  if (k >= 32 && k <= 0xff00) {
    res += QChar((uint)k);
    return normalizeKbd(res, true);
  }
  for (int f = 0; keyList[f].key; f++) {
    if (keyList[f].key == k) {
      res += keyList[f].name;
      return normalizeKbd(res, true);
    }
  }
  return QString();
}


///////////////////////////////////////////////////////////////////////////////
KeyComboList::KeyComboList () {
  clear();
}


KeyComboList::~KeyComboList () {
}


void KeyComboList::clear () {
  mBinds.clear();
  mCombos.clear();
  reset();
}


bool KeyComboList::bind (const QString &kn, const QString &cmd) {
  QString newK;
  QStringList cl(kn.split(' ', QString::SkipEmptyParts));
  //qDebug() << cl;
  foreach (const QString &s, cl) {
    QString t(normalizeKbd(s));
    //qDebug() << "s:" << s << "t:" << t;
    if (t.isEmpty()) {
      qWarning() << "invalid keycombo:" << s;
      return false;
    }
    if (!newK.isEmpty()) newK += ' ';
    newK += t;
  }
  if (cmd.isEmpty()) {
    mBinds.remove(newK);
    int f; while ((f = mCombos.indexOf(newK)) >= 0) mCombos.removeAt(f);
  } else {
    mBinds[newK] = cmd;
    if (mCombos.indexOf(newK) < 0) mCombos << newK;
  }
  return true;
}


QString KeyComboList::process (QKeyEvent *keyev) {
  //qDebug() << "key:" << keyev->key();
  QString e(event2Kbd(keyev));
  if (e.isEmpty()) return QString();
  qDebug() << "ev:" << e << "cmb:" << mCurCombo << "cond:" << mCurCombo+" "+e;
  mCurComboText += keyev->text();
  bool first = mCurCombo.isEmpty();
  if (!first) mCurCombo += ' ';
  mCurCombo += e;
  bool hasMore = false;
  QString ee(mCurCombo+' ');
  //qDebug() << "first:" << first << "cmb:" << mCurCombo;
  for (int f = 0, len = mCombos.length(); f < len; f++) {
    //qDebug() << "at f:" << mCombos[f] << "cur:" << mCurCombo;
    if (mCombos[f] == mCurCombo) {
      // hit it!
      mLastCombo = mCurCombo;
      mLastComboText = mCurComboText;
      reset();
      return mBinds[mLastCombo];
    }
    if (mCombos[f].startsWith(ee)) hasMore = true;
  }
  if (!hasMore) {
    // not found
    reset();
    if (first) return QString();
    return QString(" ?");
  }
  //qDebug() << "HAS MORE";
  return QString(" ");
}


void KeyComboList::reset () {
  mCurCombo.clear();
  mCurComboText.clear();
}


//FIXME: slooooow!
static QString quoteStr (const QString &s) {
  QString res;
  res.reserve(s.size()+2); // at least
  res += '"';
  for (int f = 0; f < s.size(); ++f) {
    QChar ch = s[f];
    if (ch == '"' || ch == '\\') { res += '\\'; res += ch; }
    else if (ch == '\t') res += "\\t";
    else if (ch == '\n') res += "\\n";
    else if (ch.unicode() <= 32 || ch.unicode() >= 0xff00) res += ' ';
    else res += ch;
  }
  res += '"';
  return res;
}


QStringList KeyComboList::toString () const {
  QStringList res;
  foreach (const QString &kk, mCombos) {
    res << quoteStr(kk);
    res << quoteStr(mBinds[kk]);
  }
  return res;
}


bool KeyComboList::isProcessing () const {
  return !(mCurCombo.isEmpty());
}


void KeyComboList::appendFrom (KeyComboList *kl) {
  if (!kl) return;
  foreach (const QString &kk, kl->mCombos) bind(kk, kl->mBinds[kk]);
}
