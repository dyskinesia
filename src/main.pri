QT += gui webkit
CONFIG += uitools

DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD

include($$PWD/k8utils/k8utils.pri)
include($$PWD/k8tpl/k8tpl.pri)
include($$PWD/k8popups/k8popups.pri)
include($$PWD/k8sdbjs/k8sdb.pri)
include($$PWD/ghotkey/ghotkey.pri)

CONFIG(json_history) {
  DEFINES += HISTORY_OK
  DEFINES += JSON_HISTORY
  include($$PWD/k8jshistory/k8jshistory.pri)
}

!CONFIG(json_history) {
  CONFIG(use_leveldb) {
    DEFINES += HISTORY_OK
    DEFINES += LDB_HISTORY
    include($$PWD/k8ldbhistory/k8ldbhistory.pri)
  }
  !CONFIG(use_leveldb) {
    CONFIG(use_bhf) {
      DEFINES += HISTORY_OK
      DEFINES += BHF_HISTORY
      include($$PWD/k8bhfhistory/k8bhfhistory.pri)
    }
  }
}

include($$PWD/k8history/k8history.pri)

DEFINES += K8JSON_INCLUDE_WRITER
DEFINES += K8JSON_INCLUDE_GENERATOR
DEFINES += K8JSON_INCLUDE_COMPLEX_GENERATOR
include($$PWD/k8json/k8json.pri)


HEADERS += \
  $$PWD/dlogf.h \
  $$PWD/main.h \
  $$PWD/authwin.h \
  $$PWD/accwin.h \
  $$PWD/conwin.h \
  $$PWD/pktwin.h \
  $$PWD/joinwin.h \
  $$PWD/chatform.h \
  $$PWD/genkeywin.h \
  $$PWD/settings.h \
  $$PWD/k8webview.h \
  $$PWD/psyccontact.h \
  $$PWD/winutils.h \
  $$PWD/floatwin.h \
  $$PWD/dysversion.h \
  $$PWD/keycmb.h \
  $$PWD/eng_commands.h \
  $$PWD/eng_bindings.h \
  $$PWD/floatman.h \
  $$PWD/chatjsapi.h \


SOURCES += \
  $$PWD/dlogf.c \
  $$PWD/main.cpp \
  $$PWD/accwin.cpp \
  $$PWD/conwin.cpp \
  $$PWD/pktwin.cpp \
  $$PWD/joinwin.cpp \
  $$PWD/chatform.cpp \
  $$PWD/genkeywin.cpp \
  $$PWD/settings.cpp \
  $$PWD/k8webview.cpp \
  $$PWD/psyccontact.cpp \
  $$PWD/winutils.cpp \
  $$PWD/floatwin.cpp \
  $$PWD/keycmb.cpp \
  $$PWD/eng_commands.cpp \
  $$PWD/eng_bindings.cpp \
  $$PWD/floatman.cpp \
  $$PWD/chatjsapi.cpp \


FORMS += \
  $$PWD/ui/authwin.ui \
  $$PWD/ui/accwin.ui \
  $$PWD/ui/chatform.ui \
  $$PWD/ui/console.ui \
  $$PWD/ui/genkeywin.ui \
  $$PWD/ui/pktform.ui \
  $$PWD/ui/joinwin.ui \
