/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QString>


QString settingsAppPath ();
QString settingsPrefsPath ();
QString settingsMainDBFile ();
QString settingsHtmlPath ();
QString settingsDBFile (const QString &aUNI);
QString settingsDBPath (const QString &aUNI);
QString settingsHistoryPath (const QString &aUNI);
QString settingsHistoryFile (const QString &aUNI, const QString &userUNI);
QString settingsUnreadHistoryFile (const QString &aUNI, const QString &userUNI);


#endif
