/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef K8JSHISTORY_H
#define K8JSHISTORY_H

#include <QDateTime>
#include <QFile>
#include <QString>
#include <QList>

//#include "k8history.h"


class HistoryFile;


class HistoryMessage : public HistoryMessageBase {
  friend class HistoryFile;

public:
  HistoryMessage () : HistoryMessageBase() {}
  virtual ~HistoryMessage () {}

private:
  const uchar *parse (const uchar *ptr, int *maxleft, const QString &myUni);
};


class HistoryFile : public HistoryFileBase {
  friend class HistoryMessage;

public:
  HistoryFile (const QString &fname, const QString &myUni);
  virtual ~HistoryFile ();

  virtual bool open (OpenMode newmode);
  //virtual bool open (bool allowCreate);
  virtual bool isOpen () const;
  //virtual bool exists () const;
  virtual void close ();
  virtual void remove ();

  virtual int count ();

  virtual bool append (const HistoryMessage &msg);

  /* idx: 0..count-1; idx<0: idx += count; */
  virtual bool read (int idx, HistoryMessage &msg);

private:
  bool scanMessages ();

private:
  int mCount; // -1: not counted yet
  QFile mFile;
  uchar *mMap;
  int mSize;
  QString mMyUni;
  QList<uint> mMsgOfs; // [0..mCount] (the last one marks position for adding new message)
};


#endif
