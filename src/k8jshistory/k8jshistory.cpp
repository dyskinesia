/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QDir>

#include "k8strutils.h"
#include "k8json.h"

#include "k8history.h"
//#include "k8jshistory.h"


//#define KH_DEBUG


///////////////////////////////////////////////////////////////////////////////
#define KHISTORY_DATE_FORMAT  "yyyy/MM/dd HH:mm:ss"
/*
static QString dateToString (const QDateTime &dt) {
  if (dt.isNull()) return QDateTime::currentDateTime().toString(KHISTORY_DATE_FORMAT);
  return dt.toString(KHISTORY_DATE_FORMAT);
}
*/


///////////////////////////////////////////////////////////////////////////////
const uchar *HistoryMessage::parse (const uchar *ptr, int *maxleft, const QString &myUni) {
  valid = false;
  QVariant rec;
  const uchar *res = K8JSON::parseRecord(rec, ptr, maxleft);
  if (!res) return 0; // alas
  if (rec.type() != QVariant::Map) return res; // alas
  QVariantMap m(rec.toMap());
  uni = m["uni"].toString().trimmed();
  action = m["action"].toString();
  text = m["text"].toString();
  rec = m["timestamp"];
  switch (rec.type()) {
    case QVariant::Int:
    case QVariant::UInt:
      date = QDateTime::fromTime_t(rec.toUInt()).toLocalTime();
      break;
    default: date = QDateTime::currentDateTime();
  }
  if (!myUni.compare(uni, Qt::CaseInsensitive)) type = Outgoing;
  else if (!uni.compare("info", Qt::CaseInsensitive)) type = Info;
  else if (!uni.compare("server", Qt::CaseInsensitive)) type = Server;
  else type = Incoming;
  valid = true;
  return res;
}
/*
  "uni": "psyc://psyced.org/~lynX",
  "timestamp": 1235744956, //2009/02/27 16:29:16
  "action": "expels",
  "text": "and that friend is the president now"
*/


///////////////////////////////////////////////////////////////////////////////
HistoryFile::HistoryFile (const QString &fname, const QString &myUni) :
  mCount(-1), mFile(fname+".json"), mMap(0), mSize(-1), mMyUni(myUni)

{
  //qDebug() << fname;
}


HistoryFile::~HistoryFile () {
  close();
}


bool HistoryFile::isOpen () const { return mFile.isOpen(); }
//bool HistoryFile::exists () const { return mFile.exists(); }
int HistoryFile::count () { return mCount; }


void HistoryFile::remove () {
  close();
  mFile.remove();
}


//bool HistoryFile::open (bool allowCreate)
bool HistoryFile::open (OpenMode newmode) {
  close();
  bool newFile = false;
  if (!mFile.exists()) {
    if (newmode != Write) return false;
    QString dir(mFile.fileName());
    int idx = dir.lastIndexOf('/');
    if (idx > 0) {
      dir.truncate(idx);
      QDir d;
      d.mkdir(dir);
    }
    newFile = true;
  }
  if (!mFile.open(newmode==Write?QIODevice::ReadWrite:QIODevice::ReadOnly)) return false;
  if (newFile) {
    mFile.write("//Dyskinesia JS history file\n");
    mFile.write("//'timestamp' is UTC unixtime, but commented date is local\n");
  }
  mSize = mFile.size();
  if (!(mMap = mFile.map(0, mSize))) {
    close();
    return false;
  }
  if (!scanMessages()) {
    close();
    return false;
  }
  return true;
}


bool HistoryFile::scanMessages () {
  //fprintf(stderr, "scanning...\n");
  mCount = 0;
  int len = mSize;
  const uchar *sj = K8JSON::skipBlanks(mMap, &len);
  if (!sj || !sj[0]) {
    // no messages at all
    //fprintf(stderr, "empty file.\n");
    mMsgOfs << mSize;
    return true;
  }
  if (*sj++ != '[') {
    // invalid ON file
    //fprintf(stderr, "not a list (%u).\n", sj[-1]);
    return false;
  }
  len--;
  while (len > 0) {
    sj = K8JSON::skipBlanks(sj, &len);
    if (!sj || *sj != '{') {
      // invalid ON file
      //fprintf(stderr, "bad (%u) (%c).\n", sj[0], sj[0]);
      return false;
    }
    mMsgOfs << (int)(sj-mMap); // save current message start
    //fprintf(stderr, "ofs: 0x%08x\n", mMsgOfs[mCount]);
    mCount++;
    sj = K8JSON::skipRec(sj, &len);
    if (!sj) return false; // invalid ON file
    switch (*sj) {
      case ',': break;
      case ']':
        // eof
        mMsgOfs << (int)(sj-mMap); // save last message end
/*
        fprintf(stderr, "---------------------\n");
        for (int f = 0; f < mMsgOfs.count(); f++) {
          fprintf(stderr, "%3i: 0x%08x\n", f, (uint)(mMsgOfs[f]));
        }
*/
        return true; // done
      default: return false;
    }
    //fprintf(stderr, " end: 0x%08x\n", (uint)(sj-mMap));
    len--; sj++; // skip ','
  }
  return false; // no eof
}


void HistoryFile::close () {
  if (mFile.isOpen()) {
    if (mMap) mFile.unmap(mMap);
    mFile.close();
  }
  mCount = -1; mSize = -1;
  mMap = 0;
  mMsgOfs.clear();
}


/*
 *FIXME: check for errors!
 */
bool HistoryFile::append (const HistoryMessage &msg) {
  if (!mFile.isOpen()) return false;
  if (mMap) {
    mFile.unmap(mMap);
    mMap = 0;
  }
  mFile.resize(mMsgOfs[mCount]); // trunc file
  mFile.seek(mFile.size());
  if (!mCount) {
    // new file, no entries
    mFile.write("[\n ");
  } else mFile.write(",");
  // save start of the new message
  mMsgOfs[mCount] = mFile.size();

  mFile.write("{//#");
  QByteArray ba(QString::number(mCount).toAscii());
  mFile.write(ba);

  mFile.write("\n  \"uni\": ");
  ba = K8JSON::quote(msg.uni).toUtf8();
  mFile.write(ba);

  mFile.write(",\n  \"timestamp\": ");
  QDateTime utc(msg.date.toUTC());
  ba = QString::number(utc.toTime_t()).toAscii();
  mFile.write(ba);
  mFile.write(", //");
  ba = msg.date.toString(KHISTORY_DATE_FORMAT).toAscii();
  mFile.write(ba);
  mFile.write("\n  ");

  if (!msg.action.isEmpty()) {
    mFile.write("\"action\": ");
    ba = K8JSON::quote(msg.action).toUtf8();
    mFile.write(ba);
    mFile.write(",\n  ");
  }
  mFile.write("\"text\": ");
  ba = K8JSON::quote(msg.text).toUtf8();
  mFile.write(ba);
  mFile.write("\n }\n");

  mCount++;
  // save end of the written message
  mMsgOfs << mFile.size();
  mFile.write("]\n");

  if (!(mMap = mFile.map(0, mSize))) {
    close();
    return false;
  }

  return true;
}


bool HistoryFile::read (int idx, HistoryMessage &msg) {
  if (!mMap) return false;
  if (idx < 0) idx += mCount;
  if (idx < 0 || idx >= mCount) return false;
  int left = mSize-(int)mMsgOfs[idx];
  if (!msg.parse(mMap+mMsgOfs[idx], &left, mMyUni)) return false;
  return true;
}
