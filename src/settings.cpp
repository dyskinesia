/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>
#include <QCoreApplication>
#include <QDir>

#include "settings.h"


static QString spathStr;
static QString apathStr;


inline static bool isSpecial (const QChar &ch) {
  return
    ch == '*' ||
    ch == '?' ||
    ch == ':' ||
    ch == '/' ||
    ch == '\\' ||
    ch == '<' ||
    ch == '>' ||
    ch == '|' ||
    ch == '&'
  ;
}


static QString normUNI (const QString &str) {
  QString res(str);
  for (int f = res.length()-1; f >= 0; f--) {
    QChar ch(res[f]);
    if (!ch.isLetterOrNumber() && (ch.unicode() > 126 || isSpecial(ch))) res[f] = '_';
  }
  return res.toLower();
}


static QString apath () {
  if (apathStr.isEmpty()) {
    apathStr = QCoreApplication::applicationDirPath();
    if (apathStr.isEmpty()) apathStr += "./";
    else if (apathStr[apathStr.length()-1] != '/' && apathStr[apathStr.length()-1] != '\\') apathStr += "/";
  }
  return apathStr;
}


#define PREFS_DIR_NAME  "dyskprefs"
static QString spath (const QString &uni=QString("")) {
  if (spathStr.isEmpty()) {
    QString lsp(apath());
    lsp += "." PREFS_DIR_NAME "/";
    QDir lsd(lsp);
    if (!lsd.exists()) {
      QString hsp = QDir::homePath();
      if (!hsp.isEmpty() && (hsp[hsp.length()-1] != '/' && hsp[hsp.length()-1] != '\\')) hsp += '/';
      hsp += "." PREFS_DIR_NAME "/";
      //QDir hsd(res);
      spathStr = hsp;
    } else spathStr = lsp;
  }
  if (uni.isEmpty()) return spathStr;
  QString res(spathStr);
  res += normUNI(uni);
  res += "/";
  return res;
}


///////////////////////////////////////////////////////////////////////////////
QString settingsPrefsPath () {
  return spath();
}


QString settingsAppPath () {
  return apath();
}


QString settingsMainDBFile () {
  return QString(spath()+"profiles.ddb");
}


QString settingsHtmlPath () {
  return QString(apath()+"html/");
}


QString settingsDBPath (const QString &aUNI) {
  return spath(aUNI);
}


QString settingsDBFile (const QString &aUNI) {
  return QString(spath(aUNI)+"profile.ddb");
}


QString settingsHistoryPath (const QString &aUNI) {
  return spath(aUNI)+"history/";
}


QString settingsHistoryFile (const QString &aUNI, const QString &userUNI) {
  return QString(spath(aUNI)+"history/"+normUNI(userUNI));
}


QString settingsUnreadHistoryFile (const QString &aUNI, const QString &userUNI) {
  return QString(spath(aUNI)+"history/unread_"+normUNI(userUNI));
}
