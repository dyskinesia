/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <ctype.h>

#include <QtCore>
#include <QDebug>

#include <QCryptographicHash>
#include <QDateTime>
#include <QRegExp>
#include <QTcpSocket>
#include <QTimer>
#include <QUrl>

#include "psycvars.h"
#include "psycpkt.h"

#include "psycproto.h"


///////////////////////////////////////////////////////////////////////////////
static bool isValidPSYCNick (const QString &nick) {
  if (nick.isEmpty() || nick.length() > 255) return false; // 255 is my own invention
  for (int f = nick.length()-1; f >= 0; f--) {
    QChar sch(nick[f]);
    ushort uch = sch.unicode();
    if (uch < 33 || uch > 126) return false; // no specials or unicode in nicks
    char ch = uch;
    if (isdigit(ch)) {
      if (f == 0) return false; // nick can't start with digit
    } else if (!isalpha(ch)) {
      if (ch != '-' && ch != '_' && ch != '=' && ch != '+' && ch != '.' && ch != '[' && ch != ']' &&
          ch != '<' && ch != '>') return false;
      if (f == 0 && (ch == '=' || ch == '-')) return false; // illegal 1st char
    }
  }
  return true;
}


static bool isValidXMPPNick (const QString &nick) {
  if (nick.isEmpty() || nick.length() > 255) return false; // 255 is my own invention
  if (nick == "<agent>") return true;
  for (int f = nick.length()-1; f >= 0; f--) {
    QChar sch(nick[f]);
    ushort uch = sch.unicode();
    if (uch < 33 || uch > 126) return false; // no specials or unicode in nicks
    char ch = uch;
    if (!isalnum(ch)) {
      //FIXME: it's my own rules, should be fixed!
      if (ch != '-' && ch != '_' && ch != '.' && ch != '*' && ch != '#' &&
          ch != '~') return false; // not sure about '~'
    }
  }
  return true;
}


static bool isValidIRCNick (const QString &nick) {
  if (nick.isEmpty() || nick.length() > 255) return false; // 255 is my own invention
  for (int f = nick.length()-1; f >= 0; f--) {
    QChar sch(nick[f]);
    ushort uch = sch.unicode();
    if (uch < 33 || uch > 126) return false; // no specials or unicode in nicks
    char ch = uch;
    if (!isalnum(ch)) {
      //FIXME: it's my own rules, should be fixed!
      if (ch != '-' && ch != '_' && ch != '.' && ch != '[' && ch != ']') return false;
    }
  }
  return true;
}


static bool isValidHost (const QString &host) {
  int llen = 0, slen = host.length();
  if (slen < 1 || slen > 255) return false; // RFC, afair
  for (int f = 0; f < slen; f++) {
    QChar sch(host[f]);
    ushort uch = sch.unicode();
    if (uch < 33 || uch > 126) return false; // no specials or unicode
    char ch = uch;
    if (!isalnum(ch)) {
      if (ch == '.') llen = -1;
      else if (ch != '-') return false; // other chars are bad-bad-bad! (fuck off, windoze!)
    }
    if (++llen > 63) return false; // RFC, afair
  }
  return true;
}


static QString buildUNI (const QString &proto, const QString &host, quint16 port, const QString &nick,
  const QString &chan=QString(""), bool isPlace=false)
{
  QString res("");
  if (!isValidHost(host) || nick.isEmpty()) return res;
  if (!proto.compare("psyc", Qt::CaseInsensitive)) {
    if (!isValidPSYCNick(nick)) return res;
    res = "psyc://"+host.toLower();
    if (port && port != 4404) res += QString(":%1").arg(port);
    if (isPlace) res += "/@"+nick; else res += "/~"+nick;
    if (!chan.isEmpty() && chan != "#") {
      QString c(chan);
      if (c[0] == '#') c.remove(0, 1);
      if (!isValidPSYCNick(c)) return QString("");
      res += '#';
      res += c;
    }
  } else if (!proto.compare("xmpp", Qt::CaseInsensitive)) {
    if (!isValidXMPPNick(nick)/* || !chan.isEmpty()*/) return res;
    if (nick == "<agent>") res = "xmpp:"+host.toLower();
    else res = "xmpp:"+nick+"@"+host.toLower();
    if (res.length() > 3071) return QString(""); // xmpp standard enforces this
  } else if (!proto.compare("irc", Qt::CaseInsensitive)) {
    if (!isValidIRCNick(nick)/* || !chan.isEmpty()*/) return res;
    res = "irc://"+host.toLower();
    if (isPlace) res += "/@"+nick; else res += "/~"+nick;
  }
  return res;
}


///////////////////////////////////////////////////////////////////////////////
PsycEntity::PsycEntity () :
  mProto(QString()), mHost(QString()), mPort(0), mNick(QString()), mVerbatim(QString()),
  mUNI(QString()), mStatus(PsycProto::Offline), mStatusText(QString()), mMood(PsycProto::Unspecified),
  mPlace(false)
{
}


PsycEntity::PsycEntity (const QString &aUNI) :
  mProto(QString()), mHost(QString()), mPort(0), mNick(QString()), mVerbatim(QString()),
  mUNI(QString()), mStatus(PsycProto::Offline), mStatusText(QString()), mMood(PsycProto::Unspecified),
  mPlace(false)
{
  setUNI(aUNI);
}


PsycEntity::PsycEntity (const PsycEntity &user) {
  *this = user;
}


PsycEntity::~PsycEntity () {
  //clear();
}


PsycEntity &PsycEntity::operator = (const PsycEntity &user) {
  if (&user != this) {
    mProto = user.mProto;
    mHost = user.mHost;
    mPort = user.mPort;
    mNick = user.mNick;
    mVerbatim = user.mVerbatim;
    mChan = user.mChan;
    mUNI = user.mUNI;
    mStatus = user.mStatus;
    mStatusText = user.mStatusText;
    mMood = user.mMood;
    mNotification = user.mNotification;
    mPlace = user.mPlace;
  }
  return *this;
}


void PsycEntity::clear () {
  mHost = QString();
  mPort = 0;
  mNick = QString();
  mVerbatim = QString();
  mChan = QString();
  mUNI = QString();
  mStatus = PsycProto::Offline;
  mStatusText = QString();
  mMood = PsycProto::Unspecified;
  mNotification = PsycProto::None;
  mPlace = false;
}


bool PsycEntity::isValidPSYCNick (const QString &nick) {
  return ::isValidPSYCNick(nick);
}


/*
 * relax and allow psyc://a.b/~u#n
 */
bool PsycEntity::parsePSYC (const QString &aUNI) {
  QUrl url(aUNI);
  if (!url.isValid()) return false;
  if (url.scheme().toLower() != "psyc" || url.host().isEmpty()) return false;
  QString nick(url.path());
  if (nick.length() < 3 || nick[0] != '/' || (nick[1] != '~' && nick[1] != '@')) return false; // expect "/~..." or "/@..."
  bool place = (nick[1] == '@');
  nick.remove(0, 2);
  int sidx = nick.indexOf('/');
  if (sidx == 0) return false; // "/~/" is very bad
  if (sidx > 0) {
    int hidx = nick.indexOf('#');
    if (hidx < 0 || hidx > sidx) {
      // '#' is not here or after '/'; remove '/...' till '#'
      nick.remove(sidx, hidx-sidx); //CHECKME!
    }
  }
  if (!isValidPSYCNick(nick)) return false;
  mProto = "psyc";
  mHost = url.host();
  mPort = url.port()<=0?4404:url.port();
  mNick = nick;
  mVerbatim = nick;
  mPlace = place;
  mChan = url.fragment().trimmed(); //FIXME: is trimmed() really necessary?
  if (mChan.startsWith('#')) mChan.remove(0, 1);
  mUNI = buildUNI(mProto, mHost, mPort, mNick, mChan, mPlace);
  return true;
}


/*
 * relax and allow a@b/res
 * also allow xmpp:ag.server.com
 */
bool PsycEntity::parseXMPP (const QString &aUNI) {
  if (aUNI.left(5).toLower() != "xmpp:") return false;
  QUrl url("xmpp://"+aUNI.mid(5));
  if (!url.isValid()) return false;
  if (!url.password().isEmpty() ||
      /*url.userName().isEmpty() ||*/
      url.host().isEmpty()/* ||
      !url.path().isEmpty() ||
      !url.fragment().isEmpty()*/ ||
      url.port() != -1) return false;
  mProto = "xmpp";
  mHost = url.host();
  mPort = 0;
  mNick = url.userName();
  if (mNick.isEmpty()) {
    int t = mHost.indexOf('.');
    mNick = "<agent>";
    mVerbatim = "xmpp-agent";
    if (t > 0) {
      mVerbatim += " (";
      mVerbatim.append(mHost.left(t));
      mVerbatim += ")";
    } else {
    }
  } else mVerbatim = mNick;
  mChan = url.path()+url.fragment();
  if (mChan.startsWith('/')) mChan.remove(0, 1);
  mPlace = (mNick[0] == '*'); // no way to determine this, another one xmpp fail %-(
  mUNI = buildUNI(mProto, mHost, mPort, mNick, mChan, mPlace);
  return true;
}


bool PsycEntity::parseIRC (const QString &aUNI) {
  QUrl url(aUNI);
  if (!url.isValid()) return false;
  if (url.scheme() != "irc" || url.host().isEmpty()) return false;
  QString nick(url.path());
  if (nick.length() < 3 || nick[0] != '/' || (nick[1] != '~' && nick[1] != '@')) return false; // expect "/~..." or "/@..."
  bool place = (nick[1] == '@');
  nick.remove(0, 2);
  if (!isValidIRCNick(nick)) return false;
  mProto = "irc";
  mHost = url.host();
  mPort = 0;
  mNick = nick;
  mVerbatim = nick;
  mPlace = place;
  mChan.clear(); // allow chans?
  mUNI = buildUNI(mProto, mHost, mPort, mNick, mChan, mPlace);
  return true;
}


bool PsycEntity::setUNI (const QString &aUNI) {
  return parsePSYC(aUNI) || parseXMPP(aUNI) || parseIRC(aUNI);
}


bool PsycEntity::setNick (const QString &nick) {
  QString s(buildUNI(mProto, mHost, mPort, nick, mChan, mPlace));
  if (s.isEmpty() && !isXMPP()) return false;
  mNick = nick;
  mUNI = s;
  return true;
}


bool PsycEntity::setVerbatim (const QString &nick) {
  QString t(nick.trimmed());
  if (t.isEmpty()) {
    mVerbatim = mNick;
    return true;
  }
  mVerbatim = t;
  return true;
}


bool PsycEntity::setHost (const QString &host) {
  QString s(buildUNI(mProto, host, mPort, mNick, mChan, mPlace));
  if (s.isEmpty()) return false;
  mHost = host;
  mUNI = s;
  return true;
}


bool PsycEntity::setPort (quint16 port) {
  QString s(buildUNI(mProto, mHost, port, mNick, mChan, mPlace));
  if (s.isEmpty()) return false;
  mPort = port;
  mUNI = s;
  return true;
}


bool PsycEntity::setProto (const QString &proto) {
  QString s(buildUNI(proto, mHost, mPort, mNick, mChan, mPlace));
  if (s.isEmpty()) return false;
  mProto = proto;
  mUNI = s;
  return true;
}


bool PsycEntity::setPlace (bool place) {
  QString s(buildUNI(mProto, mHost, mPort, mNick, mChan, place));
  if (s.isEmpty()) return false;
  mPlace = place;
  mUNI = s;
  return true;
}


bool PsycEntity::setChannel (const QString &chan) {
  QString s(buildUNI(mProto, mHost, mPort, mNick, chan, mPlace));
  if (s.isEmpty()) return false;
  mChan = chan;
  mUNI = s;
  return true;
}


///////////////////////////////////////////////////////////////////////////////
PsycProto::PsycProto (QObject *parent) :
  QObject(parent), mHost(QString()), mPort(4404), mNick(QString()), mUNI(QString()),
  mUseSSL(false), mConnected(false), mLinked(false), mLoggedIn(false),
  mMsgCookie(1), mSecureLogin(true),
  mStatus(Offline), mStatusText(QString("")), mMood(Unspecified)
{
  mPVars = new PsycVars();
  mCurPacket = new PsycPacket(mPVars);
  mPingTimer = new QTimer(this);
  connect(mPingTimer, SIGNAL(timeout()), this, SLOT(sendPing()));
  connect(&mSock, SIGNAL(connected()), this, SLOT(onConnected()));
  connect(&mSock, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
  connect(&mSock, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
  connect(&mSock, SIGNAL(readyRead()), this, SLOT(bytesComes()));
  mUserAgent = "k8qtPSYC/0.0.1";
#ifdef USE_SSL
  connect(&mSock, SIGNAL(peerVerifyError(const QSslError &)), this, SLOT(onPeerVerifyError(const QSslError &)));
  connect(&mSock, SIGNAL(sslErrors(const QList<QSslError> &)), this, SLOT(onSSLErrors(const QList<QSslError> &)));
#endif
}


PsycProto::~PsycProto () {
  if (mSock.state() != QAbstractSocket::UnconnectedState) mSock.close();
  mPingTimer->stop();
  if (mCurPacket) delete mCurPacket;
  if (mPVars) delete mPVars;
}


bool PsycProto::isValidNick (const QString &nick) {
  return PsycEntity::isValidPSYCNick(nick);
}


QString PsycProto::statusName (Status ad) {
  static const char *statuses[] = {
    "internal",
    "offline",
    "vacation",
    "away",
    "dnd",
    "nearby",
    "busy",
    "here",
    "ffc",
    "realtime"
  };

  int st = (int)ad;
  if (st < 0 || st > 9) st = 1;
  return QString(statuses[st]);
}


QString PsycProto::statusName (int ad) {
  return statusName((Status)ad);
}


QString PsycProto::moodName (Mood mood) {
  static const char *moods[] = {
    "unspecified",
    "dead",
    "angry",
    "sad",
    "moody",
    "okay",
    "good",
    "happy",
    "bright",
    "nirvana"
  };

  int st = (int)mood;
  if (st < 0 || st > 9) st = 0;
  return QString(moods[st]);
}


QString PsycProto::moodName (int mood) {
  return moodName((Mood)mood);
}


void PsycProto::clear () {
  if (mSock.state() != QAbstractSocket::UnconnectedState) mSock.close();
  mPingTimer->stop();
  mPVars->clear();
  mCurPacket->clear();
  mPassHash = Plain;
  mConnected = false;
  mLinked = false;
  mLoggedIn = false;
  mStatus = Offline;
  mStatusText = "";
  mMood = Unspecified;
  mFSPending.clear();
  mFSDups.clear();
}


bool PsycProto::setUNI (const QString &aUNI) {
  PsycEntity u(aUNI);
  if (!u.isValid() || !u.isPSYC()) return false;
  mHost = u.host();
  mPort = u.port();
  mNick = u.nick();
  mUNI = u.uni();
  return true;
}


/////////////////////////////////////////////////////////////////////////
/*
:_target  psyc://10.0.0.66:-44371/
:_source_relay  xmpp:xyz@jabbus.org
:_source  psyc://ketmar.no-ip.org/~ketmar

:_description_presence  nianoptrious
:_nick  xmpp:xyz@jabbus.org
:_location  xmpp:xyz@jabbus.org/Panopticum
:_degree_availability 4
_status_presence_here_busy
xmpp:xyz@jabbus.org is busy. nianoptrious
*/
bool PsycProto::extractPacketUser (PsycEntity &dst, const PsycPacket &pkt) const {
  bool ok = false;
  QString nick(pkt.get("_nick"));
  if (dst.setUNI(nick)) ok = true; // wow, i have full UNI here
  else {
    // do it the hard way
    QString sr(pkt.get("_source_relay")); // i'll trust this, i'm the client after all
    if (sr.isEmpty()) sr = pkt.get("_source"); // no relay? then grab src
#if 0
    // HACKHACKHACK!
    // seems that tg fixed this bug in psyced; let's see
    if (sr.endsWith("/~0")) {
      sr.chop(1);
      sr.append(nick);
    }
    // HACKHACKHACK!
#endif
    if (dst.setUNI(sr)) ok = true; // got it!
    if (ok && !nick.isEmpty()) {
      if (!dst.setNick(nick)) ok = false;
    }
  }
  if (ok) {
    // wow! a fish!
    dst.setPlace(false); // it SHOULD be user anyway
    dst.setVerbatim(pkt.get("_nick_verbatim")); // this one preserves case
    if (dst.isXMPP()) {
      // xmpp "location" field gives us a channel
      QString loc(pkt.get("_location"));
      int lp = loc.indexOf('/');
      if (lp > 0) {
        loc = loc.mid(lp+1).trimmed();
        if (!loc.isEmpty()) dst.setChannel(loc);
      }
    }
  } else {
    // too bad
    dst.clear();
  }
  return dst.isValid();
}


bool PsycProto::extractPacketPlace (PsycEntity &dst, const PsycPacket &pkt) const {
  bool ok = false;
  QString nick(pkt.get("_nick_place"));
  if (dst.setUNI(nick)) ok = true; // wow, i have full UNI here
  else {
    // do it the hard way
    QString sr(pkt.get("_context")); // this must come from place entity
    // but if not...
    if (sr.isEmpty()) sr = pkt.get("_source_relay"); // i'll trust this, i'm the client after all
    if (sr.isEmpty()) sr = pkt.get("_source"); // no relay? then grab src as a last resort
    if (dst.setUNI(sr)) ok = true; // got it!
    if (ok && !nick.isEmpty()) {
      if (!dst.setNick(nick)) ok = false;
    }
  }
  if (ok) {
    // wow! a fish!
    dst.setPlace(true); // it SHOULD be place anyway
    //dst.setVerbatim(""); // this one preserves case (yeah, it really does?)
  } else {
    // too bad
    dst.clear();
  }
  return dst.isValid();
}


void PsycProto::clearEntityList (QList<PsycEntity *> &list) {
  foreach (PsycEntity *e, list) if (e) delete e;
  list.clear();
}


QList<PsycEntity *> PsycProto::parseNickList (const PsycPacket &pkt, const QString &uniField, const QString &nickField) {
  QRegExp reBlanks("\\s+");
  QStringList unis(pkt.get(uniField).split(reBlanks, QString::SkipEmptyParts));
  QStringList nicks(pkt.get(nickField).split(reBlanks, QString::SkipEmptyParts));
  QList<PsycEntity *> res;
  PsycEntity *e = 0;
  for (int f = 0; f < unis.size(); f++) {
    //qDebug() << "chk:" << unis[f];
    if (!e) e = new PsycEntity(unis[f]); else e->setUNI(unis[f]);
    if (e->isValid()) {
      if (f < nicks.size()) {
        //qDebug() << " nick:" << nicks[f];
        PsycEntity xt(nicks[f]);
        if (xt.isValid()) *e = xt; else e->setVerbatim(nicks[f]);
      }
      e->setPlace(false);
      //qDebug() << " ent:" << e->uni();
      res << e;
      e = 0;
    }
  }
  if (e) delete e;
  return res;
}


/////////////////////////////////////////////////////////////////////////
void PsycProto::disconnectFromServer () {
  mPingTimer->stop();
  if (mSock.state() != QAbstractSocket::UnconnectedState) mSock.close();
  clear();
}


bool PsycProto::connectToServer () {
  clear();
  if (mHost.isEmpty()) return false;
  //mStream.setDevice(&mSock);
  //mStream.setCodec("UTF-8");
  quint16 p = mPort;
  if (p < 1) p = 4404;
  if (mUseSSL && p == 4404) p = 9404;
  if (mUseSSL) qDebug() << "using SSL!";
  mSock.connectToHost(mHost, p);
  return true;
}


void PsycProto::onConnected () {
  //qDebug() << "connected";
  if (mUseSSL) mSock.startClientEncryption();
  mConnected = true;
  mPVars->clear();
  mCurPacket->clear();
  mPassHash = Plain;
  mLinked = false; mLoggedIn = false;
  // send 'hello'
  mSock.write(".\n");
  mPingTimer->start(30000+qrand()%10000);
  emit connected();
  //FIXME: do qsrand()
}


void PsycProto::onDisconnected () {
  //qDebug() << "disconnected";
  mPingTimer->stop();
  mConnected = false; mLinked = false; mLoggedIn = false;
  emit disconnected();
}


void PsycProto::onError (QAbstractSocket::SocketError socketError) {
  Q_UNUSED(socketError)
  mPingTimer->stop();
  QString str = mSock.errorString();
  //qDebug() << "error:" << str;
  emit netError(str);
  if (mSock.state() != QAbstractSocket::UnconnectedState) mSock.close();
}


#ifdef USE_SSL
static void dumpCertificate (const QSslCertificate &sf) {
  if (sf.isNull()) return;
  qDebug() <<
    "version:" << sf.version() <<
    "\ns/n:" << sf.serialNumber() <<
    "\nissuer:" <<
    "\n  organization:" << sf.issuerInfo(QSslCertificate::Organization) <<
    "\n  common name:" << sf.issuerInfo(QSslCertificate::CommonName) <<
    "\n  locality name:" << sf.issuerInfo(QSslCertificate::LocalityName) <<
    "\n  org. unit name:" << sf.issuerInfo(QSslCertificate::OrganizationalUnitName) <<
    "\n  country name:" << sf.issuerInfo(QSslCertificate::CountryName) <<
    "\n  province name:" << sf.issuerInfo(QSslCertificate::StateOrProvinceName) <<
    "\nsubject:" <<
    "\n  organization:" << sf.subjectInfo(QSslCertificate::Organization) <<
    "\n  common name:" << sf.subjectInfo(QSslCertificate::CommonName) <<
    "\n  locality name:" << sf.subjectInfo(QSslCertificate::LocalityName) <<
    "\n  org. unit name:" << sf.subjectInfo(QSslCertificate::OrganizationalUnitName) <<
    "\n  country name:" << sf.subjectInfo(QSslCertificate::CountryName) <<
    "\n  province name:" << sf.subjectInfo(QSslCertificate::StateOrProvinceName) <<
    "\neffective date:" << sf.effectiveDate() <<
    "\nexpiry date:" << sf.expiryDate();
  QMultiMap<QSsl::AlternateNameEntryType, QString> km(sf.alternateSubjectNames());
  QList <QSsl::AlternateNameEntryType> keys(km.keys());
  foreach (const QSsl::AlternateNameEntryType key, keys) {
    int cnt = km.count(key);
    if (cnt < 1) continue;
    switch (key) {
      case QSsl::EmailEntry: qDebug() << "email:"; break;
      case QSsl::DnsEntry: qDebug() << "host:"; break;
      default: qDebug() << "unknown:"; break;
    }
    QList<QString> values = km.values(key);
    for (int f = 0; f < values.size(); f++) {
      qDebug() << "  value:" << values[f];
    }
  }
}


static bool isFatalSSLError (const QSslError &err) {
  switch (err.error()) {
    case QSslError::UnableToGetIssuerCertificate: return true;
    case QSslError::UnableToDecryptCertificateSignature: return true;
    case QSslError::UnableToDecodeIssuerPublicKey: return true;
    case QSslError::CertificateSignatureFailed: return true;
    case QSslError::CertificateNotYetValid: return true;
    case QSslError::CertificateExpired: return false;
    case QSslError::InvalidNotBeforeField: return true;
    case QSslError::InvalidNotAfterField: return true;
    case QSslError::SelfSignedCertificate: return false;
    case QSslError::SelfSignedCertificateInChain: return false;
    case QSslError::UnableToGetLocalIssuerCertificate: return true;
    case QSslError::UnableToVerifyFirstCertificate: return true;
    case QSslError::CertificateRevoked: return true;
    case QSslError::InvalidCaCertificate: return true;
    case QSslError::PathLengthExceeded: return true;
    case QSslError::InvalidPurpose: return true;
    case QSslError::CertificateUntrusted: return true;
    case QSslError::CertificateRejected: return true;
    case QSslError::SubjectIssuerMismatch: return true;
    case QSslError::AuthorityIssuerSerialNumberMismatch: return true;
    case QSslError::NoPeerCertificate: return true;
    case QSslError::HostNameMismatch: return true; //FIXME: this SHOULD be fatal!
    case QSslError::UnspecifiedError: return true;
    case QSslError::NoSslSupport: return true;
    default: ;
  }
  return true;
}


void PsycProto::onPeerVerifyError (const QSslError &error) {
  qDebug() << "SSL ERROR:" << error.errorString() << isFatalSSLError(error);
  dumpCertificate(error.certificate());
  if (!isFatalSSLError(error)) mSock.ignoreSslErrors();
}


void PsycProto::onSSLErrors (const QList<QSslError> &errors) {
  foreach (const QSslError &err, errors) {
    qDebug() << "*SSL ERROR:" << err.errorString() << isFatalSSLError(err);
    dumpCertificate(err.certificate());
    if (!isFatalSSLError(err)) mSock.ignoreSslErrors();
  }
}
#endif


void PsycProto::bytesComes () {
  // no packet length checking (the server should to that!)
  //static QStringList ps;
  while (mSock.state() == QAbstractSocket::ConnectedState) {
    if (!mSock.canReadLine()) break;
    QByteArray ba(mSock.readLine(65536));
    QString s(QString::fromUtf8(ba));
    if (s.isEmpty() || s[s.length()-1] != '\n') {
      emit netError(tr("invalid PSYC packet comes"));
      disconnectFromServer();
      break;
    }
    s.chop(1); // remove '\n'
    //qDebug() << "["+s+"]";
    if (s == ".") {
      if (!mCurPacket->method().isEmpty()) emit packetComes(*mCurPacket);
      if (!dispatchPacket(*mCurPacket)) {
        //!!qDebug() << ">>>UNKNOWN PACKET:\n" << mCurPacket->toString();
      }
      mCurPacket->clear();
      //ps.clear();
    } else {
      //ps << s;
/*
      if (ps.count() > 127) {
        emit netError(tr("can't parse PSYC packet -- too long"));
        disconnectFromServer();
        return;
      }
*/
      if (!mCurPacket->parseLine(s)) {
        qDebug() << "ERR:" << s;
        qDebug() << mCurPacket->toString();
        //ps.clear();
        emit netError(tr("can't parse PSYC packet"));
        disconnectFromServer();
        break;
      }
    }
  }
}


bool PsycProto::dispatchPacket (const PsycPacket &pkt) {
  QString mt(pkt.method());
  if (mt.isEmpty()) return true; // nothing to call
  if (mt.length() > 128) return false; // toooo long!
  // and now the fun part
  // find packet handler starting from the whole name and down to root
  // this is Qt black magic, so don't touch it.
  char mtName[128], buf[256];
  // get name
  QByteArray bt(mt.toAscii());
  sprintf(mtName, "%s", bt.constData());
  // find handler
  while (mtName[0] && mtName[1]) {
    sprintf(buf, "pkt_%s(PsycPacket)", mtName);
    if (metaObject()->indexOfSlot(buf) >= 0) {
      sprintf(buf, "pkt_%s", mtName);
      QMetaObject::invokeMethod(this, buf, Qt::AutoConnection, Q_ARG(PsycPacket, *mCurPacket));
      return true;
    }
    // strip one leg %-)
    char *et = strrchr(mtName, '_');
    if (!et) break;
    *et = '\0';
  }
  return false;
}


void PsycProto::sendPacket (const PsycPacket &pkt) {
  //!!!qDebug() << "<<<SENDING PACKET:\n" << pkt.toString() << "========================";
  QByteArray ba(pkt.toBA());
  mSock.write(ba);
  emit packetSent(pkt);
}


void PsycProto::logoff () {
  setStatus(Offline, mStatusText, mMood);
  PsycPacket pkt("_request_do_quit");
  pkt.put("_target", mUNI, PsycPacket::Route);
  pkt.put("_source_identification", mUNI, PsycPacket::Route);
  sendPacket(pkt);
}


void PsycProto::sendPing () {
  if (mLinked) mSock.write("\n.\n");
}


void PsycProto::sendPassword (const QString &pass) {
  //qDebug() << "sending login info";
  PsycPacket ps("_set_password");
  ps.endRoute();

  if (!mSecureLogin || mPassHash == Plain) {
    //qDebug() << "plain password";
    ps.put("_password", pass);
  } else {
    //qDebug() << "hashed password:" << mPassHash;
    const char *mt = mPassHash==SHA1 ? "sha1" : "md5";
    ps.put("_method", mt);
    QString ss(mPassSalt); ss.append(pass);
    QByteArray utf(ss.toUtf8());
    QByteArray hash = QCryptographicHash::hash(utf, mPassHash==SHA1 ? QCryptographicHash::Sha1 : QCryptographicHash::Md5);
    utf = hash.toHex();
    QString s(utf);
    ps.put("_password", s);
  }

  sendPacket(ps);
}


quint64 PsycProto::sendMessage (const PsycEntity &dest, const QString &text, const QString &action) {
  if (!dest.isValid()) return 0; // invalid user

  quint64 ck = ++mMsgCookie;
  QString tag("msg_"+QString::number(ck)); // there is no reason to send this, psyced will not return it %-(

  if (dest.isPlace()) {
    // hack removed (new psyced seems to work ok without this hack
    //enterPlace(dest); // this is a HACK due to possible bug in psyced
    PsycPacket pkt("_request_do_message");
    pkt.put("_source_identification", mUNI);
    pkt.put("_group", dest.uni());
    pkt.put("_tag", tag);
    pkt.endRoute();
    if (!action.isEmpty()) pkt.put("_action", action);
    pkt.setBody(text);
    sendPacket(pkt);
  } else {
    PsycPacket pkt("_request_do_message_private");
    pkt.put("_target", mUNI);
    pkt.put("_source_identification", mUNI);
    pkt.put("_tag", tag);
    pkt.endRoute();
    pkt.put("_person", dest.uni());
    if (!action.isEmpty()) pkt.put("_action", action);
    pkt.setBody(text);
    sendPacket(pkt);
  }
  return ck;
}


void PsycProto::requestPeerList () {
  PsycPacket pkt("_request_do_list_peers");
  pkt.put("_target", mUNI);
  pkt.put("_source_identification", mUNI);
  pkt.endRoute();
  sendPacket(pkt);
  mFSPending.clear();
  mFSDups.clear();
}


void PsycProto::clearLog () {
  PsycPacket pkt("_request_do_clear_log");
  pkt.put("_target", mUNI);
  pkt.put("_source_identification", mUNI);
  pkt.endRoute();
  sendPacket(pkt);
}


void PsycProto::requestAuth (const PsycEntity &dest, const QString &text) {
  if (!dest.isValid() || dest.isPlace()) return; // invalid user

  PsycPacket pkt("_request_do_add_friend");
  pkt.put("_target", mUNI);
  pkt.put("_source_identification", mUNI);
  pkt.endRoute();
  pkt.put("_person", dest.uni());
  pkt.setBody(text);
  sendPacket(pkt);
/*
  PsycPacket pkt("_request_context_enter_friends");
  pkt.put("_target", dest.uni());
  pkt.put("_source_identification", mUNI);
  pkt.endRoute();
  //pkt.put("_person", dest.uni());
  pkt.setBody(text);
  sendPacket(pkt);
*/
}


void PsycProto::sendAuth (const PsycEntity &dest, const QString &text) {
  PsycPacket pkt("_request_do_add_friend");
  pkt.put("_target", mUNI);
  pkt.put("_source_identification", mUNI);
  pkt.endRoute();
  pkt.put("_person", dest.uni());
  pkt.setBody(text);
  sendPacket(pkt);

  doCommand("notify "+dest.uni()+" i");
  doCommand("expose "+dest.uni()+" 0");
}


void PsycProto::removeAuth (const PsycEntity &dest, const QString &text) {
  if (!dest.isValid() || dest.isPlace()) return; // invalid user
/*
  PsycPacket pkt("_request_context_leave_friends");
  pkt.put("_target", dest.uni());
  pkt.put("_source_identification", mUNI);
  pkt.endRoute();
  //pkt.put("_person", dest.uni());
  pkt.setBody(text);
  sendPacket(pkt);
*/
  PsycPacket pkt("_request_do_clear_friend");
  pkt.put("_target", mUNI);
  pkt.put("_source_identification", mUNI);
  pkt.endRoute();
  pkt.put("_person", dest.uni());
  pkt.setBody(text);
  sendPacket(pkt);
}


void PsycProto::setStatus (Status degree, const QString &text, Mood mood) {
  mStatus = degree;
  mStatusText = text;
  mMood = mood;

  PsycPacket pkt("_request_do_presence");
  pkt.put("_target", mUNI);
  pkt.put("_source_identification", mUNI);
  pkt.endRoute();
  int d = (int)degree; if (d < 1 || d > 8) d = 7;
  pkt.putInt("_degree_availability", d);
  if (!text.isEmpty()) {
    QString t(text);
    t.replace("\r\n", " ");
    t.replace("\r", " ");
    // fuckshitkill!
    for (int f = 0; f < t.length(); f++) {
      QChar ch(t[f]);
      if (ch.isSpace()) t[f] = ' ';
    }
    t = t.trimmed();
    if (!t.isEmpty()) pkt.put("_description_presence", t);
  }
  int m = (int)mood; if (m < 0 || m > 9) m = 0;
  pkt.putInt("_degree_mood", m);
  sendPacket(pkt);
}


void PsycProto::doCommand (const QString &cmd, const QString &destUni, const QString &focus, const QString &tag) {
  PsycPacket pkt("_request_execute");
  pkt.put("_source_identification", mUNI);
  PsycEntity dst(destUni);
  if (!dst.isValid()) pkt.put("_target", mUNI); else pkt.put("_target", dst.uni());
  if (!tag.isEmpty()) pkt.put("_tag", tag);
  pkt.endRoute();
  if (!focus.isEmpty()) pkt.put("_focus", focus); // Kol_Panic says it is necessary for rooms. i'm dumb!
  pkt.setBody(cmd);
  sendPacket(pkt);
}


void PsycProto::invite (const QString &place, const QString &destUni, const QString &tag) {
  PsycPacket pkt("_request_do_invite");
  pkt.put("_source_identification", mUNI);
  PsycEntity dst(destUni);
  pkt.put("_target", mUNI);
  if (!tag.isEmpty()) pkt.put("_tag", tag);
  pkt.endRoute();
  pkt.put("_focus", place); // Kol_Panic says it is necessary for rooms. i'm dumb!
  pkt.put("_person", destUni);
  sendPacket(pkt);
}


/////////////////////////////////////////////////////////////////////////
// TODO: warnings, etc
void PsycProto::pkt__warning (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
  //!!qDebug() << "WARNING:" << pkt.toString();
}


void PsycProto::pkt__error (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
  //!!qDebug() << "ERROR:" << pkt.toString();
}


void PsycProto::pkt__failure (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
  //!!qDebug() << "FAILURE:" << pkt.toString();
}


void PsycProto::pkt__error_invalid_password (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
  emit invalidPassword();
}


/////////////////////////////////////////////////////////////////////////
// TODO: conference support!
void PsycProto::enterPlace (const PsycEntity &dest) {
  if (!dest.isValid()) return;
  PsycEntity p(dest);
  p.setPlace(true);

  PsycPacket pe("_request_do_enter");
  pe.put("_source_identification", mUNI);
  pe.endRoute();
  pe.put("_group", p.uni());
  sendPacket(pe);
}


void PsycProto::leavePlace (const PsycEntity &dest) {
  if (!dest.isValid()) return;
  PsycEntity p(dest);
  p.setPlace(true);

  PsycPacket pe("_request_do_leave");
  pe.put("_source_identification", mUNI);
  pe.endRoute();
  pe.put("_group", p.uni());
  sendPacket(pe);
}


void PsycProto::pkt__error_status_place_matches (const PsycPacket &pkt) {
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  emit placeEntered(place);
}


void PsycProto::pkt__echo_place_enter (const PsycPacket &pkt) {
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  emit placeEntered(place);
  QList<PsycEntity *> members(parseNickList(pkt, "_list_members", "_list_members_nicks"));
  //qDebug() << pkt.method() << place.uni();
  //qDebug() << " " << members;
  foreach (PsycEntity *e, members) {
    //qDebug() << " *" << e->uni();
    if (*e != mUNI) emit userEntersPlace(place, *e);
    delete e; // just a little speedup
  }
}


void PsycProto::pkt__echo_place_leave (const PsycPacket &pkt) {
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  //qDebug() << pkt.method() << place.uni();
  emit placeLeaved(place);
}


void PsycProto::pkt__notice_place_enter (const PsycPacket &pkt) {
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  PsycEntity who;
  if (!extractPacketUser(who, pkt)) return;
  //qDebug() << pkt.method() << place.uni() << who.uni();
  if (who == mUNI) emit placeEntered(place); else emit userEntersPlace(place, who);
}


void PsycProto::pkt__notice_place_leave (const PsycPacket &pkt) {
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  PsycEntity who;
  if (!extractPacketUser(who, pkt)) return;
  //qDebug() << pkt.method() << place.uni() << who.uni();
  if (who == mUNI) emit placeLeaved(place); else emit userLeavesPlace(place, who);
}


/////////////////////////////////////////////////////////////////////////
void PsycProto::pkt__notice_circuit_established (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
}


void PsycProto::pkt__notice_link (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
  mLinked = true;
  //!!!???
/*
  PsycPacket p("_request_do_presence");
  p.put("_target", mUNI);
  p.put("_source_identification", mUNI);
  p.endRoute();
  p.putInt("_degree_availability", 0);
  sendPacket(p);
*/
}


void PsycProto::pkt__notice_unlink (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
  disconnectFromServer();
  mLinked = false;
  mConnected = false;
}


void PsycProto::pkt__notice_logon_last (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
  emit loginComplete();
}


void PsycProto::pkt__info_nickname (const PsycPacket &pkt) {
  QString myNick = pkt.get("_nick", mNick);
  if (myNick != mNick) {
    mNick = myNick;
    emit nickEnforced();
  }
}


void PsycProto::pkt__status_circuit (const PsycPacket &pkt) {
  mPassHash = Plain;

  PsycPacket ps("_request_link");
  ps.put("_target", mUNI, PsycPacket::RoutePerm);
  if (!mUserAgent.isEmpty()) ps.put("_version_agent", mUserAgent, PsycPacket::NormalPerm);
  sendPacket(ps);

  // check if we can do md5 login
  QString s(pkt.get("_available_hashes"));
  QStringList lst(s.split(';', QString::SkipEmptyParts));
  if (lst.indexOf("md5") >= 0) { mPassHash = MD5; return; }
  if (lst.indexOf("sha1") >= 0) { mPassHash = SHA1; return; }
}


void PsycProto::pkt__query_password (const PsycPacket &pkt) {
  mPassSalt = pkt.get("_nonce");
  emit needPassword();
}


void PsycProto::pkt__notice_login (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
  mLoggedIn = true;
  //clearLog();
}


void PsycProto::pkt__status_log_none (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
}


void PsycProto::pkt__status_log_new (const PsycPacket &pkt) {
  Q_UNUSED(pkt)
  //mLogPktLeft = pkt.getInt("_amount_new", 0);
}


void PsycProto::pkt__status_presence_description (const PsycPacket &pkt) {
/*
  PsycEntity u(pkt.get("_source"));
  if (!u.isValid()) return;
  int mood = pkt.getFloat1("_degree_mood", 0);
  int av = pkt.getInt("_degree_availability", 7);
  if (av < 1 || av > 8) av = 7;
  //if (!mUNI.compare(u.uni(), Qt::CaseInsensitive)) {
    //qDebug() << "STATUS:\n" << pkt.toString();
    mMood = (Mood)mood;
    mStatus = (Status)av;
    emit selfStatusChanged();
  //} else {
    //qDebug() << "ALIEN STATUS:\n" << pkt.toString();
    //u.setMood((Mood)mood);
    //u.setStatus((Status)av);
    //emit userStatusChanged(u);
  //}
*/
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  int mood = pkt.getFloat1("_degree_mood", 0);
  int av = pkt.getFloat1("_degree_availability");
  user.setMood((Mood)mood);
  user.setStatus((Status)av);
  user.setStatusText(pkt.get("_description_presence"));
  if (user == mUNI) {
    if (mood != Unspecified) mMood = (Mood)mood;
    mStatus = (Status)av;
    emit selfStatusChanged();
  } else {
    emit userStatusChanged(user);
  }
}


void PsycProto::pkt__status_presence (const PsycPacket &pkt) {
  pkt__status_presence_description(pkt);
}


void PsycProto::pkt__notice_presence (const PsycPacket &pkt) {
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  int mood = pkt.getFloat1("_degree_mood", 0);
  int av = pkt.getFloat1("_degree_availability");
  user.setMood((Mood)mood);
  user.setStatus((Status)av);
  user.setStatusText(pkt.get("_description_presence"));
  emit userStatusChanged(user);
}


void PsycProto::pkt__status_person_present (const PsycPacket &pkt) {
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  user.setStatus(Here);
  emit userStatusChanged(user);
}


///////////////////////////////////////////////////////////////////////////////
void PsycProto::pkt__list_friends (const PsycPacket &pkt) {
  // here
  QList<PsycEntity *> members(parseNickList(pkt, "_list_friends", "_list_friends_nicknames"));
  foreach (PsycEntity *e, members) {
    if (*e != mUNI) {
      e->setStatus(Here);
      emit userStatusChanged(*e);
    }
    delete e; // just a little speedup
  }
  members.clear();
  // away
  members = parseNickList(pkt, "_list_friends_away", "_list_friends_nicknames_away");
  foreach (PsycEntity *e, members) {
    if (*e != mUNI) {
      e->setStatus(Away);
      emit userStatusChanged(*e);
    }
    delete e; // just a little speedup
  }
}


void PsycProto::pkt__list_user_description (const PsycPacket &pkt) {
  Q_UNUSED(pkt);
  //TODO
}


///////////////////////////////////////////////////////////////////////////////
void PsycProto::pkt__message_echo (const PsycPacket &pkt) {
  //qDebug() << ">>>MSGACK:" << pkt.toString();
  QString tag = pkt.get("_tag_reply");
/*
  if (tag.left(4) != "msg_") return; // unknown source
  tag.remove(0, 4); // remove prefix
  bool ok;
  quint64 cc = tag.toULongLong(&ok);
  if (!ok) return; // bad cookie
  //qDebug() << "message ack:" << cc << pkt.get("_source");
  emit messageAck(cc, pkt.get("_source"));
*/
  QString action(pkt.get("_action").trimmed());
  QString t(pkt.method().left(20));
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  PsycEntity target;
  PsycEntity place;
  QString timeVar;
  if (t == "_message_echo_privat") {
    if (pkt.has("_time_sent")) timeVar = "_time_sent"; else timeVar = "_time_INTERNAL";
    // extract target
    PsycPacket p2(pkt);
    p2.put("_nick", pkt.get("_nick_target"));
    if (!extractPacketUser(target, p2)) return;
  } else if (t == "_message_echo_public") {
    if (!extractPacketPlace(place, pkt)) return;
    timeVar = "_time_place";
  }
  QDateTime time(QDateTime::fromTime_t(pkt.getInt(timeVar, QDateTime::currentDateTime().toTime_t())));
  emit messageEcho(place, user, target, time, pkt.body(), action, tag);
}


void PsycProto::pkt__message_private (const PsycPacket &pkt) {
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  //uint t = pkt.getInt("_time_INTERNAL", 0);
  //if (t) time.fromTime_t(t);
  QString action(pkt.get("_action").trimmed());
  PsycEntity place;
  QString timeVar;
  if (pkt.has("_time_log")) {
    QDateTime time(QDateTime::fromTime_t(pkt.getInt("_time_log", QDateTime::currentDateTime().toTime_t())));
    emit historyMessage(place, user, time, pkt.body(), action);
  } else {
    if (pkt.has("_time_sent")) timeVar = "_time_sent"; else timeVar = "_time_INTERNAL";
    //QDateTime time(QDateTime::currentDateTime());
    QDateTime time(QDateTime::fromTime_t(pkt.getInt(timeVar, QDateTime::currentDateTime().toTime_t())));
    emit message(place, user, time, pkt.body(), action);
  }
}


void PsycProto::pkt__message_public (const PsycPacket &pkt) {
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  //uint t = pkt.getInt("_time_INTERNAL", 0);
  //if (t) time.fromTime_t(t);
  QString action(pkt.get("_action").trimmed());
  QString timeVar;
  if (pkt.has("_time_sent")) timeVar = "_time_sent";
  else if (pkt.has("_time_log")) timeVar = "_time_log";
  else timeVar = "_time_INTERNAL";
  //FIXME: remove code duplication
  if (pkt.has("_time_place")) {
    //qDebug() << pkt.get("_time_place") << pkt.getInt("_time_place");
    QDateTime time(QDateTime::fromTime_t(pkt.getInt("_time_place", QDateTime::currentDateTime().toTime_t())));
    emit historyMessage(place, user, time, pkt.body(), action);
  } else {
    QDateTime time(QDateTime::fromTime_t(pkt.getInt(timeVar, QDateTime::currentDateTime().toTime_t())));
    emit message(place, user, time, pkt.body(), action);
  }
}


///////////////////////////////////////////////////////////////////////////////
void PsycProto::pkt__warning_pending_friendship (const PsycPacket &pkt) {
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  if (mFSPending.contains(user.uni().toLower())) return;
  mFSDups.remove(user.uni().toLower());
  mFSPending << user.uni().toLower();
  //user.setNotification(Pending);
  //emit authWaiting(user);
}


void PsycProto::pkt__warning_duplicate_friendship (const PsycPacket &pkt) {
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  if (mFSDups.contains(user.uni().toLower())) return;
  mFSDups << user.uni().toLower();
  mFSPending.remove(user.uni().toLower());
  user.setNotification(Delayed); //FIXME
  emit authGranted(user);
}


void PsycProto::pkt__echo_friendship_removed (const PsycPacket &pkt) {
  if (!pkt.get("_time_log").isEmpty()) return;
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  if (user == mUNI) return;
  mFSDups.remove(user.uni().toLower());
  mFSPending.remove(user.uni().toLower());
  user.setNotification(None);
  emit authDenied(user);
}


void PsycProto::pkt__request_friendship (const PsycPacket &pkt) {
  if (!pkt.get("_time_log").isEmpty()) return;
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  if (user == mUNI) return;
  if (mFSDups.contains(user.uni().toLower())) return;
  mFSPending << user.uni().toLower();
  user.setNotification(Pending);
  emit authRequested(user);
}


void PsycProto::pkt__notice_friendship_established (const PsycPacket &pkt) {
  if (!pkt.get("_time_log").isEmpty()) return;
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  if (user == mUNI) return;
  if (mFSDups.contains(user.uni().toLower())) return;
  user.setNotification(Delayed);
  mFSDups << user.uni().toLower();
  mFSPending.remove(user.uni().toLower());
  emit authGranted(user);
}


void PsycProto::pkt__notice_friendship_removed (const PsycPacket &pkt) {
  if (!pkt.get("_time_log").isEmpty()) return;
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  if (user == mUNI) return;
  mFSDups.remove(user.uni().toLower());
  mFSPending.remove(user.uni().toLower());
  user.setNotification(None);
  emit authDenied(user);
}


void PsycProto::pkt__status_friendship_established (const PsycPacket &pkt) {
  if (!pkt.get("_time_log").isEmpty()) return;
  pkt__notice_friendship_established(pkt);
}


///////////////////////////////////////////////////////////////////////////////
void PsycProto::pkt__list_acquaintance_notification_offered (const PsycPacket &pkt) {
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  if (user == mUNI) return;
  emit authGranted(user);
}


void PsycProto::pkt__list_acquaintance_each (const PsycPacket &pkt) {
  PsycEntity user;
  if (!extractPacketUser(user, pkt)) return;
  if (user == mUNI) return;
  //parsePacketNick(user, pkt, "_acquaintance");
  QString nt(pkt.get("_type_notification"));
  if (nt.isEmpty()) return;
  Notification ntT;
  if (nt == "_none") ntT = None;
  else if (nt == "_offered") ntT = Offered;
  else if (nt == "_pending") ntT = Pending;
  else if (nt == "_mute") ntT = Mute;
  else if (nt == "_delayed_more") ntT = DelayedMore;
  else if (nt == "_delayed") ntT = Delayed;
  else if (nt == "_immediate") ntT = Immediate;
  else return;
  switch (ntT) {
    case Pending: mFSPending << user.uni().toLower(); break;
    case None: /*case Offered:*/ break;
    default: mFSDups << user.uni().toLower(); break;
  }
  if (ntT == None) return;
  user.setNotification(ntT);
  emit notificationType(user);
/*
:_degree_trust  -
:_type_notification _pending
:_type_display  _normal
:_degree_expose -
:_acquaintance  psyc://bakemono.pb.id/~ketmar
:_nick  psyc://bakemono.pb.id/~ketmar
_list_acquaintance_each
[_nick] ([_acquaintance]): D[_type_display], N[_type_notification], T[_degree_trust], E[_degree_expose].
*/
/*
 * ends with _list_acquaintance_end
 */
}


void PsycProto::pkt__notice_update_scratchpad (const PsycPacket &pkt) {
  QString spUrl(pkt.get("_page_scratchpad"));
  if (spUrl.isEmpty()) return;
  emit scratchpadUpdated(spUrl);
/*
:_source_relay  psyc://psyced.org/@welcome
:_target_forward        psyc://10.0.0.66:-52732/
:_context       psyc://psyced.org/@welcome

:_page_scratchpad       https://psyced.org/@welcome?scratchpad
:_nick_place    psyc://psyced.org/@welcome
:_nick  psyc://psyced.org/@welcome
_notice_update_scratchpad
The [_page_scratchpad] has changed.
*/
}


void PsycProto::pkt__notice_examine_web_person (const PsycPacket &pkt) {
  //TODO
  Q_UNUSED(pkt)
}


void PsycProto::pkt__notice_examine_web_place (const PsycPacket &pkt) {
/*
:_source_relay	psyc://psyced.org/@welcome
:_target_forward	psyc://10.0.0.66:-32718/
:_context	psyc://psyced.org/@welcome

:_web_referrer	http://xmpp.org/software/servers.shtml
:_web_page	http://www.psyced.org/
:_web_on	http://www.psyced.org/
:_nick_place	psyc://psyced.org/@welcome
:_web_from	http://xmpp.org/software/servers.shtml
:_web_browser	Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; InfoPath.2; .NET CLR 3.0.04506.648)
:_host_name	74.87.96.179
:_nick	psyc://psyced.org/@welcome
_notice_place_examine_web
[_nick_place] inspected on [_web_on] coming from [_web_from].
*/
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  emit placeWebExamined(place, pkt);
}


void PsycProto::pkt__notice_place_examine_web (const PsycPacket &pkt) {
  pkt__notice_examine_web_place(pkt);
}


void PsycProto::pkt__notice_update (const PsycPacket &pkt) {
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  emit noticeUpdate(place, pkt);
/*
:_source_relay	psyc://10.0.0.66:-56038/
:_target_forward	psyc://10.0.0.66:-38516/
:_context	psyc://ketmar.no-ip.org/@dyskgit

:_hash_commit_long	faeef101c04ea8058cb3a59e3bf1db19b9f06939
:_name_editor	ketmar
:_comment	some games with editors...
:_nick_editor	ketmar
:_hash_commit	faeef10
:_nick_place	DyskGIT
:_project	Dyskinesia
:_nick	psyc://10.0.0.66:-56038/
:_mailto_editor	ketmar@ketmar.no-ip.org
_notice_update_software_GIT
[_project]: [_name_editor] "[_comment]"
.
*/
}


/**************************************************************
 * NEWS
 **************************************************************/
/*
 _headline: news title
 _page_news: URL
 _channel: news channel title
 _nick_place: place
 */
void PsycProto::pkt__notice_headline_news (const PsycPacket &pkt) {
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  QString title(pkt.get("_headline").trimmed()),
     channel(pkt.get("_channel").trimmed()),
     url(pkt.get("_page_news").trimmed());
  if (title.isEmpty()) {
    if (url.isEmpty()) return;
    title = url;
  }
  emit placeNews(place, channel, title, url);
}
/*
:_context       psyc://bakemono.pb.id/@slashdot
:_source_relay  psyc://bakemono.pb.id/@slashdot

:_channel       Slashdot
:_nick  zed
:_nick_place    slashdot
:_headline      GPL Firmware For Canon 5D Mark II Adds Features For Film Makers
:_page_news     http://rss.slashdot.org/~r/Slashdot/slashdot/~3/fkUSPmcACIw/GPL-Firmware-For-Canon-5D-Mark-II-Adds-Features-For-Film-Makers
_notice_headline_news
([_nick_place]News) [_headline] [_page_news]
.
*/


/*
 _file_title: file title
 _uniform_torrent: URL
 _channel_title: news channel title
 _nick_place: place
 */
void PsycProto::pkt__notice_offered_file_torrent (const PsycPacket &pkt) {
  PsycEntity place;
  if (!extractPacketPlace(place, pkt)) return;
  QString title(pkt.get("_file_title").trimmed()),
     channel(pkt.get("_channel_title").trimmed()),
     url(pkt.get("_uniform_torrent").trimmed());
  if (title.isEmpty()) {
    if (url.isEmpty()) return;
    title = url;
  }
  if (!channel.isEmpty()) channel.append(' ');
  channel.append("(torrent)");
  emit placeNews(place, channel, title, url);
}
