/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef PSYCVARS_H
#define PSYCVARS_H

#include <QHash>
#include <QObject>
#include <QString>
#include <QStringList>


class PsycVars {
public:
  PsycVars ();
  PsycVars (const PsycVars &aObj);
  ~PsycVars ();

  void clear ();

  inline const QString get (const char *name, const QString &def=QString()) const { return get(QLatin1String(name), def); }
  inline int getInt (const char *name, int def=-1) const { return getInt(QLatin1String(name), def); }
  inline int getFloat1 (const char *name, int def=0) const { return getFloat1(QLatin1String(name), def); }

  const QString get (const QString &name, const QString &def=QString()) const;
  int getInt (const QString &name, int def=-1) const;
  int getFloat1 (const QString &name, int def=0) const;

  inline void put (const char *name, const QString &value) { put(QLatin1String(name), value); }
  inline void putInt (const char *name, int value) { putInt(QLatin1String(name), value); }

  void put (const QString &name, const QString &value);
  void putInt (const QString &name, int value);

  inline void del (const char *name) { del(QLatin1String(name)); }
  void del (const QString &name);

  inline bool has (const char *name) const { return has(QLatin1String(name)); }
  inline bool has (const QString &name) const { return mList.contains(name); }

  QStringList varList () const;

  PsycVars &operator = (const PsycVars &vars);

private:
  QHash<QString, QString> mList;
};


#endif
