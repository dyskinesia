/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QDebug>

#include "psycpkt.h"

#include "psycvars.h"


PsycPacket::PsycPacket (PsycVars *pvars) : mPVars(pvars) {
  clear();
}


PsycPacket::PsycPacket (const QString &aMethod) : mPVars(0) {
  clear();
  mMethod = aMethod;
}


PsycPacket::PsycPacket (const PsycPacket &pkt) {
  *this = pkt;
}


PsycPacket::~PsycPacket () {
  //clear();
}


void PsycPacket::clear () {
  mVars.clear();
  mInRoute = true;
  mInBody = false;
  mMethod = QString();
  mBody = QString();
  mRVars.clear();
  mPerm.clear();
  mLastVar = QString();
  mLastPerm = false;
}


PsycPacket &PsycPacket::operator = (const PsycPacket &pkt) {
  if (&pkt != this) {
    mPVars = pkt.mPVars;
    mVars = pkt.mVars;
    mMethod = pkt.mMethod;
    mBody = pkt.mBody;
    mInRoute = pkt.mInRoute;
    mInBody = pkt.mInBody;
    mLastVar = pkt.mLastVar;
    mLastPerm = pkt.mLastPerm;
    mRVars = pkt.mRVars;
    mPerm = pkt.mPerm;
  }
  return *this;
}


const QString PsycPacket::get (const QString &name, const QString &def) const {
  if (!mVars.has(name)) {
    if (mPVars) return mPVars->get(name, def); else return def;
  }
  return mVars.get(name, def);
}


int PsycPacket::getInt (const QString &name, int def) const {
  if (!mVars.has(name)) {
    if (mPVars) return mPVars->getInt(name, def); else return def;
  }
  return mVars.getInt(name, def);
}


int PsycPacket::getFloat1 (const QString &name, int def) const {
  if (!mVars.has(name)) {
    if (mPVars) return mPVars->getFloat1(name, def); else return def;
  }
  return mVars.getFloat1(name, def);
}


bool PsycPacket::has (const QString &name) const {
  if (!mVars.has(name)) {
    if (mPVars) return mPVars->has(name);
    return false;
  }
  return true;
}


void PsycPacket::put (const QString &name, const QString &value, Type type) {
  //qDebug() << "put:" << name << "value:" << value << "type:" << type;
  mVars.put(name, value);
  if (type == Normal || type == NormalPerm) mRVars.remove(name);
  else if (mInRoute || type == Route || type == RoutePerm) mRVars.insert(name);
  else mRVars.remove(name);
  if (type == DefaultPerm || type == RoutePerm || type == NormalPerm) {
    if (mPVars) mPVars->put(name, value);
    mPerm.insert(name);
  } else mPerm.remove(name);
}


void PsycPacket::putInt (const QString &name, int value, Type type) {
  mVars.putInt(name, value);
  if (type == Normal || type == NormalPerm) mRVars.remove(name);
  else if (mInRoute || type == Route || type == RoutePerm) mRVars.insert(name);
  else mRVars.remove(name);
  if (type == DefaultPerm || type == RoutePerm || type == NormalPerm) {
    if (mPVars) mPVars->putInt(name, value);
    mPerm.insert(name);
  } else mPerm.remove(name);
}


void PsycPacket::del (const QString &name) {
  mVars.del(name);
  mRVars.remove(name);
  mPerm.remove(name);
}


void PsycPacket::setMethod (const QString &name) {
  mMethod = name;
}


void PsycPacket::setBody (const QString &value) {
  mBody = value;
  if (mBody == ".") mBody = " .";
  mBody.replace("\n.\n", "\n . \n");
  if (mBody.left(2) == ".\n") mBody = " . "+mBody.mid(1);
}


const QString PsycPacket::psycTextBody () const {
  QRegExp vre(QLatin1String("\\[([0-9A-Za-z_]+)\\]"), Qt::CaseInsensitive, QRegExp::RegExp2);
  QString t(mBody);
  int pos = 0;
  while ((pos = vre.indexIn(t, pos)) >= 0) {
    int len = vre.matchedLength();
    QString vn(vre.cap(1));
    if (has(vn)) {
      vn = get(vn);
      t.remove(pos, len);
      t.insert(pos, vn);
      len = vn.length();
    }
    pos += len;
  }
  return t;
}


QStringList PsycPacket::routeVars () const {
  QStringList res;
  foreach (const QString &n, mRVars) if (mRVars.contains(n)) res << n;
  return res;
}


QStringList PsycPacket::vars () const {
  QStringList vl(mVars.varList()), res;
  foreach (const QString &n, vl) if (!mRVars.contains(n)) res << n;
  return res;
}


const QString PsycPacket::toString (bool rawBody) const {
  QString res;

  // route vars
  foreach (const QString &rv, mRVars) {
    if (mPerm.contains(rv)) res.append('='); else res.append(':');
    res.append(rv);
    QString v = get(rv);
    if (!v.isNull()) {
      res.append('\t');
      res.append(v);
    }
    res.append('\n');
  }
  // end of route vars
  res.append('\n');
  // normal vars
  QStringList vl(mVars.varList());
  foreach (const QString &n, vl) {
    if (mRVars.contains(n)) continue;
    if (mPerm.contains(n)) res.append('='); else res.append(':');
    res.append(n);
    QString v = get(n);
    if (!v.isNull()) {
      res.append('\t');
      res.append(v);
    }
    res.append('\n');
  }
  // method
  res.append(mMethod);
  res.append('\n');
  // body
  if (!mBody.isNull()) {
    QString v;
    if (rawBody) v = mBody; else v = psycTextBody();
    v.replace("\r\n", "\n");
    if (v.length() > 0 && v[v.length()-1] != '\n') v.append('\n');
    while (v.indexOf("\n.\n") >= 0) v.replace("\n.\n", "\n. \n");
    res.append(v);
  }
  // end
  res.append(".\n");

  return res;
}


QByteArray PsycPacket::toBA () const {
  QByteArray res(toString().toUtf8());
  return res;
}


bool PsycPacket::parseLine (const QString &line) {
  // body?
  //qDebug() << "parse:" << line;
  if (mInBody) {
    if (!mBody.isEmpty()) mBody.append('\n');
    mBody.append(line);
    return true;
  }
  // end of route vars?
  if (line.isEmpty()) {
    // clear continuation
    if (!mLastVar.isNull()) { mLastVar = QString(); mLastPerm = false; }
    mInRoute = false;
    return true;
  }
  QChar ch = line[0];
  bool perm = (ch == '=');
  if (ch == '=' || ch == ':' || ch.isSpace()) {
    // var
    if (line.length() == 1) return true; // actually this packet is invalid, but...
    bool cont = ch.isSpace();
    if (!cont) {
      ch = line[1];
      cont = ch.isSpace();
    }
    if (cont) {
      // continuation; make it tab-delimited
      if (mLastVar.isEmpty()) return false; // continuation w/o var
      //int f; for (f = 1; f < line.length() && line[f].isSpace(); f++) ;
      QString v(line.mid(1).trimmed());
      if (v.isEmpty()) return true;
      QString ov = get(mLastVar);
      if (!ov.isEmpty()) ov.append('\t');
      ov.append(v);
      put(mLastVar, ov, perm?DefaultPerm:Default);
      return true;
    }
    // normal var
    // clear continuation
    if (!mLastVar.isNull()) { mLastVar = QString(); mLastPerm = false; }
    int f; for (f = 1; f < line.length() && !line[f].isSpace(); f++) ;
    QString vn(line.mid(1, f).trimmed());
    if (vn.isEmpty()) return false;
    QString vv(line.mid(f+1).trimmed());
    mLastVar = vn; mLastPerm = perm;
    //qDebug() << "vn:" << vn;
    //qDebug() << "vv:" << vv;
    put(vn, vv, perm?DefaultPerm:Default);
    return true;
    // parse var/value
  } else if (ch != '_') return false;
  // method
  mMethod = line;
  mInBody = true;
  return true;
}


PsycPacket &PsycPacket::operator << (const QString &s) {
  parseLine(s);
  return *this;
}
