/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef PSYCPKT_H
#define PSYCPKT_H

#include <QByteArray>
#include <QObject>
#include <QSet>
#include <QString>

#include "psycvars.h"


class PsycPacket {
public:
  PsycPacket (PsycVars *pvars);
  PsycPacket (const QString &aMethod=QString(""));
  PsycPacket (const PsycPacket &pkt);
  ~PsycPacket ();

  enum Type {
    Default,
    Route,
    Normal,
    DefaultPerm,
    RoutePerm,
    NormalPerm
  };

  void clear ();

  inline const QString get (const char *name, const QString &def=QString()) const { return get(QLatin1String(name), def); }
  inline int getInt (const char *name, int def=-1) const { return getInt(QLatin1String(name), def); }
  inline int getFloat1 (const char *name, int def=0) const { return getFloat1(QLatin1String(name), def); }

  const QString get (const QString &name, const QString &def=QString()) const;
  int getInt (const QString &name, int def=-1) const;
  int getFloat1 (const QString &name, int def=0) const;

  inline void put (const char *name, const QString &value, Type type=Default) { put(QLatin1String(name), value, type); }
  inline void putInt (const char *name, int value, Type type=Default) { putInt(QLatin1String(name), value, type); }

  void put (const QString &name, const QString &value, Type type=Default);
  void putInt (const QString &name, int value, Type type=Default);

  inline void del (const char *name) { del(QLatin1String(name)); }
  void del (const QString &name);

  inline bool has (const char *name) const { return has(QLatin1String(name)); }
  bool has (const QString &name) const;

  inline void endRoute () { mInRoute = false; }
  inline bool inRoute () const { return mInRoute; }

  inline const QString &method () const { return mMethod; }
  inline void setMethod (const char *name) { setMethod(QLatin1String(name)); }
  void setMethod (const QString &name);

  inline const QString &body () const { return mBody; }
  inline void setBody (const char *value) { setBody(QLatin1String(value)); }
  void setBody (const QString &value);

  const QString toString (bool rawBody=false) const;
  QByteArray toBA () const;

  // return packet body as 'interpreted' psyctext
  const QString psycTextBody () const;

  // packet parser
  bool parseLine (const QString &line);

  PsycPacket &operator = (const PsycPacket &user);
  PsycPacket &operator << (const QString &s);

  QStringList routeVars () const ;
  QStringList vars () const ;

private:
  PsycVars *mPVars;
  PsycVars mVars;
  QString mMethod;
  QString mBody;
  bool mInRoute;
  bool mInBody;
  QString mLastVar;
  bool mLastPerm;
  QSet<QString> mRVars;
  QSet<QString> mPerm;
};


#endif
