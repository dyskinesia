/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "psycvars.h"


PsycVars::PsycVars () {
}


PsycVars::PsycVars (const PsycVars &aObj) {
  *this = aObj;
}


PsycVars::~PsycVars () {
  //clear();
}


PsycVars &PsycVars::operator = (const PsycVars &vars) {
  if (&vars != this) {
    mList.clear();
    QStringList keys(vars.mList.keys());
    foreach (const QString &s, keys) mList[s] = vars.mList[s];
  }
  return *this;
}


void PsycVars::clear () {
  mList.clear();
}


const QString PsycVars::get (const QString &name, const QString &def) const {
  if (!mList.contains(name)) return def;
  return mList[name];
}


int PsycVars::getInt (const QString &name, int def) const {
  if (!mList.contains(name)) return def;
  QString v(mList[name]);
  bool ok;
  int res = v.toInt(&ok);
  if (!ok) res = def;
  return res;
}


int PsycVars::getFloat1 (const QString &name, int def) const {
  if (!mList.contains(name)) return def;
  QString v(mList[name]);
  if (v.isEmpty() || !v[0].isDigit()) return def;
  return v[0].unicode()-'0';
}


void PsycVars::put (const QString &name, const QString &value) {
  mList[name] = value;
}


void PsycVars::putInt (const QString &name, int value) {
  QString v(QString::number(value));
  mList[name] = v;
}


void PsycVars::del (const QString &name) {
  mList.remove(name);
}


QStringList PsycVars::varList () const {
  QStringList res = mList.keys();
  //foreach (const QString &n, mList) res<<n;
  return res;
}
