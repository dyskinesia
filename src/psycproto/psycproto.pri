DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD

HEADERS += \
  $$PWD/psycvars.h \
  $$PWD/psycpkt.h \
  $$PWD/psycproto.h

SOURCES += \
  $$PWD/psycvars.cpp \
  $$PWD/psycpkt.cpp \
  $$PWD/psycproto.cpp
