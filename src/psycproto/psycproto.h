/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef PSYCPROTO_H
#define PSYCPROTO_H

#include <QAbstractSocket>
#include <QTcpSocket>
#include <QObject>

#ifdef USE_SSL
# include <QSslSocket>
#endif

class PsycVars;
class PsycPacket;
class PsycEntity;
class QDateTime;
class QTimer;


class PsycProto : public QObject {
  Q_OBJECT

public:
  PsycProto (QObject *parent=0);
  ~PsycProto ();

  enum Status {
    Internal=0,
    Offline=1,
    Vacation=2,
    Away=3,
    // others are avail
    DND=4,
    Nearby=5,
    Busy=6,
    Here=7,
    FFC=8, // free-for-chat (talkative)
    Realtime=9 // unused
  };

  enum Mood {
    Unspecified=0,
    Dead=1,
    Angry=2,
    Sad=3,
    Moody=4,
    Okay=5,
    Good=6,
    Happy=7,
    Bright=8,
    Nirvana=9
  };

  enum Notification {
    None=0,
    Offered,
    Pending,
    Mute,
    DelayedMore,
    Delayed,
    Immediate
  };

  static QString statusName (Status ad);
  static QString statusName (int ad);
  static QString moodName (Mood mood);
  static QString moodName (int mood);

  static bool isValidNick (const QString &nick);

  inline PsycVars *pvars () const { return mPVars; }

  void clear ();

  bool setUNI (const QString &aUNI);

  inline const QString &uni () const { return mUNI; }
  inline const QString &host () const { return mHost; }
  inline quint16 port () const { return mPort; }
  inline const QString &nick () const { return mNick; }
  inline const QString &userAgent () const { return mUserAgent; }
  inline void setUserAgent (const QString &ua) { mUserAgent = ua; }

  inline bool secureLogin () const { return mSecureLogin; }
  inline void setSecureLogin (bool doSec) { mSecureLogin = doSec; }

  inline bool useSSL () const { return mUseSSL; }
  inline void setUseSSL (bool flg) {
    //FIXME!
#ifdef USE_SSL
    mUseSSL = flg;
#else
    mUseSSL = false;
#endif
  }

  bool connectToServer ();
  void disconnectFromServer ();

  inline bool isConnected () const { return mConnected; }
  inline bool isLinked () const { return mLinked; }
  inline bool isLoggedIn () const { return mLoggedIn; }

  void logoff ();

  void sendPacket (const PsycPacket &pkt);

  void sendPassword (const QString &pass);

  // return cookie for ack (acks don't work now)
  quint64 sendMessage (const PsycEntity &dest, const QString &text, const QString &action=QString(""));

  void enterPlace (const PsycEntity &dest);
  void leavePlace (const PsycEntity &dest);

  void requestPeerList ();

  void clearLog ();

  void requestAuth (const PsycEntity &dest, const QString &text=QString());
  void sendAuth (const PsycEntity &dest, const QString &text=QString());
  void removeAuth (const PsycEntity &dest, const QString &text=QString());

  void setStatus (Status degree, const QString &text=QString(""), Mood mood=Unspecified);
  inline Status status () const { return mStatus; }
  inline const QString &statusText () const { return mStatusText; }
  inline void setStatusText (const QString &st) { mStatusText = st; }
  inline Mood mood () const { return mMood; }

  void doCommand (const QString &cmd, const QString &destUni=QString(""), const QString &focus=QString(""),
    const QString &tag=QString(""));

  void invite (const QString &place, const QString &destUni, const QString &tag=QString(""));

public slots:
  void sendPing ();

signals:
  void packetComes (const PsycPacket &pkt);
  void packetSent (const PsycPacket &pkt);

  void netError (const QString &errStr);
  void connected ();
  void disconnected ();

  void needPassword (); // call sendPassword()
  void invalidPassword ();
  void loginComplete ();
  //void personOnOff (const PsycEntity &user);

  void userStatusChanged (const PsycEntity &user);
  void selfStatusChanged ();

  void nickEnforced (); // emits when server enforces new nick

  void placeEntered (const PsycEntity &place);
  void placeLeaved (const PsycEntity &place);
  void userEntersPlace (const PsycEntity &place, const PsycEntity &who);
  void userLeavesPlace (const PsycEntity &place, const PsycEntity &who);

  // message ack (aUNI: recepient)
  void messageAck (quint64 cookie, const QString &aUNI);

  void authRequested (const PsycEntity &dest);
  void authGranted (const PsycEntity &dest);
  void authDenied (const PsycEntity &dest);

  void notificationType (const PsycEntity &dest);

  void message (const PsycEntity &place, const PsycEntity &user, const QDateTime &time, const QString &text,
    const QString &action);
  void messageEcho (const PsycEntity &place, const PsycEntity &user, const PsycEntity &target, const QDateTime &time,
    const QString &text, const QString &action, const QString &tag);
  void historyMessage (const PsycEntity &place, const PsycEntity &user, const QDateTime &time, const QString &text,
    const QString &action);

  void scratchpadUpdated (const QString &spUrl);
  void placeWebExamined (const PsycEntity &place, const PsycPacket &pkt);

  void placeNews (const PsycEntity &place, const QString &channel, const QString &title, const QString &url);

  void noticeUpdate (const PsycEntity &place, const PsycPacket &pkt);

private slots:
  void bytesComes ();
  void onConnected ();
  void onDisconnected ();
  void onError (QAbstractSocket::SocketError socketError);
  void onPeerVerifyError (const QSslError &error);
  void onSSLErrors (const QList<QSslError> &errors);

  void pkt__warning (const PsycPacket &pkt);
  void pkt__error (const PsycPacket &pkt);
  void pkt__failure (const PsycPacket &pkt);

  void pkt__error_invalid_password (const PsycPacket &pkt);

  void pkt__error_status_place_matches (const PsycPacket &pkt);
  void pkt__echo_place_enter (const PsycPacket &pkt);
  void pkt__echo_place_leave (const PsycPacket &pkt);
  void pkt__notice_place_enter (const PsycPacket &pkt);
  void pkt__notice_place_leave (const PsycPacket &pkt);

  void pkt__notice_logon_last (const PsycPacket &pkt);

  void pkt__notice_circuit_established (const PsycPacket &pkt);
  void pkt__notice_link (const PsycPacket &pkt);
  void pkt__notice_unlink (const PsycPacket &pkt);
  void pkt__info_nickname (const PsycPacket &pkt);

  void pkt__status_circuit (const PsycPacket &pkt);
  void pkt__query_password (const PsycPacket &pkt);
  void pkt__notice_login (const PsycPacket &pkt);
  void pkt__status_log_none (const PsycPacket &pkt);
  void pkt__status_log_new (const PsycPacket &pkt);

  void pkt__status_presence_description (const PsycPacket &pkt);
  void pkt__status_presence (const PsycPacket &pkt);

  void pkt__status_person_present (const PsycPacket &pkt);
  void pkt__list_friends (const PsycPacket &pkt);
  void pkt__list_user_description (const PsycPacket &pkt);
  void pkt__notice_presence (const PsycPacket &pkt);

  void pkt__message_echo (const PsycPacket &pkt);

  void pkt__warning_pending_friendship (const PsycPacket &pkt);
  void pkt__warning_duplicate_friendship (const PsycPacket &pkt);
  void pkt__request_friendship (const PsycPacket &pkt);
  void pkt__echo_friendship_removed (const PsycPacket &pkt);
  void pkt__notice_friendship_established (const PsycPacket &pkt);
  void pkt__notice_friendship_removed (const PsycPacket &pkt);
  void pkt__status_friendship_established (const PsycPacket &pkt);
  void pkt__list_acquaintance_notification_offered (const PsycPacket &pkt);
  void pkt__list_acquaintance_each (const PsycPacket &pkt);

  void pkt__message_private (const PsycPacket &pkt);
  void pkt__message_public (const PsycPacket &pkt);

  void pkt__notice_update_scratchpad (const PsycPacket &pkt);
  void pkt__notice_examine_web_person (const PsycPacket &pkt);
  void pkt__notice_examine_web_place (const PsycPacket &pkt); // new synthax
  void pkt__notice_place_examine_web (const PsycPacket &pkt); // old synthax

  void pkt__notice_update (const PsycPacket &pkt);

  void pkt__notice_headline_news (const PsycPacket &pkt);
  void pkt__notice_offered_file_torrent (const PsycPacket &pkt);

private:
  // the very stupid packet dispatcher %-)
  bool dispatchPacket (const PsycPacket &pkt);

  bool extractPacketUser (PsycEntity &dst, const PsycPacket &pkt) const;
  bool extractPacketPlace (PsycEntity &dst, const PsycPacket &pkt) const;
  QList<PsycEntity *> parseNickList (const PsycPacket &pkt, const QString &uniField, const QString &nickField);
  void clearEntityList (QList<PsycEntity *> &list);

  QString mUserAgent;
  QString mHost;
  quint16 mPort;
  QString mNick;
  QString mUNI;
#ifdef USE_SSL
  QSslSocket mSock;
#else
  QTcpSocket mSock;
#endif
  bool mUseSSL;

  bool mConnected;
  bool mLinked;
  bool mLoggedIn;

  PsycVars *mPVars;
  PsycPacket *mCurPacket;

  enum PassHash { Plain, MD5, SHA1 };
  PassHash mPassHash;
  QString mPassSalt;
  int mLogPktLeft;

  quint64 mMsgCookie;
  bool mSecureLogin;

  Status mStatus;
  QString mStatusText;
  Mood mMood;

  QTimer *mPingTimer;

  // the following two are for avoiding endless "auth granted", etc
  QSet<QString> mFSPending; // list of pending friendships
  QSet<QString> mFSDups; // list of duplicate friendships
};


///////////////////////////////////////////////////////////////////////////////
class PsycEntity {
public:
  PsycEntity ();
  PsycEntity (const QString &aUNI);
  PsycEntity (const PsycEntity &user);
  ~PsycEntity ();

  void clear ();

  inline bool isValid () const { return !(mUNI.isEmpty()); }

  inline bool isXMPP () const { return mProto == "xmpp"; }
  inline bool isIRC () const { return mProto == "irc"; }
  inline bool isPSYC () const { return mProto == "psyc"; }

  inline const QString &uni () const { return mUNI; }
  inline const QString &host () const { return mHost; }
  inline quint16 port () const { return mPort; }
  inline const QString &proto () const { return mProto; }
  inline const QString &nick () const { return mNick; }
  inline const QString &verbatim () const { return mVerbatim; }
  inline const QString &channel () const { return mChan; }
  inline bool isPlace () const { return mPlace; }

  bool setUNI (const QString &aUNI);
  bool setNick (const QString &nick);
  bool setHost (const QString &host);
  bool setPort (quint16 port);
  bool setProto (const QString &proto);
  bool setPlace (bool place);
  bool setVerbatim (const QString &nick);
  bool setChannel (const QString &chan);

  inline PsycProto::Status status () const { return mStatus; }
  inline const QString &statusText () const { return mStatusText; }
  inline PsycProto::Mood mood () const { return mMood; }
  inline PsycProto::Notification notification () const { return mNotification; }

  inline void setStatus (PsycProto::Status av) { mStatus = av; }
  inline void setStatusText (const QString &dsc) { mStatusText = dsc; }
  inline void setMood (PsycProto::Mood mood) { mMood = mood; }
  inline void setNotification (PsycProto::Notification n) { mNotification = n; }

  static bool isValidPSYCNick (const QString &nick);

  PsycEntity &operator = (const PsycEntity &user);
  inline bool operator == (const PsycEntity &user) const { return sameUNI(user.uni()); }
  inline bool operator == (const QString &uni) const { return sameUNI(uni); }
  inline bool operator != (const PsycEntity &user) const { return !sameUNI(user.uni()); }
  inline bool operator != (const QString &uni) const { return !sameUNI(uni); }

  inline bool sameUNI (const QString &uni) const { return uni.compare(mUNI, Qt::CaseInsensitive) == 0; }

private:
  bool parsePSYC (const QString &aUNI);
  bool parseXMPP (const QString &aUNI);
  bool parseIRC (const QString &aUNI);

  QString mProto;
  QString mHost;
  quint16 mPort;
  QString mNick;
  QString mVerbatim;
  QString mChan;
  QString mUNI;
  PsycProto::Status mStatus;
  QString mStatusText;
  PsycProto::Mood mMood;
  PsycProto::Notification mNotification;
  bool mPlace;
};


#endif
