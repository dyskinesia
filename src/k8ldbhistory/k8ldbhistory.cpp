/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QDir>

#include "leveldb/write_batch.h"

#include "k8strutils.h"
#include "k8json.h"

#include "k8history.h"


//#define KH_DEBUG

#define KHISTORY_DATE_FORMAT  "yyyy/MM/dd HH:mm:ss"


///////////////////////////////////////////////////////////////////////////////
HistoryFile::HistoryFile (const QString &fname, const QString &myUni) :
  mMode(Read), mName(fname+".ldb"), mMyUni(myUni), mCount(-1), mDb(0)
{
}


HistoryFile::~HistoryFile () {
  close();
}


bool HistoryFile::isOpen () const {
  return mDb!=0;
}


int HistoryFile::count () {
  return mCount;
}


void HistoryFile::remove () {
  close();
  leveldb::DestroyDB(std::string(mName.toUtf8().constData()), leveldb::Options());
}


//bool HistoryFile::open (bool allowCreate)
bool HistoryFile::open (OpenMode newmode) {
  bool newFile = false;
  leveldb::Status status;
  //
  close();
  leveldb::Options options;
  if (newmode == Read) {
    options.create_if_missing = false;
    options.error_if_exists = true;
    status = leveldb::DB::Open(options, std::string(mName.toUtf8().constData()), &mDb);
    if (status.ok()) return false; //wtf?
    options.error_if_exists = false;
  } else {
    options.create_if_missing = true;
    options.error_if_exists = false;
  }
  status = leveldb::DB::Open(options, std::string(mName.toUtf8().constData()), &mDb);
  if (!status.ok()) {
    if (newmode != Write) return false;
    QString dir(mName);
    int idx = dir.lastIndexOf('/');
    if (idx > 0) {
      dir.truncate(idx);
      QDir d;
      d.mkdir(dir);
    }
    options.create_if_missing = true;
    status = leveldb::DB::Open(options, std::string(mName.toUtf8().constData()), &mDb);
    if (!status.ok()) { remove(); return false; }
    newFile = true;
  }
  mMode = newmode;
  if (!initialize()) {
    close();
    if (newFile) remove();
    return false;
  }
  return true;
}


bool HistoryFile::initialize () {
  //fprintf(stderr, "scanning...\n");
  std::string cnt;
  leveldb::Status res;
  bool ok = false;
  //
  res = mDb->Get(leveldb::ReadOptions(), "/info/total", &cnt);
  if (!res.ok()) {
    if (mMode != Write) return false;
    res = mDb->Put(leveldb::WriteOptions(), "/info/total", "0");
    if (!res.ok()) return false;
    mCount = 0;
  } else {
    mCount = QString(cnt.c_str()).toInt(&ok, 10);
    if (!ok || mCount < 0 || mCount > 1024*1024*1024) return false;
  }
  return true;
}


void HistoryFile::close () {
  if (mDb) {
    delete mDb;
    mDb = 0;
    mCount = -1;
    mMode = Read;
  }
}


bool HistoryFile::append (const HistoryMessage &msg) {
  std::string k;
  leveldb::Status res;
  int type;
  qint64 ms;
  //
  if (!mDb || mMode != Write || !msg.valid) return false;
  switch (msg.type) {
    case HistoryMessage::Incoming: type = 0; break;
    case HistoryMessage::Outgoing: type = 1; break;
    case HistoryMessage::Server: type = 2; break;
    case HistoryMessage::Info:
    default: type = 3; break;
  };
  ms = msg.date.toUTC().toMSecsSinceEpoch();
  k = "/messages/"+std::string(QString::number(mCount, 10).toAscii().constData());
  {
    leveldb::WriteBatch batch;
    batch.Put(k+"/type", std::string(QString::number(type, 10).toAscii().constData()));
    //batch.Put(k+"/date", std::string(dateToString(msg.date).toUtf8().constData()));
    batch.Put(k+"/date", std::string(QString::number(ms, 10).toAscii().constData()));
    batch.Put(k+"/uni", std::string(msg.uni.toUtf8().constData()));
    batch.Put(k+"/text", std::string(msg.text.toUtf8().constData()));
    if (!msg.action.isEmpty()) {
      batch.Put(k+"/action", std::string(msg.action.toUtf8().constData()));
    }
    batch.Put("/info/total", std::string(QString::number(mCount+1, 10).toAscii().constData()));
    res = mDb->Write(leveldb::WriteOptions(), &batch);
    if (!res.ok()) return false;
  }
  ++mCount;
  return true;
}


bool HistoryFile::read (int idx, HistoryMessage &msg) {
  std::string k, s;
  leveldb::Status res;
  qint64 ms;
  int type;
  bool ok = false;
  //
  msg.clear();
  if (!mDb || idx < 0 || idx >= mCount) return false;
  k = "/messages/"+std::string(QString::number(idx, 10).toAscii().constData());
  //
  res = mDb->Get(leveldb::ReadOptions(), k+"/type", &s);
  if (!res.ok()) return true;
  type = QString(s.c_str()).toInt(&ok, 10);
  if (!ok) return true;
  switch (type) {
    case 0: msg.type = HistoryMessage::Incoming; break;
    case 1: msg.type = HistoryMessage::Outgoing; break;
    case 2: msg.type = HistoryMessage::Server; break;
    case 3: msg.type = HistoryMessage::Info; break;
    default: return true;
  }
  //
  res = mDb->Get(leveldb::ReadOptions(), k+"/date", &s);
  if (!res.ok()) return true;
  ms = QString(s.c_str()).toLongLong(&ok, 10);
  if (!ok || ms < 0) return true;
  msg.date.setTimeSpec(Qt::UTC);
  msg.date.setMSecsSinceEpoch(ms);
  msg.date = msg.date.toLocalTime();
  //
  res = mDb->Get(leveldb::ReadOptions(), k+"/uni", &s);
  if (!res.ok()) return true;
  msg.uni = QString::fromUtf8(s.c_str());
  //
  res = mDb->Get(leveldb::ReadOptions(), k+"/text", &s);
  if (!res.ok()) return true;
  msg.text = QString::fromUtf8(s.c_str());
  //
  res = mDb->Get(leveldb::ReadOptions(), k+"/action", &s);
  if (res.ok()) {
    msg.action = QString::fromUtf8(s.c_str());
    if (msg.action.isEmpty()) msg.action = QString();
  } else {
    msg.action = QString();
  }
  //
  msg.valid = true;
  return true;
}
