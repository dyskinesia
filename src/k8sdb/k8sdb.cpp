/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
/*
 * TODO
 *  * page joining/splitting
 *  * some sort of tree to find the best page
 *  * easy: get a used page and move key from it to another?
 *  * easy: compact unused pages (join 'em)
 */
#include <QDebug>

#include "string.h"

#include <QByteArray>
#include <QDateTime>
#include <QDir>

#include "k8sdb.h"


static const char *dbSign = "DyskinesiaDB001";

struct __attribute__((__packed__)) tsHeader {
  char sign[16]; // DyskinesiaDB001\0
  qint32 firstPageOfs; // from here and to the end of the file
  qint32 freePlaceOfs; // from here and to the end of the file
};

struct __attribute__((__packed__)) tsPageHeader {
  quint16 pageSize; // with header!
  quint16 dataSize;
  quint8 keyType; // 0: free page
  quint8 nameLen;
  uchar name[1];
};

const int PH_HEADER_SIZE = (int)sizeof(struct tsPageHeader)-1;

// page:
// dw size
// dw datasize
// db nameLen (w/o 0)
// name (utf8, zterm)
// data


///////////////////////////////////////////////////////////////////////////////
K8SDB::K8SDB (const QString &aPathName, CreateMode cMode) :
  mDB(aPathName), mMode(cMode), mMap(0), mHeader(0), mTypeOverwrite(true)
{
  if (cMode == WipeAlways || (!mDB.exists() && cMode == OpenAlways)) {
    mDB.remove();

    QString s(aPathName);
    s.replace('\\', "/");
    int li0 = s.lastIndexOf('/');
    if (li0 > 0) {
      s.truncate(li0);
      if (s != ".") {
        QDir d(s); d.mkpath(s);
      }
    }

    if (mDB.open(QIODevice::ReadWrite | QIODevice::Truncate)) {
      writeInitHeader();
      mDB.close();
    }
  }
  if (mDB.exists()) {
    if (mDB.open(QIODevice::ReadWrite)) {
      mMap = mDB.map(0, mDB.size());
      if (mMap) readInitHeader();
      else mDB.close();
    }
  }
}


K8SDB::~K8SDB () {
  if (mMap) mDB.unmap(mMap);
}


bool K8SDB::hasKey (const QString &name) const {
  if (name.isEmpty()) return false;
  return mKeys.contains(name);
}


K8SDB::Type K8SDB::type (const QString &name) {
  struct tsPageHeader *ph = keyPagePtr(name);
  if (!ph) return Invalid;
  if (ph->keyType >= (quint8)(MaxKeyType)) return Invalid;
  return (Type)(ph->keyType);
}


QStringList K8SDB::keys () const {
  return mKeys.keys();
}


QStringList K8SDB::keysPfx (const QString &pfx) const {
  if (pfx.isEmpty()) return keys();
  QStringList res(mKeys.keys());
  for (int f = 0; f < res.count(); ) {
    if (res[f].startsWith(pfx)) f++; else res.removeAt(f);
  }
  return res;
}


///////////////////////////////////////////////////////////////////////////////
bool K8SDB::set (const QString &name, bool v) {
  if (!mMap) return false;
  struct tsPageHeader *ph = keyPagePtr(name);
  if (ph && ph->keyType != Boolean) {
    if (!mTypeOverwrite) return false;
    remove(name);
    ph = 0;
  }
  if (!ph) {
    ph = addKeyNoData(name, Boolean, 1);
    if (!ph) return false;
  }
  quint8 *kvp = (quint8 *)keyDataPtr(name);
  *kvp = v?1:0;
  return true;
}


bool K8SDB::set (const QString &name, qint32 v) {
  if (!mMap) return false;
  struct tsPageHeader *ph = keyPagePtr(name);
  if (ph && ph->keyType != Integer) {
    if (!mTypeOverwrite) return false;
    remove(name);
    ph = 0;
  }
  if (!ph) {
    ph = addKeyNoData(name, Integer, 4);
    if (!ph) return false;
  }
  qint32 *kvp = (qint32 *)keyDataPtr(name);
  *kvp = v;
  return true;
}


bool K8SDB::set (const QString &name, const QString &v) {
  if (!mMap) return false;
  struct tsPageHeader *ph = keyPagePtr(name);
  if (ph && ph->keyType != String) {
    if (!mTypeOverwrite) return false;
    remove(name);
    ph = 0;
  }
  QByteArray ba(v.toUtf8());
  if (!ph) {
    ph = addKeyNoData(name, String, ba.size());
    if (!ph) return false;
  }
  int vsz = ba.size();
  char *kvp = (char *)updateSettingPtr(name, vsz);
  if (vsz > 0) memcpy(kvp, ba.constData(), vsz);
  return true;
}


bool K8SDB::set (const QString &name, const QByteArray &v) {
  if (!mMap) return false;
  struct tsPageHeader *ph = keyPagePtr(name);
  if (ph && ph->keyType != ByteArray) {
    if (!mTypeOverwrite) return false;
    remove(name);
    ph = 0;
  }
  if (!ph) {
    ph = addKeyNoData(name, ByteArray, v.size());
    if (!ph) return false;
  }
  int vsz = v.size();
  char *kvp = (char *)updateSettingPtr(name, vsz);
  if (vsz > 0) memcpy(kvp, v.constData(), vsz);
  return true;
}


bool K8SDB::set (const QString &name, const QKeySequence &v) {
  if (!mMap) return false;
  struct tsPageHeader *ph = keyPagePtr(name);
  if (ph && ph->keyType != KeySequence) {
    if (!mTypeOverwrite) return false;
    remove(name);
    ph = 0;
  }
  QByteArray ba(v.toString().toUtf8());
  if (!ph) {
    ph = addKeyNoData(name, KeySequence, ba.size());
    if (!ph) return false;
  }
  int vsz = ba.size();
  char *kvp = (char *)updateSettingPtr(name, vsz);
  if (vsz > 0) memcpy(kvp, ba.constData(), vsz);
  return true;
}


bool K8SDB::set (const QString &name, Type type, const QString &v) {
  if (!mMap) return false;
  QString s1(v.trimmed().toLower());
  bool ok = false;
  int i;
  QKeySequence sq;
  QByteArray ba;
  switch (type) {
    case Boolean:
      return set(name, !s1.isEmpty() &&
        s1 != "0" && s1 != "no" && s1 != "off" && s1 != "ona" && s1 != "false" && s1 != "n" && s1 != "o");
    case Integer:
      i = s1.toInt(&ok);
      if (ok) return set(name, i);
      return false;
    case String:
      return set(name, v);
    case ByteArray:
      ba = v.toUtf8();
      return set(name, ba);
    case KeySequence:
      sq = QKeySequence::fromString(s1);
      return set(name, sq);
    default: ;
  }
  return false;
}


///////////////////////////////////////////////////////////////////////////////
bool K8SDB::toBool (const QString &name, bool *ok) {
  if (type(name) != Boolean) {
    if (ok) *ok = false;
    return false;
  }
  if (ok) *ok = true;
  quint8 *v = (quint8 *)keyDataPtr(name);
  return (*v != 0);
}


qint32 K8SDB::toInt (const QString &name, bool *ok) {
  if (type(name) != Integer) {
    if (ok) *ok = false;
    return 0;
  }
  if (ok) *ok = true;
  qint32 *v = (qint32 *)keyDataPtr(name);
  return *v;
}


QString K8SDB::toString (const QString &name, bool *ok) {
  if (type(name) != String) {
    if (ok) *ok = false;
    return QString();
  }
  if (ok) *ok = true;
  struct tsPageHeader *ph = keyPagePtr(name);
  if (!ph->dataSize) return QString();
  char *v = (char *)keyDataPtr(name);
  QByteArray ba(v, ph->dataSize);
  return QString::fromUtf8(ba);
}


QByteArray K8SDB::toByteArray (const QString &name, bool *ok) {
  if (type(name) != ByteArray) {
    if (ok) *ok = false;
    return QByteArray();
  }
  if (ok) *ok = true;
  struct tsPageHeader *ph = keyPagePtr(name);
  if (!ph->dataSize) return QByteArray();
  char *v = (char *)keyDataPtr(name);
  return QByteArray(v, ph->dataSize);
}


QKeySequence K8SDB::toKeySequence (const QString &name, bool *ok) {
  if (type(name) != KeySequence) {
    if (ok) *ok = false;
    return QKeySequence();
  }
  if (ok) *ok = true;
  struct tsPageHeader *ph = keyPagePtr(name);
  if (!ph->dataSize) return QKeySequence();
  char *v = (char *)keyDataPtr(name);
  QByteArray ba(v, ph->dataSize);
  QString s(QString::fromUtf8(ba));
  return QKeySequence::fromString(s);
}


QString K8SDB::asString (const QString &name, bool *ok) {
  if (type(name) == Invalid) {
    if (ok) *ok = false;
    return QString();
  }
  if (ok) *ok = true;
  struct tsPageHeader *ph = keyPagePtr(name);
  void *dv = keyDataPtr(name);
  switch (ph->keyType) {
    case Boolean: return QString(*((quint8 *)dv)!=0?"true":"false");
    case Integer: return QString::number(*((qint32 *)dv));
  }
  if (!ph->dataSize) return QString();
  QByteArray ba((char *)dv, ph->dataSize);
  if (ph->keyType == ByteArray) {
    QByteArray r(ba.toBase64());
    return QString(r);
  }
  return QString::fromUtf8(ba);
}


///////////////////////////////////////////////////////////////////////////////
void K8SDB::writeInitHeader () {
  struct tsHeader h;
  memset(&h, 0, sizeof(struct tsHeader));
  memcpy(h.sign, dbSign, 16);
  h.firstPageOfs = h.freePlaceOfs = h.firstPageOfs+sizeof(struct tsHeader);
  mDB.write((const char *)(&h), sizeof(struct tsHeader));
}


// i LOVE gotos!
void K8SDB::readInitHeader () {
  mHeader = 0;
  mKeys.clear();
  mPages.clear();
  mFreePages.clear();
  //
  int kp = 0, pgIdx = 0;
  qint32 fsz = mDB.size();
  if (fsz < (int)sizeof(tsHeader)) goto error;
  mHeader = (struct tsHeader *)mMap;
  if (memcmp(mHeader, dbSign, 16)) goto error;
  if (fsz < mHeader->firstPageOfs || mHeader->freePlaceOfs < mHeader->firstPageOfs ||
      mHeader->freePlaceOfs < (int)sizeof(struct tsHeader)) goto error;
  fsz = mHeader->freePlaceOfs-mHeader->firstPageOfs;
  // scan pages, build list
  kp = mHeader->firstPageOfs;
  pgIdx = 0;
  while (fsz > 0) {
    if (fsz-PH_HEADER_SIZE < 0) goto error; // no room for header
    struct tsPageHeader *ph = (struct tsPageHeader *)(mMap+kp);
    fsz -= ph->pageSize;
    if (fsz < 0) goto error;
    mPages << kp; // remember this page
    if (ph->keyType > 0 && ph->keyType <= (int)MaxKeyType && ph->nameLen) {
      // used page
      const char *knc = (const char *)(ph->name);
      QByteArray ba(knc, ph->nameLen);
      QString name(QString::fromUtf8(ba));
      if (!name.isEmpty()) {
        mKeys[name] = pgIdx;
        //!!qDebug() << "key:" << name << "type:" << ph->keyType;
      } else {
        qDebug() << "invalid key at:" << kp;
        mFreePages << pgIdx;
      }
    } else {
      // free page, remember it
      mFreePages << pgIdx;
    }
    // move to next page
    kp += ph->pageSize;
    pgIdx++;
  }
  goto done;
error:
  mDB.unmap(mMap);
  mDB.close();
  mMap = 0;
  mHeader = 0;
  mKeys.clear();
  mPages.clear();
  mFreePages.clear();
done:
  return;
}


///////////////////////////////////////////////////////////////////////////////
struct tsPageHeader *K8SDB::keyPagePtr (const QString &name) {
  if (!mMap) return 0;
  if (name.isEmpty() || !mKeys.contains(name)) return 0;
  return (struct tsPageHeader *)(mMap+mPages[mKeys[name]]);
}


void *K8SDB::keyDataPtr (const QString &name) {
  if (!mMap) return 0;
  if (name.isEmpty() || !mKeys.contains(name)) return 0;
  struct tsPageHeader *ph = (struct tsPageHeader *)(mMap+mPages[mKeys[name]]);
  return (void *)(((uchar *)ph)+PH_HEADER_SIZE+ph->nameLen);
}


uchar *K8SDB::updateSettingPtr (const QString &name, int dataSize) {
  if (!mMap) return 0;
  if (name.isEmpty() || !mKeys.contains(name)) return 0;
  int idx = mKeys[name];
  int ko = mPages[idx];
  int fsp;
  quint8 kt;
  struct tsPageHeader *ph = (struct tsPageHeader *)(mMap+ko);
  int hdrLen = PH_HEADER_SIZE+ph->nameLen;
  uchar *dp = (uchar *)(mMap+ko+hdrLen);
  switch ((Type)ph->keyType) {
    case KeySequence: case String: case ByteArray:
      fsp = ph->pageSize-hdrLen; // free space in page
      if (fsp < dataSize) {
        // need to get new page (free this one)
        kt = ph->keyType;
        freePage(idx);
        ph = addKeyNoData(name, (Type)kt, dataSize);
        if (!ph) {
          mKeys.remove(name);
          return 0;
        }
        ko = mPages[mKeys[name]];
        dp = (mMap+ko+PH_HEADER_SIZE+ph->nameLen);
      } else {
        // we *SHOULD* have enough space
        ph->dataSize = dataSize;
      }
    default: ;
  }
  return dp;
}


///////////////////////////////////////////////////////////////////////////////
bool K8SDB::freePage (int pgIdx) {
  if (!mMap || pgIdx < 0 || pgIdx >= mPages.count()) return 0;
  mFreePages << pgIdx;
  struct tsPageHeader *ph = (struct tsPageHeader *)(mMap+mPages[pgIdx]);
  ph->keyType = 0;
  // remove free pages at the end of the file
  qint32 newFO = -1;
  int f = mPages.count()-1;
  while (f >= 0 && mFreePages.contains(f)) {
    newFO = mPages[f];
    mPages.removeAt(f);
    mFreePages.remove(f);
    f--;
  }
  if (newFO > 0) mHeader->freePlaceOfs = newFO;
  return true;
}


bool K8SDB::remove (const QString &name) {
  if (!mMap || name.isEmpty() || !mKeys.contains(name)) return false;
  int idx = mKeys[name];
  mKeys.remove(name);
  return freePage(idx);
}


bool K8SDB::removeAll (const QString &name) {
  if (!mMap || name.isEmpty()) return false;
  QStringList keys(keysPfx(name));
  bool res = false;
  foreach (const QString &key, keys) if (remove(key)) res = true;
  return res;
}


///////////////////////////////////////////////////////////////////////////////
// no need to check if we have such key here -- it's already done
// it doesn't matter even if we have one
struct tsPageHeader *K8SDB::addKeyNoData (const QString &name, Type type, int dataSize) {
  if (!mMap) return 0;
  QByteArray ba(name.toUtf8());
  if (ba.size() > 254) return 0; // bad name, don't be crazy!
  int aDelta = 0;
  switch (type) {
    case Boolean: dataSize = 1; break;
    case Integer: dataSize = 4; break;
    default: aDelta = 64; break; // reserve some space for variable-length keys
  }
  int pageSz = dataSize+PH_HEADER_SIZE+ba.size();
  if (dataSize < 0 || dataSize > 65535) return 0; // invalid data size
  if (aDelta > 0 && pageSz+aDelta > 65535) aDelta = 65535-pageSz;
  // find best free page (if any)
  int pgToUse = -1, bestSize = 70000;
  foreach (int idx, mFreePages) {
    struct tsPageHeader *ph = (struct tsPageHeader *)(mMap+mPages[idx]);
    int s = ph->pageSize;
    if (s >= pageSz && s < bestSize) {
      pgToUse = idx; bestSize = s;
      if (s == pageSz) break; // no need to continue, we found a perfectly fitting page
    }
  }
  bool fixSize = false;
  if (pgToUse == -1) {
    // allocate space for new page (rewrite this!)
    int fleft = mDB.size()-mHeader->freePlaceOfs;
    if (fleft < pageSz+aDelta) {
      // let the file grow
      int xp = mDB.size();//mHeader->freePlaceOfs;
      mDB.unmap(mMap);
      mDB.seek(xp);
      int toWrite = pageSz+aDelta;
      // determine growth
      char buf[1024]; memset(buf, 0, sizeof(buf));
      while (toWrite > 0) {
        int wr = toWrite; if (wr > (int)sizeof(buf)) wr = (int)sizeof(buf);
        mDB.write(buf, wr);
        toWrite -= wr;
      }
      mMap = mDB.map(0, mDB.size());
      if (!mMap) {
        mDB.close();
        mMap = 0;
        mHeader = 0;
        return 0;
      }
      mHeader = (struct tsHeader *)mMap;
    }
    // add new page
    pgToUse = mPages.count();
    mPages << mHeader->freePlaceOfs;
    mHeader->freePlaceOfs += pageSz+aDelta;
    fixSize = true;
  }
  struct tsPageHeader *ph = (struct tsPageHeader *)(mMap+mPages[pgToUse]);
  if (fixSize) ph->pageSize = pageSz+aDelta;
  ph->dataSize = dataSize;
  ph->keyType = (quint8)type;
  ph->nameLen = ba.size();
  if (ph->nameLen) memcpy(ph->name, ba.constData(), ph->nameLen);
  mKeys[name] = pgToUse;
  return ph;
}
