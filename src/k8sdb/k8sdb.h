/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef K8SDB_H
#define K8SDB_H

#include <QByteArray>
#include <QFile>
#include <QHash>
#include <QList>
#include <QKeySequence>
#include <QSet>
#include <QString>
#include <QStringList>


class K8SDB {
public:
  enum CreateMode {
    OpenExisting=0,
    OpenAlways=1,
    WipeAlways=2
  };

  enum Type {
    Invalid=0,
    Boolean=1,
    Integer=2, //32-bit signed integer
    String=3,
    ByteArray=4,
    KeySequence=5,
    MaxKeyType
  };

  K8SDB (const QString &aPathName, CreateMode cMode=OpenExisting);
  ~K8SDB ();

  inline bool isOpen () const { return mDB.isOpen(); }
  inline QString pathname () const { return mDB.fileName(); }

  /*
   * TypeOverwrite: overwrite existing keys with different type in set()?
   * default: yes
   */
  inline bool typeOverwrite () const { return mTypeOverwrite; }
  inline void setTypeOverwrite (bool tover) { mTypeOverwrite = tover; }

  bool remove (const QString &name);
  /*
   * remove all keys with names starting from 'name'
   */
  bool removeAll (const QString &name);
  bool hasKey (const QString &name) const;
  Type type (const QString &name);

  bool set (const QString &name, bool v);
  inline bool setBool (const QString &name, bool v) { return set(name, v); }
  bool set (const QString &name, qint32 v);
  inline bool setInt (const QString &name, qint32 v) { return set(name, v); }
  bool set (const QString &name, const QString &v);
  inline bool set (const QString &name, const char *v) { return set(name, QString(v)); }
  bool set (const QString &name, const QKeySequence &v);
  bool set (const QString &name, const QByteArray &v);
  bool set (const QString &name, Type type, const QString &v);

  bool toBool (const QString &name, bool *ok=0);
  qint32 toInt (const QString &name, bool *ok=0);
  QString toString (const QString &name, bool *ok=0);
  QByteArray toByteArray (const QString &name, bool *ok=0);
  QKeySequence toKeySequence (const QString &name, bool *ok=0);

  QString asString (const QString &name, bool *ok=0);

  QStringList keys () const;
  QStringList keysPfx (const QString &pfx) const;

private:
  struct tsPageHeader *keyPagePtr (const QString &name);
  void *keyDataPtr (const QString &name);

  uchar *updateSettingPtr (const QString &name, int dataSize);

  /*
   * false: can't create key
   * dup checks must be done prior to calling this method
   * dataSize have any meaning only for String/KeySequence keys
   */
  struct tsPageHeader *addKeyNoData (const QString &name, Type type, int dataSize);
  bool freePage (int idx);

  void writeInitHeader ();
  void readInitHeader ();

  void flushKeys ();
  void flushKey (const QString &name);

  QFile mDB;
  CreateMode mMode;
  uchar *mMap;
  struct tsHeader *mHeader;
  bool mTypeOverwrite;

  QHash<QString, qint32> mKeys; // key: page index in mPages
  QList<qint32> mPages; // offset for all the pages
  QSet<qint32> mFreePages; // indicies for all free pages
};



#endif
