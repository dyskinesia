/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QDialog>
#include <QFile>
#include <QScrollBar>
#include <QTextStream>

#include "psycproto.h"

#include "joinwin.h"


JoinWin::JoinWin (QWidget *parent) : QDialog(parent) {
  setupUi(this);
  setAttribute(Qt::WA_QuitOnClose, false);
  setAttribute(Qt::WA_DeleteOnClose, true);

  btEnter->setEnabled(false);
}


JoinWin::~JoinWin () {
}


void JoinWin::on_leNick_textChanged (void) {
}


void JoinWin::on_leHost_textChanged (void) {
  PsycEntity u("psyc://"+leHost->text()+"/@"+leRoom->text());
  btEnter->setEnabled(u.isValid());
}


void JoinWin::on_leRoom_textChanged (void) {
  PsycEntity u("psyc://"+leHost->text()+"/@"+leRoom->text());
  btEnter->setEnabled(u.isValid());
}


void JoinWin::on_btEnter_clicked (void) {
  PsycEntity u("psyc://"+leHost->text()+"/@"+leRoom->text());
  if (!u.isValid()) return;
  emit enter(u);
  close();
}
