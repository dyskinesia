/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef K8HISTORY_H
#define K8HISTORY_H

#ifndef HISTORY_OK
# error Please, select history module
#endif

#include <QDateTime>
#include <QFile>
#include <QString>


///////////////////////////////////////////////////////////////////////////////
class HistoryMessage {
public:
  enum Type {
    Incoming=0,
    Outgoing,
    Server, // message from server
    Info
  };

public:
  HistoryMessage () { clear(); }
  ~HistoryMessage () { clear(); }

  virtual void clear () {
    valid = false;
    uni = QString();
    date = QDateTime();
    type = Info;
    text = QString();
    action = QString();
    date.setTimeSpec(Qt::LocalTime);
  }

public:
  bool valid;
  QString uni;
  QDateTime date; // must be localtime!
  Type type;
  QString text;
  QString action;
};


///////////////////////////////////////////////////////////////////////////////
class HistoryFileBase {
public:
  enum OpenMode {
    Read,
    Write
  };

public:
  virtual ~HistoryFileBase ();

  virtual bool open (OpenMode newmode)=0;
  virtual bool isOpen () const=0;
  virtual void close ()=0;
  virtual void remove ()=0;

  virtual int count ()=0;

  virtual bool append (const HistoryMessage &msg)=0;

  /* idx: 0..count-1; idx<0: idx += count; */
  virtual bool read (int idx, HistoryMessage &msg)=0;
};


#ifdef JSON_HISTORY
# include "k8jshistory.h"
#endif

#ifdef LDB_HISTORY
# include "k8ldbhistory.h"
#endif

#ifdef BHF_HISTORY
# include "k8bhfhistory.h"
#endif


#endif
