/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * we are the Borg.
 */
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <dysversion.h>

//#include <QtCore>
#include <QDebug>

/*
#ifdef WIN32
# include <QMessageBox>
#endif
*/

#include <QApplication>
#include <QClipboard>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QInputDialog>
//#include <QHttp>
#include <QMetaMethod>
#include <QMessageBox>
#include <QPainter>
#include <QProcess>
#include <QRegExp>
#include <QShortcut>
#include <QScrollBar>
#include <QTextCodec>
#include <QTextStream>
#include <QTcpSocket>
#include <QThread>
#include <QUrl>
#include <QWebFrame>
#include <QWebHitTestResult>
#include <QWebPage>
#include <QWebSettings>

#include "ghotkeyman.h"

#include "k8tpl.h"
#include "k8strutils.h"
#include "k8ascii85.h"

#include "settings.h"
#include "k8sdb.h"
#include "k8history.h"

#include "psycvars.h"
#include "psycpkt.h"
#include "psycproto.h"

#include "psyccontact.h"

#include "k8popups.h"

#include "authwin.h"
#include "accwin.h"
#include "conwin.h"
#ifdef USE_OTR
# include "genkeywin.h"
#endif
#include "pktwin.h"
#include "joinwin.h"

#include "chatform.h"

#include "eng_commands.h"
#include "eng_bindings.h"
#include "floatman.h"
#include "chatjsapi.h"


#define DYSKINESIA  "Dyskinesia"


///////////////////////////////////////////////////////////////////////////////
static const QString bool2js (bool b) {
  if (b) return QString("true");
  return QString("false");
}


static QString urlStr (const QString &spUrl) {
  return "<a href='"+K8Str::escapeStr(spUrl)+"'>"+K8Str::escapeStr(spUrl)+"</a>";
}


static QString loadFile (const QString &fileName, bool *found=0) {
  if (found) *found = false;
  if (fileName.isEmpty()) return QString();

  // try local file first
  if (fileName[0] == ':' && fileName.length() > 1) {
    QString fn(fileName);
    if (fileName[1] == '/') fn.remove(0, 2); else fn.remove(0, 1);
    bool ff;
    QString r;
    /*
    r = loadFile(settingsPrefsPath()+fn, &ff);
    if (ff) {
      if (found) *found = true;
      return r;
    }
    */
    r = loadFile(settingsAppPath()+fn, &ff);
    if (ff) {
      if (found) *found = true;
      return r;
    }
  }

  QFile file(fileName);
  QString res;
  if (file.open(QIODevice::ReadOnly)) {
    QTextStream stream;
    stream.setDevice(&file);
    stream.setCodec("UTF-8");
    res = stream.readAll();
    file.close();
    if (found) *found = true;
  }
  return res;
}


///////////////////////////////////////////////////////////////////////////////
#include "chat_otr.cpp"


///////////////////////////////////////////////////////////////////////////////
Option::Option (const QString &def, const QString &aHelp) {
  mValid = parseDef(def);
  help = aHelp;
}


bool Option::parseDef (const QString &def) {
  QStringList pd(def.split(':'));
  if (pd.count() < 4) return false;
  // [0]: name
  // [1]: type
  // [2]: dbname
  // [3]: default
  // [4]: scope (can be omited)

  //name
  name = pd[0].trimmed();
  if (name.isEmpty()) return false;
  //type
  QString tp(pd[1].trimmed().toLower());
  if (tp == "bool" || tp == "boolean") type = K8SDB::Boolean;
  else if (tp == "int" || tp == "integer") type = K8SDB::Integer;
  else if (tp == "str" || tp == "string") type = K8SDB::String;
  else return false;
  //dbname
  dbName = pd[2].trimmed();
  if (dbName.isEmpty()) return false;
  //default
  defVal = pd[3].trimmed();
  //scope
  scope = Both; //default scope
  if (pd.count() < 5) return true;
  QString sc(pd[4].trimmed().toLower());
  if (sc == "nolocal") scope = OnlyGlobal;
  else if (sc == "onlylocal" || sc == "noglobal") scope = OnlyLocal;
  else if (!sc.isEmpty()) return false;
  return true;
}


///////////////////////////////////////////////////////////////////////////////
NetworkAccessManager::NetworkAccessManager (QObject *parent) : QNetworkAccessManager(parent) {
}


QNetworkReply *NetworkAccessManager::createRequest (Operation op, const QNetworkRequest &req, QIODevice *outgoingData) {
  QNetworkRequest newR(req);
  if (newR.url().scheme().toLower() == "dys") {
    QString pt(settingsHtmlPath());
    pt.chop(1);
    if (!pt.isEmpty() && pt[0] == '/') pt.remove(0, 1);
    QString host;
    QString path(newR.url().path());
    QString p("file://localhost/"+pt+host+path);
    newR.setUrl(QUrl(p));
  }
  QNetworkReply *res = QNetworkAccessManager::createRequest(op, newR, outgoingData);
  return res;
}


///////////////////////////////////////////////////////////////////////////////
EnterFilter::EnterFilter (QObject *parent) : QObject(parent), mCtrlDown(false) {
  mForm = dynamic_cast<ChatForm *>(parent);
}


EnterFilter::~EnterFilter () {
}


bool EnterFilter::eventFilter (QObject *obj, QEvent *event) {
  bool down;

  switch (event->type()) {
    case QEvent::KeyPress: down = true; break;
    case QEvent::KeyRelease: down = false; break;
    case QEvent::WindowActivate:
      //qDebug() << "activated!";
      mForm->mUnreadInChat = false;
      if (mForm->mChatUser) {
        mForm->mPopMan->removeById("", mForm->mChatUser->uni().toLower());
        if (mForm->mFMan) mForm->mFMan->noMessages(mForm->mChatUser->uni());
      }
      mForm->checkBlink();
      return false;
    default: return false; // don't steal
  }

  QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
  if (keyEvent->key() == Qt::Key_Control) {
    if (!down) {
      if (mCtrlDown) mForm->switchTab(true);
    }
    mCtrlDown = down;
    return false;
  }

  if (!down) return false;

  if (keyEvent->modifiers() == Qt::ControlModifier && keyEvent->key() == Qt::Key_Tab) {
    mForm->switchTab(false);
    //mForm->switchTab(true);
    return true;
  }

  //qDebug() << "key!";
  QPlainTextEdit *e = dynamic_cast<QPlainTextEdit *>(obj);
  if (mForm->processKeyCombo(keyEvent)) return true;

  if (!e) return false;
  //qDebug() << " EDITOR";

  return false;
}


///////////////////////////////////////////////////////////////////////////////
ChatForm::ChatForm (QWidget *parent) : QWidget(parent),
  mConForm(0),
  mTrayIcon(new QSystemTrayIcon(QIcon(MAIN_TRAY_ICON))),
  mAccName(QString()),
  mProto(new PsycProto(this)), mPassword(QString("")),
  mMyStatus(PsycProto::Offline), mMyMood(PsycProto::Unspecified),
  mChBlock(false),
  mEFilter(new EnterFilter(this)),
  mChatUser(0), mSelText(QString()),
  mPopMan(0),
  mPopPos(K8PopupManager::RightBottom),
  mCloseWaiting(false), mServerStore(false),
  mJSAPI(0), mCLJSAPI(0),
  mSDB(0),
  mBlinkTimer(new QTimer(this)),
  mBlinkMsg(false), mTrayMsgNoteActive(false), mUnreadInChat(false),
  mCListOnRight(false),
  mDoTypo(true),
  mChatLoaded(false), mCListLoaded(false),
  mTabDown(false), mTabSwitchNo(-1),
  mComboList(0)
{
#ifdef USE_OTR
  mOTRState = 0;
  mOTRTimer = new QTimer(this);
  connect(mOTRTimer, SIGNAL(timeout()), this, SLOT(onOTRTimer()));
#endif
  mFMan = 0;

  mComboList = new KeyComboList;
  mEditComboList = new KeyComboList;
  mCurCombos = new KeyComboList;

  mEngCmds = new EngineCommands(this);
  mEngBinds = new EngineBindings(this);

  mVerStr = QString("%1.%2.%3.%4").arg(DYS_VERSION_HI).arg(DYS_VERSION_MID).
    arg(DYS_VERSION_LO).arg(DYS_BUILD);

  setupUi(this);
  setAttribute(Qt::WA_DeleteOnClose, true);
  setAttribute(Qt::WA_QuitOnClose, true);

  edChat->installEventFilter(mEFilter);
  this->installEventFilter(mEFilter);

  connect(this, SIGNAL(destroyed()), this, SLOT(windowDestroyed()));

  mTrayIcon->setContextMenu(menu_tray);
  connect(mTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(onTrayActivate(QSystemTrayIcon::ActivationReason)));
  connect(mBlinkTimer, SIGNAL(timeout()), this, SLOT(onTrayBlink()));

  loadRecodeMap();

  QDir d;
  d.mkdir(settingsPrefsPath());

  QString css = loadFile(":/html/qss/default.qss");
  css = css.trimmed();
  if (!css.isEmpty()) qApp->setStyleSheet(css);
  //DEBUG//
/*
  mConForm = new ConsoleForm(this);
  connect(mConForm, SIGNAL(destroyed()), this, SLOT(consoleDestroyed()));
  mConForm->cbActive->setChecked(true);
  mConForm->hide();
*/
  //DEBUG//

  NetworkAccessManager *mm = new NetworkAccessManager(this);
  webChat->page()->setNetworkAccessManager(mm);
  webChat->setAllowContextMenu(false);
  webCList->page()->setNetworkAccessManager(mm);

  QWebFrame *fr = webChat->page()->mainFrame();
  mJSAPI = new ChatJSAPI(fr, this);
  connect(fr, SIGNAL(javaScriptWindowObjectCleared()), this, SLOT(javaScriptWindowObjectCleared()));

  fr = webCList->page()->mainFrame();
  mCLJSAPI = new ChatJSAPI(fr, this);
  connect(fr, SIGNAL(javaScriptWindowObjectCleared()), this, SLOT(javaScriptWindowObjectCleared()));

  connect(webChat, SIGNAL(webContextMenu(const QWebHitTestResult &, const QPoint &)), this, SLOT(onWebContextMenuNone(const QWebHitTestResult &, const QPoint &)));
  connect(webCList, SIGNAL(webContextMenu(const QWebHitTestResult &, const QPoint &)), this, SLOT(onWebContextMenuNone(const QWebHitTestResult &, const QPoint &)));

  connect(webChat, SIGNAL(loadFinished(bool)), this, SLOT(onLoadFinishedChat(bool)));
  connect(webCList, SIGNAL(loadFinished(bool)), this, SLOT(onLoadFinishedCList(bool)));

  connect(webChat, SIGNAL(linkClicked(const QUrl &)), this, SLOT(onLinkClicked(const QUrl &)));
  connect(webCList, SIGNAL(linkClicked(const QUrl &)), this, SLOT(onLinkClicked(const QUrl &)));

  mProto->setUserAgent("Dyskinesia/"+mVerStr);
  connect(mProto, SIGNAL(packetComes(const PsycPacket &)), this, SLOT(onPacket(const PsycPacket &)));
  connect(mProto, SIGNAL(packetSent(const PsycPacket &)), this, SLOT(onPacketSent(const PsycPacket &)));
  connect(mProto, SIGNAL(netError(const QString &)), this, SLOT(onNetError(const QString &)));
  connect(mProto, SIGNAL(connected()), this, SLOT(onConnected()));
  connect(mProto, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
  connect(mProto, SIGNAL(needPassword()), this, SLOT(onNeedPassword()));
  connect(mProto, SIGNAL(loginComplete()), this, SLOT(onLoginComplete()));
  connect(mProto, SIGNAL(invalidPassword()), this, SLOT(onInvalidPassword()));
  connect(mProto, SIGNAL(authRequested(const PsycEntity &)), this, SLOT(onAuthRequested(const PsycEntity &)));
  connect(mProto, SIGNAL(authGranted(const PsycEntity &)), this, SLOT(onAuthGranted(const PsycEntity &)));
  connect(mProto, SIGNAL(authDenied(const PsycEntity &)), this, SLOT(onAuthDenied(const PsycEntity &)));
  connect(mProto, SIGNAL(message(const PsycEntity &, const PsycEntity &, const QDateTime &, const QString &, const QString &)), this, SLOT(onMessage(const PsycEntity &, const PsycEntity &, const QDateTime &, const QString &, const QString &)));
  connect(mProto, SIGNAL(messageEcho(const PsycEntity &, const PsycEntity &, const PsycEntity &, const QDateTime &, const QString &, const QString &, const QString &)), this, SLOT(onMessageEcho(const PsycEntity &, const PsycEntity &, const PsycEntity &, const QDateTime &, const QString &, const QString &, const QString &)));
  connect(mProto, SIGNAL(historyMessage(const PsycEntity &, const PsycEntity &, const QDateTime &, const QString &, const QString &)), this, SLOT(onHistoryMessage(const PsycEntity &, const PsycEntity &, const QDateTime &, const QString &, const QString &)));

  connect(mProto, SIGNAL(scratchpadUpdated(const QString &)), this, SLOT(onScratchpadUpdated(const QString &)));
  connect(mProto, SIGNAL(placeWebExamined(const PsycEntity &, const PsycPacket &)), this, SLOT(onPlaceWebExam(const PsycEntity &, const PsycPacket &)));
  connect(mProto, SIGNAL(placeNews(const PsycEntity &, const QString &, const QString &, const QString &)), this, SLOT(onPlaceNews(const PsycEntity &, const QString &, const QString &, const QString &)));

  connect(mProto, SIGNAL(noticeUpdate(const PsycEntity &, const PsycPacket &)), this, SLOT(onNoticeUpdate(const PsycEntity &, const PsycPacket &)));

  connect(mProto, SIGNAL(userStatusChanged(const PsycEntity &)), this, SLOT(onUserStatusChanged(const PsycEntity &)));
  connect(mProto, SIGNAL(selfStatusChanged()), this, SLOT(onSelfStatusChanged()));
  connect(mProto, SIGNAL(notificationType(const PsycEntity &)), this, SLOT(onNotificationType(const PsycEntity &)));

  connect(mProto, SIGNAL(placeEntered(const PsycEntity &)), this, SLOT(onPlaceEntered(const PsycEntity &)));
  connect(mProto, SIGNAL(placeLeaved(const PsycEntity &)), this, SLOT(onPlaceLeaved(const PsycEntity &)));
  connect(mProto, SIGNAL(userEntersPlace(const PsycEntity &, const PsycEntity &)), this, SLOT(onUserEntersPlace(const PsycEntity &, const PsycEntity &)));
  connect(mProto, SIGNAL(userLeavesPlace(const PsycEntity &, const PsycEntity &)), this, SLOT(onUserLeavesPlace(const PsycEntity &, const PsycEntity &)));

  //connect(splitter0, SIGNAL(splitterMoved(int, int)), this, SLOT(splitterMoved(int, int)));
  //connect(splitter1, SIGNAL(splitterMoved(int, int)), this, SLOT(splitterMoved(int, int)));

  btStatus->setMenu(menu_status);
  btMainMenu->setMenu(menu_main);

  //connect(mSaveTimer, SIGNAL(timeout()), this, SLOT(doAutosave()));

  //mPopMan = new K8PopupManager(300, 120, 6, mPopPos);
  //connect(mPopMan, SIGNAL(click(const QString &, const QString &, Qt::MouseButton)), this, SLOT(onPopupClick(const QString &, const QString &, Qt::MouseButton)));
  recreatePopMan();

  loadAccount();

  edChat->setTabChangesFocus(false);
}


void ChatForm::onGShortcutShow () {
  if (isMinimized() || !isVisible()) {
    showNormal();
    activateWindow();
  } else hide();
}


void ChatForm::onGShortcutHidePopups () {
  mPopMan->removeAll();
}


ChatForm::~ChatForm () {
  GHotKeyMan::instance()->clear();

  if (mProto->isLoggedIn()) storeAppData();
  if (mProto->isLinked()) mProto->logoff();

  if (mSDB) {
    QRect geo(geometry());
    mSDB->set("/chatwindow/x", geo.x());
    mSDB->set("/chatwindow/y", geo.y());
    mSDB->set("/chatwindow/w", geo.width());
    mSDB->set("/chatwindow/h", geo.height());
    mSDB->set("/chatwindow/saved", true);
    if (mConForm) {
      QRect geo(mConForm->geometry());
      mSDB->set("/pktconsolewindow/x", geo.x());
      mSDB->set("/pktconsolewindow/y", geo.y());
      mSDB->set("/pktconsolewindow/w", geo.width());
      mSDB->set("/pktconsolewindow/h", geo.height());
      mSDB->set("/pktconsolewindow/active", mConForm->cbActive->isChecked());
      mSDB->set("/pktconsolewindow/saved", true);
    }
  }
  //doAutosave();
  clear();
  clearOptionList();
  clearBindings();
  delete mPopMan;
  mTrayIcon->hide();

  delete mComboList;
  delete mEditComboList;
  delete mCurCombos;

#ifdef USE_OTR
  dlogfDeinit();
#endif
}


void ChatForm::windowDestroyed () {
  if (mProto->isLoggedIn()) storeAppData();
  if (mProto->isLinked()) mProto->logoff();
  //doAutosave();
}


void ChatForm::consoleDestroyed () {
  mConForm = 0;
}


void ChatForm::recreatePopMan () {
  delete mPopMan;
  mPopMan = new K8PopupManager(300, 120, 6, mPopPos);
  connect(mPopMan, SIGNAL(click(const QString &, const QString &, Qt::MouseButton)), this, SLOT(onPopupClick(const QString &, const QString &, Qt::MouseButton)));
}


void ChatForm::javaScriptWindowObjectCleared () {
  //qDebug() << "***";
  webChat->page()->mainFrame()->addToJavaScriptWindowObject("api", mJSAPI);
  webCList->page()->mainFrame()->addToJavaScriptWindowObject("api", mCLJSAPI);
}


void ChatForm::runChatJS (const QString &js) {
  QWebFrame *fr = webChat->page()->mainFrame();
  fr->evaluateJavaScript(js);
}


void ChatForm::runCListJS (const QString &js) {
  QWebFrame *fr = webCList->page()->mainFrame();
  fr->evaluateJavaScript(js);
}


void ChatForm::clear () {
  mChatUser = 0;
  disconnectAllOTR();
  if (mProto->isLoggedIn()) storeAppData();
  if (mProto->isLinked()) mProto->logoff();
  delete mSDB; mSDB = 0;
  mProto->clear();
  foreach (PsycContact *cc, mContactList) delete cc;
  mContactList.clear();
  mTabList.clear();
  clearCList();
  clearChat();
#ifdef USE_OTR
  mOTRTimer->stop();
  if (mOTRState) otrl_userstate_free(mOTRState);
  mOTRState = 0;
#endif
}


void ChatForm::showPopup (const PsycEntity &place, const PsycEntity &from,
  const QString &type, const QString &id, const QString &msg, const QString &action, bool update)
{
  //qDebug() << "showPopup:" << from.uni() << msg << action;
  mPopMan->setFloatHeight(toBoolOpt("/popup/dynsize", true));

  QString pt(settingsHtmlPath()+"popups/"+type);
  mPopMan->addType(type, loadFile(pt+"/style.qss"));
  K8Template tpl;
  QString fn(pt);
  if (action.trimmed().isEmpty()) fn += "/template.html";
  else fn += "/template_action.html";
  tpl.loadFromFile(fn);
  tpl.setVar("uni", K8Popups::escapeStr(from.uni()));
  tpl.setVar("nick", K8Popups::escapeStr(from.nick()));
  tpl.setVar("verbatim", K8Popups::escapeStr(from.verbatim()));
  tpl.setVar("placeuni", K8Popups::escapeStr(place.uni()));
  tpl.setVar("placenick", K8Popups::escapeStr(place.nick()));
  tpl.setVar("placeverbatim", K8Popups::escapeStr(place.verbatim()));
  tpl.setVar("action", K8Popups::escapeStr(action));
  if (!msg.isEmpty() && msg[0] == '\x1') pt = msg.mid(1);
  else {
    pt = K8Popups::escapeStr(msg.trimmed()); pt.replace("\n", "<br />");
  }
  tpl.setVar("text", pt);
  pt = tpl.result();
  if (update) mPopMan->updateMessage(type, id, pt);
  else mPopMan->showMessage(type, id, pt);
}


void ChatForm::showPrivatePopup (const PsycEntity &from, const QString &msg, const QString &action) {
  if (!from.isValid()) return;
  if (!toBoolOpt("/popup/active", true, from.uni())) return;
  if (!toBoolOpt("/notifications/message", true, from.uni())) return;
  PsycEntity place;
  PsycEntity fr(from);
  PsycContact *cc = findContact(from);
  /*if (cc && cc->isHidden() && !toBoolOpt("/contactlist/showhidden", false)) return;*/
  if (cc) fr.setVerbatim(cc->verbatim());
  showPopup(place, fr, "private", fr.uni().toLower(), msg, action);
}


void ChatForm::showPublicPopup (const PsycEntity &place, const PsycEntity &from, const QString &msg, const QString &action) {
  if (!from.isValid() || !place.isValid()) return;
  if (!toBoolOpt("/popup/active", true, from.uni())) return;
  if (!toBoolOpt("/popup/active", true, place.uni())) return;
  if (!toBoolOpt("/notifications/message", true, from.uni())) return;
  if (!toBoolOpt("/notifications/message", true, place.uni())) return;
  PsycEntity plc(place);
  PsycContact *cc = findContact(place);
  if (cc && cc->isHidden() && !toBoolOpt("/contactlist/showhidden", false)) return;
  if (cc) plc.setVerbatim(cc->verbatim());
  PsycEntity fr(from);
  cc = findContact(from);
  if (cc) fr.setVerbatim(cc->verbatim());
  showPopup(place, fr, "public", plc.uni().toLower(), msg, action);
}


//FIXME: generalize showPopup()?
void ChatForm::showStatusPopup (const PsycEntity &user) {
  if (mMyStatus == PsycProto::Offline) return;
  if (!user.isValid()) return;
  if (!toBoolOpt("/popup/active", true, user.uni())) return;
  if (!toBoolOpt("/notifications/status", true, user.uni())) return;

  mPopMan->setFloatHeight(toBoolOpt("/popup/dynsize", true));

  //qDebug() << "showStatus:" << user.uni();
  PsycEntity fr(user);
  PsycContact *cc = findContact(user);
  if (cc && cc->isHidden() && !toBoolOpt("/contactlist/showhidden", false)) return;
  if (cc) fr.setVerbatim(cc->verbatim());

  QString type("status");
  QString pt(settingsHtmlPath()+"popups/"+type);
  mPopMan->addType(type, loadFile(pt+"/style.qss"));
  K8Template tpl;
  tpl.loadFromFile(pt+"/template.html");
  tpl.setVar("uni", K8Popups::escapeStr(fr.uni()));
  tpl.setVar("nick", K8Popups::escapeStr(fr.nick()));
  tpl.setVar("verbatim", K8Popups::escapeStr(fr.verbatim()));
  tpl.setVar("status", K8Popups::escapeStr(PsycProto::statusName(fr.status())));
  tpl.setVar("mood", K8Popups::escapeStr(PsycProto::moodName(fr.mood())));
  pt = K8Popups::escapeStr(user.statusText().simplified()); pt.replace("\n", "<br />");
  tpl.setVar("text", pt);
  pt = tpl.result();
  mPopMan->updateMessage(type, user.uni().toLower(), pt);
}


void ChatForm::showInfoPopup (const QString &msg) {
  if (!toBoolOpt("/popup/active", true)) return;
  PsycEntity me(mProto->uni());
  PsycEntity place;
  showPopup(place, me, "info", "infopopup", msg, "");
}


void ChatForm::showErrorPopup (const QString &msg) {
  if (!toBoolOpt("/popup/active", true)) return;
  PsycEntity me(mProto->uni());
  PsycEntity place;
  showPopup(place, me, "error", "errorpopup", msg, "");
}


void ChatForm::onLoadFinishedChat (bool ok) {
  Q_UNUSED(ok)
  //qDebug() << "onLoadFinishedChat" << ok;
  mChatLoaded = true;
}


void ChatForm::onLoadFinishedCList (bool ok) {
  //qDebug() << "onLoadFinishedCList" << ok;
  Q_UNUSED(ok)
  mCListLoaded = true;
}


void ChatForm::clearChat () {
  mChatLoaded = false;
  mSelText = QString();
  webChat->load(QUrl("dys://theme/chat.html"));
  while (!mChatLoaded) {
    qApp->processEvents();
  }
  //qDebug("ch...");

  lbChatDest->setText("...");
  if (mChatUser) {
    setOptCU("/editor/state/text", edChat->toPlainText());
    QTextCursor cur = edChat->textCursor();
    int cpos = cur.position();
    setOptCU("/editor/state/curpos", cpos);
    PsycContact *cc = mChatUser;
    mChatUser = 0;
    redrawContact(cc);
  }

  runChatJS("setMyUNI("+K8Str::jsStr(mProto->uni())+");");
  //qDebug() << "clearChat()";

  mStartingNickCompletion.clear();
  mLatestNickCompletion.clear();
}


void ChatForm::clearCList () {
  mCListLoaded = false;
  webCList->load(QUrl("dys://theme/clist.html"));
  while (!mCListLoaded) {
    qApp->processEvents();
  }
  //qDebug("cl...");

  runCListJS("setMyUNI("+K8Str::jsStr(mProto->uni())+");");
  //qDebug() << "clearCList()";
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::onTrayActivate (QSystemTrayIcon::ActivationReason reason) {
  if ((toBoolOpt("/tray/ondoubleclick") && reason == QSystemTrayIcon::DoubleClick) ||
      reason == QSystemTrayIcon::Trigger) {
    if (isMinimized() || !isVisible()) {
      showNormal();
      activateWindow();
    } else hide();
    return;
  }
  if (toBoolOpt("/tray/middleexit", true) && reason == QSystemTrayIcon::MiddleClick) {
    close();
  }
}


void ChatForm::checkBlink () {
  bool doBlink = mUnreadInChat;
  if (mFMan && !mUnreadInChat && mChatUser) mFMan->noMessages(mChatUser->uni());
  QString tip;
#ifndef WIN32
  QString br("<br />");
#else
  QString br("\n");
#endif

  if (mChatUser && mUnreadInChat) {
    tip += K8Str::escapeStr(mChatUser->verbatim())+br+"written something";
    if (toBoolOpt("/tray/notify", true, mChatUser->uni())) doBlink = true;
  }

  foreach (PsycContact *cc, mContactList) {
    if (cc->isTemp()) continue;
    if (!cc->messageCount()) continue;
    if (!tip.isEmpty()) {
#ifndef WIN32
      tip += "<hr>";
#else
      tip += "\n";
#endif
    }
    tip += K8Str::escapeStr(cc->verbatim())+br;
    tip += QString::number(cc->messageCount())+" unread message";
    if (cc->messageCount() > 0) tip += "s";
    if (toBoolOpt("/tray/notify", true, cc->uni())) doBlink = true;
  }

  //qDebug() << mTrayMsgNoteActive;
  if (tip.isEmpty()) tip = "Dyskinesia v"+mVerStr;
  mTrayIcon->setToolTip(tip);
  if (!doBlink) {
    if (mTrayMsgNoteActive) {
      mBlinkTimer->stop();
      if (toBoolOpt("/status/trayicon")) {
#ifndef WIN32
        mTrayIcon->setIcon(LinuxizeTrayIcon(statusIcon(mProto->status())));
#else
        mTrayIcon->setIcon(statusIcon(mProto->status()));
#endif
      } else mTrayIcon->setIcon(QIcon(MAIN_TRAY_ICON));
      mTrayMsgNoteActive = false;
    }
  } else {
    if (!mTrayMsgNoteActive) {
      int bt = toIntOpt("/tray/blinktime", 500);
      if (bt > 0) {
        if (bt < 10) bt = 10;
        mBlinkTimer->start(bt);
      }
      mBlinkMsg = true;
      if (toBoolOpt("/status/trayicon")) {
#ifndef WIN32
        mTrayIcon->setIcon(LinuxizeTrayIcon(statusIcon(mProto->status())));
#else
        mTrayIcon->setIcon(statusIcon(mProto->status()));
#endif
      } else mTrayIcon->setIcon(QIcon(MAIN_TRAY_ICON));
      //mTrayIcon->setIcon(QIcon(MSG_TRAY_ICON));
      mTrayMsgNoteActive = true;
    }
  }
}


void ChatForm::onTrayBlink () {
  mBlinkMsg = !mBlinkMsg;
  if (mBlinkMsg) mTrayIcon->setIcon(QIcon(MSG_TRAY_ICON));
  else {
    //mTrayIcon->setIcon(QIcon(MAIN_TRAY_ICON));
    if (toBoolOpt("/status/trayicon")) {
#ifndef WIN32
      mTrayIcon->setIcon(LinuxizeTrayIcon(statusIcon(mProto->status())));
#else
      mTrayIcon->setIcon(statusIcon(mProto->status()));
#endif
    } else mTrayIcon->setIcon(QIcon(MAIN_TRAY_ICON));
  }
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::requestStoredAppData () {
  if (!mProto->isLoggedIn()) return;
  PsycPacket pkt("_request_retrieve");
  pkt.put("_target", mProto->uni());
  pkt.put("_source_identification", mProto->uni());
  pkt.endRoute();
  mProto->sendPacket(pkt);
}


/*
 * syntax for _request_store:
 *
 * :_application_pypsyc_window_size>3000x2000
 * :_application_pypsyc_skin<><>darkstar
 * :_character_command<><><>%
 * _request_store
 * .
 *
 * so whenever the client starts up it can connect
 * the UNI and ask for its settings by issuing an
 * empty _request_retrieve. one day we may specify
 * subsets of the data pool.. using families i guess.
 */
void ChatForm::storeAppData () {
  if (!mProto->isLoggedIn()) return;
  PsycPacket pkt("_request_store");
  pkt.put("_target", mProto->uni());
  pkt.put("_source_identification", mProto->uni());
  pkt.endRoute();
  if (mServerStore) {
    pkt.putInt("_application_window_x", pos().x());
    pkt.putInt("_application_window_y", pos().y());
    pkt.putInt("_application_window_w", size().width());
    pkt.putInt("_application_window_h", size().height());
    if (mConForm) {
      pkt.putInt("_application_console_x", mConForm->pos().x());
      pkt.putInt("_application_console_y", mConForm->pos().y());
      pkt.putInt("_application_console_w", mConForm->size().width());
      pkt.putInt("_application_console_h", mConForm->size().height());
      int act = mConForm->cbActive->isChecked()?1:0;
      pkt.putInt("_application_console_active", act);
      int vis = (mConForm->isMinimized() || !mConForm->isVisible())?0:1;
      pkt.putInt("_application_console_visible", vis);
    }
    if (mChatUser) pkt.put("_application_chatwith", mChatUser->uni());
    else pkt.put("_application_chatwith", ".");
  }
  mProto->sendPacket(pkt);
}


void ChatForm::parseStoredAppData (const PsycPacket &pkt) {
  if (mServerStore) {
    if (pkt.has("_application_window_x") && pkt.has("_application_window_y")) {
      int x = pkt.getInt("_application_window_x", 42);
      int y = pkt.getInt("_application_window_y", 42);
      move(x, y);
    }
    if (pkt.has("_application_window_w") && pkt.has("_application_window_h")) {
      int w = pkt.getInt("_application_window_w", 606);
      int h = pkt.getInt("_application_window_h", 500);
      resize(w, h);
    }
    if (pkt.has("_application_console_x") && pkt.has("_application_console_y")) {
      on_action_showconsole_triggered(true);
      if (pkt.getInt("_application_console_visible", 0)) mConForm->show(); else mConForm->hide();
      mConForm->cbActive->setChecked(pkt.getInt("_application_console_active", 0) != 0);
      activateWindow(); // get focus back
      int x = pkt.getInt("_application_console_x", 0);
      int y = pkt.getInt("_application_console_y", 0);
      mConForm->move(x, y);
    }
    if (mConForm) {
      if (pkt.has("_application_console_w") && pkt.has("_application_console_h")) {
        int w = pkt.getInt("_application_console_w", 0);
        int h = pkt.getInt("_application_console_h", 0);
        mConForm->resize(w, h);
      }
    }
    QString cw(pkt.get("_application_chatwith", ""));
    if (!cw.isEmpty()) {
      PsycEntity u(cw);
      if (u.isValid()) startChatWith(cw);
    }
  }
  //knockAutosave();
}


///////////////////////////////////////////////////////////////////////////////
static QString optNameEx (const QString &name, const QString &aUNI=QString()) {
  QString res(name);
  if (!res.isEmpty() && res[0] != '/') res.prepend('/');
  if (aUNI.isEmpty()) return res;
  return "*<"+aUNI.toLower()+">"+res;
}


bool ChatForm::removeOpt (const QString &name, const QString &aUNI) {
  if (!mSDB) return false;
  if (mSDB->remove(optNameEx(name, aUNI))) {
    runCListJS("optionRemoved("+K8Str::jsStr(name)+");");
    runChatJS("optionRemoved("+K8Str::jsStr(name)+");");
    return true;
  }
  return false;
}


bool ChatForm::hasKeyOpt (const QString &name, const QString &aUNI) const {
  if (!mSDB) return false;
  return mSDB->hasKey(optNameEx(name, aUNI));
}


K8SDB::Type ChatForm::typeOpt (const QString &name, const QString &aUNI) {
  if (!mSDB) return K8SDB::Invalid;
  return mSDB->type(optNameEx(name, aUNI));
}


void ChatForm::optionChangedSignal (const QString &name, const QString &uni) {
  //qDebug() << "optionChangedSignal:" << name << uni;
  char mtName[256];
  QString nn("onOptChanged_"+name); nn.replace("/", "_");
  QByteArray bt(nn.toAscii());
  sprintf(mtName, "%s(QString)", bt.constData());
  //fprintf(stderr, " [%s]\n", mtName);
  if (metaObject()->indexOfSlot(mtName) >= 0) {
    sprintf(mtName, "%s", bt.constData());
    //fprintf(stderr, " [%s]\n", mtName);
    QMetaObject::invokeMethod(this, mtName, Qt::AutoConnection, Q_ARG(QString, uni));
  }
  runCListJS("optionChanged("+K8Str::jsStr(name)+");");
  runChatJS("optionChanged("+K8Str::jsStr(name)+");");
  if (name.left(14) == "/app/shortcut/") {
    //qDebug() << "shortcuts changed";
    setAccountShortcuts();
  }
}


bool ChatForm::setOpt (const QString &name, bool v, const QString &aUNI) {
  if (!mSDB) return false;
  if (mSDB->set(optNameEx(name, aUNI), v)) {
    optionChangedSignal(name, aUNI);
    return true;
  }
  return false;
}


bool ChatForm::setOpt (const QString &name, qint32 v, const QString &aUNI) {
  if (!mSDB) return false;
  if (mSDB->set(optNameEx(name, aUNI), v)) {
    optionChangedSignal(name, aUNI);
    return true;
  }
  return false;
}


bool ChatForm::setOpt (const QString &name, const QString &v, const QString &aUNI) {
  if (!mSDB) return false;
  if (mSDB->set(optNameEx(name, aUNI), v)) {
    optionChangedSignal(name, aUNI);
    return true;
  }
  return false;
}


bool ChatForm::setOpt (const QString &name, const QKeySequence &v, const QString &aUNI) {
  if (!mSDB) return false;
  if (mSDB->set(optNameEx(name, aUNI), v)) {
    optionChangedSignal(name, aUNI);
    return true;
  }
  return false;
}


bool ChatForm::setOpt (const QString &name, K8SDB::Type type, const QString &v, const QString &aUNI) {
  if (!mSDB) return false;
  if (mSDB->set(optNameEx(name, aUNI), type, v)) {
    //qDebug() << "option:" << name;
    if (name == "/popup/position") {
      int pp = toIntOpt("/popup/position", 3);
      if (pp < 0 || pp > 3) pp = 3;
      mPopPos = (K8PopupManager::Position)(pp);
      mPopMan->setPosition(mPopPos);
      //recreatePopMan();
    }
    optionChangedSignal(name, aUNI);
    return true;
  }
  return false;
}


bool ChatForm::toBoolOpt (const QString &name, bool def, const QString &aUNI) {
  if (!mSDB) return def;
  bool ok = false;
  bool res = mSDB->toBool(optNameEx(name, aUNI), &ok);
  if (!ok && !aUNI.isEmpty()) res = mSDB->toBool(optNameEx(name), &ok);
  if (!ok) return def;
  return res;
}


qint32 ChatForm::toIntOpt (const QString &name, qint32 def, const QString &aUNI) {
  if (!mSDB) return def;
  bool ok = false;
  int res = mSDB->toInt(optNameEx(name, aUNI), &ok);
  if (!ok && !aUNI.isEmpty()) res = mSDB->toInt(optNameEx(name), &ok);
  if (!ok) return def;
  return res;
}


QString ChatForm::toStringOpt (const QString &name, const QString &def, const QString &aUNI) {
  if (!mSDB) return def;
  bool ok = false;
  QString res = mSDB->toString(optNameEx(name, aUNI), &ok);
  if (!ok && !aUNI.isEmpty()) res = mSDB->toString(optNameEx(name), &ok);
  if (!ok) return def;
  return res;
}


QByteArray ChatForm::toBAOpt (const QString &name, const QByteArray &def, const QString &aUNI) {
  if (!mSDB) return def;
  bool ok = false;
  QByteArray res = mSDB->toByteArray(optNameEx(name, aUNI), &ok);
  if (!ok && !aUNI.isEmpty()) res = mSDB->toByteArray(optNameEx(name), &ok);
  if (!ok) return def;
  return res;
}


QKeySequence ChatForm::toKeySequenceOpt (const QString &name, const QKeySequence &def, const QString &aUNI) {
  if (!mSDB) return def;
  bool ok = false;
  QKeySequence res = mSDB->toKeySequence(optNameEx(name, aUNI), &ok);
  if (!ok && !aUNI.isEmpty()) res = mSDB->toKeySequence(optNameEx(name), &ok);
  if (!ok) return def;
  return res;
}



QString ChatForm::asStringOpt (const QString &name, const QString &aUNI) {
  if (!mSDB) return QString();
  bool ok = false;
  QString res = mSDB->asString(optNameEx(name, aUNI), &ok);
  if (!ok && !aUNI.isEmpty()) res = mSDB->asString(optNameEx(name), &ok);
  if (!ok) return QString();
  return res;
}


///////////////////////////////////////////////////////////////////////////////
bool ChatForm::removeOptCU (const QString &name) {
  if (mChatUser) return removeOpt(name, mChatUser->uni());
  return removeOpt(name);
}


bool ChatForm::hasKeyOptCU (const QString &name) const {
  if (mChatUser) return hasKeyOpt(name, mChatUser->uni());
  return hasKeyOpt(name);
}


K8SDB::Type ChatForm::typeOptCU (const QString &name) {
  if (mChatUser) return typeOpt(name, mChatUser->uni());
  return typeOpt(name);
}


bool ChatForm::setOptCU (const QString &name, bool v) {
  if (mChatUser) return setOpt(name, v, mChatUser->uni());
  return setOpt(name, v);
}


bool ChatForm::setOptCU (const QString &name, qint32 v) {
  if (mChatUser) return setOpt(name, v, mChatUser->uni());
  return setOpt(name, v);
}


bool ChatForm::setOptCU (const QString &name, const QString &v) {
  if (mChatUser) return setOpt(name, v, mChatUser->uni());
  return setOpt(name, v);
}


bool ChatForm::setOptCU (const QString &name, const QKeySequence &v) {
  if (mChatUser) return setOpt(name, v, mChatUser->uni());
  return setOpt(name, v);
}


bool ChatForm::setOptCU (const QString &name, K8SDB::Type type, const QString &v) {
  if (mChatUser) return setOpt(name, type, v, mChatUser->uni());
  return setOpt(name, type, v);
}


bool ChatForm::toBoolOptCU (const QString &name, bool def) {
  if (mChatUser) return toBoolOpt(name, def, mChatUser->uni());
  return toBoolOpt(name, def);
}


qint32 ChatForm::toIntOptCU (const QString &name, qint32 def) {
  if (mChatUser) return toIntOpt(name, def, mChatUser->uni());
  return toIntOpt(name, def);
}


QString ChatForm::toStringOptCU (const QString &name, const QString &def) {
  if (mChatUser) return toStringOpt(name, def, mChatUser->uni());
  return toStringOpt(name, def);
}


QByteArray ChatForm::toBAOptCU (const QString &name, const QByteArray &def) {
  if (mChatUser) return toBAOpt(name, def, mChatUser->uni());
  return toBAOpt(name, def);
}


QKeySequence ChatForm::toKeySequenceOptCU (const QString &name, const QKeySequence &def) {
  if (mChatUser) return toKeySequenceOpt(name, def, mChatUser->uni());
  return toKeySequenceOpt(name, def);
}


QString ChatForm::asStringOptCU (const QString &name) {
  if (mChatUser) return asStringOpt(name, mChatUser->uni());
  return asStringOpt(name);
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::setControlsAccActive (bool accAct) {
  action_stoffline->setEnabled(accAct);
  action_stvacation->setEnabled(accAct);
  action_staway->setEnabled(accAct);
  action_stdnd->setEnabled(accAct);
  action_stnear->setEnabled(accAct);
  action_stbusy->setEnabled(accAct);
  action_sthere->setEnabled(accAct);
  action_stffc->setEnabled(accAct);
  action_strtime->setEnabled(accAct);

  action_quit->setEnabled(true);
  action_accedit->setEnabled(true);
  action_showconsole->setEnabled(true);
  action_sendpacket->setEnabled(accAct);
  action_join->setEnabled(accAct);
}


void ChatForm::setAccountShortcuts () {
  //qDebug() << "setting shortcuts...";
  GHotKeyMan::instance()->clear();
  GHotKeyMan::instance()->connect(QKeySequence(toStringOpt("/app/shortcut/global/showwindow", "Alt+Meta+D")), this, SLOT(onGShortcutShow()));
  GHotKeyMan::instance()->connect(QKeySequence(toStringOpt("/app/shortcut/global/hidepopups", "Alt+Space")), this, SLOT(onGShortcutHidePopups()));
  //qDebug() << "shortcuts set";
}


void ChatForm::loadAccount () {
  delete mFMan; mFMan = 0;

  setStatus(PsycProto::Offline, false);
  clear();
  loadOptionList();

#ifdef USE_OTR
  if (mOTRState) otrl_userstate_free(mOTRState);
  mOTRState = 0;
  mOTRState = otrl_userstate_create();
#endif

  delete mSDB; mSDB = 0;

  K8SDB *mainDB = new K8SDB(settingsMainDBFile(), K8SDB::OpenExisting);
  if (mainDB->isOpen()) {
    bool ok;
    QString actAcc = mainDB->toString("/active", &ok);
    if (!ok || actAcc.isEmpty()) mAccName = QString(); else mAccName = actAcc;
  }
  delete mainDB;

  if (mAccName.isEmpty()) {
    setControlsAccActive(false);
    return;
  }

  setControlsAccActive(true);

  QDir d;
  d.mkdir(settingsDBPath(mAccName));
  d.mkdir(settingsHistoryPath(mAccName));

  mSDB = new K8SDB(settingsDBFile(mAccName), K8SDB::OpenAlways);

  QString host = toStringOpt("/server/host", "127.0.0.1");
  int port = toIntOpt("/server/port", 0);
  if (port < 1 || port > 65536) port = 0;
  QString nick = toStringOpt("/login/nick", "guest");
  QString pass = K8Str::dePass(toStringOpt("/login/password", ""));
  bool seclogin = toBoolOpt("/login/secure", true);
  mServerStore = false;//toBoolOpt("/server/storedata", false);
  bool useSSL = toBoolOpt("/login/useSSL", false);

  setAccDefaultOptions(mSDB); // if i added something new...

  int pp = toIntOpt("/popup/position", 3);
  if (pp < 0 || pp > 3) pp = 3;
  mPopPos = (K8PopupManager::Position)(pp);
  mPopMan->setPosition(mPopPos);

  mPopMan->setFloatHeight(toBoolOpt("/popup/dynsize", true));

  PsycEntity uu(QString("psyc://%1:%2/~%3").arg(host).arg(port).arg(nick));
  mProto->clear();
  mProto->setUNI(uu.uni());
  mPassword = pass;
  mProto->setSecureLogin(seclogin);
  mProto->setUseSSL(useSSL);
  if (useSSL) qDebug() << "SSL";

  mMyStatus = PsycProto::Offline;
  setStatus(PsycProto::Offline, false);
  mDoTypo = toBoolOptCU("/editor/typography");

#ifdef USE_OTR
  {
  FILE *fl = qxfopen(getOTRKeyFile(), true);
  if (fl) {
    otrl_privkey_read_FILEp(mOTRState, fl);
    fclose(fl);
  }
  fl = qxfopen(getOTRFingerFile(), true);
  if (fl) {
    otrl_privkey_read_fingerprints_FILEp(mOTRState, fl, 0, 0);
    fclose(fl);
  }
  }
#endif

  if (!loadBindings(settingsDBPath(mAccName)+"keybinds.txt")) loadBindings(":/data/keybinds.txt");
  setAccountShortcuts();
  loadAutosaved();
}


void ChatForm::loadContactUnread (PsycContact *cc) {
  cc->clearMessages();
  bool wasMsg = false;
  HistoryFile hf(settingsUnreadHistoryFile(mAccName, cc->uni()), mProto->uni());
  if (hf.open(HistoryFile::Read)) {
    //qDebug() << "unread for:" << cc->uni();
    HistoryMessage msg;
    int mNo = 0;
    while (hf.read(mNo++, msg)) {
      if (!msg.valid) continue;
      if (msg.type == HistoryMessage::Incoming || msg.type == HistoryMessage::Outgoing) {
        PsycEntity u(msg.uni);
        if (u.isValid()) {
          cc->addMessage(u, msg.text, msg.action, msg.date);
          wasMsg = true;
        }
      }
    }
    hf.close();
  }
  if (wasMsg && !cc->isPlace()) mFMan->newMessage(cc->uni().toLower());
}


void ChatForm::loadContactList () {
  // do this faster/another way?
  QStringList keys(mSDB->keysPfx("/contacts/unis/"));
  foreach (const QString &key, keys) {
    // found *XXX/uni key
    QString kk(key.mid(15));
    if (kk.isEmpty()) continue;
    //qDebug() << kk;
    PsycEntity ux(kk);
    if (!ux.isValid()) continue;
    kk = optNameEx("/clist/", kk);
    bool ok;
    QString uni = mSDB->toString(kk+"uni", &ok);
    if (!ok) continue;
    PsycEntity user(uni);
    if (!user.isValid()) continue;
    QString verb = mSDB->toString(kk+"verbatim", &ok);
    if (!ok) verb = QString();
    QString group = mSDB->toString(kk+"group", &ok);
    if (!ok) group = QString();

    PsycContact *cc = addContact(uni, group, 0, DontSaveContact);
    if (cc) {
      cc->setStatus(PsycProto::Offline);
      if (!verb.isEmpty()) cc->setVerbatim(verb);
      bool hid = mSDB->toBool(kk+"hidden", &ok);
      cc->setHidden(hid);
      if (!cc->isPlace()) {
        int auth = mSDB->toInt(kk+"authorized", &ok);
        qDebug() << kk << uni << auth << ok;
        //if (uni.toLower().indexOf("kol_panic") >= 0) qDebug() << "!!!!!!";
        if (!ok) auth = 0;
        cc->setAuthorized(auth != 0);
        int lasts = mSDB->toInt(kk+"lastseen", &ok);
        if (lasts < 0 || !ok) lasts = 0;
        cc->setLastSeen(lasts);
        QString lst = mSDB->toString(kk+"laststatustext", &ok);
        if (!ok) lst = QString();
        cc->setStatusText(lst);
      }
    }
  }

  // this will add temporary 'me' contact if it isn't exist
  addContact(mProto->uni(), "", 0, TempContact);

  if (!mFMan) {
    mFMan = new FloatersManager(this,
      toIntOpt("/floaty/intervals/title", 500),
      toIntOpt("/floaty/intervals/icon", 300)
    );
    connect(mFMan, SIGNAL(mouseDblClicked(int, int, const QString &)), this, SLOT(onFloatyDblClicked(int, int, const QString &)));
  }

  // load unread messages
  foreach (PsycContact *cc, mContactList) {
    loadContactUnread(cc);
    redrawContact(cc);
  }
}


void ChatForm::loadAutosaved () {
  if (toBoolOpt("/contactlist/onright") != mCListOnRight) {
    mCListOnRight = toBoolOpt("/contactlist/onright");
    if (mCListOnRight) {
      // was on the left
      splitter0->addWidget(layoutWidget0);
    } else {
      // was on the right
      splitter0->addWidget(layoutWidget1);
    }
  }

  QByteArray sp = toBAOpt("/chatwindow/splitters/0");
  if (!sp.isEmpty()) splitter0->restoreState(sp);
  sp = toBAOpt("/chatwindow/splitters/1");
  if (!sp.isEmpty()) splitter1->restoreState(sp);

  if (toBoolOpt("/chatwindow/saved")) {
/*
    if (mSDB->hasKey("/chatwindow/geometry")) {
      QByteArray geo(mSDB->toByteArray("/chatwindow/geometry"));
      restoreGeometry(geo);
    } else
*/
    {
      int x = toIntOpt("/chatwindow/x");
      int y = toIntOpt("/chatwindow/y");
      int w = toIntOpt("/chatwindow/w");
      int h = toIntOpt("/chatwindow/h");
      setGeometry(x, y, w, h);
    }
  }

  mBlinkMsg = false;
  mTrayIcon->setIcon(QIcon(MAIN_TRAY_ICON));
  if (toBoolOpt("/tray/visible", true)) mTrayIcon->show();
  else mTrayIcon->hide();

  clearCList();
  clearChat();
  //startChatWith("");

  loadContactList();
  checkBlink();

  mProto->setStatusText(toStringOpt("/status/text"));

  startChatWith(toStringOpt("/chatwindow/uni"));
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::saveOneContact (PsycContact *cc) {
  if (!cc) return;
  mSDB->set("/contacts/unis/"+cc->uni().toLower(), 1); // just a flag
  QString kk(optNameEx("/clist/", cc->uni()));
  mSDB->set(kk+"uni", cc->uni());
  mSDB->set(kk+"verbatim", cc->verbatim());
  QString grp(cc->groupName());
  if (!grp.isEmpty()) mSDB->set(kk+"group", grp); else mSDB->remove(kk+"group");
  mSDB->set(kk+"hidden", cc->isHidden());
  if (!cc->isPlace()) {
    qint32 af = 1;
    if (!cc->authorized()) af = 0;
    qDebug() << "***uni:" << cc->uni() << "auth:" << af;
    mSDB->setInt(kk+"authorized", af);
    mSDB->setInt(kk+"lastseen", cc->lastSeenUnix());
    mSDB->set(kk+"laststatustext", cc->statusText());
  }
}


void ChatForm::on_splitter0_splitterMoved (int pos, int index) {
  Q_UNUSED(pos)
  Q_UNUSED(index)
  if (!mSDB) return;
  mSDB->set("/chatwindow/splitters/0", splitter0->saveState());
}


void ChatForm::on_splitter1_splitterMoved (int pos, int index) {
  Q_UNUSED(pos)
  Q_UNUSED(index)
  if (!mSDB) return;
  mSDB->set("/chatwindow/splitters/1", splitter1->saveState());
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::loadRecodeMap (void) {
  QFile file(settingsAppPath()+"data/recode.txt");
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    file.setFileName(":/data/recode.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;
  }

  QTextStream stream;
  QTextCodec *codec = QTextCodec::codecForName("UTF-8");
  if (codec) {
    stream.setDevice(&file);
    stream.setCodec(codec);
    mRecodeMap.clear();
    while (!stream.atEnd()) {
      QString line(stream.readLine());
      QString cFrom(line.section('=', 0, 0)), cTo(line.section('=', 1, 1));
      if (cFrom.length() != 1 || cTo.length() != 1) continue;
      mRecodeMap.insert(cFrom[0], cTo[0]);
      mRecodeMap.insert(cTo[0], cFrom[0]);
    }
  }
  file.close();
}


QString ChatForm::recodeMsg (const QString &msg) {
  QString res = msg;
  for (int f = 0; f < res.length(); f++) {
    QChar ch = res.at(f);
    res[f] = mRecodeMap.value(ch, ch);
  }
  return res;
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::redrawContact (PsycContact *cc) {
  if (!cc || cc->isTemp()) return;
  runCListJS("redrawContact("+K8Str::jsStr(cc->uni())+");");
}


PsycContact *ChatForm::findContact (const QString &uni) {
  QString u(uni.toLower());
  if (!mContactList.contains(u)) return 0;
  return mContactList[u];
}


PsycContact *ChatForm::findContact (const PsycEntity &cont) {
  if (!cont.isValid()) return 0;
  return findContact(cont.uni());
}


PsycContact *ChatForm::addContact (const PsycEntity &user, const QString &group, bool *isNew, ACSaveMode mode) {
  if (!user.isValid()) return 0;
  PsycContact *cc = findContact(user);
  if (cc) {
    if (isNew) *isNew = false;
  } else {
    if (isNew) *isNew = true;
    cc = new PsycContact(user.uni());
    cc->setStatus(user.status());
    cc->setStatusText(user.statusText());
    cc->setMood(user.mood());
    cc->setGroupName(group);
    mContactList[cc->uni().toLower()] = cc;
    switch (mode) {
      case SaveContact: saveOneContact(cc); break;
      case TempContact: cc->setTemp(true); break;
      default: ;
    }
  }
  return cc;
}


void ChatForm::deleteContact (PsycContact *cc) {
  if (!cc) return;
  if (mChatUser == cc) startChatWith("");
  QString uc(cc->uni().toLower());
  if (!mContactList.contains(uc)) return;

  mContactList.remove(uc);

  int idx = mTabList.indexOf(cc);
  if (idx >= 0) mTabList.removeAt(idx);

  delete cc;

  mSDB->removeAll(optNameEx("/", uc));
  mSDB->remove("/contacts/unis/"+uc);

  mSDB->removeAll("/floaty/contacts/"+uc);
  mFMan->remove(uc.toLower());
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::on_action_quit_triggered (bool) {
  close();
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::onUpdateAccountInfo (AccWindow *au, const QString &text) {
  //qDebug() << "update info for account" << text;
  QString odb;

  K8SDB *db = 0;
  QDir ppdir(settingsPrefsPath()+text.toLower());

  QString pass, host;
  int p;

  if (text.isEmpty() || !ppdir.exists()) goto clear;

  if (mSDB) {
    odb = mSDB->pathname();
    delete mSDB;
    mSDB = 0;
  }

  db = new K8SDB(settingsDBFile(text.toLower()), K8SDB::OpenExisting);
  if (!db->isOpen()) goto clear;

/*
  PsycEntity u(QString("psyc://%1:%2/%3"
    .arg(db->toString("/server/host"))
    .arg(db->toInt("/server/port"))
    .arg(db->toString("/login/nick"))
    .arg(db->toString("/login/password"))
  ));
*/

  au->leNick->setText(db->toString("/login/nick"));

  host = db->toString("/server/host");
  p = db->toInt("/server/port");
  if (p > 0 && p != 4404) host += QString(":%1").arg(p);
  au->leHost->setText(host);

  pass = K8Str::dePass(db->toString("/login/password"));
  au->lePass->setText(pass);

  au->cbSecure->setChecked(db->toBool("/login/secure"));
#ifdef USE_SSL
  au->cbSSL->setChecked(db->toBool("/login/useSSL"));
#else
  au->cbSSL->setChecked(false);
  au->cbSSL->setEnabled(false);
#endif

  au->btDelAcc->setEnabled(true);
  goto done;

clear:
  au->btDelAcc->setEnabled(false);
  au->leNick->setText("");
  au->leHost->setText("");
  au->lePass->setText("");
  au->cbSecure->setChecked(true);

done:
  delete db;
  if (!odb.isEmpty()) {
    mSDB = new K8SDB(odb, K8SDB::OpenAlways);
  }
}


QString ChatForm::defAccount () {
  QString res;
  K8SDB *mainDB = new K8SDB(settingsMainDBFile(), K8SDB::OpenExisting);
  if (mainDB->isOpen()) {
    bool ok;
    res = mainDB->toString("/active", &ok);
    if (!ok || res.isEmpty()) res = QString();
  }
  delete mainDB;
  return res;
}


void ChatForm::setDefAccount (const QString &name) {
  if (name.isEmpty()) return;

  K8SDB *mainDB = new K8SDB(settingsMainDBFile(), K8SDB::OpenAlways);
  if (mainDB->isOpen()) {
    mainDB->set("/active", name);
  }
  delete mainDB;
}


void ChatForm::accountSetDefaults (K8SDB *db) {
  // defaults
  if (!db || !db->isOpen()) return;
  setAccDefaultOptions(db);
}


//FIXME: check for errors!
void ChatForm::onAccountCreated (AccWindow *au, const QString &text) {
  Q_UNUSED(au)
  qDebug() << "account created:" << text;

  K8SDB *db = 0;
  QDir ppdir(settingsPrefsPath());
  ppdir.mkdir(text.toLower());

  db = new K8SDB(settingsDBFile(text.toLower()), K8SDB::OpenAlways);
  if (!db->isOpen()) { delete db; return; }

/*
  au->btDelAcc->setEnabled(true);
  au->leNick->setText("");
  au->leHost->setText("");
  au->lePass->setText("");
  au->cbSecure->setChecked(true);
*/

  accountSetDefaults(db);

  delete db;
}


void ChatForm::onAccountDeleted (AccWindow *au, const QString &text) {
  Q_UNUSED(au)
  qDebug() << "account deleted:" << text;

  K8SDB *db = new K8SDB(settingsMainDBFile(), K8SDB::OpenAlways);
  if (!db->isOpen()) { delete db; return; }

  db->remove(text.toLower());
  db->removeAll(text.toLower()+"/");

  bool ok = false;
  QString act = db->toString("/active", &ok);
  if (ok && !act.compare(text, Qt::CaseInsensitive)) db->remove("/active");

  QDir ppdir;
  ppdir.rename(settingsPrefsPath()+text.toLower(), settingsPrefsPath()+"_"+text.toLower());

  delete db;
}


void ChatForm::on_action_accedit_triggered (bool) {
/*
  K8SDB *mainDB = new K8SDB(settingsMainDBFile(), K8SDB::OpenExisting);
  if (mainDB->isOpen()) {
    bool ok;
    QString actAcc = mainDB->toString("/active", &ok);
    if (!ok || actAcc.isEmpty()) mAccName = QString(); else mAccName = actAcc;
  }
  delete mainDB;
*/
  QDir ppdir(settingsPrefsPath());
  QStringList accList(ppdir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot | QDir::Readable | QDir::Writable,
    QDir::Name | QDir::IgnoreCase | QDir::LocaleAware));
  for (int f = 0; f < accList.count(); f++) {
    QString s(accList[f]);
    if (s.isEmpty() || s[0] == '_' || s[0] == '.') {
      accList.removeAt(0);
      f--;
    }
  }

  AccWindow *au = new AccWindow(this);

  connect(au, SIGNAL(updateAccountInfo(AccWindow *, const QString &)), SLOT(onUpdateAccountInfo(AccWindow *, const QString &)));
  connect(au, SIGNAL(accountCreated(AccWindow *, const QString &)), SLOT(onAccountCreated(AccWindow *, const QString &)));
  connect(au, SIGNAL(accountDeleted(AccWindow *, const QString &)), SLOT(onAccountDeleted(AccWindow *, const QString &)));

  au->cbAccount->addItems(accList);
  if (mAccName.isEmpty()) au->cbAccount->setCurrentIndex(-1);
  else au->cbAccount->setCurrentIndex(accList.indexOf(mAccName));

  au->maxIdx = accList.count()-1;

/*
  PsycEntity u(mProto->uni());
  if (u.isValid()) {
    au->leNick->setText(u.nick());
    if (u.port() != 0 && u.port() != 4404) au->leHost->setText(QString("%1:%2").arg(u.host()).arg(u.port()));
    else au->leHost->setText(u.host());
  } else {
    au->leNick->setText("");
    au->leHost->setText("");
  }
  au->lePass->setText(mPassword);
  au->cbSecure->setChecked(mProto->secureLogin());
*/
  do {
    if (au->exec() != 1) break;
    PsycEntity u("psyc://"+au->leHost->text()+"/~"+au->leNick->text());
    if (!u.isValid()) break;

    //FIXME: idiotic w32!
    bool sameAcc = (mAccName == au->curAccName) && u.sameUNI(mProto->uni());

    if (!sameAcc) {
      mProto->logoff();
      mProto->clear();
      mProto->setUNI(u.uni());

      delete mSDB;

      mAccName = au->curAccName;
      mSDB = new K8SDB(settingsDBFile(mAccName), K8SDB::OpenAlways);

      setDefAccount(mAccName);
    }

    mSDB->set("/server/host", u.host());
    mSDB->set("/server/port", (qint32)u.port());
    mSDB->set("/login/nick", u.nick());
    mSDB->set("/login/password", K8Str::enPass(au->lePass->text()));
    mSDB->set("/login/secure", au->cbSecure->isChecked());
#ifdef USE_SSL
    mSDB->set("/login/useSSL", au->cbSSL->isChecked());
#endif

    mProto->setSecureLogin(au->cbSecure->isChecked());
    mPassword = au->lePass->text();

    if (!sameAcc) loadAccount();
    else {
      mProto->setSecureLogin(au->cbSecure->isChecked());
      mProto->setUseSSL(au->cbSSL->isChecked());
    }
  } while (0);

  delete au;
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::onAuthRequested (const PsycEntity &user) {
  if (user.isPlace()) return;
  PsycContact *cc = addContact(user.uni());
  if (!cc) return;
  if (cc->authorized()) {
    mProto->sendAuth(user);
    return;
  }
  //cc->setAuthorized(true);
  AuthWindow *au = new AuthWindow(user, this);
  au->setWindowTitle("Dyskinesia: "+user.uni()+tr(" requested authorization from you"));
  au->edText->setPlainText(user.uni()+tr(" kindly asks for your friendship."));
  au->btOk->setFocus();
  connect(au, SIGNAL(granted(const PsycEntity &, const QString &)), this, SLOT(onAuthClicked(const PsycEntity &, const QString &)));
  au->show();
}


void ChatForm::onAuthClicked (const PsycEntity &entity, const QString &text) {
  Q_UNUSED(text)
  mProto->sendAuth(entity);
  bool isNew;
  PsycContact *cc = addContact(entity.uni(), "", &isNew);
  if (cc && (isNew || !cc->authorized())) {
    cc->setAuthorized(true);
    saveOneContact(cc);
    redrawContact(cc);
  }
  qDebug() << "auth granted to" << entity.uni() << "text:" << text;
}


void ChatForm::onAuthGranted (const PsycEntity &user) {
  if (user.isPlace()) return;
  if (toBoolOpt("/popup/active", true, user.uni())) showInfoPopup(user.uni()+"\ngranted authorization");
  bool isNew;
  PsycContact *cc = addContact(user.uni(), "", &isNew);
  if (!cc) return;
  if (isNew || !cc->authorized()) {
    cc->setAuthorized(true);
    //mProto->sendAuth(user, tr("auth granted"));
    saveOneContact(cc);
    redrawContact(cc);
  }
}


void ChatForm::onAuthDenied (const PsycEntity &user) {
  if (user.isPlace()) return;
  if (toBoolOpt("/popup/active", true, user.uni())) showInfoPopup(user.uni()+"\nrevoked authorization");
  PsycContact *cc = findContact(user);
  if (!cc || !cc->authorized()) return;
  cc->setAuthorized(false);
  saveOneContact(cc);
  redrawContact(cc);
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::on_edChat_textChanged (void) {
  //if (mChBlock || !mChatUser || !mDoTypo) return;
  if (mChBlock) return; // don't recurse
  if (mChatUser) {
    if (!mDoTypo) return;
  } else if (!toBoolOptCU("/editor/typography")) return;
  QString txt = edChat->toPlainText();
  if (txt.isEmpty()) return;
  QChar ch = txt.at(0);
  if (ch == '/' || ch == ' ' || ch == 13 || ch == 10) return;
  int mv = K8Str::fixTypograpy(txt);
  if (mv >= 0) {
    mChBlock = true;
    int pos = edChat->textCursor().position()-mv;
    if (pos > txt.length()) pos = txt.length();
    if (pos < 0) pos = 0;
    edChat->setPlainText(txt);
    QTextCursor cur = edChat->textCursor();
    cur.setPosition(pos);
    edChat->setTextCursor(cur);
    mChBlock = false;
  }
}


void ChatForm::chatAutoComplete () {
  if (!mChatUser || !mChatUser->isPlace()) {
    mStartingNickCompletion.clear();
    mLatestNickCompletion.clear();
    return;
  }
  QString txt = edChat->toPlainText();
  if (txt.isEmpty() || txt[0] == '/') {
    mStartingNickCompletion.clear();
    mLatestNickCompletion.clear();
    return;
  }
  QTextCursor tcur = edChat->textCursor();
  int pos = tcur.position();
  //TODO: write regexp!
  QString nick = txt.left(pos);
  QRegExp nre("^\\S*:?\\s*");
  if (nick.isEmpty() || !nre.exactMatch(nick)) {
/*
    mStartingNickCompletion.clear();
    mLatestNickCompletion.clear();
*/
    return;
  }
  nick = nick.trimmed();
  if (nick[nick.length()-1] == ':') nick.chop(1);
  nick = nick.trimmed();
  if (nick.isEmpty()) {
    mStartingNickCompletion.clear();
    mLatestNickCompletion.clear();
    return;
  }
  QStringList ppl(mChatUser->persons());
  if (ppl.size() < 1) {
    mStartingNickCompletion.clear();
    mLatestNickCompletion.clear();
    return;
  }
  // rebuild the list
  for (int f = ppl.size()-1; f >= 0; f--) {
    QString s(ppl[f]);
    PsycEntity u(s);
    PsycContact *cc = findContact(s);
    if (cc) s = cc->verbatim(); else s = u.nick();
    ppl[f] = s;
  }
  ppl.sort();
  // find the position
  if (!mStartingNickCompletion.startsWith(nick) && !nick.startsWith(mStartingNickCompletion)) {
    mStartingNickCompletion.clear();
    mLatestNickCompletion.clear();
  }
  int st = 0, cur = 0;
  if (mStartingNickCompletion.isEmpty()) {
    // first time
    mStartingNickCompletion = nick;
  } else {
    // continue
    //st = (ppl.indexOf(mLatestNickCompletion)+1)%ppl.size();
    for (int f = 0; f < ppl.size(); f++) {
      if (!ppl[f].compare(mLatestNickCompletion, Qt::CaseInsensitive)) {
        st = f; cur = (f+1)%ppl.size();
        break;
      }
    }
  }
  nick = mStartingNickCompletion;
  mLatestNickCompletion.clear();
  qDebug() << st << cur << nick;
  do {
    QString s(ppl[cur]);
    qDebug() << s;
    if (s.length() >= nick.length() && !s.leftRef(nick.length()).compare(nick, Qt::CaseInsensitive)) {
      // match found!
      mLatestNickCompletion = s;
      nick = s+": ";
      qDebug() << "found!" << nick;
      txt = edChat->toPlainText();
      // shitty code!
      int i = nre.indexIn(txt);
      if (i >= 0) txt.remove(0, nre.matchedLength());
/*
      while (!txt.isEmpty() || txt[0].isSpace()) txt.remove(0, 1);
      while (!txt.isEmpty() && !(txt[0].isSpace()) && txt[0] != ':') txt.remove(0, 1);
      if (!txt.isEmpty() && txt[0] == ':') txt.remove(0, 1);
      if (!txt.isEmpty() || txt[0].isSpace()) nick.chop(1);
*/
      txt.prepend(nick);
      mChBlock = true;
      edChat->setPlainText(txt);
      tcur.setPosition(nick.length());
      edChat->setTextCursor(tcur);
      mChBlock = false;
      break;
    }
    cur = (cur+1)%ppl.size();
  } while (cur != st);
  // do nothing
}


void ChatForm::keyPressEvent (QKeyEvent *event) {
  Q_UNUSED(event)
/*
  if (event->modifiers() != Qt::CTRL) {
    if (event->key() == Qt::Key_Escape) {
      if (toBoolOpt("/app/eschides")) hide(); else showMinimized();
      return;
    }
*/
/*
    if (edChat->hasFocus()) return;
    edChat->setFocus();
    QCoreApplication::sendEvent(edChat, event);
    return;
  }
*/
}


void ChatForm::showEvent (QShowEvent *event) {
  //if (toBoolOptCU("/chat/show/forcescroll", false)) scrollToBottom();
  edChat->setFocus();
  mUnreadInChat = false;
  if (mChatUser) {
    mPopMan->removeById("", mChatUser->uni().toLower());
    if (mFMan) mFMan->noMessages(mChatUser->uni());
  }
  QWidget::showEvent(event);
  checkBlink();
}


void ChatForm::closeEvent (QCloseEvent *event) {
  if (!mCloseWaiting) {
    mCloseWaiting = true;
    storeAppData();
    bool h = mProto->isConnected();
    setStatus(PsycProto::Offline);
    if (h) {
      QTimer::singleShot(5000, this, SLOT(close()));
      event->ignore();
      return;
    }
  }
  event->accept();
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::scrollToBottom () {
  runChatJS("do{var ___tmp___=window.pageYOffset;window.scrollTo(0,1000000);}while(window.pageYOffset!=___tmp___);");
}


void ChatForm::translateMessage () {
  mChBlock = true;
  QString txt = edChat->toPlainText();
  if (edChat->textCursor().hasSelection()){
    QString selText = edChat->textCursor().selectedText();
    // Qt internally uses U+FDD0 and U+FDD1 to mark the beginning and the end of frames.
    // They should be seen as non-printable characters, as trying to display them leads
    // to a crash caused by a Qt "noBlockInString" assertion.
    selText.replace(QChar(0xFDD0), " ");
    selText.replace(QChar(0xFDD1), " ");
    selText.replace(QChar(0x2028), "\n");
    selText = recodeMsg(selText);
    txt.replace(edChat->textCursor().selectionStart(), selText.length(), selText);
  } else txt = recodeMsg(txt);
  edChat->clear();
  edChat->insertPlainText(txt);
  edChat->ensureCursorVisible();
  edChat->setFocus();
  mChBlock = false;
  mStartingNickCompletion.clear();
  mLatestNickCompletion.clear();
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::doSendMessage () {
  mStartingNickCompletion.clear();
  mLatestNickCompletion.clear();

  QString txt(edChat->toPlainText());

  if (txt.length() > 1 && txt[0] == '/') {
    edChat->clear();
    mEngCmds->doCommand(txt);
    return;
  }

  if (!mProto->isLoggedIn()) return;

  txt = txt.trimmed();
  edChat->clear();
  if (txt.isEmpty()) return;
  if (!mChatUser) return; //FIXME: send to main entity as command?
  QString act;
  QString achr(toStringOpt("/editor/actionstart", QString(), mChatUser->uni()));
  if (!achr.isEmpty() && txt.startsWith(achr)) {
    // the first line is action
    int idx = txt.indexOf('\n');
    if (idx < 0) idx = txt.length();
    act = txt.left(idx);
    act.remove(0, achr.length());
    act = act.simplified();
    txt.remove(0, idx+1);
    txt = txt.trimmed();
    if (txt.isEmpty() && act.isEmpty()) return;
  }
  mWasSentTo << mChatUser->uni().toLower();
#ifdef USE_OTR
  otrSendMessage(mChatUser->uni(), txt, act);
#else
  mProto->sendMessage(mChatUser->uni(), txt, act);
#endif
  if (!mChatUser->isOTRActive()) {
    // no OTR, check 'immediate output mode'
#ifdef USE_OTR
    dlogf("NO OTR!");
#endif
    if (!toBoolOptCU("/chat/messaging/immediateout", false)) return; // out this when echo comes
  } else {
#ifdef USE_OTR
    dlogf("OTR active!");
#endif
    //qDebug() << txt;
  }
  QDateTime now(QDateTime::currentDateTime());
  PsycEntity me(mProto->uni());
  addMessageToHistory(*mChatUser->entity(), me, txt, act, now, MessageRead);
  addMessageToChat(me, txt, act, now);
}


///////////////////////////////////////////////////////////////////////////////
QIcon ChatForm::LinuxizeTrayIcon (const QIcon &ico) {
  QPixmap ipm(ico.pixmap(16, 16));
  QPixmap fpm(ipm.width(), ipm.height());
  fpm.fill(Qt::black);
  {
    QPainter p(&fpm);
    p.drawPixmap(0, 0, ipm);
    p.end();
  }
  return QIcon(fpm);
}


QIcon ChatForm::statusIcon (PsycProto::Status st) {
  switch (st) {
    case PsycProto::Internal: return QIcon(":/icons/status/connecting.png");
    case PsycProto::Offline: return action_stoffline->icon();
    case PsycProto::Vacation: return action_stvacation->icon();
    case PsycProto::Away: return action_staway->icon();
    case PsycProto::DND: return action_stdnd->icon();
    case PsycProto::Nearby: return action_stnear->icon();
    case PsycProto::Busy: return action_stbusy->icon();
    case PsycProto::Here: return action_sthere->icon();
    case PsycProto::FFC: return action_stffc->icon();
    case PsycProto::Realtime: return action_strtime->icon();
    default: ;
  }
  return action_stoffline->icon();
}


void ChatForm::setStatus (PsycProto::Status st, bool doSend) {
  bool connecting = false;
  if (mMyStatus == PsycProto::Offline && st != PsycProto::Offline) {
    connecting = true;
    if (doSend) mProto->connectToServer();
  }
  if (mMyStatus != PsycProto::Offline && st == PsycProto::Offline) {
    if (doSend) {
      disconnectAllOTR();
      if (mProto->isLoggedIn()) storeAppData();
      mProto->logoff();
    }
  }

  switch (st) {
    //case PsycProto::Internal: return QIcon(":/icons/status/connecting.png");
    case PsycProto::Offline:
      action_stoffline->setChecked(true);
      break;
    case PsycProto::Vacation:
      action_stvacation->setChecked(true);
      break;
    case PsycProto::Away:
      action_staway->setChecked(true);
      break;
    case PsycProto::DND:
      action_stdnd->setChecked(true);
      break;
    case PsycProto::Nearby:
      action_stnear->setChecked(true);
      break;
    case PsycProto::Busy:
      action_stbusy->setChecked(true);
      break;
    case PsycProto::Here:
      action_sthere->setChecked(true);
      break;
    case PsycProto::FFC:
      action_stffc->setChecked(true);
      break;
    case PsycProto::Realtime:
      action_strtime->setChecked(true);
      break;
    default: ;
  }
  if (connecting) btStatus->setIcon(QIcon(":/icons/status/connecting.png"));
  else btStatus->setIcon(statusIcon(st));

  if (toBoolOpt("/status/trayicon")) {
#ifndef WIN32
    mTrayIcon->setIcon(LinuxizeTrayIcon(statusIcon(st)));
#else
    mTrayIcon->setIcon(statusIcon(st));
#endif
  }

  mMyStatus = st;
  if (!doSend) return;
  //if (mProto->isLoggedIn() && (mMyStatus != mProto->status() || mMyMood != mProto->mood())) {
  mProto->setStatusText(toStringOpt("/status/text"));
  if (mProto->isLoggedIn()) {
    //if (mMyStatus == mProto->status()) mProto->setStatus(PsycProto::Internal, mProto->statusText(), mMyMood);
    mProto->setStatus(mMyStatus, mProto->statusText(), mMyMood);
    //mProto->setStatus(mMyStatus, "seven are they!", mMyMood);
  }
}


void ChatForm::on_action_stoffline_triggered (bool) { setStatus(PsycProto::Offline); }
void ChatForm::on_action_stvacation_triggered (bool) { setStatus(PsycProto::Vacation); }
void ChatForm::on_action_staway_triggered (bool) { setStatus(PsycProto::Away); }
void ChatForm::on_action_stdnd_triggered (bool) { setStatus(PsycProto::DND); }
void ChatForm::on_action_stnear_triggered (bool) { setStatus(PsycProto::Nearby); }
void ChatForm::on_action_stbusy_triggered (bool) { setStatus(PsycProto::Busy); }
void ChatForm::on_action_sthere_triggered (bool) { setStatus(PsycProto::Here); }
void ChatForm::on_action_stffc_triggered (bool) { setStatus(PsycProto::FFC); }
void ChatForm::on_action_strtime_triggered (bool) { setStatus(PsycProto::Realtime); }


///////////////////////////////////////////////////////////////////////////////
void ChatForm::setMood (PsycProto::Mood md) {
  switch (md) {
    case PsycProto::Unspecified: action_moodunspecified->setChecked(true); break;
    case PsycProto::Dead: action_mooddead->setChecked(true); break;
    case PsycProto::Angry: action_moodangry->setChecked(true); break;
    case PsycProto::Sad: action_moodsad->setChecked(true); break;
    case PsycProto::Moody: action_moodmoody->setChecked(true); break;
    case PsycProto::Okay: action_moodokay->setChecked(true); break;
    case PsycProto::Good: action_moodgood->setChecked(true); break;
    case PsycProto::Happy: action_moodhappy->setChecked(true); break;
    case PsycProto::Bright: action_moodbright->setChecked(true); break;
    case PsycProto::Nirvana: action_moodnirvana->setChecked(true); break;
  }
  mMyMood = md;
  if (!mProto->isLoggedIn()) return;
  if (mMyStatus != mProto->status()) setStatus(mMyStatus);
  else {
    mProto->setStatusText(toStringOpt("/status/text"));
    if (mMyStatus != PsycProto::Offline) {
      //FIXME
      mProto->setStatus(PsycProto::Offline, mProto->statusText(), mMyMood);
      mProto->setStatus(PsycProto::Here, mProto->statusText(), mMyMood);
      if (mMyStatus != PsycProto::Here) mProto->setStatus(mMyStatus, mProto->statusText(), mMyMood);
    }
  }
}


void ChatForm::on_action_moodunspecified_triggered (bool) { setMood(PsycProto::Unspecified); }
void ChatForm::on_action_mooddead_triggered (bool) { setMood(PsycProto::Dead); }
void ChatForm::on_action_moodangry_triggered (bool) { setMood(PsycProto::Angry); }
void ChatForm::on_action_moodsad_triggered (bool) { setMood(PsycProto::Sad); }
void ChatForm::on_action_moodmoody_triggered (bool) { setMood(PsycProto::Moody); }
void ChatForm::on_action_moodokay_triggered (bool) { setMood(PsycProto::Okay); }
void ChatForm::on_action_moodgood_triggered (bool) { setMood(PsycProto::Good); }
void ChatForm::on_action_moodhappy_triggered (bool) { setMood(PsycProto::Happy); }
void ChatForm::on_action_moodbright_triggered (bool) { setMood(PsycProto::Bright); }
void ChatForm::on_action_moodnirvana_triggered (bool) { setMood(PsycProto::Nirvana); }


///////////////////////////////////////////////////////////////////////////////
void ChatForm::on_action_showconsole_triggered (bool) {
  if (!mConForm) {
    mConForm = new ConsoleForm(this);
    mConForm->mUni = mProto->uni();
    connect(mConForm, SIGNAL(destroyed()), this, SLOT(consoleDestroyed()));
    mConForm->cbActive->setChecked(true);

    if (toBoolOpt("/pktconsolewindow/saved", false)) {
      int x = toIntOpt("/pktconsolewindow/x");
      int y = toIntOpt("/pktconsolewindow/y");
      int w = toIntOpt("/pktconsolewindow/w");
      int h = toIntOpt("/pktconsolewindow/h");
      mConForm->setGeometry(x, y, w, h);
/*
      QByteArray geo(mSDB->toByteArray("/pktconsolewindow/geometry"));
      mConForm->restoreGeometry(geo);
*/
      mConForm->cbActive->setChecked(toBoolOpt("/pktconsolewindow/active", true));
    }
  }
  mConForm->hide();
  mConForm->show();
  if (mConForm->isMinimized()) mConForm->showNormal();
  mConForm->activateWindow();
}


void ChatForm::onUserPktSend () {
  if (!mProto->isLoggedIn()) return;
  PktForm *frm = dynamic_cast<PktForm *>(sender());
  if (!frm) return;
  QStringList sl(frm->edPacket->toPlainText().trimmed().split("\n"));
  //qDebug() << sl;
  if (sl.count() > 1) {
    PsycPacket pkt;
    foreach (const QString &s, sl) {
      if (s == ".") break;
      pkt << s;
    }
    mProto->sendPacket(pkt);
  }
  frm->close();
}


void ChatForm::on_action_sendpacket_triggered (bool) {
  PktForm *frm = new PktForm(this);
  //connect(frm->btSend, SIGNAL(clicked()), this, SLOT(onUserPktSend()));
  connect(frm, SIGNAL(send()), this, SLOT(onUserPktSend()));
  PsycPacket pkt("_");
  pkt.put("_target", mProto->uni());
  pkt.put("_source_identification", mProto->uni());
  pkt.endRoute();
  if (mChatUser) {
    pkt.put("_person", mChatUser->uni());
  }
  QString s(pkt.toString());
  if (s.rightRef(3) == "\n.\n") s.chop(3);
  frm->edPacket->setPlainText(s);
  frm->edPacket->moveCursor(QTextCursor::End);
  frm->edPacket->ensureCursorVisible();
  frm->edPacket->setFocus(Qt::OtherFocusReason);
  frm->show();
}


///////////////////////////////////////////////////////////////////////////////
bool ChatForm::addMessageToChat (const PsycEntity &from, const QString &msg, const QString &action, const QDateTime &date) {
  bool mine = from.sameUNI(mProto->uni());

  PsycContact *cc = findContact(from);
  QString vn(from.verbatim());
  if (cc) vn = cc->verbatim();

  QString act(action), text(msg);
  if (act.startsWith(" DONTTOUCH|")) {
    act.remove(0, 11);
  } else {
    text = K8Str::fixURLs(msg);
    text.replace("\n", "<br />");
  }
  act = K8Str::escapeStr(act);

  QString js(QString("addChatMessage({")+
    "ismine:"+bool2js(mine)+","+
    "unixdate:"+QString::number(date.toTime_t())+","+
    "uni:"+K8Str::jsStr(from.uni())+","+
    "text:"+K8Str::jsStr(text)+","+
    "action:"+K8Str::jsStr(act)+
    "});"
  );

  runChatJS(js);
  //qDebug() << "addChatMessage()";
  return true;
}


bool ChatForm::addMessageToHistory (const PsycEntity &dest, const PsycEntity &src, const QString &text,
  const QString &action, const QDateTime &time, UnreadFlag isUnread)
{
  //qDebug() << "addMessageToHistory: acc:" << mProto->uni() << "dest:" << dest.uni() << "from:" << src.uni();
  if (text.startsWith("?OTR")) return true; // skip OTR messages

  QString hfname;
  if (isUnread == MessageUnread) {
    hfname = settingsUnreadHistoryFile(mAccName, dest.uni());
  } else {
    hfname = settingsHistoryFile(mAccName, dest.uni());
  }
  HistoryFile hf(hfname, mProto->uni());
  if (!hf.open(HistoryFile::Write)) return false;
  QString uni(src.uni());
  HistoryMessage msg;
  msg.uni = uni;
  msg.date = time;
  msg.text = text;
  msg.text.replace("\r\n", "\n");
  msg.action = action;
  msg.action.replace("\r\n", "\n");
  msg.type = mProto->uni().compare(uni, Qt::CaseInsensitive) ? HistoryMessage::Incoming : HistoryMessage::Outgoing;
  msg.valid = true; // just in case
  hf.append(msg);
  hf.close();
  //if (isUnread != MessageRead) addMessageToHistory(dest, src, text, actuib, time, MessageRead);
  return true;
}


void ChatForm::startChatWith (const QString &aUNI, bool dontRearrange) {
  edChat->setFocus();
  PsycEntity user(aUNI);
  clearChat();
  if (!user.isValid()) {
    setWindowTitle(DYSKINESIA);
    mChatUser = 0;
    mSDB->set("/chatwindow/uni", "");
    mDoTypo = toBoolOptCU("/editor/typography");
    runCListJS("chatStarted('');");
    runChatJS("chatStarted('');");
    checkBlink();
    return;
  }
  bool isNew = false;
  PsycContact *cc = addContact(aUNI, "", &isNew);
  if (!cc) return;

  int idx = mTabList.indexOf(cc);
  if (dontRearrange) {
    if (idx < 0) mTabList << cc;
  } else {
    if (idx >= 0) mTabList.removeAt(idx);
    mTabList.insert(0, cc);
  }

  if (isNew && !user.isPlace()) {
    cc->setAuthorized(false);
    cc->setStatus(PsycProto::Offline);
    saveOneContact(cc);
  }
  mChatUser = cc;
  setWindowTitle(cc->verbatim()+" -- "+DYSKINESIA);
  mSDB->set("/chatwindow/uni", cc->uni());
  mDoTypo = toBoolOptCU("/editor/typography");
  lbChatDest->setText(cc->verbatim()+" ["+cc->uni()+"]");

  runCListJS("chatStarted("+K8Str::jsStr(cc->uni())+");");
  runChatJS("chatStarted("+K8Str::jsStr(cc->uni())+");");

  bool wasHist = false;
  int toShow = toIntOptCU("/history/showcount", 3);
  if (toShow > 0) {
    HistoryFile hf(settingsHistoryFile(mAccName, cc->uni()), mProto->uni());
    if (hf.open(HistoryFile::Read)) {
      //fprintf(stderr, "!!!\n");
      HistoryMessage msg;
      toShow = hf.count()-toShow;
      if (toShow < 0) toShow = 0;
      //fprintf(stderr, " %i\n", toShow);
      while (hf.read(toShow++, msg)) {
        if (msg.valid) {
          //qDebug() << msg.text;
          PsycEntity uu(msg.uni);
          addMessageToChat(uu, msg.text, msg.action, msg.date);
          wasHist = true;
        }
      }
      hf.close();
    }
  }

  if (cc->messageCount() > 0) {
    if (wasHist) runChatJS("addHR();");
    QList<MyMessage *>list(cc->messages());
    QString uni(cc->uni());
    QString nick(cc->nick());
    foreach (MyMessage *msg, list) {
      //qDebug() << "unread of:" << cc->entity()->uni() << "from:" << msg->user.uni();
      addMessageToHistory(*cc->entity(), msg->user, msg->text, msg->action, msg->time, MessageRead);
      addMessageToChat(msg->user, msg->text, msg->action, msg->time);
    }
    cc->clearMessages();
    // remove saved history
    HistoryFile hf(settingsUnreadHistoryFile(mAccName, cc->uni()), mProto->uni());
    hf.remove();
  }

  mFMan->noMessages(cc->uni());

  redrawContact(cc);
  scrollToBottom();
  mPopMan->removeById("", cc->uni().toLower());

  //knockAutosave();
  mSDB->set("/chatwindow/uni", cc->uni());
  checkBlink();

  edChat->setPlainText(toStringOptCU("/editor/state/text", ""));
  mStartingNickCompletion.clear();
  mLatestNickCompletion.clear();
  int cpos = toIntOptCU("/editor/state/curpos", -1);
  if (cpos < 0) edChat->moveCursor(QTextCursor::End);
  else {
    QTextCursor cur(edChat->textCursor());
    cur.setPosition(cpos);
    edChat->setTextCursor(cur);
  }
  edChat->ensureCursorVisible();
}


void ChatForm::markAsRead (const QString &aUNI) {
  PsycContact *cc = findContact(aUNI);
  if (!cc || cc->messageCount() < 1) return;
  QList<MyMessage *>list(cc->messages());
  QString uni(cc->uni());
  foreach (MyMessage *msg, list) {
    //qDebug() << "unread of:" << cc->entity()->uni() << "from:" << msg->user.uni();
    addMessageToHistory(*cc->entity(), msg->user, msg->text, msg->action, msg->time, MessageRead);
  }
  cc->clearMessages();
  // remove saved history
  HistoryFile hf(settingsUnreadHistoryFile(mAccName, cc->uni()), mProto->uni());
  hf.remove();
  mFMan->noMessages(cc->uni());
  redrawContact(cc);
  checkBlink();
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::onPopupClick (const QString &type, const QString &id, Qt::MouseButton button) {
  if (button != Qt::LeftButton) return;
  if (isMinimized() || !isVisible()) showNormal();
  activateWindow();
  if (type != "private" && type != "public" && type != "status") return;
  PsycEntity user(id);
  if (!user.isValid()) return;
  if (!mChatUser || !mChatUser->entity()->sameUNI(user.uni())) startChatWith(user.uni());
  mPopMan->removeById("", id);
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::onConnected () {
  mWasSentTo.clear();
}


void ChatForm::onDisconnected () {
  foreach (PsycContact *cc, mContactList) {
    cc->setStatus(PsycProto::Offline);
    if (cc->isPlace()) onPlaceLeaved(*cc->entity()); else otrDisconnect(cc->uni());
    mFMan->newStatus(cc->uni(), PsycProto::Offline);
    redrawContact(cc);
  }
  mMyStatus = PsycProto::Offline;
  setStatus(mMyStatus, false);
  if (mCloseWaiting) close();
}


void ChatForm::onNetError (const QString &errStr) {
  qDebug() << "NETWORK ERROR:" << errStr;
  //QMessageBox::warning(this, tr("PSYC ERROR"), tr("NETWORK ERROR: ")+errStr);
  if (mPopupActive) showErrorPopup(tr("NETWORK ERROR: ")+errStr);
}


// export packet to WebKit JS
void ChatForm::packetToJSAPI (const PsycPacket &pkt) {
  // build a dictionary
  QString pktS = "psycpkt={";
  pktS += K8Str::jsStr("method")+":"+K8Str::jsStr(pkt.method());
  pktS += ","+K8Str::jsStr("vars")+":{";
  bool needComma = false;
  foreach (const QString &n, pkt.routeVars()) {
    if (needComma) pktS += ","; else needComma = true;
    pktS += K8Str::jsStr(n)+":"+K8Str::jsStr(pkt.get(n));
  }
  foreach (const QString &n, pkt.vars()) {
    if (needComma) pktS += ","; else needComma = true;
    pktS += K8Str::jsStr(n)+":"+K8Str::jsStr(pkt.get(n));
  }
  pktS += "},rvars:{";
  needComma = false;
  foreach (const QString &n, pkt.routeVars()) {
    if (needComma) pktS += ","; else needComma = true;
    pktS += K8Str::jsStr(n)+":true";
  }
  pktS += "},"+K8Str::jsStr("body")+":"+K8Str::jsStr(pkt.body());
  pktS += "};";
  //
  //qDebug() << pktS;
  //
  webCList->page()->mainFrame()->evaluateJavaScript(pktS);
  webChat->page()->mainFrame()->evaluateJavaScript(pktS);
}


void ChatForm::onPacket (const PsycPacket &pkt) {
  //Q_UNUSED(pkt)
  //qDebug() << ">>>GOT PACKET\n:" << pkt.toString() << "========================";
  QString m(pkt.method());
  if (m == "_status_storage") {
    parseStoredAppData(pkt);
    return;
  }
  if (m == "_echo_request_store") return;

  if (!mChatUser && toBoolOpt("/debug/dumptochat", false)) {
    QString s(pkt.toString());
    if (s.rightRef(3) == "\n.\n") s.chop(3);
    s = K8Str::deHTMLize(s, K8Str::deHTMLnone);
    PsycEntity u("psyc://0.0.0.0/~psycman");
    addMessageToChat(u, s.trimmed(), "", QDateTime::currentDateTime());
  }

  if (mConForm) {
    mConForm->addPacket(pkt, false);
  }

  // call WebKit JS code which can handle this packet
  // WARNING: there is no way to change/cancel packet for now!
  //          signal/slot thingy is generally bad with such thing
  //          maybe i should change the packet processing so
  //          PsycProto first emits signal 'packetpre' or smth.,
  //          and handlers can change/cancel packet then?
  packetToJSAPI(pkt);
  runCListJS("onPSYCPacket();");
  runChatJS("onPSYCPacket();");
}


void ChatForm::onPacketSent (const PsycPacket &pkt) {
  if (!mConForm) return;
  QString m(pkt.method());
  if (m == "_request_store" || m == "_request_retrieve") return;
  mConForm->addPacket(pkt, true);
}


void ChatForm::onNeedPassword () {
  mProto->sendPassword(mPassword);
}


void ChatForm::onInvalidPassword () {
  //QMessageBox::warning(this, tr("PSYC ERROR"), tr("ERROR: invalid password!"));
  if (mPopupActive) showErrorPopup(tr("ERROR: invalid password!"));
  setStatus(PsycProto::Offline);
}


void ChatForm::onLoginComplete () {
  mWasSentTo.clear();
  if (mServerStore) requestStoredAppData();
  PsycProto::Status st = mMyStatus;

  if (mMyStatus != PsycProto::Offline) {
    mProto->setStatusText(toStringOpt("/status/text"));
    mProto->setStatus(PsycProto::Offline, mProto->statusText(), mMyMood);
    mProto->setStatus(PsycProto::Here, mProto->statusText(), mMyMood);
    //mProto->setStatus(mMyStatus, mProto->statusText(), mMyMood);
  }
  setStatus(st);

  //mProto->setStatus(st, mProto->statusText(), mProto->mood());
  //show(mProto->nick(), mProto->uni(), "connected!");
  mProto->requestPeerList();
  //storeAppData();
}


void ChatForm::onMessage (const PsycEntity &place, const PsycEntity &user, const QDateTime &time,
  const QString &text, const QString &action, const QString &forPopup)
{
  if (place.isValid() && toBoolOpt("/ignored", false, user.uni().toLower())) return;

  bool privMsg;
  QString act(K8Str::deHTMLize(action, K8Str::deHTMLnone));
  QString textD(K8Str::deHTMLize(text, K8Str::deHTMLnone));
  QString actX(act);
  if (actX.startsWith(" DONTTOUCH|")) {
    actX.remove(0, 11);
    actX = actX.simplified();
  }

  PsycEntity histDest;
  if (place.isValid()) {
    histDest = place;
    privMsg = false;
  } else {
    histDest = user;
    privMsg = true;
  }

  // ignore unauthorized users
  if (privMsg && !toBoolOpt("/contactlist/unauthorized/accept", false, histDest.uni())) {
    PsycContact *cx = findContact(histDest);
    if (!cx) return; // alas, new contact -- it can't be authorized
    if (!cx->authorized()) return; // get off!
  }

  bool toCurrChat = (mChatUser && mChatUser->entity()->sameUNI(histDest.uni()));
  PsycContact *cc = 0;
  if (!toCurrChat) cc = addContact(histDest); // either place or person

#ifdef USE_OTR
  if (privMsg) {
    QString realText(otrGotMessage(user.uni(), text));
    if (realText.isNull()) return; // internal OTR message
    textD = K8Str::deHTMLize(realText, K8Str::deHTMLnone);
  }
#endif

  bool showPopup;
  UnreadFlag urf;
  if (toCurrChat) {
    // to chat
    //showPopup = false;
    urf = MessageRead;
    runChatJS("isChatAtBottom();");
    showPopup = toBoolOpt("/popup/active", true);
    addMessageToChat(user, textD, act, time);
    if (isMinimized() || !isActiveWindow() || !isVisible()) {
      mUnreadInChat = true;
      mFMan->newMessage(mChatUser->uni());
    } else {
      mUnreadInChat = false;
      if (showPopup && toIntOptCU("/popup/mode", 1) != 0) {
        showPopup = !mWasAtBottom;
      } else showPopup = false;
      //qDebug() << showPopup << mWasAtBottom << toIntOptCU("/popup/mode", 1);
      mFMan->noMessages(mChatUser->uni());
    }
    redrawContact(mChatUser);
  } else {
    // to unread
    showPopup = toBoolOpt("/popup/active", true);
    urf = MessageUnread;
    if (cc) {
      cc->addMessage(user, textD, act, time);
      mFMan->newMessage(cc->uni());
      redrawContact(cc);
    }
  }
  addMessageToHistory(histDest, user, textD, act, time, urf);
  // show popup
  if (showPopup &&
      toBoolOpt("/popup/active", true, histDest.uni()) &&
      toBoolOpt("/popup/active", true, user.uni())
     ) {
    QString ptext(forPopup);
    if (ptext.isEmpty()) ptext = textD;
    if (privMsg) showPrivatePopup(user, ptext, actX);
    else showPublicPopup(place, user, ptext, actX);
  }
  // update tray
  checkBlink();
}


void ChatForm::onMessageEcho (const PsycEntity &place, const PsycEntity &user, const PsycEntity &target, const QDateTime &time,
  const QString &text, const QString &action, const QString &tag)
{
  Q_UNUSED(user)
  Q_UNUSED(tag)
  //if (!mChatUser) return; //FIXME: should remember this?
  if (toBoolOptCU("/chat/messaging/immediateout", false)) return; // already done
  PsycEntity me(mProto->uni());
  PsycEntity tgt;
  QString realText(text);
  if (place.isValid()) {
    if (!mWasSentTo.contains(place.uni().toLower())) return; // nothing was sent there, history echo
    addMessageToHistory(place, me, realText, action, time, MessageRead);
    tgt = place;
  } else {
    //qDebug() << "echo from" << user.uni() << "tag:" << tag;
/*
    PsycContact *cc = findContact(user.uni());
    if (!cc) dlogf("shit!");
    else {
      if (!cc->isOTRActive()) dlogf("fuck!");
      if (cc && cc->isOTRActive()) return; // OTR echo
    }
*/
    if (text.startsWith("?OTR")) return; // OTR message
    if (!mWasSentTo.contains(target.uni().toLower())) return; // nothing was sent there, history echo
/*
#ifdef USE_OTR
    realText = otrGotMessage(user.uni(), realText);
    if (realText.isNull()) return; // internal OTR message
#endif
*/
    addMessageToHistory(target, me, realText, action, time, MessageRead);
    tgt = target;
  }
  if (mChatUser && *mChatUser->entity() == tgt) addMessageToChat(me, realText, action, time);
}


void ChatForm::onHistoryMessage (const PsycEntity &place, const PsycEntity &user, const QDateTime &time,
  const QString &text, const QString &action)
{
  //qDebug() << "history" << place.uni() << user.uni();
  bool addMsg = true;
  if (place.isValid()) {
    PsycEntity hus(place.isValid()?place:user);
    HistoryFile hf(settingsHistoryFile(mAccName, hus.uni()), mProto->uni());
    if (hf.open(HistoryFile::Read)) {
      // walk history backward and look for this message
      HistoryMessage msg;
      int wlk, cnt;
      for (wlk = 64, cnt = hf.count()-1; wlk > 0 && cnt >= 0; --cnt, --wlk) {
        if (!hf.read(cnt, msg)) break;
        if (!msg.valid) continue;
        if (!user.sameUNI(msg.uni)) continue;
        if (msg.action == action && msg.text == text) { addMsg = false; break; }
      }
      hf.close();
    }
  } else {
    addMsg = !text.startsWith("?OTR");
  }
  if (!addMsg) return; // duplicate found
  onMessage(place, user, time, text, action);
}


void ChatForm::onUserStatusChanged (const PsycEntity &user) {
  //qDebug() << "status:" << user.uni() << user.availDegree();
  bool showPopup = true;
  PsycContact *cc = addContact(user.uni());
  if (cc) {
    if (user.isXMPP() && !user.channel().isEmpty()) cc->entity()->setChannel(user.channel());
    if (user.status() != PsycProto::Offline) {
      if (!cc->authorized()) {
        cc->setAuthorized(true);
        //saveOneContact(cc);
      }
      QString st(user.statusText().simplified());
      QString ost(cc->statusText());
      if (ost.isEmpty() || !st.isEmpty()) cc->setStatusText(st);
      cc->setLastSeen(QDateTime::currentDateTime());
      saveOneContact(cc);
    } else {
      if (!cc->isPlace()) otrDisconnect(cc->uni());
    }
    //PsycProto::Status ost = cc->status();
    cc->setStatus(user.status());
    //showPopup = (ost != user.status());
  }
  if (showPopup) showStatusPopup(user);
  if (cc) {
    mFMan->newStatus(cc->uni(), cc->status());
    redrawContact(cc);
  }
}


void ChatForm::onSelfStatusChanged () {
  if (!mProto->isLoggedIn()) return;
  if ((int)(mProto->status()) < 2) {
    if (mProto->status() == PsycProto::Offline) {
      showErrorPopup(tr("STATUS CHANGED: offline"));
    }
    if (toBoolOpt("/status/trayicon")) mTrayIcon->setIcon(statusIcon(PsycProto::Offline));
    return;
  }
  //qDebug() << "selfstatus:" << (int)(mProto->availDegree());
  setStatus(mProto->status(), false);
}


void ChatForm::onNotificationType (const PsycEntity &dest) {
  //qDebug() << "nt:" << dest.uni() << "t:" << dest.notification();
  PsycContact *cc = addContact(dest.uni());
  PsycProto::Notification nt = dest.notification();
  bool oldAuth = cc->authorized();
  if (nt == PsycProto::Offered || nt == PsycProto::Pending) cc->setAuthorized(false);
  else cc->setAuthorized(true);
  if (oldAuth != cc->authorized()) saveOneContact(cc);
  redrawContact(cc);
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::onPlaceEntered (const PsycEntity &place) {
  //qDebug() << "entered:" << place.uni();
  bool isNew;
  PsycContact *cc = addContact(place, "", &isNew);
  if (!cc) return;
  cc->setStatus(PsycProto::Here);
  if (isNew) redrawContact(cc); // allow cl to add it
  if (cc->hasPerson(mProto->uni())) return;
  //addContact(mProto->uni(), "", 0, TempContact);
  cc->addPerson(mProto->uni());
  QString js("meEnters("+K8Str::jsStr(place.uni())+");");

  runCListJS(js);
  redrawContact(cc);
}


void ChatForm::onPlaceLeaved (const PsycEntity &place) {
  //qDebug() << "leaved:" << place.uni();
  bool isNew;
  PsycContact *cc = addContact(place, "", &isNew);
  if (!cc) return;
  //qDebug() << " place found/created";
  if (isNew) qDebug() << "  actually, created";
  cc->setStatus(PsycProto::Offline);
  if (isNew) redrawContact(cc); // allow cl to add it
  //qDebug() << " we are there";
  //addContact(mProto->uni(), "", 0, TempContact);
  if (cc->hasPerson(mProto->uni())) cc->delPerson(mProto->uni());
  //qDebug() << " and now we not";

  runCListJS("meLeaves("+K8Str::jsStr(place.uni())+");");
  //qDebug() << " meLeaves()";
  redrawContact(cc);
}


void ChatForm::onUserEntersPlace (const PsycEntity &place, const PsycEntity &who) {
  //qDebug() << "user entered to:" << place.uni() << who.uni();
  bool isNew;
  PsycContact *cc = addContact(place, "", &isNew);
  if (!cc) return;
  if (isNew) redrawContact(cc);
  addContact(who.uni(), "", 0, TempContact);
  if (!cc->hasPerson(who.uni())) cc->addPerson(who.uni());
  cc->setStatus(PsycProto::Here);

  runCListJS("userEnters("+K8Str::jsStr(place.uni())+","+K8Str::jsStr(who.uni())+");");
  //qDebug() << "userEnters()";
  redrawContact(cc);
}


void ChatForm::onUserLeavesPlace (const PsycEntity &place, const PsycEntity &who) {
  //qDebug() << "user parted from:" << place.uni() << who.uni();
  bool isNew;
  PsycContact *cc = addContact(place, "", &isNew);
  if (!cc) return;
  if (isNew) redrawContact(cc);
  addContact(who.uni(), "", 0, TempContact);
  if (cc->hasPerson(who.uni())) cc->delPerson(who.uni());

  runCListJS("userLeaves("+K8Str::jsStr(place.uni())+","+K8Str::jsStr(who.uni())+")");
  //qDebug() << "userLeaves()";
  redrawContact(cc);
}


void ChatForm::onEnterCustomPlace (const PsycEntity &place) {
  if (!place.isValid() || !place.isPlace()) return;
  if (!mProto->isLoggedIn()) return;
  mProto->enterPlace(place);
}


void ChatForm::on_action_join_triggered (bool) {
  JoinWin *jw = new JoinWin(this);
  connect(jw, SIGNAL(enter(const PsycEntity &)), this, SLOT(onEnterCustomPlace(const PsycEntity &)));
  PsycEntity me(mProto->uni());
  jw->leNick->setText(me.nick());
  jw->leHost->setText(me.host()); //FIXME: port!
  jw->leRoom->setFocus();
  jw->show();
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::onScratchpadUpdated (const QString &spUrl) {
  optPrint("scratchpad updated: "+urlStr(spUrl));
}


void ChatForm::onPlaceWebExam (const PsycEntity &place, const PsycPacket &pkt) {
  if (!toBoolOptCU("/web/showexam")) return;
  QString txt(
    "place <b>"+K8Str::escapeStr(place.uni())+"</b> examined from web<br />"
    "page: "+urlStr(pkt.get("_web_page"))+"<br />"
    "on: "+urlStr(pkt.get("_web_on"))+"<br />"
    "from: "+urlStr(pkt.get("_web_from"))+"<br />"
    "referrer: "+urlStr(pkt.get("_web_referrer"))+"<br />"
    "browser: "+K8Str::escapeStr(pkt.get("_web_browser"))+"<br />"
    "host: "+K8Str::escapeStr(pkt.get("_host_name"))+"<br />"
  );
  optPrint(txt);
}


void ChatForm::onPlaceNews (const PsycEntity &place, const QString &channel, const QString &title, const QString &url) {
  QString msg, tmp;
  if (!channel.isEmpty()) msg = QString("<b>%1</b>: ").arg(channel);
  msg.append(K8Str::escapeStr(title));
  tmp = msg;
  if (!url.isEmpty()) {
    msg.append("\n<a href=\x22");
    msg.append(url);
    msg.append("\x22>link</a>");
  }
  /*
  tmp = pkt.get("_project").trimmed();
  if (!tmp.isEmpty()) msg += "project: <b>"+K8Str::escapeStr(tmp)+"</b><br />";
  msg += "hash: "+K8Str::escapeStr(pkt.get("_hash_commit_long", pkt.get("_hash_commit")));
  msg += "<br />commiter: <b>"+K8Str::escapeStr(pkt.get("_nick_editor"))+"</b>";
  tmp = pkt.get("_mailto_editor").trimmed();
  if (!tmp.isEmpty()) msg += " (mailto: <i>"+K8Str::escapeStr(tmp)+"</i>)";
  msg += "<br /><i>"+K8Str::escapeStr(pkt.get("_comment"))+"</i>";
  */
  tmp.prepend('\x1');
  onMessage(place, place, QDateTime::currentDateTime(), msg, " DONTTOUCH|", tmp);
}


void ChatForm::onNoticeUpdate (const PsycEntity &place, const PsycPacket &pkt) {
  // update message
  QString msg, tmp;
  tmp = pkt.get("_project").trimmed();
  if (!tmp.isEmpty()) msg += "project: <b>"+K8Str::escapeStr(tmp)+"</b><br />";
  msg += "hash: "+K8Str::escapeStr(pkt.get("_hash_commit_long", pkt.get("_hash_commit")));
  msg += "<br />commiter: <b>"+K8Str::escapeStr(pkt.get("_nick_editor"))+"</b>";
  tmp = pkt.get("_mailto_editor").trimmed();
  if (!tmp.isEmpty()) msg += " (mailto: <i>"+K8Str::escapeStr(tmp)+"</i>)";
  msg += "<br /><i>"+K8Str::escapeStr(pkt.get("_comment"))+"</i>";

  onMessage(place, place, QDateTime::currentDateTime(), msg, " DONTTOUCH|", "\x1"+msg);
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::onLinkClicked (const QUrl &url) {
  //qDebug() << "link:" << url << "scheme:" << url.scheme().toLower();
  QString sc(url.scheme().toLower());
  if (sc != "http" && sc != "https" && sc != "ftp") return;
  //qDebug() << " " << toIntOptCU("/web/url/opentype") << toStringOptCU("/web/url/browser");
  switch (toIntOptCU("/web/url/opentype")) {
    case 0: return; // don't open urls
    case 1: QDesktopServices::openUrl(url); return; // use system settings
    default: ;
  }
  // use our settings
  QString cmd(toStringOptCU("/web/url/browser"));
  if (cmd.isEmpty()) return;
  QString addr(url.toString());
  //qDebug() << "url:" << addr;
  QByteArray ba(addr.toUtf8());
  QString surl = "\"";
  char buf[16];
  for (int f = 0; f < ba.size(); f++) {
    unsigned char ch = (unsigned char)ba[f];
    /*if (ch < '\x2a' || ch == '\x3b' || ch == '\x3c' || ch == '\x3e' || ch == '\x3f' ||
        (ch >= '\x5b' && ch <= '\x5d') || ch == '\x7b' || ch == '\x7d' || ch == '\x60') {*/
    switch (ch) {
      case '!': case '`': case '$': case '"': case '\\': surl.append('\\'); break;
      case '\t': surl.append("\\t"); break;
      case '\r': surl.append("\\r"); break;
      case '\n': surl.append("\\n"); break;
      default: ;
    }
    if (ch > ' ' && ch <= '\x7e') surl.append(ch);
    else if (ch == ' ' || ch >= '\x7e') { sprintf(buf, "%%%02X", ch); surl.append(buf); }
  }
  surl.append('"');
  cmd.replace("\"%s\"", "%s");
  if (cmd.indexOf("%s") == -1) cmd += " "+surl; else cmd.replace("%s", surl);
  //qDebug() << "cmd" << cmd;
  QProcess browser;
  browser.startDetached(cmd);
}


///////////////////////////////////////////////////////////////////////////////
// options
void ChatForm::clearOptionList () {
  foreach (Option *o, mOptionList) delete o;
  mOptionList.clear();
}


void ChatForm::clearBindings () {
  mComboList->clear();
  mEditComboList->clear();
  mCurCombos->clear();
}


void ChatForm::loadOptionList () {
  clearOptionList();
  bool ok;
  QString fs(loadFile(":/data/options.txt", &ok));
  if (!ok) return;
  QStringList fl(fs.split('\n'));
  for (int f = 0; f < fl.count(); ) {
    QString s(fl[f++].trimmed());
    if (s.isEmpty() || s[0] == '#' || s[0] == ';') continue;
    // collect help
    QString help;
    for (; f < fl.count(); f++) {
      QString s(fl[f].trimmed());
      if (s.isEmpty()) break;
      if (s[0] == '#' || s[0] == ';') continue;
      if (!help.isEmpty()) help += '\n';
      help += s;
    }
    Option *o = new Option(s, help);
    if (o->isValid()) {
      QString nl(o->name.toLower());
      if (mOptionList.contains(nl)) delete mOptionList[nl];
      mOptionList[nl] = o;
      //qDebug() << "option found:" << o->name;
    } else delete o;
  }
}


bool ChatForm::loadBindings (const QString &fname) {
  clearBindings();
  bool ok;
  QString fs(loadFile(fname, &ok));
  if (!ok) return false;
  QStringList fl(fs.split('\n'));
  for (int f = 0; f < fl.count(); ) {
    QString s(fl[f++].trimmed());
    if (s.isEmpty() || s[0] == ';') continue;
    if (s.length() < 3 || s[0] != '(' || s[s.length()-1] != ')') continue;
    s = s.mid(1, s.length()-2);
    QStringList cl(K8Str::parseCmdLine(s));
    if (cl.length() < 1 || cl.length() > 3) continue;
    QString arg; if (cl.length() > 2) arg = cl[2];
    if (cl[0] == "global-key-bind") {
      mComboList->bind(cl[1], arg);
    } else if (cl[0] == "editor-key-bind") {
      mEditComboList->bind(cl[1], arg);
    }
  }
  mCurCombos->clear();
  mCurCombos->appendFrom(mComboList);
  if (edChat->hasFocus()) {
    mPrevFocusWasEdit = true;
    mCurCombos->appendFrom(mEditComboList);
  } else mPrevFocusWasEdit = false;
  return true;
}


void ChatForm::setAccDefaultOptions (K8SDB *db) {
  foreach (Option *o, mOptionList) {
    if (o->scope == Option::OnlyLocal) continue;
    if (db->type(o->dbName) == o->type) continue; // option already set
    db->set(o->dbName, o->type, o->defVal);
    qDebug() << "default for" << o->name << "set to" << o->defVal;
  }
}


void ChatForm::optPrint (const QString &s) {
  runChatJS("addSysMsg("+K8Str::jsStr(s)+");");
}


QStringList ChatForm::findByUniPart (const QString &up) const {
  QStringList res;
  if (!up.isEmpty()) {
    qDebug() << "findByUniPart:" << up;
    foreach (PsycContact *cc, mContactList) {
      QString v(cc->verbatim());
      qDebug() << "v:" << v << "up:" << up << "contains:" << v.contains(up, Qt::CaseInsensitive);
      if (v.contains(up, Qt::CaseInsensitive)) {
        if (v.compare(up, Qt::CaseInsensitive) == 0) {
          res.clear();
          res << cc->uni();
          return res;
        }
        res << cc->uni();
      }
    }
  }
  return res;
}


PsycContact *ChatForm::findByUniOne (QString up) const {
  if (up.isEmpty()) return 0;
  bool isPlace = up[0] == '@';
  if (isPlace) {
    up.remove(0, 1);
    if (up.isEmpty()) return 0;
  }
  QString fc; int lpos = 100000; PsycContact *res = 0;
  foreach (PsycContact *cc, mContactList) {
    if (cc->isPlace() != isPlace) continue;
    QString v(cc->verbatim());
    if (!v.compare(up, Qt::CaseInsensitive)) return cc; // exact!
    int i = v.indexOf(up, 0, Qt::CaseInsensitive);
    if (i < 0 || i > lpos) continue;
    if (i < lpos) {
      // absolutely better match
      res = cc;
      fc = v;
      lpos = i;
      continue;
    }
    // possible better match (the longest is better)
    if (!fc.isEmpty() && fc.length() > v.length()) continue;
    res = cc;
    fc = v;
    lpos = i;
  }
  return res;
}


void ChatForm::onFloatyDblClicked (int x, int y, const QString &uni) {
  Q_UNUSED(x)
  Q_UNUSED(y)
  if (isMinimized() || !isVisible()) {
    showNormal();
    activateWindow();
  }
  if (!mChatUser || mChatUser->uni().compare(uni, Qt::CaseInsensitive)) startChatWith(uni);
}


void ChatForm::refreshBindings () {
  mCurCombos->clear();
  mCurCombos->appendFrom(mComboList);
  if (edChat->hasFocus()) mCurCombos->appendFrom(mEditComboList);
}


bool ChatForm::processKeyCombo (QKeyEvent *keyEvent) {
  QString cmb;
  bool chatInFocus = edChat->hasFocus();
  if (chatInFocus != mPrevFocusWasEdit) {
    mComboList->reset();
    mEditComboList->reset();
    mPrevFocusWasEdit = chatInFocus;
    refreshBindings();
  }
  //
  cmb = mCurCombos->process(keyEvent);
  if (!cmb.isEmpty()) {
    if (cmb[0] != ' ') {
      //qDebug() << "combo:" << cmb << mEditComboList->lastCombo(), mEditComboList->lastComboText();
      mEngBinds->dispatchBinding(cmb, mCurCombos->lastCombo(), mCurCombos->lastComboText());
    }
    return true;
  }
  return false;
}


///////////////////////////////////////////////////////////////////////////////
void ChatForm::onOptChanged__tray_visible (const QString &uni) {
  Q_UNUSED(uni)
  if (toBoolOpt("/tray/visible")) {
    if (!mTrayIcon->isVisible()) {
      mTrayIcon->show();
      checkBlink();
    }
  } else mTrayIcon->hide();
}


void ChatForm::onOptChanged__tray_blinktime (const QString &uni) {
  Q_UNUSED(uni)
  int ot = mBlinkTimer->interval();
  int tt = toIntOpt("/tray/blinktime", 500);
  if (ot == tt) return;
  if (mBlinkTimer->isActive()) {
    if (tt < 1) {
      if (mTrayMsgNoteActive) {
        mBlinkTimer->stop();
        mTrayIcon->setIcon(QIcon(MSG_TRAY_ICON));
        mBlinkMsg = true;
      }
    } else {
      mBlinkTimer->stop();
      mBlinkTimer->start(tt>10?tt:10);
    }
  } else {
    if (mTrayMsgNoteActive) mBlinkTimer->start(tt>10?tt:10);
  }
}


void ChatForm::onOptChanged__tray_notify (const QString &uni) {
  Q_UNUSED(uni)
  checkBlink();
}


void ChatForm::onOptChanged__contactlist_onright (const QString &uni) {
  Q_UNUSED(uni)
  if (toBoolOpt("/contactlist/onright") == mCListOnRight) return;
  mCListOnRight = toBoolOpt("/contactlist/onright");
  if (mCListOnRight) {
    // was on the left
    splitter0->addWidget(layoutWidget0);
  } else {
    // was on the right
    splitter0->addWidget(splitter1);
  }
  mSDB->set("/chatwindow/splitters/0", splitter0->saveState());
  mSDB->set("/chatwindow/splitters/1", splitter1->saveState());
}


void ChatForm::onOptChanged__contactlist_showalways (const QString &uni) {
  PsycContact *cc = findContact(uni);
  if (!cc) return;
  redrawContact(cc);
}


void ChatForm::switchTab (bool done) {
  if (mTabList.size() < 2) {
    mTabSwitchNo = -1;
    return;
  }
  if (done) {
    if (mTabSwitchNo > 0) {
      // rearrange
      PsycContact *cc = mTabList[mTabSwitchNo];
      mTabList.removeAt(mTabSwitchNo);
      mTabList.insert(0, cc);
    }
    mTabSwitchNo = -1;
    return;
  }
  // not done
  if (mTabSwitchNo < 0) mTabSwitchNo = 0;
  int no = (mTabSwitchNo+1)%mTabList.count();
  mTabSwitchNo = no;
  //qDebug() << "switchTab:" << done << no;
  startChatWith(mTabList[no]->uni(), true);
}


void ChatForm::switchToUnread () {
  foreach (PsycContact *cc, mContactList) {
    if (cc->isTemp()) continue;
    if (!cc->messageCount()) continue;
    if (toBoolOpt("/contactlist/skipunreadcycle", false, cc->uni())) continue;
    startChatWith(cc->uni());
    break;
  }
}


void ChatForm::clearTabHistory (void) {
  if (mTabList.count() > 0) {
    PsycContact *cc = mTabList[0];
    //
    mTabList.clear();
    mTabList << cc;
  }
}
