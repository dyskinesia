/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QDialog>
#include <QFile>
#include <QScrollBar>
#include <QTextStream>

#include "k8strutils.h"
#include "psycpkt.h"
#include "psycproto.h"
#include "winutils.h"

#include "conwin.h"


ConsoleForm::ConsoleForm (QWidget *parent) : QDialog(parent) {
  setupUi(this);
  setAttribute(Qt::WA_QuitOnClose, false);
  setAttribute(Qt::WA_DeleteOnClose, false);
  //centerWidget(this);
  //logStr("=========================================\n", "");
}


ConsoleForm::~ConsoleForm () {
}


void ConsoleForm::on_btClear_clicked (void) {
  tbLog->clear();
}


void ConsoleForm::logStr (const QString &pfx, const QString &str) {
  QString spathStr = QCoreApplication::applicationDirPath();
  if (spathStr.isEmpty()) spathStr += "./";
  else if (spathStr[spathStr.length()-1] != '/' && spathStr[spathStr.length()-1] != '\\') spathStr += "/";

  QFile fl(spathStr+"packets.log");
  if (!fl.open(QIODevice::Append)) return;
  QTextStream st(&fl);
  st.setCodec("UTF-8");
  if (!pfx.isEmpty()) st << pfx;
  if (!str.isEmpty()) st << str;
}


void ConsoleForm::addPacket (const PsycPacket &pkt, bool isOutgoing) {
  if (!cbActive->isChecked()) return;

  QString str(pkt.toString(true)); // raw dump

  QString pfx;
  if (isOutgoing) pfx = "<<< OUTGOING >>>\n"; else pfx = ">>> INCOMING <<<\n";
  logStr(pfx, str);

  QTextCursor cr(tbLog->textCursor());
  cr.movePosition(QTextCursor::End, QTextCursor::MoveAnchor);
  cr.clearSelection();
  //tbLog->moveCursor(QTextCursor::End);
  QString s(K8Str::escapeStr(str, true));
  s.replace("\n", "<br />\n");
  //quint64 cursorPosition = tbLog->textCursor().position();
  if (!isOutgoing) cr.insertHtml("<font color='#00ff00'>"+s+"</font>");
  else cr.insertHtml(s);
  cr.insertHtml("\n<hr>\n");
  cr.clearSelection();
  //tbLog->moveCursor(QTextCursor::End);
  tbLog->verticalScrollBar()->setValue(tbLog->verticalScrollBar()->maximum());
  //tbLog->verticalScrollBar()->setEnabled(true);
}
