/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef ENG_COMMANDS_H
#define ENG_COMMANDS_H

#include <QContextMenuEvent>
#include <QDesktopServices>
#include <QHash>
#include <QLabel>
#include <QList>
#include <QMap>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSet>
#include <QShortcut>
#include <QString>
#include <QSystemTrayIcon>
#include <QWebFrame>
#include <QWidget>
#include <QWidgetAction>

#include "chatform.h"
#include "keycmb.h"


///////////////////////////////////////////////////////////////////////////////
class EngineCommands : public QObject {
  Q_OBJECT

public:
  EngineCommands (ChatForm *aChat);
  ~EngineCommands ();

  void doCommand (const QString &cmd);
  bool dispatchCommand (const QString &cmd, const QStringList &args);

private slots:
  void concmd_help (const QString &cmd, const QStringList &args);
  void concmd_quit (const QString &cmd, const QStringList &args);
  void concmd_about (const QString &cmd, const QStringList &args);
  void concmd_optlist (const QString &cmd, const QStringList &args);
  void concmd_optreload (const QString &cmd, const QStringList &args);
  void concmd_clistreload (const QString &cmd, const QStringList &args);
  void concmd_ignore (const QString &cmd, const QStringList &args);
  void concmd_unignore (const QString &cmd, const QStringList &args);
  void concmd_ignorelist (const QString &cmd, const QStringList &args);
  void concmd_ilist (const QString &cmd, const QStringList &args);
  void concmd_otr (const QString &cmd, const QStringList &args);
  void concmd_nootr (const QString &cmd, const QStringList &args);
  void concmd_otrsmp (const QString &cmd, const QStringList &args);
  void concmd_otrfinger (const QString &cmd, const QStringList &args);
  void concmd_float (const QString &cmd, const QStringList &args);
  void concmd_unfloat (const QString &cmd, const QStringList &args);
  void concmd_hide (const QString &cmd, const QStringList &args);
  void concmd_show (const QString &cmd, const QStringList &args);
  void concmd_forcechat (const QString &cmd, const QStringList &args);
  void concmd_chat (const QString &cmd, const QStringList &args);
  void concmd_handle (const QString &cmd, const QStringList &args);
  void concmd_find (const QString &cmd, const QStringList &args);
  void concmd_bind (const QString &cmd, const QStringList &args);
  void concmd_ebind (const QString &cmd, const QStringList &args);
  void concmd_savebinds (const QString &cmd, const QStringList &args);
  void concmd_loadbinds (const QString &cmd, const QStringList &args);
  void concmd_showbinds (const QString &cmd, const QStringList &args);
  void concmd_historyexport (const QString &cmd, const QStringList &args);
  void concmd_historyimport (const QString &cmd, const QStringList &args);
  void concmd_historyclear (const QString &cmd, const QStringList &args);
  void concmd_cleartabhistory (const QString &cmd, const QStringList &args);

private:
  ChatForm *mChat;
};


#endif
