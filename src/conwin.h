/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef CONWIN_H
#define CONWIN_H

#include <QDialog>

class PsycPacket;


#include "ui_console.h"
class ConsoleForm : public QDialog, public Ui_consoleForm {
  Q_OBJECT

public:
  ConsoleForm (QWidget *parent = 0);
  ~ConsoleForm ();

public slots:
  void addPacket (const PsycPacket &pkt, bool isOutgoing);

private slots:
  void on_btClear_clicked (void);

private:
  void logStr (const QString &pfx, const QString &str);

public:
  QString mUni;
};


#endif
