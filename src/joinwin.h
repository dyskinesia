/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef JOINWIN_H
#define JOINWIN_H

#include <QDialog>


class PsycEntity;


#include "ui_joinwin.h"
class JoinWin : public QDialog, public Ui_joinWin {
  Q_OBJECT

public:
  JoinWin (QWidget *parent = 0);
  ~JoinWin ();

private slots:
  void on_btEnter_clicked (void);
  void on_leNick_textChanged (void);
  void on_leHost_textChanged (void);
  void on_leRoom_textChanged (void);

signals:
  void enter (const PsycEntity &room);
};


#endif
