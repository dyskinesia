/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QByteArray>
#include <QNetworkRequest>
#include <QTextCodec>
#include <QString>
#include <QUiLoader>
#include <QWebFrame>
#include <QWebHitTestResult>
#include <QWebPage>
#include <QWebView>

#include "k8webview.h"


extern bool debugOutput;


///////////////////////////////////////////////////////////////////////////////
static void printQStr (const QString &str) {
  //QTextCodec *codec = QTextCodec::codecForName("koi8-r");
  QTextCodec *codec = QTextCodec::codecForLocale();
  QByteArray ba;
  if (codec) ba = codec->fromUnicode(str); else ba = str.toLatin1();
  const char *s = ba.constData();
  int sz = ba.size();
  while (sz > 0) {
    int len = strlen(s);
    if (debugOutput) fprintf(stdout, "%s", s);
    sz -= len;
    if (sz > 0) {
      if (debugOutput) fprintf(stdout, "\n ");
      sz--;
      s += len+1;
    }
  }
  if (debugOutput) fprintf(stdout, "\n");
}


K8WebPage::K8WebPage (QObject *parent) : QWebPage(parent)
{
}


void K8WebPage::javaScriptConsoleMessage (const QString &message, int lineNumber, const QString &sourceID) {
  QString us = mainFrame()->url().toString();
  if (debugOutput) fprintf(stderr, "----------------------------\n");
  printQStr(QString("line: %1; sid: %2").arg(QString::number(lineNumber)).arg(sourceID));
  printQStr("msg: "+message);
  if (debugOutput) fflush(stderr);
}


QObject *K8WebPage::createPlugin (const QString &classId, const QUrl &url, const QStringList &paramNames, const QStringList &paramValues) {
  Q_UNUSED(url);
  Q_UNUSED(paramNames);
  Q_UNUSED(paramValues);
  //qDebug() << "plugin class:" << classId;
  QUiLoader loader;
  return loader.createWidget(classId, view());
}


///////////////////////////////////////////////////////////////////////////////
K8WebView::K8WebView (QWidget *parent) : QWebView(parent), mPage(new K8WebPage(this)), mAllowContextMenu(false) {
  setPage(mPage);

  QWebSettings *ws = page()->settings();
  //ws->setFontFamily(QWebSettings::SerifFont, "Arial");
  //ws->setFontSize(QWebSettings::SerifFont, 12);
  page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);

  ws->setAttribute(QWebSettings::AutoLoadImages, true);
  ws->setAttribute(QWebSettings::JavascriptEnabled, true);
  ws->setAttribute(QWebSettings::JavaEnabled, false);
  ws->setAttribute(QWebSettings::PluginsEnabled, false);
  ws->setAttribute(QWebSettings::PrivateBrowsingEnabled, true);
  ws->setAttribute(QWebSettings::JavascriptCanOpenWindows, false);
  ws->setAttribute(QWebSettings::JavascriptCanAccessClipboard, true);
  ws->setAttribute(QWebSettings::DeveloperExtrasEnabled, false);
  ws->setAttribute(QWebSettings::LinksIncludedInFocusChain, false);
#if QT_VERSION >= 0x040500
  ws->setAttribute(QWebSettings::ZoomTextOnly, false);
  ws->setAttribute(QWebSettings::OfflineStorageDatabaseEnabled, false);
  ws->setAttribute(QWebSettings::OfflineWebApplicationCacheEnabled, false);
  ws->setAttribute(QWebSettings::LocalStorageDatabaseEnabled, false);
  ws->setAttribute(QWebSettings::SiteSpecificQuirksEnabled, false);
#endif
  connect(mPage, SIGNAL(downloadRequested(const QNetworkRequest &)), this, SLOT(onDownloadRequested(const QNetworkRequest &)));
}


K8WebView::~K8WebView () {
}


void K8WebView::contextMenuEvent (QContextMenuEvent *event) {
  if (mAllowContextMenu) {
    QWebView::contextMenuEvent(event);
  } else {
    QWebHitTestResult r = page()->mainFrame()->hitTestContent(event->pos());
    emit webContextMenu(r, event->pos());
  }
}


void K8WebView::onDownloadRequested (const QNetworkRequest &request) {
  qDebug() << "dlrq:" << request.url();
}
