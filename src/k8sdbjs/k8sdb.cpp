/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include "string.h"

#include <QByteArray>
#include <QDateTime>
#include <QDir>

#include "k8sdb.h"


///////////////////////////////////////////////////////////////////////////////
K8SDB::K8SDB (const QString &aPathName, CreateMode cMode) : mMode(cMode), mTypeOverwrite(true), mIsOpen(false) {
  QString s(aPathName);
  s.replace('\\', "/");
  mFileName = s;

  QFile fdb(s);
  if (cMode == WipeAlways || (!fdb.exists() && cMode == OpenAlways)) {
    fdb.remove();

    int li0 = s.lastIndexOf('/');
    if (li0 > 0) {
      s.truncate(li0);
      if (s != ".") { QDir d(s); d.mkpath(s); }
    }

    if (fdb.open(QIODevice::ReadWrite | QIODevice::Truncate)) fdb.close();
  }

  readJSON();
  mIsOpen = fdb.exists();
}


K8SDB::~K8SDB () {
  writeJSON();
}


bool K8SDB::hasKey (const QString &name) const {
  if (name.isEmpty()) return false;
  return mKeys.contains(name);
}


K8SDB::Type K8SDB::type (const QString &name) const {
  if (name.isEmpty() || !mKeys.contains(name)) return Invalid;
  switch (mKeys[name].type()) {
    case QVariant::ByteArray: return ByteArray;
    case QVariant::Bool: return Boolean;
    case QVariant::String: case QVariant::Char: return String;
    case QVariant::KeySequence: return KeySequence;
    case QVariant::Int: case QVariant::LongLong:
    case QVariant::UInt: case QVariant::ULongLong:
      return Integer;
    default: ;
  }
  return Invalid;
}


QStringList K8SDB::keys () const {
  return mKeys.keys();
}


QStringList K8SDB::keysPfx (const QString &pfx) const {
  if (pfx.isEmpty()) return keys();
  QStringList res(mKeys.keys());
  for (int f = 0; f < res.count(); ) {
    if (res.at(f).startsWith(pfx)) f++; else res.removeAt(f);
  }
  return res;
}


///////////////////////////////////////////////////////////////////////////////
bool K8SDB::set (const QString &name, bool v) {
  if (name.isEmpty()) return false;
  if (mKeys.contains(name) && type(name) != Boolean) {
    if (!mTypeOverwrite) return false;
    remove(name);
  }
  mKeys[name] = v;
  return writeJSON();
}


bool K8SDB::set (const QString &name, qint32 v) {
  if (name.isEmpty()) return false;
  if (mKeys.contains(name) && type(name) != Integer) {
    if (!mTypeOverwrite) return false;
    remove(name);
  }
  mKeys[name] = v;
  return writeJSON();
}


bool K8SDB::set (const QString &name, const QString &v) {
  if (name.isEmpty()) return false;
  if (mKeys.contains(name) && type(name) != String) {
    if (!mTypeOverwrite) return false;
    remove(name);
  }
  mKeys[name] = v;
  return writeJSON();
}


bool K8SDB::set (const QString &name, const QByteArray &v) {
  if (name.isEmpty()) return false;
  if (mKeys.contains(name) && type(name) != ByteArray) {
    if (!mTypeOverwrite) return false;
    remove(name);
  }
  mKeys[name] = v;
  return writeJSON();
}


bool K8SDB::set (const QString &name, const QKeySequence &v) {
  if (name.isEmpty()) return false;
  if (mKeys.contains(name) && type(name) != KeySequence) {
    if (!mTypeOverwrite) return false;
    remove(name);
  }
  mKeys[name] = v;
  return writeJSON();
}


bool K8SDB::set (const QString &name, Type type, const QString &v) {
  if (name.isEmpty()) return false;
  QString s1(v.trimmed().toLower());
  bool ok = false;
  int i;
  QKeySequence sq;
  QByteArray ba;
  switch (type) {
    case Boolean:
      return set(name, !s1.isEmpty() &&
        s1 != "0" && s1 != "no" && s1 != "off" && s1 != "ona" && s1 != "false" && s1 != "n" && s1 != "o");
    case Integer:
      i = s1.toInt(&ok);
      if (ok) return set(name, i);
      return false;
    case String:
      return set(name, v);
    case ByteArray:
      ba = v.toUtf8();
      return set(name, ba);
    case KeySequence:
      sq = QKeySequence::fromString(s1);
      return set(name, sq);
    default: ;
  }
  return false;
}


///////////////////////////////////////////////////////////////////////////////
bool K8SDB::toBool (const QString &name, bool *ok) {
  if (type(name) != Boolean) {
    if (ok) *ok = false;
    return false;
  }
  if (ok) *ok = true;
  return mKeys[name].toBool();
}


qint32 K8SDB::toInt (const QString &name, bool *ok) {
  if (type(name) != Integer) {
    if (ok) *ok = false;
    return 0;
  }
  if (ok) *ok = true;
  return mKeys[name].toInt();
}


QString K8SDB::toString (const QString &name, bool *ok) {
  if (type(name) != String) {
    if (ok) *ok = false;
    return QString();
  }
  if (ok) *ok = true;
  return mKeys[name].toString();
}


QByteArray K8SDB::toByteArray (const QString &name, bool *ok) {
  if (type(name) != ByteArray) {
    if (ok) *ok = false;
    return QByteArray();
  }
  if (ok) *ok = true;
  return mKeys[name].toByteArray();
}


QKeySequence K8SDB::toKeySequence (const QString &name, bool *ok) {
  if (type(name) != KeySequence) {
    if (ok) *ok = false;
    return QKeySequence();
  }
  if (ok) *ok = true;
  return mKeys[name].value<QKeySequence>();
}


QString K8SDB::asString (const QString &name, bool *ok) {
  if (type(name) == Invalid) {
    if (ok) *ok = false;
    return QString();
  }
  if (ok) *ok = true;
  QVariant v = mKeys[name];
  switch (type(name)) {
    case Invalid: if (ok) *ok = false; return QString();
    case Boolean: return QString(v.toBool() ? "true" : "false");
    case Integer: return QString::number(v.toInt());
    case ByteArray: return QString(v.toByteArray());
    case String: return v.toString();
    case KeySequence: {
      QKeySequence ks = v.value<QKeySequence>();
      return ks.toString();
      }
    default: ;
  }
  return QString();
}


bool K8SDB::remove (const QString &name) {
  if (name.isEmpty() || !mKeys.contains(name)) return false;
  mKeys.remove(name);
  return writeJSON();
}


bool K8SDB::removeAll (const QString &name) {
  if (name.isEmpty()) return false;
  QStringList keys(keysPfx(name));
  if (keys.size() < 1) return false;
  foreach (const QString &key, keys) mKeys.remove(key);
  return writeJSON();
}


const char *K8SDB::typeName (K8SDB::Type type) {
  switch (type) {
    case K8SDB::Invalid: return "invalid";
    case K8SDB::Boolean: return "boolean";
    case K8SDB::Integer: return "integer";
    case K8SDB::String: return "string";
    case K8SDB::KeySequence: return "keyseq";
    case K8SDB::ByteArray: return "bytearray";
    default: ;
  }
  return "unknown";
}


static bool generatorCB (void *udata, QString &err, QByteArray &res, const QVariant &val, int indent) {
  Q_UNUSED(udata)
  Q_UNUSED(indent)
  switch (val.type()) {
    case QVariant::ByteArray: {
      QByteArray ba(val.toByteArray());
      QByteArray a85;
      K8ASCII85::encode(a85, ba);
      res += K8JSON::quote(QString::fromAscii(a85));
      } break;
    case QVariant::KeySequence: {
      QKeySequence ks = val.value<QKeySequence>();
      res += K8JSON::quote(ks.toString());
      } break;
    default:
      err = QString("invalid variant type: %1").arg(val.typeName());
      return false;
  }
  return true;
}


bool K8SDB::writeJSON () {
  QVariantMap dbase;
  foreach (const QString &key, mKeys.keys()) {
    QVariantMap item;
    item["type"] = typeName(type(key));
    item["value"] = mKeys[key];
    dbase[key] = item;
  }

  QByteArray json;
  QString err;
  if (!K8JSON::generateExCB(0, generatorCB, err, json, dbase)) {
    qDebug() << "ERROR!" << err;
    return false;
  }

  QFile fo(mFileName);
  if (!fo.open(QIODevice::WriteOnly)) return false;
  fo.write("// Dyskinesia database. DO NOT EDIT!\n");
  fo.write(json);
  fo.close();
  return true;
}


bool K8SDB::readJSON () {
  mKeys.clear();
  QByteArray ba;
  QFile fl(mFileName);
  if (!fl.open(QIODevice::ReadOnly)) return false;
  int len = fl.size();
  const uchar *fmap = (const uchar *)(fl.map(0, fl.size()));
  if (!fmap) {
    ba = fl.readAll();
    fl.close();
    fmap = (const uchar *)(ba.constData());
  }
  const uchar *sj = K8JSON::skipBlanks(fmap, &len);
  if (!sj) return false;
  uchar qch = *sj;
  if (qch != '{') return false;
  // read records
  QVariant val;
  sj = K8JSON::parseRecord(val, sj, &len);
  if (!sj) return false;
  if (val.type() != QVariant::Map) return false;
  // process key list
  QVariantMap kl = val.toMap();
  foreach (const QString &key, kl.keys()) {
    bool setIt = true;
    QVariant kk = kl[key];
    if (kk.type() != QVariant::Map) continue;
    QVariantMap m = kk.toMap();
    //qDebug() << key;
    QString type = m["type"].toString();
    QVariant val = m["value"];
    switch (val.type()) {
      case QVariant::ByteArray:
        if (type != "bytearray") setIt = false;
        break;
      case QVariant::Bool:
        if (type != "boolean") setIt = false;
        break;
      case QVariant::String:
        if (type != "string") {
          if (type == "bytearray") {
            QByteArray ba;
            if (!K8ASCII85::decode(ba, val.toString().toAscii())) setIt = false;
            val = ba;
          } else if (type == "keyseq") {
            QKeySequence k(val.toString());
            val = k;
          } else setIt = false;
        }
        break;
      case QVariant::KeySequence:
        if (type != "keyseq") setIt = false;
        break;
      case QVariant::Int:
      case QVariant::LongLong:
      case QVariant::UInt:
      case QVariant::ULongLong:
        if (type != "integer") setIt = false;
        break;
      case QVariant::Char:
        if (type != "string") setIt = false; else val = val.toString();
        break;
      default: setIt = false; break;
    }
    if (setIt) mKeys[key] = val;
  }
  return true;
}
