/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef CHATJSAPI_H
#define CHATJSAPI_H

#include <QIcon>
#include <QHash>
#include <QString>
#include <QVariant>


class ChatForm;
class QWebFrame;


///////////////////////////////////////////////////////////////////////////////
class ChatJSAPI : public QObject {
  Q_OBJECT
  //Q_PROPERTY(bool isMainFrame READ isMainFrame);

public:
  ChatJSAPI (QWebFrame *frame, ChatForm *cform);
  ~ChatJSAPI ();

public slots:
  void print (const QString &str);
  void printLn (const QString &str);
  void refresh ();
  void scrollToBottom ();

  void startChat (const QString &uni);

  bool hasOption (const QString &name);
  bool hasOption (const QString &name, const QString &uni);
  bool removeOption (const QString &name);
  bool removeOption (const QString &name, const QString &uni);
  QVariant getOption (const QString &name);
  QVariant getOption (const QString &name, const QString &uni);
  bool setOption (const QString &name, const QVariant &v);
  bool setOption (const QString &name, const QString &uni, const QVariant &v);

  void requestAuth (const QString &uni);
  void cancelAuth (const QString &uni);
  void sendAuth (const QString &uni);

  void deleteContact (const QString &uni);

  void enterPlace (const QString &uni);
  void leavePlace (const QString &uni);

  QVariant test (const QVariant &v);

  QVariant contactInfo (const QString &uni);
  void setContactVerbatim (const QString &uni, const QString &v);
  void setContactHidden (const QString &uni, bool v);

  void setStatus (int newStatus);
  int status ();

  void markAsRead (const QString &aUNI);

/*
  // 'text' should be properly escaped!
  void showPopupMessage (const QString &uni, const QString &text);
  void showPopupSystem (const QString &text);
  void showPopupError (const QString &text);
  void showPopupStatus (const QString &uni, int status);

  void sendMessage (const QString &uni, const QString &text);
  void sendMessage (const QString &uni, const QString &text, const QString &action);

  void addMessageToHistory (const QString &dest, const QString &src, const QString &text,
    const QString &action, uint unixtime, bool isUnread);
  bool addMessageToChat (const QString &from, const QString &msg, const QString &action, uint unixtime);
*/

  void selectedText (const QString &s);

  QString editorText ();
  void setEditorText (const QString &s);
  void insertEditorText (const QString &s);

  int editorCurPos ();
  void setEditorCurPos (int pos);

  void setClipboardText (const QString &s);

  void setWasBottom (bool bflag);

  // OTR
  void otrStart (const QString &aUNI);
  void otrEnd (const QString &aUNI);
  void otrSetTrust (const QString &aUNI, bool tflag);
  void otrForget (const QString &aUNI);
  void otrInitiateSMP (const QString &aUNI, const QString &secret, const QString &question);

  void doPSYC (const QString &cmdNargs);
  void doPSYC (const QString &cmdNargs, const QString &destUni);

  // this will return an UNI or ""
  QString chattingWith ();
  // this can be done in JS code, but who really cares?
  bool isInPlace ();

  void invite (const QString &uni, const QString &place);

  bool addMessage (const QString &placeUni, const QString &userUni, uint time, const QString &text,
    const QString &action);

  bool addMessageEx (const QString &placeUni, const QString &userUni, uint time, const QString &text,
    const QString &action, const QString &popupMsg);

private:
  QWebFrame *mFrame;
  ChatForm *mForm;
};


#endif
