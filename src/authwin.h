/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef AUTHWIN_H
#define AUTHWIN_H

#include <QDialog>

#include "psycproto.h"
#include "winutils.h"


#include "ui_authwin.h"
class AuthWindow : public QDialog, public Ui_authwin {
  Q_OBJECT

public:
  AuthWindow (const PsycEntity &aEntity, QWidget *parent=0) : QDialog(parent), mEntity(aEntity) {
    setupUi(this);
    setAttribute(Qt::WA_QuitOnClose, false);
    setAttribute(Qt::WA_DeleteOnClose, true);
    centerWidget(this);
  }
  ~AuthWindow () {}


private slots:
  void on_btOk_clicked (bool) {
    emit granted(mEntity, edText->toPlainText());
  }


public:
  PsycEntity mEntity;

signals:
  void granted (const PsycEntity &entity, const QString &text);
};


#endif
