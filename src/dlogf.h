/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef DLOGF_H
#define DLOGF_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdarg.h>


//#define NO_DEBUG_LOG

/* defaults: write to file; write to stderr */

#ifndef NO_DEBUG_LOG
/*
 * close log file, shutdown logger
 */
void dlogfDeinit (void);

/*
 * set output file name for log
 *  NULL: no file output
 */
void dlogfSetFile (const char *fname);
void dlogfSetStdErr (int doIt);

void dlogf (const char *fmt, ...) __attribute__((format(printf, 1, 2)));
void dlogfVA (const char *fmt, va_list ap);
#else
#define dlogf(...)
#define dlogfVA(...)
#define dlogfDeinit()
#define dlogfSetFile(fname)
#define dlogfSetStdErr(doIt)
#endif


#ifdef __cplusplus
}
#endif


#endif
