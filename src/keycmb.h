/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef KEYCMB_H
#define KEYCMB_H

#include <QHash>
#include <QKeyEvent>
#include <QList>
#include <QMap>
#include <QSet>
#include <QShortcut>
#include <QString>
#include <QWidget>


///////////////////////////////////////////////////////////////////////////////
class KeyComboList {
public:
  KeyComboList ();
  ~KeyComboList ();

  void clear ();

  bool bind (const QString &kn, const QString &cmd);

  void reset ();

  /*
   * result:
   * "": not a combo;
   * " ": in the middle
   * " ?": bad combo-binding, error
   * else: binding
   */
  QString process (QKeyEvent *keyev);

  const QString &lastCombo () const { return mLastCombo; }
  const QString &lastComboText () const { return mLastComboText; }

  QStringList toString () const;

  bool isProcessing () const;

  void appendFrom (KeyComboList *kl);

public:
  static QString event2Kbd (QKeyEvent *keyev);
  static QString normalizeKbd (const QString &kbd, bool strictShift=false);

private:
  QHash<QString, QString> mBinds;
  QStringList mCombos;
  // current combo
  QString mCurCombo;
  QString mCurComboText;
  // last combo
  QString mLastCombo;
  QString mLastComboText;
};


#endif
