/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef PSYCCONTACT_H
#define PSYCCONTACT_H

#include <QDateTime>
#include <QString>
#include <QIcon>
#include <QMenu>

#include "psycproto.h"


class MyMessage {
public:
  MyMessage (const PsycEntity &aUser, const QString &aText, const QString &aAction, const QDateTime &aTime);

  QString text;
  QString action;
  QDateTime time;
  PsycEntity user;
};


class PsycContact : public QObject {
  Q_OBJECT

public:
  PsycContact (const QString &uni, bool aTemp=false, QObject *parent=0);
  ~PsycContact ();

  inline PsycEntity *entity () const { return mEntity; }
  inline bool isPlace () const { return mIsPlace; }

  inline const QString &verbatim () const { return mEntity->verbatim(); }
  inline void setVerbatim (const QString &handle) { mEntity->setVerbatim(handle); }

  inline const QString &nick () const { return mEntity->nick(); }
  inline const QString &uni () const { return mEntity->uni(); }

  inline bool isTemp () const { return mTemp; }
  inline void setTemp (bool v) { mTemp = v; }

  inline bool isHidden () const { return mHidden; }
  inline void setHidden (bool v) { mHidden = v; }

  inline bool isOTRActive () const { return mIsOTRActive; }
  inline void setOTRActive (bool v) { mIsOTRActive = v; }

  inline bool isOTRVerified () const { return mIsOTRVerified; }
  inline void setOTRVerified (bool v) { mIsOTRVerified = v; }

  inline PsycProto::Status status () const { return mEntity->status(); }
  inline void setStatus (PsycProto::Status st) { mEntity->setStatus(st); }

  inline QString statusText () const { return mEntity->statusText(); }
  inline void setStatusText (const QString &v) { mEntity->setStatusText(v); }

  inline PsycProto::Mood mood () const { return mEntity->mood(); }
  inline void setMood (PsycProto::Mood v) { mEntity->setMood(v); }

  inline uint lastSeenUnix () const { return mLastSeen; }
  inline QDateTime lastSeen () const { return QDateTime::fromTime_t(mLastSeen); }
  inline void setLastSeen (const uint &dt) { mLastSeen = dt; }
  inline void setLastSeen (const QDateTime &dt) { mLastSeen = dt.toTime_t(); }

  inline bool authorized () const { return mAuthorized; }
  inline void setAuthorized (bool auth) { mAuthorized = auth; }

  inline const QString &groupName () const { return mGroupName; }
  inline void setGroupName (const QString &gn) { mGroupName = gn; }

  void addMessage (const PsycEntity &aUser, const QString &aText, const QString &aAction, const QDateTime &aTime);
  inline int messageCount () const { return mMsgList.count(); }
  void clearMessages ();
  QList<MyMessage *> messages ();

  // person list
  inline int personCount () const { return mPersonList.count(); }
  inline void clearPersons () { return mPersonList.clear(); }
  void addPerson (const QString &uni);
  void delPerson (const QString &uni);
  bool hasPerson (const QString &uni) const;
  QStringList persons () const;
  QString person (int idx) const;

protected:
  PsycEntity *mEntity;
  uint mLastSeen;
  bool mAuthorized;
  QString mGroupName;
  QList<MyMessage *> mMsgList;
  QStringList mPersonList;
  bool mIsPlace;
  bool mHidden;
  // temporary contacts will not be shown in clist and will not be stored in db
  // i need 'em to make api.contactInfo() works
  bool mTemp;
  bool mIsOTRActive;
  bool mIsOTRVerified;
};


#endif
