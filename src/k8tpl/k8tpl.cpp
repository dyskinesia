/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QFile>
#include <QStack>
#include <QTextStream>

#include "k8tpl.h"


class K8TemplatePart {
public:
  enum Type {
    Text,
    Var,
    OptValue,
    Cond,
    EndCond
  };

  K8TemplatePart (Type tp, const QString &val) {
    type = tp;
    value = val;
    isSet = true;
    //qDebug() << "*PART:" << tp << val;
  }

public:
  Type type;
  QString value;
  bool isSet; // for condition; value is var name
};


///////////////////////////////////////////////////////////////////////////////
K8Template::K8Template () : mIsEmpty(true), mTpl(QString()), mResultReady(false), mResult(QString()) {
}


K8Template::~K8Template () {
  clear();
}


void K8Template::clearVars (void) {
  mVars.clear();
}


void K8Template::clear (void) {
  // clear template
  mIsEmpty = true;
  foreach (K8TemplatePart *p, mParsed) delete p;
  mParsed.clear();
  mOptions.clear();
  mVars.clear();
  mResultReady = false;
  mResult.clear();
  mTpl.clear();
}


void K8Template::parseTemplate (void) {
  // clear template
  QString curStr(mTpl);
  clear();
  mTpl = curStr; curStr.clear();
  // start parsing
  int tLen = mTpl.length(), cPos = 0;
  while (cPos < tLen) {
    QChar ch(mTpl[cPos++]);
    //qDebug() << ch << cPos;
    if (ch == '<' && cPos < tLen) {
      // maybe our tag
      ch = mTpl[cPos];
      if (ch == '/' && cPos+1 < tLen && mTpl[cPos+1] == '?') {
        // end of condition
        //qDebug() << "ENDCOND";
        // flush the string
        if (!curStr.isEmpty()) {
          K8TemplatePart *p = new K8TemplatePart(K8TemplatePart::Text, curStr);
          mParsed << p;
          curStr.clear();
        }
        K8TemplatePart *p = new K8TemplatePart(K8TemplatePart::EndCond, QString());
        mParsed << p;
        while (cPos < tLen && mTpl[cPos] != '>') cPos++; cPos++;
      } else if (ch == '?') {
        // condition tag
        //qDebug() << "COND";
        // flush the string
        if (!curStr.isEmpty()) {
          K8TemplatePart *p = new K8TemplatePart(K8TemplatePart::Text, curStr);
          mParsed << p;
          curStr.clear();
        }
        bool isNot = false;
        cPos++; while (cPos < tLen && mTpl[cPos].isSpace()) cPos++;
        if (cPos >= tLen) break;
        // check for 'not'
        if (mTpl[cPos] == '!') {
          isNot = true;
          cPos++; while (cPos < tLen && mTpl[cPos].isSpace()) cPos++;
          if (cPos >= tLen) break;
        }
        // get var name
        int sPos = cPos;
        while (cPos < tLen && !mTpl[cPos].isSpace() && mTpl[cPos] != '>') cPos++;
        K8TemplatePart *p = new K8TemplatePart(K8TemplatePart::Cond, mTpl.mid(sPos, cPos-sPos));
        p->isSet = !isNot;
        mParsed << p;
        while (cPos < tLen && mTpl[cPos] != '>') cPos++; cPos++;
        //qDebug() << " " << p->isSet << p->value;
      } else if (ch == '=') {
        // option tag
        //qDebug() << "OPT";
        // flush the string
        if (!curStr.isEmpty()) {
          K8TemplatePart *p = new K8TemplatePart(K8TemplatePart::Text, curStr);
          mParsed << p;
          curStr.clear();
        }
        cPos++; while (cPos < tLen && mTpl[cPos].isSpace()) cPos++;
        if (cPos >= tLen) break;
        // get opt name
        int sPos = cPos;
        while (cPos < tLen && !mTpl[cPos].isSpace() && mTpl[cPos] != '>') cPos++;
        curStr = mTpl.mid(sPos, cPos-sPos);
        // skip spaces
        while (cPos < tLen && mTpl[cPos].isSpace()) cPos++;
        // get opt value
        sPos = cPos;
        while (cPos < tLen && mTpl[cPos] != '>') cPos++;
        QString v(mTpl.mid(sPos, cPos-sPos).trimmed());
        // norm chars
        v.replace("&space;", " ");
        v.replace("&lt;", "<");
        v.replace("&gt;", ">");
        v.replace("&amp;", "&");
        mOptions[curStr] = v;
        //qDebug() << " " << curStr << v;
        curStr.clear();
        cPos++;
      } else {
        // just a char
        curStr += '<';
        //qDebug() << "ch0:" << '<';
      }
    } else if (ch == '%') {
      // maybe var
      if (cPos >= tLen) curStr += '%';
      else if (mTpl[cPos] == '%') { curStr += '%'; cPos++; }
      else if (!mTpl[cPos].isLetter() && mTpl[cPos] != '=') { curStr += '%'; }
      else if (mTpl[cPos] == '=' && (cPos+1 >= tLen || !mTpl[cPos+1].isLetter())) { curStr += '%'; }
      else {
        // yes! it's a var or an option!
        //qDebug() << "VAR/OPTION";
        // flush the string
        if (!curStr.isEmpty()) {
          K8TemplatePart *p = new K8TemplatePart(K8TemplatePart::Text, curStr);
          mParsed << p;
          curStr.clear();
        }
        // collect var/option name
        int sPos = cPos;
        while (cPos < tLen && mTpl[cPos] != '%') cPos++;
        K8TemplatePart *p;
        if (mTpl[sPos] != '=') {
          p = new K8TemplatePart(K8TemplatePart::Var, mTpl.mid(sPos, cPos-sPos));
        } else {
          p = new K8TemplatePart(K8TemplatePart::OptValue, mTpl.mid(sPos+1, cPos-sPos-1));
        }
        mParsed << p;
        cPos++; // skip possible '%'
        //qDebug() << " " << p->value;
      }
    } else {
      // just a char
      curStr += ch;
      //qDebug() << "ch1:" << ch;
    }
  }
  // flush the string
  if (!curStr.isEmpty()) {
    K8TemplatePart *p = new K8TemplatePart(K8TemplatePart::Text, curStr);
    mParsed << p;
  }
  mIsEmpty = false;
}


void K8Template::setTpl (const QString &newTpl) {
  mTpl = newTpl;
  parseTemplate();
}


bool K8Template::loadFromFile (const QString &fileName) {
  QFile file(fileName);
  if (!file.open(QIODevice::ReadOnly)) return false;
  QString res;
  QTextStream stream;
  stream.setDevice(&file);
  stream.setCodec("UTF-8");
  res = stream.readAll();
  file.close();
  setTpl(res);
  return true;
}


const QString &K8Template::result (void) {
  if (mResultReady) return mResult;
  mResult.clear();
  QStack<K8TemplatePart *> condStack;
  K8TemplatePart *cond = 0; // NOT on the stack
  QString s;
  foreach (K8TemplatePart *p, mParsed) {
    switch (p->type) {
      case K8TemplatePart::Text:
        s = p->value;
        break;
      case K8TemplatePart::Var:
        s = var(p->value);
        break;
      case K8TemplatePart::OptValue:
        s = option(p->value);
        break;
      case K8TemplatePart::Cond:
        s.clear();
        if (cond) condStack.push(cond);
        cond = p;
        break;
      case K8TemplatePart::EndCond:
        s.clear();
        if (condStack.size()) cond = condStack.pop(); else cond = 0;
        break;
      default: ;
    }
    if (!s.isEmpty()) {
      bool add = true;
      if (cond) {
        bool here = mVars.contains(cond->value);
        if (here) here = !mVars[cond->value].isEmpty();
        if (cond->isSet != here) add = false;
      }
      if (add) mResult += s;
    }
  }
  mResultReady = true;
  mResult = mResult.trimmed();
  return mResult;
}
