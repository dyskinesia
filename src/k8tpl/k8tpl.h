/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef K8TPL_H
#define K8TPL_H


#include <QHash>
#include <QList>
#include <QString>
#include <QStringList>


/*
<b>%nick%</b>
  substitute `var`
  write "%%" to emit single percent char

<?comment><u>%comment%</u></?>
  emit "<u>%comment%</u>" only if `var` 'comment' is set (not empty)

<?!comment><s>no comments</s></?>
  emit "<u>%comment%</u>" only if `var` 'comment' is not set (empty)

<=quote0 #ffff00>
  set `option` value
  option value WILL NOT be reset on clearVars()!

<font color="%=quote%">
  substitute `option` value

`var`/`option` name must start with the letter
*/


class K8TemplatePart;

class K8Template {
public:
  K8Template ();
  ~K8Template ();

  bool loadFromFile (const QString &fileName);

  void setTpl (const QString &newTpl);
  inline const QString &tpl () const { return mTpl; }

  void clear (void);
  void clearVars (void);

  inline bool isEmpty () const { return mIsEmpty; }

  inline QString var (const QString &name) const {
    if (mVars.contains(name)) return mVars[name];
    return QString();
  }
  inline void setVar (const QString &name, const QString &value) { mVars[name] = value; mResultReady = false; }

  QString option (const QString &name, const QString &def=QString()) const {
    if (mOptions.contains(name)) return mOptions[name];
    return def;
  }
  inline void setOption (const QString &name, const QString &value) { mOptions[name] = value; mResultReady = false; }

  const QString &result (void);

private:
  void parseTemplate (void);

private:
  bool mIsEmpty;
  QString mTpl; // original template
  QList<K8TemplatePart *> mParsed;
  QHash<QString, QString> mOptions;
  QHash<QString, QString> mVars;
  bool mResultReady;
  QString mResult;
};


#endif
