/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef KWEBVIEW_H
#define KWEBVIEW_H

#include <QContextMenuEvent>
#include <QWidget>
#include <QWebView>


class K8WebPage : public QWebPage {
  Q_OBJECT

public:
  K8WebPage (QObject *parent = 0);

protected:
  QObject *createPlugin (const QString &classId, const QUrl &url, const QStringList &paramNames, const QStringList &paramValues);
/*
  bool acceptNavigationRequest (QWebFrame *frame, const QNetworkRequest &request, NavigationType type);
  QWebPage *createWindow (QWebPage::WebWindowType type);
*/
  void javaScriptConsoleMessage (const QString &message, int lineNumber, const QString &sourceID);
/*
  void javaScriptAlert (QWebFrame *frame, const QString &msg);
  bool javaScriptConfirm (QWebFrame *frame, const QString &msg);
  bool javaScriptPrompt (QWebFrame *frame, const QString &msg, const QString &defaultValue, QString *result);
*/
};


class K8WebView : public QWebView {
  Q_OBJECT

public:
  K8WebView (QWidget *parent = 0);
  ~K8WebView ();

  K8WebPage *webPage () const { return mPage; }

  inline bool allowContextMenu () { return mAllowContextMenu; }
  inline void setAllowContextMenu (bool v) { mAllowContextMenu = v; }

signals:
  void webContextMenu (const QWebHitTestResult &hitRes, const QPoint &pos);

protected slots:
  void onDownloadRequested (const QNetworkRequest &request);

protected:
  void contextMenuEvent (QContextMenuEvent *event);
/*
  QWebView *createWindow (QWebPage::WebWindowType type);
*/
private:
  K8WebPage *mPage;
  bool mAllowContextMenu;
};


#endif
