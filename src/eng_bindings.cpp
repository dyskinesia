/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * we are the Borg.
 */
#include <QDebug>

#include <QInputDialog>

#include "eng_bindings.h"

#include "eng_commands.h"
#include "k8strutils.h"
#include "psyccontact.h"


///////////////////////////////////////////////////////////////////////////////
EngineBindings::EngineBindings (ChatForm *aChat) : QObject(aChat), mChat(aChat) {
}


EngineBindings::~EngineBindings () {
}


bool EngineBindings::dispatchBinding (const QString &cmd, const QString &combo, const QString &ctext) {
  if (cmd.isEmpty() || cmd.length() > 64) return false;
  // this is Qt black magic, so don't touch it
  char mtName[128], buf[256];
  // get name
  qDebug() << "binding:" << cmd << "cmb:" << combo;
  QByteArray bt(cmd.toAscii());
  sprintf(mtName, "%s", bt.constData());
  for (int f = 0; mtName[f]; f++) if (mtName[f] == '-') mtName[f] = '_';
  // find handler
  sprintf(buf, "binding_%s(QString,QString,QString)", mtName);
  if (metaObject()->indexOfSlot(buf) >= 0) {
    sprintf(buf, "binding_%s", mtName);
    QMetaObject::invokeMethod(this, buf, Qt::AutoConnection, Q_ARG(QString, cmd), Q_ARG(QString, combo), Q_ARG(QString, ctext));
    return true;
  }
  return defaultBinding(cmd, combo, ctext);
}


///////////////////////////////////////////////////////////////////////////////
bool EngineBindings::defaultBinding (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  QStringList cl(K8Str::parseCmdLine(cmd));
  if (cl.length() < 1) return false;
  if (cl[0] == "edit-insert-text") {
    for (int f = 1; f < cl.length(); f++) mChat->edChat->insertPlainText(cl[f]);
    mChat->edChat->ensureCursorVisible();
    return true;
  }
  if (cl[0] == "do-command") {
    QString s(cmd);
    while (!s.isEmpty() && s[0].isSpace()) s.remove(0, 1);
    if (s.startsWith("do-command")) s.remove(0, 10);
    while (!s.isEmpty() && s[0].isSpace()) s.remove(0, 1);
    if (!s.isEmpty()) s.prepend("/");
    qDebug() << "cmd:" << s;
    mChat->mEngCmds->doCommand(s);
    return true;
  }
  if (cl[0] == "chat-with") {
    if (cl.length() < 2) {
      mChat->startChatWith("");
    } else {
      QStringList lst(mChat->findByUniPart(cl[1]));
      if (lst.size() < 1) mChat->optPrint("can't start chat: nothing's found");
      else if (lst.size() > 1) mChat->optPrint("<b>too many unis found!</b><br />\n"+lst.join("<br />\n"));
      else mChat->startChatWith(lst[0]);
    }
    return true;
  }
  return false;
}


void EngineBindings::binding_window_minimize (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->showMinimized();
}


void EngineBindings::binding_window_hide (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->hide();
}


void EngineBindings::binding_activate_menu_status (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  //mChat->btStatus->defaultAction()->activate(QAction::Trigger);
  mChat->btStatus->click();
}


void EngineBindings::binding_activate_menu_main (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  //mChat->btMainMenu->defaultAction()->activate(QAction::Trigger);
  mChat->btMainMenu->click();
}


void EngineBindings::binding_edit_recode_text (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->translateMessage();
}


void EngineBindings::binding_edit_insert_new_line (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->insertPlainText("\n");
  mChat->edChat->ensureCursorVisible();
}


void EngineBindings::binding_edit_insert_smile_oo (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->insertPlainText("O_O");
  mChat->edChat->ensureCursorVisible();
}


void EngineBindings::binding_edit_insert_smile_oo_weird (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  QString s;
  QChar ch(0x0ca0);
  s = ch;
  s += '_';
  s += ch;
  mChat->edChat->insertPlainText(s);
  mChat->edChat->ensureCursorVisible();
}


void EngineBindings::binding_edit_insert_desu (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  QChar ch(0x3067);
  mChat->edChat->insertPlainText(ch);
  ch = 0x3059;
  mChat->edChat->insertPlainText(ch);
  mChat->edChat->ensureCursorVisible();
}


void EngineBindings::binding_edit_autocomplete (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->chatAutoComplete();
}


void EngineBindings::binding_chat_send (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->doSendMessage();
}


void EngineBindings::binding_editor_activate (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->mUnreadInChat = false;
  if (mChat->mChatUser) mChat->mPopMan->removeById("", mChat->mChatUser->uni().toLower());
  mChat->checkBlink();
  mChat->edChat->setFocus();
}


void EngineBindings::binding_clist_activate (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->webCList->setFocus();
}


void EngineBindings::binding_chat_activate (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->webChat->setFocus();
}


void EngineBindings::binding_set_status_text (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  bool ok;
  QString stt = QInputDialog::getText(mChat, tr("set new status text"), tr("status text:"),
    QLineEdit::Normal, mChat->toStringOpt("/status/text"), &ok);
  if (ok) {
    mChat->setOpt("/status/text", stt);
    mChat->mProto->setStatusText(stt);
    if (!mChat->mProto->isLoggedIn()) return;
    if (mChat->mMyStatus != mChat->mProto->status()) mChat->setStatus(mChat->mMyStatus);
    else {
      if (mChat->mMyStatus != PsycProto::Offline) {
        //FIXME
        mChat->mProto->setStatus(PsycProto::Offline, mChat->mProto->statusText(), mChat->mMyMood);
        mChat->mProto->setStatus(PsycProto::Here, mChat->mProto->statusText(), mChat->mMyMood);
        if (mChat->mMyStatus != PsycProto::Here) mChat->mProto->setStatus(mChat->mMyStatus, mChat->mProto->statusText(), mChat->mMyMood);
      }
    }
  }
}


void EngineBindings::binding_chat_close (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->startChatWith("");
}


void EngineBindings::binding_chat_quote (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->runChatJS("selectedText()");
  if (mChat->mSelText.isEmpty()) return;
  int wwdt = mChat->toIntOptCU("/editor/wrappos", 64);
  QString s(K8Str::wrap(mChat->mSelText, wwdt, mChat->toStringOptCU("/editor/wrappfx", ">")));
  mChat->edChat->insertPlainText(s);
  mChat->edChat->ensureCursorVisible();
  if (mChat->toBoolOptCU("/editor/unselafterquote", false)) mChat->runChatJS("unselectText()");
}


void EngineBindings::binding_go_to_unread (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->switchToUnread();
}


void EngineBindings::binding_action_quit (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_quit->isEnabled()) mChat->action_quit->activate(QAction::Trigger);
}


void EngineBindings::binding_action_edit_account (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_accedit->isEnabled()) mChat->action_accedit->activate(QAction::Trigger);
}


void EngineBindings::binding_action_join_place (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_join->isEnabled()) mChat->action_join->activate(QAction::Trigger);
}


void EngineBindings::binding_action_send_raw_packet (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_sendpacket->isEnabled()) mChat->action_sendpacket->activate(QAction::Trigger);
}


void EngineBindings::binding_action_show_packet_console (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_showconsole->isEnabled()) mChat->action_showconsole->activate(QAction::Trigger);
}


/* rewrite! make one function to set status by number/name! */
void EngineBindings::binding_action_status_offline (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_stoffline->isEnabled()) mChat->action_stoffline->activate(QAction::Trigger);
}


void EngineBindings::binding_action_status_vacation (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_stvacation->isEnabled()) mChat->action_stvacation->activate(QAction::Trigger);
}


void EngineBindings::binding_action_status_away (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_staway->isEnabled()) mChat->action_staway->activate(QAction::Trigger);
}


void EngineBindings::binding_action_status_dnd (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_stdnd->isEnabled()) mChat->action_stdnd->activate(QAction::Trigger);
}


void EngineBindings::binding_action_status_nearby (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_stnear->isEnabled()) mChat->action_stnear->activate(QAction::Trigger);
}


void EngineBindings::binding_action_status_busy (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_stbusy->isEnabled()) mChat->action_stbusy->activate(QAction::Trigger);
}


void EngineBindings::binding_action_status_here (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_sthere->isEnabled()) mChat->action_sthere->activate(QAction::Trigger);
}


void EngineBindings::binding_action_status_ffc (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_stffc->isEnabled()) mChat->action_stffc->activate(QAction::Trigger);
}


void EngineBindings::binding_action_status_realtime (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  if (mChat->action_strtime->isEnabled()) mChat->action_strtime->activate(QAction::Trigger);
}


void EngineBindings::binding_edit_move_home (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::StartOfLine, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_move_end (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::EndOfLine, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_move_top (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_move_bottom (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_move_left (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::Left, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_move_right (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::Right, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_move_up (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::Up, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_move_down (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::Down, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_move_word_left (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::WordLeft, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_move_word_right (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->moveCursor(QTextCursor::WordRight, QTextCursor::MoveAnchor);
}


void EngineBindings::binding_edit_clear (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->clear();
  mChat->edChat->ensureCursorVisible();
}


void EngineBindings::binding_edit_select_all (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->selectAll();
}


void EngineBindings::binding_edit_cut (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->cut();
}


void EngineBindings::binding_edit_copy (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->copy();
}


void EngineBindings::binding_edit_paste (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->paste();
}


void EngineBindings::binding_edit_undo (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->undo();
}


void EngineBindings::binding_edit_redo (const QString &cmd, const QString &combo, const QString &ctext) {
  Q_UNUSED(cmd)
  Q_UNUSED(combo)
  Q_UNUSED(ctext)
  mChat->edChat->redo();
}


/*
void EngineBindings::onShortcutCopy () {
  //qDebug() << "copy shortcut pressed!";
  mChat->runChatJS("selectedText()");
  if (mChat->mSelText.isEmpty()) return;
  QClipboard *cb = QApplication::clipboard();
  cb->setText(mChat->mSelText);
  if (mChat->toBoolOptCU("/editor/unselaftercopy", false)) mChat->runChatJS("unselectText()");
}


void EngineBindings::onShortcutUnsel () {
  //qDebug() << "unsel shortcut pressed!";
  mChat->runChatJS("unselectText()");
}

void EngineBindings::onShortcutmChat->checkBlink () {
  //qDebug() << "close shortcut pressed!";
  mChat->mUnreadInChat = false;
  if (mChat->mChatUser) mChat->mPopMan->removeById("", mChat->mChatUser->uni().toLower());
  mChat->checkBlink();
}
*/
