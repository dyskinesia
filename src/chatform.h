/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef CHATFORM_H
#define CHATFORM_H

#ifdef USE_OTR
extern "C" {
# include <libotr/proto.h>
# include <libotr/context.h>
# include <libotr/userstate.h>
# include <libotr/privkey.h>
# include <libotr/tlv.h>
# include <libotr/message.h>
}
class GenKeyThread;
#endif

#include <QContextMenuEvent>
#include <QDesktopServices>
#include <QHash>
#include <QLabel>
#include <QList>
#include <QMap>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSet>
#include <QShortcut>
#include <QString>
#include <QSystemTrayIcon>
#include <QWebFrame>
#include <QWidget>
#include <QWidgetAction>

#include "psycproto.h"
#include "k8popups.h"
#include "k8sdb.h"

#include "floatwin.h"
#include "keycmb.h"

#include "floatman.h"


#ifdef WIN32
# define MAIN_TRAY_ICON  ":/icons/tray/main.png"
# define MSG_TRAY_ICON  ":/icons/tray/message.png"
#else
# define MAIN_TRAY_ICON  ":/icons/tray/solid/main.png"
# define MSG_TRAY_ICON  ":/icons/tray/solid/message.png"
#endif


class QSettings;
class PsycPacket;
class PsycContact;
class PsycPlace;
class ConsoleForm;
class ChatForm;
class AccWindow;

class EngineCommands;
class EngineBindings;
class FloatersManager;
class ChatJSAPI;


///////////////////////////////////////////////////////////////////////////////
class EnterFilter : public QObject {
  Q_OBJECT

public:
  EnterFilter (QObject *parent=0);
  ~EnterFilter ();


protected:
  bool eventFilter (QObject *obj, QEvent *event);


private:
  ChatForm *mForm;
  bool mCtrlDown;
};


///////////////////////////////////////////////////////////////////////////////
class Option {
public:
  Option (const QString &def, const QString &aHelp);

  inline bool isValid () const { return mValid; }

  enum Scope {
    Both,
    OnlyGlobal,
    OnlyLocal
  };

public:
  QString name;
  QString dbName;
  K8SDB::Type type;
  Scope scope;
  QString defVal;
  QString help;

private:
  bool parseDef (const QString &def);

private:
  bool mValid;
};


///////////////////////////////////////////////////////////////////////////////
class NetworkAccessManager : public QNetworkAccessManager {
  Q_OBJECT

public:
  NetworkAccessManager (QObject *parent = 0);

protected:
  QNetworkReply *createRequest (Operation op, const QNetworkRequest &req, QIODevice *outgoingData = 0);
};


///////////////////////////////////////////////////////////////////////////////
#include "ui_chatform.h"
class ChatForm : public QWidget, public Ui_ChatForm {
  Q_OBJECT
  friend class EnterFilter;
  friend class ChatJSAPI;
  friend class EngineCommands;
  friend class EngineBindings;

public:
  ChatForm (QWidget *parent=0);
  ~ChatForm ();

  // doesn't clear uni/pass
  void clear ();

  const QString &accName () const { return mAccName; }

  PsycProto *proto () const { return mProto; }

  inline void setPassword (const QString &aPassword) { mPassword = aPassword; }
  inline QString password () { return mPassword; }

  void scrollToBottom ();

  enum ACSaveMode { SaveContact, DontSaveContact, TempContact };

  PsycContact *findContact (const QString &uni);
  PsycContact *findContact (const PsycEntity &cont);
  PsycContact *addContact (const PsycEntity &user, const QString &group=QString(""), bool *isNew=0, ACSaveMode mode=SaveContact);
  void redrawContact (PsycContact *cc);
  void deleteContact (PsycContact *cc);

  QIcon statusIcon (PsycProto::Status st);
  void setStatus (PsycProto::Status st, bool doSend=true);
  void setMood (PsycProto::Mood md);

  void startChatWith (const QString &aUNI, bool dontRearrange=false);

  void clearChat ();
  void clearCList ();

  // settings engine
  bool removeOpt (const QString &name, const QString &aUNI=QString(""));
  bool hasKeyOpt (const QString &name, const QString &aUNI=QString("")) const;
  K8SDB::Type typeOpt (const QString &name, const QString &aUNI=QString(""));

  bool setOpt (const QString &name, bool v, const QString &aUNI=QString(""));
  bool setOpt (const QString &name, qint32 v, const QString &aUNI=QString(""));
  bool setOpt (const QString &name, const QString &v, const QString &aUNI=QString(""));
  inline bool setOpt (const QString &name, const char *v, const QString &aUNI=QString("")) { return setOpt(name, QString(v), aUNI); }
  bool setOpt (const QString &name, const QKeySequence &v, const QString &aUNI=QString(""));
  bool setOpt (const QString &name, K8SDB::Type type, const QString &v, const QString &aUNI=QString(""));

  bool toBoolOpt (const QString &name, bool def=false, const QString &aUNI=QString(""));
  qint32 toIntOpt (const QString &name, qint32 def=0, const QString &aUNI=QString(""));
  QString toStringOpt (const QString &name, const QString &def=QString(), const QString &aUNI=QString(""));
  QByteArray toBAOpt (const QString &name, const QByteArray &def=QByteArray(), const QString &aUNI=QString(""));
  QKeySequence toKeySequenceOpt (const QString &name, const QKeySequence &def=QKeySequence(), const QString &aUNI=QString(""));

  QString asStringOpt (const QString &name, const QString &aUNI=QString(""));

  bool removeOptCU (const QString &name);
  bool hasKeyOptCU (const QString &name) const;
  K8SDB::Type typeOptCU (const QString &name);

  bool setOptCU (const QString &name, bool v);
  bool setOptCU (const QString &name, qint32 v);
  bool setOptCU (const QString &name, const QString &v);
  inline bool setOptCU (const QString &name, const char *v) { return setOptCU(name, QString(v)); }
  bool setOptCU (const QString &name, const QKeySequence &v);
  bool setOptCU (const QString &name, K8SDB::Type type, const QString &v);

  bool toBoolOptCU (const QString &name, bool def=false);
  qint32 toIntOptCU (const QString &name, qint32 def=0);
  QString toStringOptCU (const QString &name, const QString &def=QString());
  QByteArray toBAOptCU (const QString &name, const QByteArray &def=QByteArray());
  QKeySequence toKeySequenceOptCU (const QString &name, const QKeySequence &def=QKeySequence());

  QString asStringOptCU (const QString &name);

#ifdef USE_OTR
  char *getOTRKeyFile (void);
  char *getOTRFingerFile (void);
  char *getOTRInsTagFile (void);
  void otrSendMessage (const QString &auni, const QString &txt, const QString &act);
  QString otrGotMessage (const QString &auni, const QString &txt); // can return empty string for "internal" messages
  void genOTRKey (const char *accName, const char *protoName);
#endif
  void otrConnect (const QString &uni);
  void otrDisconnect (const QString &uni);
  void otrSetTrust (const QString &aUNI, bool tflag);
  void otrForget (const QString &aUNI);
  void otrInitiateSMP (const QString &aUNI, const QString &secret, const QString &question);
  const QString otrGetFinger (const QString &aUNI);

  void clearTabHistory (void);

  void recreatePopMan ();

protected:
  void keyPressEvent (QKeyEvent *event);
  void showEvent (QShowEvent *event);
  void closeEvent (QCloseEvent *event);

  void doSendMessage ();

private slots:
  void onUpdateAccountInfo (AccWindow *au, const QString &text);
  void onAccountCreated (AccWindow *au, const QString &text);
  void onAccountDeleted (AccWindow *au, const QString &text);

  void javaScriptWindowObjectCleared ();
  void onWebContextMenuNone (const QWebHitTestResult &, const QPoint &) {}

  void onPopupClick (const QString &type, const QString &id, Qt::MouseButton button);

  void windowDestroyed ();
  void consoleDestroyed ();
  //void splitterMoved (int pos, int index);

  void on_splitter0_splitterMoved (int pos, int index);
  void on_splitter1_splitterMoved (int pos, int index);

  void on_edChat_textChanged (void);

  void on_action_quit_triggered (bool);
  void on_action_accedit_triggered (bool);

  void on_action_showconsole_triggered (bool);
  void on_action_sendpacket_triggered (bool);

  void on_action_join_triggered (bool);

  void onEnterCustomPlace (const PsycEntity &place);

  void onUserPktSend ();

  void on_action_stoffline_triggered (bool);
  void on_action_stvacation_triggered (bool);
  void on_action_staway_triggered (bool);
  void on_action_stdnd_triggered (bool);
  void on_action_stnear_triggered (bool);
  void on_action_stbusy_triggered (bool);
  void on_action_sthere_triggered (bool);
  void on_action_stffc_triggered (bool);
  void on_action_strtime_triggered (bool);

  void on_action_moodunspecified_triggered (bool);
  void on_action_mooddead_triggered (bool);
  void on_action_moodangry_triggered (bool);
  void on_action_moodsad_triggered (bool);
  void on_action_moodmoody_triggered (bool);
  void on_action_moodokay_triggered (bool);
  void on_action_moodgood_triggered (bool);
  void on_action_moodhappy_triggered (bool);
  void on_action_moodbright_triggered (bool);
  void on_action_moodnirvana_triggered (bool);

  void onConnected ();
  void onDisconnected ();
  void onNetError (const QString &errStr);
  void onPacket (const PsycPacket &pkt);
  void onPacketSent (const PsycPacket &pkt);

  void onNeedPassword ();
  void onInvalidPassword ();
  void onLoginComplete ();

  void onUserStatusChanged (const PsycEntity &user);
  void onSelfStatusChanged ();

  void onAuthRequested (const PsycEntity &user);
  void onAuthGranted (const PsycEntity &user);
  void onAuthDenied (const PsycEntity &user);

  void onAuthClicked (const PsycEntity &entity, const QString &text);

  void onNotificationType (const PsycEntity &dest);

  void onPlaceEntered (const PsycEntity &place);
  void onPlaceLeaved (const PsycEntity &place);
  void onUserEntersPlace (const PsycEntity &place, const PsycEntity &who);
  void onUserLeavesPlace (const PsycEntity &place, const PsycEntity &who);

  void onMessage (const PsycEntity &place, const PsycEntity &user, const QDateTime &time, const QString &text,
    const QString &action, const QString &forPopup=QString());
  void onMessageEcho (const PsycEntity &place, const PsycEntity &user, const PsycEntity &target, const QDateTime &time,
    const QString &text, const QString &action, const QString &tag);
  void onHistoryMessage (const PsycEntity &place, const PsycEntity &user, const QDateTime &time, const QString &text,
    const QString &action);

  void onScratchpadUpdated (const QString &spUrl);
  void onPlaceWebExam (const PsycEntity &place, const PsycPacket &pkt);
  void onPlaceNews (const PsycEntity &place, const QString &channel, const QString &title, const QString &url);

  void onNoticeUpdate (const PsycEntity &place, const PsycPacket &pkt);

  void onLoadFinishedChat (bool ok);
  void onLoadFinishedCList (bool ok);

  void onTrayActivate (QSystemTrayIcon::ActivationReason reason);
  void onTrayBlink ();
  //void checkForUpdates ();

  void onLinkClicked (const QUrl &url);

  void onOptChanged__tray_visible (const QString &uni);
  void onOptChanged__tray_blinktime (const QString &uni);
  void onOptChanged__tray_notify (const QString &uni);
  void onOptChanged__contactlist_onright (const QString &uni);
  void onOptChanged__contactlist_showalways (const QString &uni);

  //void onTestSCutActivated ();
  /*
  void onShortcutOSmile ();
  void onShortcutCatoSmile ();
  void onShortcutDesu ();
  void onShortcutRecode ();
  void onShortcutQuote ();
  void onShortcutCopy ();
  void onShortcutUnsel ();
  void onShortcutUnread ();
  void onShortcutCloseChat ();
  void onShortcutActivateEditor ();
  void onShortcutCheckBlink ();
  void onShortcutSetStatus ();
  */

  void onGShortcutShow ();
  void onGShortcutHidePopups ();

  void onFloatyDblClicked (int x, int y, const QString &uni);

private:
  void setAccountShortcuts ();

  void showPopup (const PsycEntity &place, const PsycEntity &from,
    const QString &type, const QString &id, const QString &msg, const QString &action, bool update=false);
  void showPrivatePopup (const PsycEntity &from, const QString &msg, const QString &action);
  void showPublicPopup (const PsycEntity &place, const PsycEntity &from, const QString &msg, const QString &action);
  void showStatusPopup (const PsycEntity &from);
  void showInfoPopup (const QString &msg);
  void showErrorPopup (const QString &msg);

  QIcon LinuxizeTrayIcon (const QIcon &ico);
  void checkBlink ();

  void optionChangedSignal (const QString &name, const QString &uni);

  void optPrint (const QString &s); // html

  void clearOptionList ();
  void clearBindings ();
  void loadOptionList ();
  bool loadBindings (const QString &fname);
  void setAccDefaultOptions (K8SDB *db);

  void accountSetDefaults (K8SDB *db);
  void saveOneContact (PsycContact *cc);
  void loadContactUnread (PsycContact *cc);
  void loadContactList ();
  void loadAutosaved ();
  void loadAccount ();
  void setDefAccount (const QString &name);
  QString defAccount ();

  void storeAppData ();
  void requestStoredAppData ();
  void parseStoredAppData (const PsycPacket &pkt);

  void loadRecodeMap (void);
  QString recodeMsg (const QString &msg);
  void translateMessage ();

  void packetToJSAPI (const PsycPacket &pkt); // export packet to WebKit JS
  void runChatJS (const QString &js);
  void runCListJS (const QString &js);

  enum UnreadFlag { MessageRead, MessageUnread };

  bool addMessageToHistory (const PsycEntity &dest, const PsycEntity &src, const QString &text,
    const QString &action, const QDateTime &time, UnreadFlag isUnread);

  bool addMessageToChat (const PsycEntity &from, const QString &msg, const QString &action, const QDateTime &date);

  void setControlsAccActive (bool accAct);

  void markAsRead (const QString &aUNI);

  void switchTab (bool done);
  void switchToUnread ();

  QStringList findByUniPart (const QString &up) const;
  PsycContact *findByUniOne (QString up) const;

  void deactivateAllOTR ();
  void disconnectAllOTR ();

  void chatAutoComplete ();

  bool processKeyCombo (QKeyEvent *keyEvent);
  void refreshBindings ();

private:
  QString mVerStr;

  ConsoleForm *mConForm;
  QSystemTrayIcon *mTrayIcon;

  QString mAccName;
  PsycProto *mProto;
  QString mPassword;
  QHash<QString, PsycContact *> mContactList;
  QSet<QString> mWasSentTo;

  //QTimer *mSaveTimer;
  PsycProto::Status mMyStatus;
  PsycProto::Mood mMyMood;

  bool mChBlock;

  QHash<QChar, QChar> mRecodeMap;
  EnterFilter *mEFilter;
  PsycContact *mChatUser;
  QString mSelText;

  K8PopupManager *mPopMan;
  K8PopupManager::Position mPopPos;

  bool mCloseWaiting;
  bool mServerStore;
  bool mPopupActive;
  QString mURLStyle;

  ChatJSAPI *mJSAPI;
  ChatJSAPI *mCLJSAPI;

  K8SDB *mSDB;
  QTimer *mBlinkTimer;
  bool mBlinkMsg;
  bool mTrayMsgNoteActive;
  bool mUnreadInChat;

  bool mCListOnRight;
  bool mDoTypo;

  bool mChatLoaded;
  bool mCListLoaded;

  QHash<QString, Option *> mOptionList;
  QList<PsycContact *> mTabList;
  bool mTabDown;
  int mTabSwitchNo;
  bool mWasAtBottom;
  //QSet<QString> mIgnores;

  QString mStartingNickCompletion;
  QString mLatestNickCompletion;

  FloatersManager *mFMan;

  KeyComboList *mCurCombos;
  KeyComboList *mComboList;
  KeyComboList *mEditComboList;
  bool mPrevFocusWasEdit;

  EngineCommands *mEngCmds;
  EngineBindings *mEngBinds;

public:
#ifdef USE_OTR
  void restartOTRTimer (unsigned int interval);
  QTimer *mOTRTimer;
  OtrlUserState mOTRState;
  friend class GenKeyThread;
public slots:
  void onOTRTimer ();
#endif
};


#endif
