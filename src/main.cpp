/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef USE_OTR
extern "C" {
# include <libotr/proto.h>
}
#endif

#include <QtCore>
#include <QDebug>

#include <QApplication>
#include <QFile>
#include <QTcpSocket>
#include <QUrl>

#ifdef USE_STATIC_PICPLUGS
# include <QtPlugin>
 Q_IMPORT_PLUGIN(qgif4)
 Q_IMPORT_PLUGIN(qjpeg4)
 Q_IMPORT_PLUGIN(qmng4)
 Q_IMPORT_PLUGIN(qico4)
 Q_IMPORT_PLUGIN(qsvg4)
 Q_IMPORT_PLUGIN(qtiff4)
#endif

#include "psycvars.h"
#include "psycpkt.h"
#include "psycproto.h"

#include "chatform.h"

#include "dlogf.h"

#include "dysversion.h"

#include "main.h"


static void fixTextCodec () {
  QTextCodec *kc;
#ifdef WIN32
  kc = QTextCodec::codecForName("windows-1251");
  if (!kc) return;
  QTextCodec::setCodecForCStrings(kc);
  QTextCodec::setCodecForLocale(kc);
#else
  const char *ll = getenv("LANG");
  if (!ll || !ll[0]) ll = getenv("LC_CTYPE");
  if (ll && ll[0] && (strcasestr(ll, "utf-8") || strcasestr(ll, "utf8"))) return;
  kc = QTextCodec::codecForName("koi8-r");
  if (!kc) return;
  QTextCodec::setCodecForCStrings(kc);
  QTextCodec::setCodecForLocale(kc);
#endif
}


bool debugOutput = false;

static void myMessageOutput (QtMsgType type, const char *msg)  {
  switch (type) {
    case QtDebugMsg: if (debugOutput) fprintf(stderr, "Debug: %s\n", msg); break;
    case QtWarningMsg: if (debugOutput) fprintf(stderr, "Warning: %s\n", msg); break;
    case QtCriticalMsg: fprintf(stderr, "Critical: %s\n", msg); break;
    case QtFatalMsg: fprintf(stderr, "Fatal: %s\n", msg); abort();
  }
}


static void setDebugOutput (int argc, char *argv[]) {
  qInstallMsgHandler(myMessageOutput);
  //
  for (int f = 1; f < argc; ++f) {
    if (strcmp(argv[f], "-goobers") == 0) {
      debugOutput = 1;
      for (int c = f; c < argc; ++c) argv[c] = argv[c+1];
      argv[--argc] = NULL;
      --f;
    }
  }
  dlogfSetStdErr(debugOutput?1:0);
  dlogfSetFile(debugOutput?"debug.log":NULL);
  dlogf("Dyskinesia v%d.%d.%d.%d log initialized", DYS_VERSION_HI, DYS_VERSION_MID, DYS_VERSION_LO, DYS_BUILD);
}


int main (int argc, char *argv[]) {
  setDebugOutput(argc, argv);

  QApplication app(argc, argv);

  fixTextCodec();

  QCoreApplication::setOrganizationName("Vampire Avalon");
  QCoreApplication::setOrganizationDomain("ketmar.no-ip.org"); // fuck macz
  QCoreApplication::setApplicationName("Dyskinesia");

#ifdef USE_OTR
  OTRL_INIT;
#endif

  ChatForm *f = new ChatForm();
  f->show();
  return app.exec();
}
