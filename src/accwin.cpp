/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>

#include <QDialog>
#include <QInputDialog>
#include <QMessageBox>

#include "psycproto.h"
#include "winutils.h"

#include "accwin.h"


AccWindow::AccWindow (QWidget *parent) : QDialog(parent), maxIdx(-2) {
  setupUi(this);
  setAttribute(Qt::WA_QuitOnClose, false);
  setAttribute(Qt::WA_DeleteOnClose, false);
  centerWidget(this);
  btOk->setEnabled(false);
  btDelAcc->setEnabled(false);
  cbAccount->setDuplicatesEnabled(false);
}


AccWindow::~AccWindow () {
}


void AccWindow::on_leNick_textChanged (void) {
  PsycEntity u("psyc://"+leHost->text()+"/~"+leNick->text());
  btOk->setEnabled(u.isValid());
}


void AccWindow::on_leHost_textChanged (void) {
  PsycEntity u("psyc://"+leHost->text()+"/~"+leNick->text());
  btOk->setEnabled(u.isValid());
}


void AccWindow::on_btDelAcc_clicked (bool) {
  int idx = cbAccount->currentIndex();
  if (maxIdx >= -1 && idx > maxIdx) return;
  if (QMessageBox::question(this, tr("account deletion"), tr("do you realy want to delete this account?"),
    QMessageBox::Yes | QMessageBox::No, QMessageBox::No) != QMessageBox::Yes) return;
  QString txt(cbAccount->itemText(idx));
  cbAccount->setCurrentIndex(0);
  cbAccount->removeItem(idx);
  leNick->clear();
  leHost->clear();
  lePass->clear();
  cbSecure->setChecked(true);
  maxIdx = cbAccount->count()-1;
  emit accountDeleted(this, txt);
}


void AccWindow::on_btAddAcc_clicked (bool) {
  bool ok;
  QString text = QInputDialog::getText(this, tr("new account creation"), tr("account name:"), QLineEdit::Normal, QString(), &ok);
  text = text.trimmed();
  if (!ok || text.isEmpty()) return;
  if (text[0] == '_' || text[0] == '.') {
    QMessageBox::question(this, tr("account creation error"), tr("invalid account name"),
      QMessageBox::Ok, QMessageBox::Ok);
    return;
  }
  for (int f = cbAccount->count()-1; f >= 0; f--) {
    QString it(cbAccount->itemText(f));
    if (!it.compare(text, Qt::CaseInsensitive)) {
      QMessageBox::question(this, tr("account creation error"), tr("account with this name already exists"),
        QMessageBox::Ok, QMessageBox::Ok);
      return;
    }
  }
  cbAccount->addItem(text);
  leNick->setText(text);
  leHost->clear();
  lePass->clear();
  cbSecure->setChecked(true);
  maxIdx = cbAccount->count()-1;
  // shitcode!
  for (int f = cbAccount->count()-1; f >= 0; f--) {
    QString it(cbAccount->itemText(f));
    if (!it.compare(text, Qt::CaseInsensitive)) {
      cbAccount->setCurrentIndex(f);
      break;
    }
  }
  emit accountCreated(this, text);
}


void AccWindow::on_cbAccount_currentIndexChanged (const QString &text) {
  curAccName = text;
  int idx = cbAccount->currentIndex();
  if (maxIdx >= -1 && idx > maxIdx) return;
  emit updateAccountInfo(this, text);
}
