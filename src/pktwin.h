/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef PKTWIN_H
#define PKTWIN_H

#include <QDialog>

class PsycPacket;


#include "ui_pktform.h"
class PktForm : public QDialog, public Ui_pktForm {
  Q_OBJECT

public:
  PktForm (QWidget *parent = 0);
  ~PktForm ();

signals:
  void send ();
};


#endif
