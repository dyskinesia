/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * we are the Borg.
 */
#include <QDebug>

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <QTextCodec>
#include <QClipboard>

#include "chatjsapi.h"

#include "chatform.h"
#include "k8strutils.h"
#include "psyccontact.h"


extern bool debugOutput;


///////////////////////////////////////////////////////////////////////////////
static void printQStr (const QString &str) {
  //QTextCodec *codec = QTextCodec::codecForName("koi8-r");
  QTextCodec *codec = QTextCodec::codecForLocale();
  QByteArray ba;
  if (codec) ba = codec->fromUnicode(str); else ba = str.toLatin1();
  const char *s = ba.constData();
  int sz = ba.size();
  while (sz > 0) {
    int len = strlen(s);
    if (debugOutput) fprintf(stdout, "%s", s);
    sz -= len;
    if (sz > 0) {
      if (debugOutput) fprintf(stdout, "\n ");
      sz--;
      s += len+1;
    }
  }
  if (debugOutput) fprintf(stdout, "\n");
}


///////////////////////////////////////////////////////////////////////////////
ChatJSAPI::ChatJSAPI (QWebFrame *frame, ChatForm *cform) : QObject(cform), mFrame(frame), mForm(cform) {
}


ChatJSAPI::~ChatJSAPI () {
}


QVariant ChatJSAPI::test (const QVariant &v) {
  qDebug() << QVariant::typeToName(v.type());
  qDebug() << v;
  return QVariant(10);
}


void ChatJSAPI::print (const QString &str) {
  printQStr(str);
  if (debugOutput) fflush(stderr);
}


void ChatJSAPI::printLn (const QString &str) {
  printQStr(str);
  if (debugOutput) fprintf(stderr, "\n");
  if (debugOutput) fflush(stderr);
}


void ChatJSAPI::refresh () {
  mFrame->evaluateJavaScript("window.scrollTo(window.pageXOffset, window.pageYOffset);");
}


void ChatJSAPI::scrollToBottom () {
  mFrame->evaluateJavaScript(
    "do{var ___tmp___=window.pageYOffset;window.scrollTo(0,1000000);}while(window.pageYOffset!=___tmp___);"
  );
}


void ChatJSAPI::startChat (const QString &uni) {
  mForm->startChatWith(uni);
}


void ChatJSAPI::requestAuth (const QString &uni) {
  PsycContact *cc = mForm->findContact(uni);
  if (!cc) return;
  mForm->mProto->requestAuth(uni, "");
}


void ChatJSAPI::cancelAuth (const QString &uni) {
  PsycContact *cc = mForm->findContact(uni);
  if (!cc) return;
  cc->setAuthorized(false);
  mForm->mProto->removeAuth(uni, "");
  //mForm->knockAutosave();
  mForm->saveOneContact(cc);
  mForm->redrawContact(cc);
}


void ChatJSAPI::sendAuth (const QString &uni) {
  PsycContact *cc = mForm->findContact(uni);
  if (!cc) return;
  cc->setAuthorized(true);
  mForm->mProto->sendAuth(uni, "");
  //mForm->knockAutosave();
  mForm->saveOneContact(cc);
  mForm->redrawContact(cc);
}


void ChatJSAPI::deleteContact (const QString &uni) {
  PsycContact *cc = mForm->findContact(uni);
  if (!cc) return;
  mForm->deleteContact(cc);
}


void ChatJSAPI::enterPlace (const QString &uni) {
  if (!mForm->mProto->isLoggedIn()) return;
  mForm->mProto->enterPlace(uni);
}


void ChatJSAPI::leavePlace (const QString &uni) {
  if (!mForm->mProto->isLoggedIn()) return;
  PsycEntity place(uni);
  mForm->mProto->leavePlace(place);
  mForm->onPlaceLeaved(place);
}


bool ChatJSAPI::hasOption (const QString &name) {
  return hasOption(name, QString());
}


bool ChatJSAPI::hasOption (const QString &name, const QString &uni) {
  return mForm->hasKeyOpt(name, uni);
}


bool ChatJSAPI::removeOption (const QString &name) {
  return removeOption(name, QString());
}


bool ChatJSAPI::removeOption (const QString &name, const QString &uni) {
  return mForm->removeOpt(name, uni);
}


QVariant ChatJSAPI::getOption (const QString &name) {
  return getOption(name, QString());
}


QVariant ChatJSAPI::getOption (const QString &name, const QString &uni) {
  switch (mForm->typeOpt(name, uni)) {
    case K8SDB::Boolean: return QVariant(mForm->toBoolOpt(name, false, uni));
    case K8SDB::Integer:
      //qDebug() << "!!" << mForm->toIntOpt(name, 0, uni);
      return QVariant(mForm->toIntOpt(name, 0, uni));
    //case K8SDB::KeySequence:
    case K8SDB::String:
      return QVariant(mForm->asStringOpt(name, uni));
    default: ;
  }
  return QVariant();
}


bool ChatJSAPI::setOption (const QString &name, const QVariant &v) {
  return setOption(name, QString(), v);
}


bool ChatJSAPI::setOption (const QString &name, const QString &uni, const QVariant &v) {
  switch (v.type()) {
    case QVariant::Bool: return mForm->setOpt(name, v.toBool(), uni);
    case QVariant::Char:
    case QVariant::String:
      return mForm->setOpt(name, v.toString(), uni);
    case QVariant::Int: return mForm->setOpt(name, v.toInt(), uni);
    default: ;
  }
  return false;
}


QVariant ChatJSAPI::contactInfo (const QString &uni) {
  PsycContact *cc = mForm->findContact(uni);
  if (!cc) return QVariant(); // null
  QVariantMap ci;
  ci["place"] = QVariant(cc->isPlace());
  ci["verbatim"] = QVariant(cc->verbatim());
  ci["hidden"] = QVariant(cc->isHidden());
  ci["uni"] = QVariant(cc->uni());
  ci["proto"] = QVariant(cc->entity()->proto());
  ci["nick"] = QVariant(cc->nick());
  ci["host"] = QVariant(cc->entity()->host());
  ci["port"] = QVariant(cc->entity()->port());
  ci["channel"] = QVariant(cc->entity()->channel());
  ci["status"] = QVariant((int)cc->status());
  ci["statusName"] = QVariant(PsycProto::statusName(cc->status()));
  ci["statusText"] = QVariant(cc->statusText());
  ci["mood"] = QVariant((int)cc->mood());
  ci["moodName"] = QVariant(PsycProto::moodName(cc->mood()));
  ci["authorized"] = QVariant(cc->authorized());
  ci["group"] = QVariant(cc->groupName());
  ci["unread"] = QVariant(cc->messageCount());
  ci["lastseen"] = QVariant(cc->lastSeenUnix());
  ci["chatting"] = QVariant(mForm->mChatUser && mForm->mChatUser==cc);
  ci["showAlways"] = mForm->toBoolOpt("/contactlist/showalways", false, cc->uni());
  ci["skipUnreadCycle"] = mForm->toBoolOpt("/contactlist/skipunreadcycle", false, cc->uni());
  ci["isOTR"] = QVariant(cc->isOTRActive());
  ci["isOTRVerified"] = QVariant(cc->isOTRVerified());
  return QVariant(ci);
}


void ChatJSAPI::setContactVerbatim (const QString &uni, const QString &v) {
  PsycContact *cc = mForm->findContact(uni);
  if (!cc) return;
  if (cc->verbatim() != v) {
    cc->setVerbatim(v);
    mForm->saveOneContact(cc);
    mForm->redrawContact(cc);
  }
}


void ChatJSAPI::setContactHidden (const QString &uni, bool v) {
  PsycContact *cc = mForm->findContact(uni);
  if (!cc) return;
  if (cc->isHidden() != v) {
    cc->setHidden(v);
    mForm->saveOneContact(cc);
    mForm->redrawContact(cc);
  }
}


void ChatJSAPI::setStatus (int newStatus) {
  if (newStatus < 1 || newStatus > 9) return;
  mForm->setStatus((PsycProto::Status)newStatus);
}


int ChatJSAPI::status () {
  return (int)mForm->mProto->status();
}


void ChatJSAPI::markAsRead (const QString &aUNI) {
  mForm->markAsRead(aUNI);
}


void ChatJSAPI::setWasBottom (bool bflag) {
  mForm->mWasAtBottom = bflag;
}


void ChatJSAPI::selectedText (const QString &s) {
  mForm->mSelText = s;
}


QString ChatJSAPI::editorText () {
  return mForm->edChat->toPlainText();
}


void ChatJSAPI::setEditorText (const QString &s) {
  mForm->edChat->setPlainText(s);
  mForm->mStartingNickCompletion.clear();
  mForm->mLatestNickCompletion.clear();
  mForm->edChat->ensureCursorVisible();
}


void ChatJSAPI::insertEditorText (const QString &s) {
  mForm->edChat->insertPlainText(s);
  mForm->edChat->ensureCursorVisible();
}


int ChatJSAPI::editorCurPos () {
  QTextCursor cur(mForm->edChat->textCursor());
  return cur.position();
}


void ChatJSAPI::setEditorCurPos (int pos) {
  QTextCursor cur(mForm->edChat->textCursor());
  cur.setPosition(pos);
  mForm->edChat->setTextCursor(cur);
}


void ChatJSAPI::setClipboardText (const QString &s) {
  QClipboard *cb = QApplication::clipboard();
  cb->setText(s);
}


void ChatJSAPI::otrStart (const QString &aUNI) {
  mForm->otrConnect(aUNI);
}


void ChatJSAPI::otrEnd (const QString &aUNI) {
  mForm->otrDisconnect(aUNI);
}


void ChatJSAPI::otrSetTrust (const QString &aUNI, bool tflag) {
  mForm->otrSetTrust(aUNI, tflag);
}


void ChatJSAPI::otrForget (const QString &aUNI) {
  mForm->otrForget(aUNI);
}


void ChatJSAPI::otrInitiateSMP (const QString &aUNI, const QString &secret, const QString &question) {
  mForm->otrInitiateSMP(aUNI, secret, question);
}


void ChatJSAPI::doPSYC (const QString &cmdNargs) {
  doPSYC(cmdNargs, "");
}


//FIXME! add 'focus' here!
void ChatJSAPI::doPSYC (const QString &cmdNargs, const QString &destUni) {
  if (mForm->mProto->isLoggedIn()) mForm->mProto->doCommand(cmdNargs, destUni);
}


// this will return an UNI or ""
QString ChatJSAPI::chattingWith () {
  if (!mForm->mChatUser) return QString();
  return mForm->mChatUser->uni();
}


// this can be done in JS code, but who really cares?
bool ChatJSAPI::isInPlace () {
  if (!mForm->mChatUser) return false;
  return mForm->mChatUser->isPlace();
}


void ChatJSAPI::invite (const QString &uni, const QString &place) {
  if (!mForm->mProto->isLoggedIn()) return;
  mForm->mProto->invite(place, uni);
}


bool ChatJSAPI::addMessage (const QString &placeUni, const QString &userUni, uint time, const QString &text,
    const QString &action)
{
  PsycEntity place(placeUni);
  PsycEntity user(userUni);
  if (!user.isValid()) return false;
  if (place.isValid() && !place.isPlace()) return false;
  QDateTime dtm;
  if (time == 0) dtm = QDateTime::currentDateTime(); else dtm.setTime_t(time);
  mForm->onMessage(place, user, dtm, text, action, "\x1"+text);
  return true;
}


bool ChatJSAPI::addMessageEx (const QString &placeUni, const QString &userUni, uint time, const QString &text,
    const QString &action, const QString &popupMsg)
{
  PsycEntity place(placeUni);
  PsycEntity user(userUni);
  if (!user.isValid()) return false;
  if (place.isValid() && !place.isPlace()) return false;
  QDateTime dtm;
  if (time == 0) dtm = QDateTime::currentDateTime(); else dtm.setTime_t(time);
  mForm->onMessage(place, user, dtm, text, action, popupMsg);
  return true;
}


