/*
 * Midori JS Framework (r80) Copyright (c) 2008 Aycan Gulez
 * http://www.midorijs.com
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

if (typeof(midorijs) != "object") midorijs = {};

if (typeof(midorijs.trim) != "function") midorijs.trim = function (st) { return st.replace(/^\s+|\s+$/g, ''); };

if (typeof(midorijs.concatUnique) != "function") {
  midorijs.concatUnique = function (a1, a2) {
    var uniqA2 = [];
    for (var i = 0, numA2 = a2.length; i < numA2; i++) if (!this.inArray(a2[i], a1)) uniqA2.push(a2[i]);
    return a1.concat(uniqA2);
  };
}

if (typeof(midorijs.inArray) != "function") {
  midorijs.inArray = function (v, a) {
    for (var i = 0, numA = a.length; i < numA; i++) if (v == a[i]) return true;
  };
}


midorijs.parseSelectors = function (selectorText) {
  var c = this.trim(selectorText).split("");
  var sI = -1;
  var bracketContent = '';
  var elements = [], attrs = [], separators = [];
  var inSelector = false, inBrackets = false, inQuotes = false;

  for (var i = 0, len = c.length; i < len; i++) {
    if (inSelector) {
      if (inBrackets) {
        switch (c[i]) {
          case '"': inQuotes = !inQuotes; break;
          case ']': if (!inQuotes) { attrs[sI].push(bracketContent); inBrackets = false; bracketContent = ''; } break;
          case '\\': bracketContent += c[++i]; break;
          default: bracketContent += c[i];
        }
      } else {
        switch (c[i]) {
          case '[': inBrackets = true; break;
          case ' ': case '>': case ',': inSelector = false; separators[sI] = c[i]; break;
          case '\\': elements[sI] += c[++i]; break;
          default: elements[sI] += c[i];
        }
      }
    } else {
      switch (c[i]) {
        case ' ': case '>': case ',': separators[sI] += c[i]; break;
        default: inSelector = true; elements[++sI] = c[i]; attrs[sI] = [];
      }
    }
  }
  return { elements: elements, attrs: attrs, separators: separators };
};


midorijs.processAttrs = function (match, a, exprs) {
  for (var i = 0, numA = a.length, attr; i < numA; i++) {
    attr = (a[i] == "class") ? (match.className ? match.className : null) : match.getAttribute(a[i]);
    switch (typeof(exprs[i])) {
      case "undefined": if (attr == null) return false; break;
      case "string": if (attr == exprs[i]) return false; break;
      default: if (!exprs[i].test(attr)) return false;
    }
  }
  return true;
};


midorijs.processPseudo = function (match, pSelector, pA, pB) {
  var pCache, nodeKey, parentChildren = [], pI = 0;

  if (!(nodeKey = match.parentNode.getAttribute("midorinodekey")))
     match.parentNode.setAttribute("midorinodekey", nodeKey = Math.random().toString().substr(2));

  if (pCache = this.pCache[nodeKey]) parentChildren = pCache["parentChildren"], pI = pCache["pI"];
  else {
    var c = match.parentNode.firstChild;
    while (c) {
      if (c.nodeType == 1) parentChildren.push(c);
      c = c.nextSibling;
    }
    this.pCache[nodeKey] = { parentChildren: parentChildren, pI: 0 };
  }
  var parentNumChildren = parentChildren.length;

  switch (pSelector) {
    case "first-child": if (match == parentChildren[0]) return true; break;
    case "last-child": if (match == parentChildren[parentNumChildren-1]) return true; break;
    case "only-child": if (parentNumChildren == 1) return true; break;
  }

  if (pSelector == "nth-child") {
    var v = pA*pI+pB;
    var oldV = -50;
    while (v > -50 && v <= parentNumChildren) {
      if (v >= 0 && parentChildren[v-1] == match) {
        this.pCache[nodeKey]["pI"] = (pA >= 0) ? pI + 1 : 0;
        return true;
      }
      pI++, v += pA;
      if (v == oldV) break;
      oldV = v;
    }
  }
};


midorijs.getMatches = function (target, s, a, oneLevelOnly) {
  this.pCache = {};
  var matches = [], exprs = [];
  var chunks, objs, numObjs, pseudo, pSelector, pOption, pA, pB;


  this.postProcess = function (me) {
    if (!numA && !pseudo) { matches.push(me); return; }
    var match = true;
    if (numA && !this.processAttrs(me, a, exprs)) match = false;
    if (pseudo && !this.processPseudo(me, pSelector, pA, pB)) match = false;
    if (match) matches.push(me);
  }


  for (var i = 0, numA = a.length; i < numA; i++) {
    chunks = a[i].match(/([a-z0-9_-]+)\s*([=^$*|~!]{0,2})\s*"?([^"]*)"?$/i); //"
    a[i] = chunks[1];
    switch (chunks[2]) {
      case "=": exprs[i] = new RegExp("^"+chunks[3]+"$", "i"); break;
      case "^=": exprs[i] = new RegExp("^"+chunks[3], "i"); break;
      case "$=": exprs[i] = new RegExp(chunks[3]+"$", "i"); break;
      case "*=": exprs[i] = new RegExp(chunks[3], "i"); break;
      case "~=": exprs[i] = new RegExp("^"+chunks[3]+"$|^"+chunks[3]+"\\s|\\s"+chunks[3]+"\\s|\\s"+chunks[3]+"$", "i"); break;
      case "!=": exprs[i] = chunks[3]; break;
    }
  }

  if (s.indexOf(":") != -1) {
    chunks = s.split(":");
    s = chunks[0];
    pseudo = chunks[1].match(/([a-z-]+)\(?([a-z0-9+-]*)\)?/i);
    pSelector = pseudo[1].toLowerCase();
    switch (pOption = pseudo[2].toLowerCase()) {
      case "odd": pOption = "2n+1"; break;
      case "even": pOption = "2n"; break;
    }
    chunks = pOption.match(/([0-9+-]*)(n?)([0-9+-]*)/i);
    pA = parseInt(chunks[2] ? (chunks[1] ? ((chunks[1] == "-") ? -1 : chunks[1]) : 1) : 0);
    pB = parseInt(chunks[3] ? chunks[3] : ((chunks[1] && !chunks[2]) ? chunks[1] : 0));
  }

  if (s.indexOf("#") != -1) this.postProcess(document.getElementById(s.substr(s.indexOf("#")+1)));
  else if (s.indexOf(".") != -1) {
    chunks = s.split(".");
    var classMatch = s.substr(chunks[0].length+1).replace(".", " ");
    var className  = new RegExp("^"+classMatch+"$|^"+classMatch+"\\s|\\s"+classMatch+"\\s|\\s"+classMatch+"$", "i");
    objs = target.getElementsByTagName(chunks[0] ? chunks[0] : "*");
    for (i = 0, numObjs = objs.length; i < numObjs; i++)
      if ((!oneLevelOnly && className.test(objs[i].className)) ||
          (oneLevelOnly && className.test(objs[i].className) && objs[i].parentNode == target))
        this.postProcess(objs[i]);
  } else if (s == "*" || /^[A-Za-z0-9]+$/.test(s))
    for (i = 0, objs = target.getElementsByTagName(s), numObjs = objs.length; i < numObjs; i++)
      if (!oneLevelOnly || (oneLevelOnly && objs[i].parentNode == target)) this.postProcess(objs[i]);

  return matches;
};


midorijs.get = function (selectorText, startAt) {
  var selectors = this.parseSelectors(selectorText);
  var numS = selectors["elements"].length;

  if (!startAt) startAt = document;
  if (numS == 1) {
    var idMatch = selectors["elements"][0].match(/^[a-z0-9*]*#([^,:]+)$/i);
    if (idMatch && selectors["attrs"][0] == "" && selectors["separators"] == "") return document.getElementById(idMatch[1]);
  }
  var objs = this.getMatches(startAt, selectors["elements"][0], selectors["attrs"][0]);
  var allObjs = [], newObjs;
  for (var i = 1; i < numS; i++) {
    newObjs = [];
    separator = this.trim(selectors["separators"][i-1]);
    if (separator == ",") {
      allObjs = this.concatUnique(allObjs, objs);
      objs = this.getMatches(startAt, selectors["elements"][i], selectors["attrs"][i]);
    } else {
      var oneLevelOnly = (separator == ">") ? true : false;
      for (var j = 0, numObjs = objs.length; j < numObjs; j++)
        if (!this.inArray(objs[j], newObjs)) newObjs = this.concatUnique(newObjs, this.getMatches(objs[j], selectors["elements"][i], selectors["attrs"][i]), oneLevelOnly);
      objs = newObjs;
    }
  }
  allObjs = this.concatUnique(allObjs, objs);
  allObjs.apply = function (p) {
    for (var i = 0, numObjs = this.length; i < numObjs; i++) (typeof(p)=="function") ? p(this[i]) : eval("this[i]." + p)
  };

  return allObjs;
};
