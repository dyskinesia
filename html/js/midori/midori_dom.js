/*
 * Midori JS Framework (r80) Copyright (c) 2008 Aycan Gulez
 * http://www.midorijs.com
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

if (typeof(midorijs) != "object") midorijs = {};

if (typeof(midorijs.browserType) != "string") {
  /*BADBADBAD!*/
  midorijs.browserType = window.opera ? "Opera" :
    (navigator.userAgent.indexOf("WebKit")!=-1 ? "Safari" :
      (navigator.userAgent.indexOf("MSIE")!=-1 ? "MSIE" : "Gecko"));
}


midorijs.addEventListener = function (target, eventType, listenerFunc) {
  this.safariReady = function () {
    this.readyTimer = setInterval(function() { if (/loaded|complete/.test(document.readyState)) midorijs.runReadyEvents() }, 10);
  };

  this.msieReady = function () {
    document.write('<script id="midori_onload" src="javascript: {}" defer="true"></script>');
    this.get("#midori_onload").onreadystatechange = function() { if (this.readyState == "complete") midorijs.runReadyEvents() };
  };


  if (target.addEventListener) {
    if (eventType == "ready") {
      switch (this.browserType) {
        case "Safari": this.domReady.push(listenerFunc); if (!this.readyTimer) this.safariReady(); return;
        case "Opera": case "Gecko": eventType = "DOMContentLoaded"; break;
        default: eventType = "load";
      }
    }
    target.addEventListener(eventType, listenerFunc, false);
    return;
  }

  if (eventType == "ready") {
    // MSIE
    if (!this.domReady.length) this.msieReady();
    this.domReady.push(listenerFunc);
    return;
  }

  target.attachEvent("on"+eventType, listenerFunc);
};


midorijs.runReadyEvents = function () {
  if (this.readyTimer) clearInterval(this.readyTimer);
  for (var i = 0, numE = this.domReady.length; i < numE; i++) this.domReady[i]();
};


midorijs.getEventTarget = function (event) {
  var target = event.target ? event.target : event.srcElement;
  if (target.nodeType == 3) target = target.parentNode;
  return target;
};


midorijs.getMousePos = function (event) {
  if (event.pageX || event.pageY) return { x: event.pageX, y: event.pageY };
  return { x: event.clientX+document.documentElement.scrollLeft-document.body.clientLeft,
           y: event.clientY+document.documentElement.scrollTop-document.body.clientTop };
};


midorijs.preventBubble = function (event) {
  event.stopPropagation ? event.stopPropagation() : window.event.cancelBubble = true;
};


midorijs.preventDefault = function (event) {
  event.preventDefault ? event.preventDefault() : window.event.returnValue = false;
};


midorijs.getFloat = function (o) { return ((this.browserType == 'MSIE') ? o.style.styleFloat : o.style.cssFloat); };
midorijs.setFloat = function (o, v) { (this.browserType == 'MSIE') ? o.style.styleFloat = v : o.style.cssFloat = v; };
midorijs.ieObjectFix = function (o, html) { o.innerHTML = html; };


midorijs.getSelection = function (target) {
  if (this.browserType != "MSIE") return target.getSelection();
  if (target == window) target = document;
  var cursorPos = target.selection.createRange();
  if (target.selection.type != "Control") return cursorPos;
};


midorijs.getSelectionText = function (cursorPos) {
  return (this.browserType == 'MSIE') ? cursorPos.htmlText : cursorPos.toString();
};


midorijs.getCookie = function (cookieName) {
  var cookies = document.cookie.split("; ");
  for (var i = 0, numCookies = cookies.length; i < numCookies; i++) {
    var parts = cookies[i].split("=");
    if (parts[0] == cookieName) return unescape(parts[1].replace(/\+/g, " "));
  }
};


midorijs.setCookie = function (name, value, expires, path, domain) {
  var today = new Date();
  document.cookie =
    name+'='+escape(value)+"; expires="+today.toUTCString(today.setSeconds(expires))+
      (path ? "; path="+path : "")+(domain ? "; domain="+domain : "");
};


midorijs.getPos = function (obj) {
  var xPos = 0, yPos = 0;
  while (obj.offsetParent != null) xPos += obj.offsetLeft, yPos += obj.offsetTop-obj.scrollTop, obj = obj.offsetParent;
  return { x: xPos, y: yPos };
};



// accepted params: id, size, maxlen, callback
function midoriInlineEdit (vars) {
  var thisObj = this;
  this.myObj = midorijs.get("#"+vars.id);
  this.editId = vars.id+"Input";
  this.editObj = "";

  this.edit = function () {
    if (this.myObj.getAttribute("editing") == "on") return;
    this.myObj.innerHTML = '<input type="text" size="'+vars.size+'" maxlength="'+(vars.maxlen ? vars.maxlen : "")+
      '" value="'+this.myObj.innerHTML.replace(/"/g, '&quot;')+'" id="'+this.editId+'" />'; //'
    this.myObj.setAttribute("editing", "on");
    this.editObj = midorijs.get("#"+this.editId);
    this.editObj.focus();
    midorijs.addEventListener(this.editObj, "keyup", function (e) {
      if (e.keyCode == 13 || e.keyCode == 27) thisObj.save();
    });
    midorijs.addEventListener(this.editObj, "blur", function (e) {
      thisObj.save();
    });
  }

  this.save = function () {
    this.myObj.setAttribute("editing", "off");
    this.myObj.innerHTML = this.editObj.value;
    if (vars.callback) vars.callback(this.editObj.value);
  }
}
