/*
 * Midori JS Framework (r80) Copyright (c) 2008 Aycan Gulez
 * http://www.midorijs.com
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

if (typeof(midorijs) != "object") midorijs = {};


midorijs.each = function (parentObj, callBack, depthFirst) {
  var c = parentObj.firstChild;
  while (c) {
    if (!depthFirst) callBack(c);
    if (c.firstChild) this.each(c, callBack, depthFirst);
    if (depthFirst) callBack(c);
    c = c.nextSibling;
  }
};


midorijs.sibling = function (obj, direction) {
  var sibling = obj;
  if (direction == "next") { do sibling = sibling.nextSibling; while (sibling && sibling.nodeType == 3); }
  else if (direction == "prev") { do sibling = sibling.previousSibling; while (sibling && sibling.nodeType == 3); }
  return (sibling == obj) ? false : sibling;
};
