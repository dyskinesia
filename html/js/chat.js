var msgFirst = 1;
var msgCur = 0;
var msgCount = 0;
var lastUNI;
var lastMsgDiv;


function selectedText () {
  var sel = window.getSelection();
  if (!sel || sel.rangeCount < 1) return "";
  var s = sel.toString();
/*
  for (var f = 0; f < s.length; f++) {
    var ch = s.charAt(f);
    if (ch < ' ') api.print("{"+ch.charCodeAt(0)+"}");
    else if (ch == ' ') api.print("{ }");
    else api.print(ch);
  }
  api.printLn("");
*/
  api.selectedText(s);
  return s;
  //.removeAllRanges();
}


function unselectText () {
  var sel = window.getSelection();
  if (!sel || sel.rangeCount < 1) return "";
  sel.removeAllRanges();
}


/*
 *
 */
function setMyUNI (uni) {
  myUNI = uni;
}


/*
 *
 */
function chatStarted (uni) {
  //api.printLn(">>>"+uni);
}


function optionChanged (optname) {
}


function optionRemoved (optname) {
}


function date2Str (dt) {
  function n2s (n, len) {
    var res = ""+n;
    len = len||2;
    while (res.length < len) res = "0"+res;
    return res;
  }
  var res = dt.getFullYear()+"/"+n2s(dt.getMonth()+1)+"/"+n2s(dt.getDate())+" "+
    n2s(dt.getHours())+":"+n2s(dt.getMinutes())+":"+n2s(dt.getSeconds());
  return res;
}


function delChatMessage (id) {
  var div = document.getElementById("msg_"+id);
  if (div) {
    div.parentNode.removeChild(div);
    api.scrollToBottom();
  }
}


/*
 *
 */
function addHR () {
  var div = document.createElement("hr");
  document.body.appendChild(div);
}


function chatAtBottom () {
  var curY = window.pageYOffset;
  var de = document.documentElement.offsetHeight;
  var vh = window.innerHeight;
  return (curY+vh >= de);
}


function isChatAtBottom () {
  api.setWasBottom(chatAtBottom());
}


/*
 *
 */
function addSysMsg (msg) {
  //msg = msg.replace(/\n/g, "<br />\n");
/*
  api.printLn("----");
  api.printLn("addSysMsg: before: h="+document.body.offsetHeight+"; ofs="+window.pageYOffset+"; ch="+document.body.clientHeight+"; ih="+window.innerHeight);
  api.printLn("  "+document.documentElement.scrollHeight);
  api.printLn("  "+document.documentElement.offsetHeight);
*/
  var wasAtBottom = chatAtBottom();

  if (!lastMsgDiv || lastUNI != "system console") {
    var div = document.createElement("div");
    div.setAttribute("class", "message");

    var hdrdiv = document.createElement("div");
    hdrdiv.setAttribute("class", "sysmsghead");
    hdrdiv.innerText = "system console";
    div.appendChild(hdrdiv);

    var txtdiv = document.createElement("div");
    txtdiv.setAttribute("class", "sysmsgtext");
    txtdiv.innerHTML = msg;
    div.appendChild(txtdiv);

    lastMsgDiv = div;
    lastUNI = "system console";

    document.body.appendChild(div);
  } else {
    var divdiv = document.createElement("div");
    divdiv.setAttribute("class", "msgdelimiter");
    lastMsgDiv.appendChild(divdiv);

    var txtdiv = document.createElement("div");
    txtdiv.setAttribute("class", "sysmsgtext");
    txtdiv.innerHTML = msg;
    lastMsgDiv.appendChild(txtdiv);
  }
/*
  api.printLn("addSysMsg: after: h="+document.body.offsetHeight+"; ofs="+window.pageYOffset+"; ch="+document.body.clientHeight+"; ih="+window.innerHeight);
  api.printLn("  "+document.documentElement.scrollHeight);
  api.printLn("  "+document.documentElement.offsetHeight);

  api.refresh();
  api.printLn("addSysMsg: refreshed: h="+document.body.offsetHeight+"; ofs="+window.pageYOffset+"; ch="+document.body.clientHeight+"; ih="+window.innerHeight);
  api.printLn("  "+document.documentElement.scrollHeight);
  api.printLn("  "+document.documentElement.offsetHeight);

  api.scrollToBottom();
  api.printLn("addSysMsg: scrolled: h="+document.body.offsetHeight+"; ofs="+window.pageYOffset+"; ch="+document.body.clientHeight+"; ih="+window.innerHeight);
  api.printLn("  "+document.documentElement.scrollHeight);
  api.printLn("  "+document.documentElement.offsetHeight);
*/
  var fs = api.getOption("/chat/forcescroll");
  //api.printLn("fs="+fs+"; wasBt="+wasAtBottom);
  if (fs > 1 || ((fs === null || fs == 1) && wasAtBottom)) {
    api.scrollToBottom();
  } else {
    api.refresh();
  }
}


function shrinkList () {
  var maxMsg = api.getOption("/chat/maxmessages");
  if (typeof(maxMsg) == "number") {
    if (maxMsg > 0 && msgCount > maxMsg) {
      var toLeft = api.getOption("/chat/maxmessagesleft");
      if (typeof(toLeft) != "number") toLeft = maxMsg;
      while (msgCount > toLeft) {
        delChatMessage(msgFirst);
        msgFirst++;
        msgCount--;
      }
    }
  }
}


function fixLongWords (s) {
  for (var f = 0; f < s.length; ) {
    while (f < s.length && s.charAt(f) <= ' ') f++;
    if (f >= s.length) break;
    var st = 0;
    while (f < s.length && s.charAt(f) > ' ') {
      st++; f++;
      if (s.charAt(f-1) == "&") {
        while (f < s.length && s.charAt(f) != ';') f++;
        f++;
        continue;
      }
      if (s.charAt(f-1) == "<") {
        while (f < s.length && s.charAt(f) != '>') f++;
        f++;
        continue;
      }
      if (st >= 32 && f < s.length) {
        s = s.substr(0, f)+"&shy;"+s.substr(f, s.length);
        st = 0;
        f += 4;
        continue;
      }
    }
  }
  return s;
}


function colorizeQuotes (s) {
/*
  var t = s.split("");
  for (var f = 0; f < t.length; f++) {
    //if (t[f] == '>') t[f] = '!';
    t[f] = t[f]+"("+(t[f].charCodeAt(0))+")";
  }
  s = t.join("");
*/
  var lines = s.split("\n");

  var trim = function (st) {
    return st.replace(/^\s+|\s+$/g, "");
  };
  var trimR = function (st) {
    return st.replace(/\s+$/g, "");
  };
  var jj = function (st, end, addnl) {
    var res = "";
    if (end >= lines.length) end = lines.length-1;
    for (var f = st; f <= end; f++) {
      if (res) res += "\n";
      res += lines[f];
    }
    if (addnl) res += "\n";
    return res;
  };
  var qlevel = function (s) {
    var lvl = -1;
    if (s.length < 1) return lvl;
    var mir = (s.charAt(0) == '\u00bb');
    for (var f = 0; f < s.length; f++) {
      var ch = s.charAt(f);
      if (ch <= ' ') {
        if (mir) return lvl;
        continue;
      }
      if (mir) {
        if (ch == '\u00bb') lvl++; else break;
      } else {
        if (ch == '>') lvl++;
        else if (ch == '&' && s.substr(f, 4) == "&gt;") { lvl++; f += 3; }
        else break;
      }
    }
    return lvl;
  };

  // search for "big" quotes
  for (var f = 0; f < lines.length; f++) {
    var s = trimR(lines[f]);
    lines[f] = s;
    if (s == "---") {
      s = '<span class="msgquotebig">'+jj(0, f)+"</span>\n"+jj(f+1, lines.length-1);
      return trimR(s);
    }
  }
  // mark "small" quotes
  for (var f = 0; f < lines.length; f++) {
    var s = trimR(lines[f]);
    var lvl = qlevel(s);
    if (lvl >= 0) {
      lvl = lvl%2;
      s = '<span class="msgquote'+lvl+'">'+s+"</span>";
    }
    lines[f] = s;
  }
  //return "cnt: "+(lines.length)+ "\n"+jj(0, lines.length-1);
  return jj(0, lines.length-1);
}


/*
 * bool .ismine
 * int .unixdate
 * string .uni
 * string .text
 * string .action (or null/false/empty string)
 */
function addChatMessage (msg) {
  var wasAtBottom = chatAtBottom();

  var user = api.contactInfo(msg.uni);
  var nick = user ? user.verbatim : msg.uni;
  lastMsgDiv = false;
  var isMine = msg.ismine, unixDate = msg.unixdate, text = msg.text;

  //api.printLn("t=["+text+"]");
  text = text.replace(/\r\n/g, "\n");
  text = text.replace(/<br\s*>/g, "\n");
  text = text.replace(/<br\s*\/>/g, "\n");

  text = fixLongWords(text);
  text = colorizeQuotes(text);
  text = text.replace(/\n/g, "<br />\n");

  msgCur++; msgCount++;

  var div = document.createElement("div");
  div.setAttribute("id", "msg_"+msgCur);
  div.setAttribute("class", "message");

  var timespan = document.createElement("span");
  timespan.setAttribute("class", "time");

  var dt = new Date();
  dt.setTime(unixDate*1000);
  timespan.innerHTML = date2Str(dt);
  div.appendChild(timespan);

/*
  var markspan = document.createElement("span");
  markspan.setAttribute("style", "float:right;padding:2px;color:#"+(id%2?"999":"0c0"));
  markspan.setAttribute("id", "mark_msg_"+id);
  markspan.innerHTML = "*";
  div.appendChild(markspan);
*/

  var hdrdiv = document.createElement("div");
  hdrdiv.setAttribute("class", "msghead "+(isMine?"msgmine":"msgalien"));
  hdrdiv.innerText = nick;
  if (!isMine) hdrdiv.innerHTML = "<b>"+hdrdiv.innerHTML+"</b>";
  //hdrdiv.style.color = isMine?"#f70":"#aaa";
  div.appendChild(hdrdiv);

  if (msg.action) {
    // has some action
    var adiv = document.createElement("div");
    adiv.setAttribute("class", "actiontext");
    adiv.innerHTML = "*<b>"+nick+"</b> "+msg.action;
    adiv.style.color = isMine?"#f70":"#aaa";
    div.appendChild(adiv);
  }

  var txtdiv = document.createElement("div");
  txtdiv.setAttribute("class", "msgtext");
  txtdiv.innerHTML = text;
  txtdiv.style.color = isMine?"#f70":"#aaa";
  div.appendChild(txtdiv);

  document.body.appendChild(div);

  var fs = api.getOption("/chat/forcescroll");
  //api.printLn("fs="+fs+"; wasBt="+wasAtBottom);
  if (fs > 1 || ((fs === null || fs == 1) && wasAtBottom)) {
    shrinkList();
    api.scrollToBottom();
  } else {
    api.refresh();
  }
}


/*
function pval (v) {
  api.printLn("type: "+typeof(v)+"; value: "+v);
}

function parr (arr) {
  for (var k in arr) {
    if (typeof(k) != "string") continue;
    var v = arr[k];
    api.printLn("["+k+"]: ("+typeof(v)+") ["+v+"]");
  }
}

function pd () {
  var info = api.contactInfo("psyc://ketmar.no-ip.org/@dyskgit");
  api.printLn(">>"+info);
  parr(info);
}
*/


/*
//api.test();
pval(api.test(10));
pval(api.test("str"));
pval(api.test(api));
pval(api.test([1,2]));
pval(api.test({a:4, b:5}));
*/


/*
var ok = api.addMessage(
  "psyc://127.0.0.1/@Testbed", // place
  "psyc://127.0.0.1/~rtorrent_bot", // user
  0, // current time (or any other unixtime)
  "test of the <b>rTorrent</b>-<i>bot</i>\nsecond line",
  " DONTTOUCH|" // no action, "\x20DONTTOUCH|" means 'don't de-htmlize message' mode; action text can be added after '|'
);

api.addMessageEx() has one more arg:
  `popupMsg` -- message for popup window; prepend it with '\x01' to
                disable de-htmlization
*/


function testAddMessage () {
  var ok = api.addMessage(
    "psyc://ketmar.no-ip.org/@Testbed",
    "psyc://ketmar.no-ip.org/~rtorrent_bot",
    0, // current time
    "test of the <b>rTorrent</b>-<i>bot</i>\nsecond line",
    " DONTTOUCH|" // no action, 'don't de-htmlize message' mode
  );
  api.printLn("addMessage: "+(ok?"ok":"FAILED"));
}


window.onload = function () {
  //api.printLn("!!!");
  document.body.innerHTML = '<div style="position:fixed;right:4px;top:4px;background-color:#fff;border:1px solid #666;padding:5px;"><img alt="" width="130" height="26" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAaCAAAAABeXdsPAAAAAnRSTlMA/1uRIrUAAAKmSURBVBgZxcFfaJV1HMfx93NOj5ud0kq7KLUJyUyPwSKjeRiRRTwIEhrK9MKLLsbsosgdDBklXUyk6I9mEWgXgpyr2M3AEPQmMEFEhIEP2J+xqRcqZtnSraPbp+f5PWuec57nG7s7rxdqOqTqvlefaZItg1MSuvkCTdQ7JdTD1kq2vQv4T+HdSqav34KPKrFd7dR7oxIbeBZY1FfJ1N8Bh8TIPI7JMLqCxLJTMozCqJzPqfPI73LehrXnZRiANvEd9MlyeT6xwo+y7IUjcsZaqeH1KdHB4jFZeuHhKwTw1J+yHG8h8olMqyBQ4vKlGj8rcbXgDcky+TQQEgDfy7QOaL8jy3Ae8mdke4dtMh0lEhIAW6uynAAOy3K3l8jGaVlGWvxhWf7ZSCQkILKylPKVnImlUJUzVEp5EueADBNraFOiv5SynFhIQLZFVTmdvKLEi1gKJ5Vp8k3olnN3MYaQAMMxxa6v5jU5FQ9T4VtlmNwEdMs5iCUkwOD5sTx4vuPxPzw/w0NEcr7jYQkJiHl+JMfc5f1YjkZ+jBo53/GwhARElpxW5LdtzNWyUcV+op5/cFqR3a3M+kzO+1hCAiLDcv5ewhx9IOcG9T5VYjuzDssJsYQEQFtViSIZFpbSxuQMUe+qEmVmdcu5/16p0SqckADo0owiGY7L9Dr1xpUoM+vR6zLcu72CSEgAdGlGkbTnJmS5Mp9640qUeWCPTHuIhARAl2YUSftYlqkNNBhXoswDj12U5RyRkNNA6x0liqQNy7KbRmeVKFOj85YM0y/DE7e4sRD4Us6vBVJW35Ohv4VGO+VUX6LW2j9kGID1QoeA/OYdscdJ67x2M9OFLaTleiqRb4rUe/5IJdMXHfih0NR+n6ZZ+oOEpF8Gd/Y0xYeDf0lCTfcvvNUp6vFYBGgAAAAASUVORK5CYII=" /></div>';

  //setTimeout(testAddMessage, 5000);
};

function htmlEscape (s) {
  s = s.replace(/[\x01-\x1f]/g, "_").
    replace(/[&]/g, "&amp;").
    replace(/[<]/g, "&lt;").
    replace(/[>]/g, "&gt;").
    replace(/[\x22]/g, "&quot;");
  return s;
}


function doPktRTorrent (pkt) {
  var s = "torrent: <b>"+htmlEscape(pkt.vars["_torrent_name"])+"</b> <i>("+pkt.vars["_torrent_hash"]+")</i>\n";
  s += "action: <b>"+pkt.vars["_torrent_action"]+"</b>\n";
  var udt = new Date();
  udt = Math.floor(udt.getTime()/1000);
  var msg = {
    ismine:false,
    unixdate:udt,
    uni:"psyc://ketmar.no-ip.org/~rtorrent",
    text:s,
    action:""
  };
  addChatMessage(msg);
}


function onPSYCPacket () {
  //dumpPacket(psycpkt);
  return;
  var pkt = psycpkt;
  if (pkt.method && pkt.method == "_notice_rtorrent") doPktRTorrent(pkt);
}
