/*
 * global vars
 */
var clist = {};
var croom = {};
var clistSorted = [];
var croomSorted = [];
var blinkPhase = false;
var blinkTimer;
var myUNI;
var flgOffVisModeRoom = true;
var flgOffVisModePerson = true;
var flgHiddenVisMode = true;
var globalCnt = 1;


// statuses
const stInternal = 0;
const stOffline = 1;
const stVacation = 2;
const stAway = 3;
// others are avail
const stDND = 4;
const stNearby = 5;
const stBusy = 6;
const stHere = 7;
const stFFC = 8; // free-for-chat (talkative)
const stRealtime = 9; // unused


/*
 *
 */
function setMyUNI (uni) {
  myUNI = uni;
  setPersonOfflineVisibility(api.getOption("/contactlist/offvisible/persons"));
  setPlaceOfflineVisibility(api.getOption("/contactlist/offvisible/places"));
  setHiddenVisibility(api.getOption("/contactlist/showhidden"));
  //api.printLn(flgOffVisModeRoom);
  //api.printLn(flgOffVisModePerson);
}


/*
 *
 */
function chatStarted (uni) {
}


function optionChanged (optname) {
  var v;
  switch (optname) {
    case "/contactlist/offvisible/persons":
      v = api.getOption("/contactlist/offvisible/persons");
      if (flgOffVisModePerson != v) setPersonOfflineVisibility(v);
      break;
    case "/contactlist/offvisible/places":
      v = api.getOption("/contactlist/offvisible/places");
      if (flgOffVisModeRoom != v) setPlaceOfflineVisibility(v);
      break;
    case "/contactlist/showhidden":
      v = api.getOption("/contactlist/showhidden");
      if (flgHiddenVisMode != v) setHiddenVisibility(v);
      break;
  }
}


function optionRemoved (optname) {
}


function strcmp (s0, s1) {
  if (s0 < s1) return -1;
  if (s0 > s1) return 1;
  return 0;
}

function strcmpI (s0, s1) {
  return strcmp(s0.toLowerCase(), s1.toLowerCase());
}


/* compare contacts (for sorting) */
function ccCompare (c0, c1) {
  // contacts with unread messages comes first
  if (c0.unread > 0) {
    if (c1.unread < 1) return -1; // c0<c1
  } else if (c1.unread > 0) {
    if (c0.unread < 1) return 1; // c0>c1
  }
  // then by status
  if (c0.status > c1.status) return -1; // c0<c1
  if (c0.status < c1.status) return 1; // c0>c1
  // then by verbatim
  var res;
  if (c0.verbatim && c1.verbatim) res = strcmpI(c0.verbatim, c1.verbatim); else res = 0;
  // then by nick
  if (!res) res = strcmpI(c0.nick, c1.nick);
  // then by proto
  if (!res) res = strcmpI(c0.proto, c1.proto);
  // then by uni
  if (!res) res = strcmpI(c0.uni, c1.uni);
  return res;
}


/* compare rooms (for sorting) */
function crCompare (c0, c1) {
  // by uni
  return strcmpI(c0.uni, c1.uni);
}


function clistSwapDiv (c0, c1) {
  if (!strcmpI(c0.uni, c1.uni)) return;
  var d0 = c0.div;
  var d1 = c1.div;
  var p0 = d0.parentNode;
  var p1 = d1.parentNode;
  p0.removeChild(d0);
  p1.removeChild(d1);
  p0.appendChild(d1);
  p1.appendChild(d0);
}


/* sort clist and rearrange divs */
// bubble sort! shame on me!
function sortCArray (sl, compFn) {
  var swapped, was = false;
  do {
    swapped = false;
    var len = sl.length-2;
    for (var f = 0; f <= len; f++) {
      var rc = compFn(sl[f], sl[f+1]);
      if (rc > 0) {
        clistSwapDiv(sl[f], sl[f+1]);
        swapped = was = true;
        var t = sl[f]; sl[f] = sl[f+1]; sl[f+1] = t;
      }
    }
  } while (swapped);
  //if (was) {
  if (true) {
    for (var f = 0; f <= sl.length-1; f++) {
      var div = sl[f].div;
      div.setAttribute("class", "contact contact"+(f%2)+
        (sl[f].chatting?" chatting":"")+
        (sl[f].unread>0?" unread":"")+
        ""
      );
    }
  }
  return was;
}


function sortCList () {
  sortCArray(clistSorted, ccCompare);
  sortCArray(croomSorted, crCompare);
}


var statusInfo = {
 images:[
  "dys://theme/img/message.png",
  "dys://theme/img/status/offline.png",
  "dys://theme/img/status/vacation.png",
  "dys://theme/img/status/away.png",
  "dys://theme/img/status/dnd.png",
  "dys://theme/img/status/nearby.png",
  "dys://theme/img/status/busy.png",
  "dys://theme/img/status/here.png",
  "dys://theme/img/status/ffc.png",
  "dys://theme/img/status/realtime.png"
 ],
 text:[
  "",
  "offline",
  "vacation (n/a)",
  "away",
  "do not disturb",
  "nearby",
  "busy",
  "here",
  "free for chat",
  "realtime"
 ]
};

function setContactStatusPic (cc) {
  var div = cc.div, status = cc.status;
  cc.imgStatus.title = statusInfo.text[status];
  if (cc.unread > 0 && (blinkPhase || cc.skipUnreadCycle)) status = 0;
  cc.imgStatus.src = statusInfo.images[status];
}


function onBlinkTimer () {
  var cnt = 0;
  var sl = clistSorted;
  var bf = !blinkPhase;
  blinkPhase = bf;
  for (var f = sl.length-1; f >= 0; f--) {
    var d = sl[f];
    if (d.unread > 0) {
      cnt++;
      d.wasBlink = true;
    } else if (d.wasBlink) {
      delete d.wasBlink;
    }
    setContactStatusPic(d);
  }
  if (!cnt && blinkTimer) {
    clearInterval(blinkTimer);
    blinkTimer = null;
  }
}


function checkBlinking () {
  var doBlink = false;
  var sl = clistSorted;
  for (var f = sl.length-1; f >= 0; f--) {
    var d = sl[f];
    if (d.unread > 0) { doBlink = true; break; }
  }
  if (!doBlink) {
    if (blinkTimer) {
      clearInterval(blinkTimer);
      blinkTimer = null;
    }
  } else if (!blinkTimer) blinkTimer = setInterval(onBlinkTimer, 300);
}


function targetToUserDiv (target) {
  var div = target;
  while (div) {
    if (div.user) return div;
    div = div.parentNode;
  }
  return false;
}


function onContactDblClick (e) {
  var div = targetToUserDiv(e.target);
  if (!div) return true;
  e.preventDefault();
  // remove selection
  setTimeout(function () { window.getSelection().removeAllRanges(); }, 0);
  api.startChat(div.user.uni);
  return false;
}


function onPlaceDblClick (e) {
  var div = targetToUserDiv(e.target);
  if (!div) return true;
  e.preventDefault();
  // remove selection
  setTimeout(function () { window.getSelection().removeAllRanges(); }, 0);
  api.startChat(div.user.uni);
  return false;
}


function deletePerson (uni) {
  uni = uni.toLowerCase();
  var user = clist[uni];
  if (!user) return;
  var dx = user.div.parentNode;
  if (dx && dx.parentNode) dx.parentNode.removeChild(dx);
  delete clist[uni];
  for (var f = 0; f < clistSorted.length;) {
    if (clistSorted[f].uni.toLowerCase() == uni) {
      for (var c = f+1; c < clistSorted.length; c++) clistSorted[c-1] = clistSorted[c];
      clistSorted.pop();
    } else f++;
  }
  api.refresh();
  api.deleteContact(uni);
}


function deNdeletePerson (uni) {
  uni = uni.toLowerCase();
  var user = clist[uni];
  if (!user) return;
  if (user.chatting) api.startChat("");
  api.doPSYC("/unfr "+uni);
  var dx = user.div.parentNode;
  if (dx && dx.parentNode) dx.parentNode.removeChild(dx);
  delete clist[uni];
  for (var f = 0; f < clistSorted.length;) {
    if (clistSorted[f].uni.toLowerCase() == uni) {
      for (var c = f+1; c < clistSorted.length; c++) clistSorted[c-1] = clistSorted[c];
      clistSorted.pop();
    } else f++;
  }
  api.refresh();
  api.deleteContact(uni);
}


function deletePlace (uni) {
  uni = uni.toLowerCase();
  var user = croom[uni];
  if (!user) return;
  var dx = user.div.parentNode;
  if (dx && dx.parentNode) dx.parentNode.removeChild(dx);
  delete croom[uni];
  for (var f = 0; f < croomSorted.length;) {
    if (croomSorted[f].uni.toLowerCase() == uni) {
      for (var c = f+1; c < croomSorted.length; c++) croomSorted[c-1] = croomSorted[c];
      croomSorted.pop();
    } else f++;
  }
  api.refresh();
  api.deleteContact(uni);
}


/*
 * "action div" for users (NOT FOR PLACES!)
 */
var actionDiv;
function setActionDivFor (user) {
  function addItem (div, text, method) {
    var i = document.createElement("div");
    i.div = div;
    i.innerText = text;
    i.mt = method;
    if (i.mt) {
      i.onclick = function (e) {
        if (i.div.uni && i.mt) i.mt(i.div.uni);
        e.preventDefault();
        return false;
      };
      i.setAttribute("class", "adiv_item");
    } else i.setAttribute("class", "adiv_item_dis");
    div.appendChild(i);
    return i;
  }

  function addSeparator (div) {
    var i = document.createElement("div");
    i.setAttribute("class", "adiv_item_sep");
    div.appendChild(i);
    return i;
  }

  var d = document.createElement("div");
  d.setAttribute("class", "adiv");
  addItem(d, "chat", api.startChat);
  if (user.unread > 0) addItem(d, "mark as read", api.markAsRead);
  addSeparator(d);
  addItem(d, "insert UNI", function (uni) { if (uni) api.insertEditorText(uni); });
  addSeparator(d);
  addItem(d, "request auth", api.requestAuth);
  addItem(d, "cancel auth", api.cancelAuth);
  addItem(d, "send auth", api.sendAuth);
  addSeparator(d);
  if (api.isInPlace()) {
    addItem(d, "invite to room", function (uni) {
      if (api.isInPlace()) api.invite(uni, api.chattingWith());
    });
    addSeparator(d);
  }
  if (user.isOTR) {
    addItem(d, "OTR: end it", api.otrEnd);
    if (user.isOTRVerified) addItem(d, "OTR: untrust", function (uni) { api.otrSetTrust(uni, false); });
    else addItem(d, "OTR: trust", function (uni) { api.otrSetTrust(uni, true); });
  } else {
    addItem(d, "OTR: start it", api.otrStart);
  }
/*
  addSeparator(d);
  addItem(d, "visible to...", function (uni) { api.doPSYC("nf "+uni+" i"); });
  addItem(d, "invisible to...", function (uni) { api.doPSYC("nf "+uni+" i"); api.doPSYC("nf "+uni); });
*/
  addSeparator(d);
  addItem(d, "delete contact", deletePerson);
  addItem(d, "defrend and delete", deNdeletePerson);

  actionDiv = d;
  actionDiv.uni = user.uni;
  actionDiv.user = user;
}


function onContactMouseDown (e) {
  if (e.which == 3) {
    var div = targetToUserDiv(e.target);
    if (!div) return true;
    var my = (actionDiv && actionDiv.parentNode && !strcmpI(actionDiv.uni, div.user.uni));
    if (actionDiv && actionDiv.parentNode) actionDiv.parentNode.removeChild(actionDiv);
    if (!my) {
      setActionDivFor(div.user);
      div.appendChild(actionDiv);
    } else actionDiv = null;
    e.preventDefault();
    // remove selection
    setTimeout(function () { window.getSelection().removeAllRanges(); }, 0);
    return false;
  }
  //return true;
}


/*
 * "action div" for places (NOT FOR USERS!)
 */
function createInfoDivFor (room) {
  function addItem (div, text, hint, method, userUNI) {
    var i = document.createElement("div");
    i.div = div;
    i.userUNI = userUNI;
    i.innerText = text;
    i.title = hint;
    i.mt = method;
    if (i.mt) {
      i.setAttribute("class", "rdiv_item");
      if (userUNI) {
        if (!strcmpI(userUNI, myUNI)) {
          i.setAttribute("class", "rdiv_item_self");
          i.onclick = function (e) {
            e.preventDefault();
            return false;
          };
        } else {
          i.onclick = function (e) {
            if (i.userUNI && i.mt) i.mt(i.userUNI);
            e.preventDefault();
            return false;
          };
        }
      } else {
        i.onclick = function (e) {
          if (i.div.uni && i.mt) i.mt(i.div.uni);
          e.preventDefault();
          return false;
        };
      }
    } else i.setAttribute("class", "rdiv_item_dis");
    div.appendChild(i);
    return i;
  }

  function addSeparator (div) {
    var i = document.createElement("div");
    i.setAttribute("class", "rdiv_item_sep");
    div.appendChild(i);
    return i;
  }

  var d = document.createElement("div");
  d.setAttribute("class", "rdiv");
  var a = [];
  for (var k in room.users) if (typeof(k) == "string") a.push({uni:k, nick:room.users[k]});
  a.sort(function (u0, u1) { return strcmpI(u0.nick, u1.nick); });

  for (var f = 0; f < a.length; f++) {
    addItem(d, a[f].nick, a[f].uni, api.startChat, a[f].uni);
  }
  addSeparator(d);
  if (room.unread > 0) addItem(d, "mark as read", "mark all messages as read", api.markAsRead);
  if (!room.entered) addItem(d, "enter", "enter room", api.enterPlace);
  if (room.entered) addItem(d, "leave", "leave room", api.leavePlace);
  addItem(d, "delete", "delete room", deletePlace);

  d.uni = room.uni;
  d.room = room;
  return d;
}


function onPlaceMouseDown (e) {
  if (e.which != 3) return;
  var div = targetToUserDiv(e.target);
  if (!div) return true;
  var room = div.user;
  var doShow = !room.infoVisible;
  room.infoVisible = doShow;
  var idiv = room.listDiv;
  if (doShow) {
    if (!idiv) {
      room.listDiv = createInfoDivFor(room);
      idiv = room.listDiv;
      div.appendChild(idiv);
    }
    idiv.style.visibility = "visible";
  } else if (idiv) {
    var pn = idiv.parentNode;
    if (pn) pn.removeChild(idiv);
    delete room.listDiv;
  }
  e.preventDefault();
  // remove selection
  setTimeout(function () { window.getSelection().removeAllRanges(); }, 0);
  return false;
}


function addContactDiv (user) {
  globalCnt++;
  var div = document.createElement("div");
  div.setAttribute("class", "contact contact"+(clistSorted.length%2));
  var s = user.uni;
  if (user.channel) s += "/"+user.channel;
  div.title = s;
  div.user = {};
  var uu = div.user;
  uu.div = div;

  var imgProto = document.createElement("img");
  var pp = user.proto;
  imgProto.src = pp == "xmpp"?"dys://theme/img/protos/xmpp.png":
    pp == "irc"?"dys://theme/img/protos/irc.png":
    "dys://theme/img/protos/psyc.png";
  imgProto.setAttribute("style", "float:right");
  imgProto.title = pp+" (click here to rename contact)";

  var ei = "hshandle_"+globalCnt;
  imgProto.onclick = function () {
    div.user.editing = true;
    sz = div.user.verbatim.length;
    if (sz < 10) sz = 10; else if (sz > 20) sz = 20;
    var ined = new midoriInlineEdit({id:ei, size:sz, maxlen:64,
      callback: function (txt) {
        div.user.editing = false;
        //api.printLn("["+txt+"]");
        //div.user.hspan.innerText = txt;
        if (txt && div.user.verbatim != txt) {
          div.user.hspan.innerText = txt;
          api.setContactVerbatim(div.user.uni, txt);
        } else div.user.hspan.innerText = div.user.verbatim;
      }
    });
    ined.edit();
  };

  div.appendChild(imgProto);
  uu.imgProto = imgProto;

  var imgStatus = document.createElement("img");
  div.appendChild(imgStatus);
  uu.imgStatus = imgStatus;

  var hspan = document.createElement("span");
  hspan.innerText = user.verbatim;
  hspan.setAttribute("style", "padding:2px");
  hspan.setAttribute("id", ei);
  div.appendChild(hspan);
  uu.hspan = hspan;

  if (user.place) {
    var infoDiv = document.createElement("div");
    infoDiv.setAttribute("class", "roomInfo");
    infoDiv.innerText = "users: 0";
    div.appendChild(infoDiv);
    uu.infoDiv = infoDiv;
    uu.usercount = 0;
    uu.entered = false;
    uu.users = {};

    div.onmousedown = onPlaceMouseDown;
    div.ondblclick = onPlaceDblClick;
  } else {
    div.onmousedown = onContactMouseDown;
    div.ondblclick = onContactDblClick;
  }

  var dddx = document.createElement("div");
  dddx.appendChild(div);

  var cd = document.getElementById(user.place?"places":"contacts");
  if (cd) cd.appendChild(dddx); else document.body.appendChild(dddx);

  if (user.place) {
    croom[user.uni.toLowerCase()] = uu;
    croomSorted.push(uu);
  } else {
    clist[user.uni.toLowerCase()] = uu;
    clistSorted.push(uu);
  }

  return uu;
}


function getContactVisibility (uu) {
  if (uu.chatting || uu.showAlways || uu.editing) return true;
  if (uu.hidden && !flgHiddenVisMode) {
    /* hidden contact */
    if (uu.unread && !uu.skipUnreadCycle) return true;
    return false;
  }
  /* have unread messages? */
  if (uu.unread && !uu.skipUnreadCycle) return true;
  /* is contact is offline? */
  if (uu.status <= stOffline) return ((uu.place && flgOffVisModeRoom) || (!uu.place && flgOffVisModePerson)); /* is corresponding 'show offline' mode is off? */
  /* this contact is online and not hidden: show it */
  return true;
}


function setPersonOfflineVisibility (vismode) {
  flgOffVisModePerson = !!vismode;
  for (var f = clistSorted.length-1; f >= 0; f--) {
    var uu = clistSorted[f];
    uu.div.style.display = (getContactVisibility(uu) ? "block" : "none");
  }
  api.refresh();
}


function setPlaceOfflineVisibility (vismode) {
  flgOffVisModeRoom = !!vismode;
  for (var f = croomSorted.length-1; f >= 0; f--) {
    var uu = croomSorted[f];
    uu.div.style.display = (getContactVisibility(uu) ? "block" : "none");
  }
  api.refresh();
}


function setHiddenVisibility (vismode) {
  flgHiddenVisMode = !!vismode;
  for (var f = clistSorted.length-1; f >= 0; f--) {
    var uu = clistSorted[f];
    uu.div.style.display = (getContactVisibility(uu) ? "block" : "none");
  }
  for (var f = croomSorted.length-1; f >= 0; f--) {
    var uu = croomSorted[f];
    uu.div.style.display = (getContactVisibility(uu) ? "block" : "none");
  }
  api.refresh();
}


function space2nbsp (s) {
  return s.replace(/\s+/g, "\u00a0");
}


function date2str (d) {
  function pad2 (n) { return (n<10?"0":"")+n; }
  const dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  return d.getDate()+" "+monthNames[d.getMonth()]+" ("+dayNames[d.getDay()]+"), "+pad2(d.getHours())+":"+pad2(d.getMinutes());
}


function redrawContact (uni) {
  var user = api.contactInfo(uni);
  if (!uni) return; // no such user
  var ulc = user.uni.toLowerCase();
  var uu = user.place ? croom[ulc] : clist[ulc];
  var isNew = false;
  if (!uu) { uu = addContactDiv(user); isNew = true; }
  var prevIC = uu.chatting;

  // copy user to uu
  for (var kn in user) {
    if (typeof(kn) != "string") continue;
    uu[kn] = user[kn];
  }
  // fix unread
  uu.unread = user.chatting?0:user.unread;
  if (!uu.unread && uu.wasBlink) delete uu.wasBlink;

  var verbFix = false;
  if (uu.verbatim != uu.hspan.innerText) {
    uu.hspan.innerText = uu.verbatim;
    verbFix = true;
  }

  //uu.div.style.backgroundColor = uu.chatting ? "#0000c0" : "";
  if (uu.place) {
    uu.authorized = true;
    uu.imgStatus.title = "";
    uu.imgStatus.src = statusInfo.images[uu.unread?0:(uu.entered?7:1)];
    sortCArray(croomSorted, crCompare);
  } else {
    var s = uu.uni;
    if (user.channel) s += "/"+space2nbsp(uu.channel);
    var t = user.statusText;
    t = t.replace(/^\s*(.*?)\s*$/, "$1");
    if (t != "") s += "\n["+space2nbsp(t)+"]";
    if (user.lastseen && user.lastseen>0) s += "\n<"+space2nbsp("last seen: "+date2str(new Date(user.lastseen*1000)))+">";
    uu.div.title = s;
    uu.hspan.style.textDecoration = uu.authorized ? "none" : "underline";
    if (uu.isOTR) {
      uu.hspan.style.fontWeight = "bold";
      uu.hspan.style.fontStyle = uu.isOTRVerified ? "normal" : "italic";
    } else {
      uu.hspan.style.fontWeight = "normal";
      uu.hspan.style.fontStyle = "normal";
    }
    setContactStatusPic(uu);
    if (uu.unread) checkBlinking();
    sortCArray(clistSorted, ccCompare);
    // fix name
    var ll = uu.uni.toLowerCase();
    for (var f = croomSorted.length-1; f >= 0; f--) {
      var usrs = croomSorted[f].users;
      for (var uni in usrs) {
        if (typeof(uni) != "string") continue;
        if (uni != ll) continue;
        usrs[uni] = uu.verbatim;
        var room = croomSorted[f];
        //api.printLn("fixed! "+room.uni);
        if (!room.infoVisible) continue;
        var idiv = room.listDiv;
        var pn = idiv.parentNode;
        if (pn) pn.removeChild(idiv);
        room.listDiv = createInfoDivFor(room);
        room.div.appendChild(room.listDiv);
      }
    }

    if (actionDiv && actionDiv.uni == uni.toLowerCase()) {
      actionDiv.parentNode.removeChild(actionDiv);
      setActionDivFor(uu);
      uu.div.appendChild(actionDiv);
    }
  }

  uu.div.style.display = (getContactVisibility(uu) ? "block" : "none");

  api.refresh();
}


function doEnterLeave (isEnter, placeUni, userUni, isMe) {
  //api.printLn("enter:"+isEnter+"; place:"+placeUni+"; user:"+userUni+"; me:"+isMe);
  var user = api.contactInfo(userUni);
  if (!user) return;
  //api.printLn(" user ok");
  var place = api.contactInfo(placeUni);
  if (!place) return;
  //api.printLn(" place ok");

  var ulc = place.uni.toLowerCase();
  var room = croom[ulc];
  if (!room) return;

  if (isMe && room.entered == isEnter) return; // nothing to do
  var usr = user.uni.toLowerCase();
  if (isEnter) {
    if (room.users[usr]) return; // already here
    room.usercount++;
    room.users[usr] = user.verbatim;
  } else {
    if (!room.users[usr]) return; // already not here
    room.usercount--;
    delete room.users[usr];
  }
  room.infoDiv.innerText = "users: "+room.usercount;
  if (isMe) room.entered = isEnter;
  if (room.infoVisible) {
    var idiv = room.listDiv;
    var pn = idiv.parentNode;
    if (pn) pn.removeChild(idiv);
    if (isEnter || !isMe) {
      room.listDiv = createInfoDivFor(room);
      room.div.appendChild(room.listDiv);
    } else {
      room.infoVisible = false;
      delete room.listDiv;
    }
  }
  api.refresh();
}


/*
 * place guaranteed to be redrawn at least once
 */
function meEnters (pluni) {
  //api.printLn("meEnters: "+pluni+"; "+myUNI);
  //var me = api.contactInfo(myUNI);
  //if (!me) return;
  //api.printLn(" *meEnters: "+pluni+"; "+myUNI);
  doEnterLeave(true, pluni, myUNI, true);
}
function meLeaves (pluni) {
  //api.printLn("meLeaves: "+pluni+"; "+myUNI);
  //var me = api.contactInfo(myUNI);
  //if (!me) return;
  //api.printLn(" *meLeaves: "+pluni+"; "+myUNI);
  doEnterLeave(false, pluni, myUNI, true);
}
function userEnters (pluni, uni) {
  //api.printLn("enters: "+pluni+"; "+uni);
  //var user = api.contactInfo(uni);
  //if (!user) return;
  doEnterLeave(true, pluni, uni, false);
}
function userLeaves (pluni, uni) {
  //var user = api.contactInfo(uni);
  //if (!user) return;
  doEnterLeave(false, pluni, uni, false);
}


function dumpPacket (pkt) {
  api.printLn("\n===========================");
  for (var n in pkt.vars) {
    if (!pkt.rvars[n]) continue;
    api.printLn(":"+n+" "+pkt.vars[n]);
  }
  api.printLn("");
  for (var n in pkt.vars) {
    if (pkt.rvars[n]) continue;
    api.printLn(":"+n+" "+pkt.vars[n]);
  }
  api.printLn(""+pkt.method);
  api.printLn(""+pkt.body);
}


function onPSYCPacket () {
  //dumpPacket(psycpkt);
}
